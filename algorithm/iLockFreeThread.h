#ifndef I_LOCKFREETHREAD_H
#define I_LOCKFREETHREAD_H
#include "ithread.h"
#include <atomic>
namespace Algorithm
{
    template <class TValue>
    class ILockFreeType
    {
        public:
        std::atomic<TValue> value;

        ILockFreeType():value() {};
        inline void Set(TValue& toSet){ value = toSet;};

        inline TValue& Get(){return value.load();}
    };


    template <class TValue>
    class ILockFreeThread : public virtual IThread, public virtual ILockFreeType<TValue>
    {
       public:
       ILockFreeThread():IThread(),ILockFreeType<TValue>(){};
    };
}
#endif
