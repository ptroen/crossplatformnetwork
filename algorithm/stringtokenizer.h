#ifndef STRING_TOKENIZER_H
#define STRING_TOKENIZER_H
#include <vector>
#include <string>
#include <sstream>
#include <algorithm> 
#include <cctype>
#include <locale>
namespace Algorithm
{
	namespace String
	{

		// citation:https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
		// trim from start (in place)
		static inline void ltrim(std::string &s) {
			s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
				return !std::isspace(ch);
			}));
		}

		// trim from end (in place)
		static inline void rtrim(std::string &s) {
			s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
				return !std::isspace(ch);
			}).base(), s.end());
		}

		// trim from both ends (in place)
		static inline void trim(std::string &s) {
			ltrim(s);
			rtrim(s);
		}

		// trim from start (copying)
		static inline std::string ltrim_copy(std::string s) {
			ltrim(s);
			return s;
		}

		// trim from end (copying)
		static inline std::string rtrim_copy(std::string s) {
			rtrim(s);
			return s;
		}

		// trim from both ends (copying)
		static inline std::string trim_copy(std::string s) {
			trim(s);
			return s;
		}

		static std::vector<std::string> split(const std::string& s, char delimiter)
		{
			std::vector<std::string> tokens;
			std::string token;
			std::istringstream tokenStream(s);
			while (std::getline(tokenStream, token, delimiter))
			{
				tokens.push_back(token);
			}
			return tokens;
		}
		// citation: https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string
		// usage std::string string("hello $name");
		//replace(string, "$name", "Somename")
		static bool replace(std::string& str, const std::string& from, const std::string& to) {
			size_t start_pos = str.find(from);
			if (start_pos == std::string::npos)
				return false;
			str.replace(start_pos, from.length(), to);
			return true;
		}

		// citation: https://stackoverflow.com/questions/51949/how-to-get-file-extension-from-string-in-c usage url.ext,extensionWithoutDot
		static bool IsFileExtension(std::string& fn, std::string extensionStringWithoutDot)
		{
			if (fn.substr(fn.find_last_of(".") + 1) == extensionStringWithoutDot) {
				return true;
			}
			return false;
		}

		static bool replace(std::wstring& str, const std::wstring& from, const std::wstring& to) {
			size_t start_pos = str.find(from);
			if (start_pos == std::wstring::npos)
				return false;
			str.replace(start_pos, from.length(), to);
			return true;
		}
	}
}
#endif