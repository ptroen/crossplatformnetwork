#ifndef ICRITICALTHREAD_H
#define ICRITICALTHREAD_H
#include "ithread.h"

namespace Algorithm
{
	// temp include fix to avoid the stupid winsock include stuff
	// threading
	class ICriticalThread;
	typedef void(*startProcessingPtr)(ICriticalThread);

	class ICriticalThread : public virtual IThread
	{
	public:
#if defined( WIN32 )
		CRITICAL_SECTION					_workerLock;
#else
		pthread_mutex_t							_workerLock;
#endif

		ICriticalThread() {
#if defined( WIN32 )
			InitializeCriticalSection(&_workerLock);
#else
			_workerLock = PTHREAD_MUTEX_INITIALIZER;
#endif
			IThread();
		};

		virtual ~ICriticalThread() {
#if defined( WIN32)
			DeleteCriticalSection(&_workerLock);
#else
			// should automatically delete itself as it goes out of scope
#endif
		};
	protected:
		// never expose lock and unlock. Use access methods instead so you can encapsulate the locking mechanism from each class
		void lock() {
#if defined( WIN32)
			EnterCriticalSection(&_workerLock);
#else
			pthread_mutex_lock(&_workerLock);
#endif
		};

		void unlock() {
#if defined( WIN32 )
			LeaveCriticalSection(&_workerLock);
#else
			pthread_mutex_unlock(&_workerLock);
#endif
		};
	};
}
#endif
