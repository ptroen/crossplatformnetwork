#ifndef IOSERVICE_TIMER_TASK_H
#define IOSERVICE_TIMER_TASK_H
#include "../Tools/OSInclude.h"
#include <boost/bind.hpp>
#include <boost/asio.hpp>

class IOService_Timer_Task
{
protected:
	boost::asio::io_service io_service;
public:
	IOService_Timer_Task() :io_service() {};
	void iosvcrun() { io_service.run(); };
};


#endif