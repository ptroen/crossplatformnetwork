#ifndef DICE_ROLL_H
#define DICE_ROLL_H
#include <cstdlib>
namespace Algorithm
{
	namespace Math
	{
		namespace Statistics
		{
			class DiceRoll
			{
				int _lowValue;
				int _highValue;
				// citation: https://stackoverflow.com/questions/45133700/c-simple-dice-roll-how-to-return-multiple-different-random-numbers
				DiceRoll(int lowValue, int highvalue):_lowValue(lowValue),_highValue(highvalue){ srand(time(0));};
				int Roll(void){
					return rand() % (_highValue - _lowValue + 1) + _lowValue;
				};
				
			};
		}
	}
}

#endif