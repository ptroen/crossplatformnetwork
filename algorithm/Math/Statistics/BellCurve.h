#ifndef BELL_CURVE_H
#define BELL_CURVE_H

#include <random>
namespace Algorithm
{
	namespace Math
	{
		namespace Statistics
		{
			class BellCurve
			{
				std::default_random_engine generator;
				std::normal_distribution<double> distribution;
				double _min;
				double _max;
			public:
				// since we use a bell curve we are measuring via standard devation. 3 standard deviations usually should be enough but not guarante
				BellCurve(double& min,double& max, double& standardDeviation):generator(),
				_min(min),
				_max(max),
				distribution(max - min,standardDeviation){	};
				double Generate(void){ return distribution(generator);}
				double GenerateWithoutClippingMinMax(void)
				{
					double ret;
					{
						ret = Generate();
					}
					if(ret >=_min && ret <= _max)
					{
						return ret;
					}
					while (1);
					return ret;
				};
				bool GenerateBool(void)
				{
					if(Generate()>=(_max-_min))
					{
						return true;
					}
					return false;
				}
			};
		}
	}
}
#endif