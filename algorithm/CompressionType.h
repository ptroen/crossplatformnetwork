#ifndef COMPRESSION_TYPE_H
#define COMPRESSION_TYPE_H
#include "GZIP.h"
namespace Algorithm
{
	template <class TInnerType,class TInit>
	class CompressionType
	{
		std::string compressedData;
		public:
			CompressionType(TInit& init):payload(init) {};
		uint8_t compressed; // 1 bit for compression for gzip but we could support more types
							// of compression such as lossy compression
		TInnerType payload;
		size_t size(void){
			if(!compressed)
			return sizeof(compressed) + payload.size();
			else
				return sizeof(compressed) + compressedData.size();
		};
		size_t max_size(void){ return sizeof(compressed) + payload.max_size();};
		
		void ToString(char* stringout,size_t dataSize) { 
			//dataIn[0] = static_cast<char>(compressed);
			CheckIfWeCanCompress();
			char* str=&stringout[1];
			if(compressed == 1)
			{
				memcpy(str,compressedData.c_str(),compressedData.size());
			}
			else
			{
				payload.ToString(str, dataSize - 1 );
			}
			
		};
		
			void CheckIfWeCanCompress(void)
			{
				std::string compressionContainer;
				size_t payloadsize = payload.size();
				compressionContainer.resize(payloadsize);
				payload.ToString(const_cast<char*>(compressionContainer.c_str()), payloadsize);
				compressedData = Algorithm::Gzip::compress(compressionContainer);
				if(compressedData.size() < compressionContainer.size())
				{
					compressed = 1;
				}
				else
				{
					compressedData.clear();
				}
			}

			void UnCompress(void)
			{
					std::string uncompressedData = Algorithm::Gzip::decompress(compressedData);
					size_t size = uncompressedData.size();
					payload.FromString(const_cast<char*>(uncompressedData.c_str()),size);
			}
		
		void FromString(char* dataIn,size_t dataInSize) { 
			compressed = static_cast<uint8_t>(dataIn[0]);
			if(compressed == 1)
			{
				size_t compressionSize = dataInSize - sizeof(compressed); 
				compressedData.resize(compressionSize);
				memcpy(dataIn,compressedData.c_str(),compressionSize);
				UnCompress();
			}
		};
	};
	
};
#endif
