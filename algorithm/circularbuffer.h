#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H
#include "../Tools/OSInclude.h"
#include <stdlib.h>
#include <vector>
namespace Algorithm
{
	template<class T, class TInit>
	class CircularBuffer
	{
		// used to traverse the circular buffer
	public:
		size_t _currentWriteIndex; // this is always ahead of write index
		size_t _currendReadIndex; // reads always lags over writes
		std::vector<T> _buffer;

		CircularBuffer(size_t bufferSize) :_buffer(bufferSize) {
			_currentWriteIndex = 0;
			_currendReadIndex = 0;
		};

		CircularBuffer(size_t bufferSize,  TInit& t):_buffer(bufferSize,t){
			_currentWriteIndex = 0;
			_currendReadIndex = 0;
		};

		virtual ~CircularBuffer() {
		};

		void UpdateIndex(size_t& index) {
			if (index == _buffer.size() -1) {
				index = 0;
			}
			else
				++index;
		};
		void Clear()
		{
			_currentWriteIndex = _currendReadIndex = 0;
		};
		const T* begin() {
			return &_buffer[_currendReadIndex];
		};

		const T* end() {
			return  &_buffer[(_currentWriteIndex)];
		};

		const CircularBuffer<T,TInit>& operator+=(const T& rhs) {
			T& rh = const_cast<T&>(rhs);
			Assign(rh);
		};


		T& Assign(T& value) {
			_buffer[_currentWriteIndex] = value;
			return GrabNewElement();
		};

		void JustAssign(T& value){
            _buffer[_currentWriteIndex] = value;
            UpdateIndex(_currentWriteIndex);
		};

		// checks if no records to read
		bool empty() {
			return (_currendReadIndex == _currentWriteIndex) ? true : false;
		}
		const CircularBuffer<T,TInit>& operator=(const CircularBuffer<T, TInit>& rhs) {
			CircularBuffer<T,TInit>& rhsref = const_cast<CircularBuffer<T, TInit>&>(rhs);

			// grab all records
			size_t imax = rhs._currentWriteIndex - rhs._currendReadIndex;
			for (size_t i = 0; i < imax; i++)
			{
				T* rhsget = (rhsref.Get());
				if (rhsget != NULL)
				{
					Assign(*rhsget);
					T* rhsget = (rhsref.Get());
				}
			}
			return *this;
		};

		// used if you need to get a newly allocated element
		T& GrabNewElement() {
			UpdateIndex(_currentWriteIndex);
			return _buffer[_currentWriteIndex];
		};


		// note we dispose of the record after we read it
		// Use this Interface for reading
		T* Get() {

			if (_currendReadIndex == _currentWriteIndex)
			{
				return NULL;
			}
			// we have a eligible record to read
			T& ptr = _buffer[_currendReadIndex];
			UpdateIndex(_currendReadIndex);
			return &ptr;
		};

		T* GetButDontUpdateIndex(size_t readIndex = !0) {
			if(readIndex == !0)
			{
				readIndex = _currendReadIndex;
			}
			if (readIndex == _currentWriteIndex)
			{
				return nullptr;
			}
			// we have a eligible record to read
			T& ptr = _buffer[readIndex];
			return &ptr;
		};

	};
}
#endif
