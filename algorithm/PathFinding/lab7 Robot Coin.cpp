/* Lab 7:
 * By Patrick Troen A00041860
*
*/
#include <iomanip> 
#include <sstream>
#include <iostream>
using namespace std;
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <queue>
#include <map>
using std::vector;

using std::cout;
using std::cin;
using std::endl;
using std::map;

class point
{
public:
  long i;
  long j;
};

class DynamicProblemSubProblems
{
	public:
		DynamicProblemSubProblems(void):resultfound(false),result(0){
			index_to_subproblems.resize(2);
		};
		virtual ~DynamicProblemSubProblems(void){};
		bool resultfound;
		long result;
		vector<DynamicProblemSubProblems*> index_to_subproblems;

		const DynamicProblemSubProblems& operator=(const DynamicProblemSubProblems& c){
			this->result=c.resultfound;
			this->result=c.result;
			this->index_to_subproblems=c.index_to_subproblems;
			return *this;
		};

		
};



 static long testboard[5][6] =
   {
    0, -9, 0, 1, 0, 0,
    1, 0, 0, -9,1, 0,
	0, 1, 0, -9, 1, 0, 
	0, 0, 0, 1, 0, 1, 
	-9, -9, -9, 0, 1, 0 
   };

 static  long F[5][6];

 // from Lecture 8 Notes

long RobotCoinCollection(long col,long row)
{
	F[0][0]=testboard[0][0];
	
	for(long j=1;j<row;j++)
	{
	  F[0][j]=F[0][j-1]+testboard[0][j];
	}

	for(long i=1;i<col;i++)
	{
	  F[i][0]=F[i-1][0]+testboard[i][0];
	  for(long j=1;j<row;j++)
	  {
	     F[i][j]=max(F[i-1][j],F[i][j-1])+testboard[i][j];
	   }
	}
	return F[col-1][row-1];
 }

void displaypath(vector<point>& path,ostream& o)
{
	o << endl;
	for(size_t i=0;i<path.size();i++)
	{
		size_t j=path.size()-1-i; // going backwards
		o <<"(" << path[j].i<< ","<<path[j].j<<")" << endl;;
	}
	o << endl;
}

long count_optimal_paths(long count,
											long i,
											long j,
											vector<point>& path,ostream& o)
{
	point p;
	p.i=i;
	p.j=j;

		 path.push_back(p);
	
	

    if(i==0 && 
		j==0)
	{
	   displaypath(path,o);
	   return count;
	}

	 if(F[i-1][j]>F[i][j-1])
	 {
	    return count_optimal_paths(count,
											i-1,
											j,
											path,o);
	 }
	 else  if(F[i-1][j]<F[i][j-1])
	 {
		  return count_optimal_paths(count,
											i,
											j-1,
											path,o);
	 }	  
	 else // ==
	 {
		 ++count;
		 vector<point> newpath;
		newpath=path;
		count_optimal_paths(count,
											i-1,
											j,
											newpath,o);
		
			 vector<point> newpath2;
		newpath2=path;
		count_optimal_paths(count,
											i,
											j-1,
											newpath2,o);


		return ++count;
	 }
}

long max_number_of_coins(void)
{
	return RobotCoinCollection(5,6);
}

void displayboard(ostream& o,long col,long row)
{
	 for(long i=0;i<col;i++)
	  {
	    for(long j=0;j<row;j++)
		{
			if(testboard[i][j]>=0)
				o << '+';
		   o <<  testboard[i][j] << " ";
		}
		o << endl;
	 }
}


class item
{
	public:
		item(void):number(0),weight(0),value(0){};
	long number;
	long weight;
	long value;

	const item& operator=(const item& i){
	  this->number=i.number;
	  this->weight=i.weight;
	  this->value=i.value;
		return *this;
	};
};

vector<vector<long>> F_knapsack;
long MFKnapsack(vector<item>& knap,long i,long j)
{
	if(F_knapsack[i][j]<0)
	{
		if(j<knap[i].weight)
		{
		
		}
	
	}
}

void solveknapsack(ostream& o,	vector<item>& knapsack,long n,long w)
{
	for(long i=0;i<n;i++)
	{
		F_knapsack[i][0]=0;
		for(long j=0;j<w;j++)
		{
			F_knapsack[i][j]=-1;
		}
	}
	for(long j=0;j<w;j++)
	{
		F_knapsack[0][j]=0;
	}

	o << "attempting to solve knapsack" << endl;
	o << "Solution is:" << MFKnapsack( knapsack) << endl;
}

int main(int argc,char* argv[])
{
	   cout << "Robot test coin problem " << endl;
		cout << "The max number of coins are:"<< max_number_of_coins()  << endl;
		displayboard(cout,5,6);
		
		long sum=1;
		vector<point> optimal_path;
		cout << "Number of optimal path are:" << endl<< count_optimal_paths(sum,5,6,optimal_path,cout) << endl;
		std::system("pause");
	
		cout << "I really wanted to attempt the knapsack problem but originally wrote the coin problem completly incorrect" << endl;
		cout << "But I rewrote the coin problem and now it works correctly" << endl;
		cout << " I really should read the notes more before beginning the assignment instead of jumping into the code" << endl;

		// it's 10 p.m on a monday and I have work tommorrow... but let's try the knapsack anyways....
		ifstream in("Lab7TestFile.txt");
		for(;;)
		{
			long w;
			long n;
			in >> w;
			in >> n;
			cout << endl;
			cout << w << endl;
			cout << n << endl;

			vector<item> knapsack;
	
			knapsack.resize(n);
			 F_knapsack.clear();
			  F_knapsack.resize(n);
			  for(size_t i=0;i<=n;i++)
				  F_knapsack[i].resize(w+1);
			for(long counter=0;counter<n;counter++)
			{
					item ii;
					in >> ii.number;
					in >> ii.weight;
					in >> ii.value;
					knapsack[ii.number-1]=ii;
			}
			solveknapsack(cout,	knapsack,n,w);
			if(in.eof())
			{
			   std::system("pause");
			   return 1;
			}
			else
			{
			  char dollarsign=in.get();
			}
		}
		
	
       return 1;
}