#ifndef SINGLE_THREAD_ALLOCATOR_H
#define SINGLE_THREAD_ALLOCATOR_H

#include <stdlib.h>
#include <new>
#include <limits>
#include <cstddef>
#include "CircularBuffer.h"
using namespace std;
template <class T, size_t const numberOfElements> 
struct SingleThreadCircularBufferAllocator 
{
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T value_type;

	static CircularBuffer<T>* _elements;
	
    template <class U> struct rebind { typedef SingleThreadCircularBufferAllocator <U> other; };
    SingleThreadCircularBufferAllocator () throw() {
		_elements= new CircularBuffer<T>(numberOfElements);
	}
    SingleThreadCircularBufferAllocator (const SingleThreadCircularBufferAllocator &) throw() {}


    pointer address(reference x) const { return &x; }
    const_pointer address(const_reference x) const { return &x; }

    pointer allocate(size_type s, void const * = 0) {
        if (0 == s)
            return NULL;
        pointer temp = (pointer)malloc(s * sizeof(T)); 
        if (temp == NULL)
            throw std::bad_alloc();
        return temp;
    }

    void deallocate(pointer p, size_type) {
        free(p);
    }

    size_type max_size() const throw() { 
        return std::numeric_limits<size_t>::max() / sizeof(T); 
    }

    void construct(pointer p, const T& val) {
        new((void *)p) T(val);
    }

    void destroy(pointer p) {
        p->~T();
    }
};

// usage    std::map<std::string, int, std::less<std::string>, 
             SingleThreadCircularBufferAllocator <std::pair<const std::string, int> > > stuff;
