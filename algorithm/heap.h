#ifndef HEAP_H
#define HEAP_H
#include <vector>
#include <algorithm> // for heap operations
// citation: https://www.geeksforgeeks.org/heap-using-stl-c/
namespace Algorithm
{
	template<class T>
	class Heap
	{
		std::vector<T>& v;
		public:
		Heap(std::vector<t>& vectorToHeap): v(vectorToHeap){
			MakeItAHeapAgain();
		};
		
		void Add(T& element){
			v.push_back(element); // push element on
			// reorganize the heap
			push_heap(v.begin(),v.end());
		}
		
		void Remove(T& element)
		{
			index = 0;
			if(FindEntry(element,index))
			{
				RemoveNthElement(index);
			}
		}
		
		bool FindEntry(T& elementToSearchFor, size_t& index){
			if(v.empty()){
				return false;
			}
			index = 0;
			while(1)
			{
				if(elementToSearchFor==v[index])
				{
					return true;
				}
				if(elementToSearchFor<v[index])
				{
					if(!goLeft(index))
						return false;
				}
				else
				{
					if(!goRight(index))
						return false;
				}
			}
		}
		void RemoveNthElement(size_t elementIndex)
		{
			v.erase(v.begin(),v.begin()+elementIndex);
			MakeItAHeapAgain();
		}
		
		T RemoveMaximumElement(void)
		{
			T = v.front();
			pop_heap(v1.begin(),v1.end());
			return T;
		}
		
		// for descending just read the list the other way
		void SortAscendingAndRemoveHeapProperty(void)
		{
			sort_heap(v.begin(),v.end());
		}
		
		bool IsStillHeap(void)
		{
			return is_heap(v.begin(),v.end());
		};
		
		void MakeItAHeapAgain(void){ std::make_heap(v.begin(),v.end());}
		
		private:
		// citation: https://www.geeksforgeeks.org/binary-heap/
		bool goRight(size_t& index){
			index=2*index + 2;
			if(index < v.size())
				return true;
			return false;
		};
		bool goLeft(size_t&index){
			index = ( 2 * index + 1);
			if(index < v.size())
			{
				return true;
			}
			return false;
		};
		
	}
}

#endif