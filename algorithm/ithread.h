#ifndef I_THREAD_H
#define I_THREAD_H
#include "../Tools/OSInclude.h"
#include "./interface/IJoinable.h"
#include <thread>
namespace Algorithm
{
    class IThread : public virtual IJoinable
	{
	public:
		std::thread* _workerthread;
        IThread(void):_workerthread(nullptr){}; // base class initializes _workerThread
		virtual ~IThread(){};

	public:
		void join() {
			_workerthread->join();
		};
    };
}
#endif
