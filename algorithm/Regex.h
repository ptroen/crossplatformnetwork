#include <stdlib.h>
#include <boost/regex.hpp>
#include <string>
#include <iostream>



class Regex
{
	boost:regex expression;

	Regex(std::string& regexSearchString):expression(regexSearchString.c_str()){};
	bool Find(std::string& stringToSearchFor,std::string& outputString){
		cmatch foundMatches;
		if(boost::regex_match(stringToSearchFor.c_str(), foundMatches, expression))
		{
			outputString = foundMatches[0]; // returning the whole string query
			return true;
		}
		return false;
   {
	};

};