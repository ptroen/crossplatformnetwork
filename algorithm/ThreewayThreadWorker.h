#ifndef THREEWAYTHREADWORKER_H
#define THREEWAYTHREADWORKER_H
#include "../../../../Tools/OSInclude.h"
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include "CircularBuffer.h"
#include "IOServiceTimerTask.h"
#include "ICriticalThread.h"
#include <boost/asio/steady_timer.hpp>


template<class TFlyWeightServerIncoming,
	class TFlyWeightServerOutgoing,
	size_t(*threewayHookHandler)(TFlyWeightServerIncoming&, TFlyWeightServerOutgoing&),
	typename TReader,
	typename TWriter,
	typename TConstructorParameters>
	class ThreewayThreadWorker : public virtual IThread, public virtual IOService_Timer_Task
{
private:
	// Call this 1st
	TWriter _writer;
	TReader _reader;
public:

	ThreewayThreadWorker(TConstructorParameters& parameters) :IThread(),
		IOService_Timer_Task(),
		_writer(parameters),
		_reader(parameters) {};

	virtual void InvokeThread() {
		_writer.InvokeThread();
		_reader.InvokeThread();
		_workerthread = new std::thread(StartProcessing, this);
	};

	virtual ~ThreewayThreadWorker() {};
private:
	// then this with std::thread
	static void StartProcessing(ThreewayThreadWorker* protocol)
	{
		ThreewayThreadWorker& thisref = *protocol;
		TFlyWeightServerOutgoing  outgoing;
		for (;;)
		{
			TFlyWeightServerIncoming* incoming = (*TReaderGet)(_reader);
			if (incoming)
			{
				// process any non empty entries to THookServer Function
				if ((*threewayHookHandler)(incoming, outgoing))
				{
					// assign to writer if TFHookServer Function returned true
					(*TWriterSet)(_writer, outgoing);
				}
			}
		}
	}
};

#endif