#ifndef SCRIPT_ITEM_H
#define SCRIPT_ITEM_H
#include "Interface/IVM.h"

class ScriptItem
{
public:
	std::shared_ptr<IVM> script;
	std::shared_ptr<VMInput> input;
};


#endif