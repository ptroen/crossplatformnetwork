#ifndef SCRIPTQUEUE_H
#define SCRIPTQUEUE_H
#include "../../Tools/OSInclude.h"
#include "../../algorithm/SingletonMacro.h"
#include <vector>
#include <memory>
#include "ScriptItem.h"
#include "UserScript.h"
#include "Interface/IVM.h"

// a queue of actions to process
// this is very useful abstraction for anything timing related
class ScriptQueue
{
	MAKEINSTANCE(ScriptQueue)
public:
	std::vector<UserScript> script_queue;
	ScriptQueue() :script_queue()
	{
		int count = 2;
		script_queue.reserve(count);
		for (int i = 0; i < count; i++)
		{
			UserScript u;
			script_queue.push_back(u);
		}
	}
	void q(std::shared_ptr<VMInput>& input, std::shared_ptr<IVM>* thisref)
	{
		ScriptItem item;
		if (thisref != nullptr)
		{
			item.script = *thisref;
			item.input = input;
			size_t n = GetUserNumber();
			script_queue[n].script.push_back(item);
		}
	};

	void Execute()
	{
		size_t n = GetUserNumber();
		UserScript& u = script_queue[n];
		size_t l = u.script.size();

		for (int i = 0; i < l; i++)
		{
			ScriptItem& item = script_queue[n].script[i];
			if (item.script != nullptr)
			{	
				item.script.get()->vm(item.input);
			}
		}
	};

	private:
	size_t GetUserNumber(){ return 0; }
};
#endif