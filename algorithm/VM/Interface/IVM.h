#ifndef IVM_H
#define IVM_H
#include "../../algorithm/SingletonMacro.h"

#include <cstdint>
#include <memory>
#include <string>

class VMInput
{
public:
	uint8_t instruction;
};

class IVM
{
	public:
		IVM(void) :thisref(nullptr){};
		std::shared_ptr<IVM>* thisref;

	    // vm entry function
		virtual uint8_t vm(std::shared_ptr<VMInput>& input){ return 0; };
		// queue the actions for the vm
		virtual  std::shared_ptr<IVM>* qvm(uint8_t){ return thisref; };
		virtual uint8_t vmCount()
		{
			return ((uint8_t)(GetMaxVMInstruction()) - ((uint8_t)GetMinVMInstruction()) + 1);
		};

		virtual void q(std::shared_ptr<VMInput>& input){};

		virtual uint8_t GetMinVMInstruction(){ return 0; };
		virtual uint8_t GetMaxVMInstruction(){ return 0; };

		virtual std::string classname(){ return std::string("IVM"); };
		virtual ~IVM(){};

		// returns number of bytes to binary
		virtual size_t ToBinary(void* out_bin){ return 0; };
		virtual void FromBinary(void* in_bin, size_t size){};
};

#endif