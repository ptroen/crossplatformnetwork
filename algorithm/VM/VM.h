#ifndef GAME_SCREEN_NODE_ACTIONS_H
#define GAME_SCREEN_NODE_ACTIONS_H
#include "./interface/IVM.h"
#include <vector>
#include "ScriptQueue.h"

#include "../../algorithm/SingletonMacro.h"

class VM : public virtual IVM
{
	public:
		VM(void) :IVM(){};
	protected:
		// queue the vm caller calls this for vm to be called(as it will sit in the script queue)
		void q(std::shared_ptr<VMInput>& input)
		{
			ScriptQueue::Get_Instance().q(input,thisref);

		};
};

#endif