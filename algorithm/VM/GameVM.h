#ifndef GAME_SCREEN_NODE_ACTIONS_H
#define GAME_SCREEN_NODE_ACTIONS_H
#include <locale.h>
#include <cstdint>
#include "IVM.h"
class GameVM;
class GameVM : public virtual IVM
{
	public:
		virtual uint8_t vm(std::shared_ptr<VMInput>& input){ return 0; };
		virtual uint8_t vmCount()
		{
			return ((uint8_t)(GetMaxVMInstruction()) - ((uint8_t)GetMinVMInstruction()) + 1);
		};
		virtual uint8_t GetMinVMInstruction(){ return 0; };
		virtual uint8_t GetMaxVMInstruction(){ return 0; };
		virtual ~GameVM(){};
	    
	 
};

#endif