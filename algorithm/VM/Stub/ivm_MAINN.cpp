#include "ImageBuffer.h"
#include "Texture.h"
#include "GraphicsVM.h"
#include "VM.h"
#include "Renderer.h"
int main(int argc, char* argv[])
{
	// multithreading example
	std::shared_ptr<IVM> g = std::make_shared<Graphics_VM>();
	// potentially dangerous shared pointer below because of scoping rules
	g.get()->thisref = &g;
	g->qvm((uint8_t)Graphics_VM_Action::GenerateImage);
	g->qvm((uint8_t)Graphics_VM_Action::TakeScreenShot);
	ScriptQueue::Get_Instance().Execute();

	RenderInfo info;
	memset(&info, 0, sizeof(RenderInfo));
	info._maxthreads = 8;
	info._pid = 0;
	info.frame_number = 0;
	info.max_framenumber = 30;
	info.bpp = 32;
	info.width = 1024;
	info.height = 768;
	MasterRenderer rendering_job(info);
	rendering_job.InvokeThread();
	rendering_job.join();

	// manually Analyze results
	//system("explorer .\0");
	return 1;
}
