#ifndef SINGLETON_MACRO_H
#define SINGLETON_MACRO_H
#define MAKEINSTANCE(classname) static classname* instance;\
	public:\
		static classname& Get_Instance(void)\
		{\
			if (instance == nullptr)\
			{\
				instance = new classname();\
			}\
			return *instance;\
		};\
		static void Release()\
		{\
			if (instance != nullptr)\
			{\
				delete instance;\
				instance = nullptr;\
			}\
		};\
		private:
#define INITINSTANCE(classname) classname* classname::instance = nullptr;

#endif