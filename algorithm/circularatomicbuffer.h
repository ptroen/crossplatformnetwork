#ifndef CIRCULAR_ATOMIC_BUFFER_H
#define CIRCULAR_ATOMIC_BUFFER_H
#include <atomic>
#include "../Tools/OSInclude.h"
#include <stdlib.h>
#include <vector>
#include "iLockFreeThread.h"

namespace Algorithm
{
    class ReadWrite;
    class ReadWrite
    {
        public:
        size_t _currentWriteIndex;
        size_t _currentReadIndex;
        ReadWrite():_currentReadIndex(0),_currentWriteIndex(0){}
        ReadWrite(ReadWrite& rw):_currentReadIndex(rw._currentReadIndex),_currentWriteIndex(rw._currentWriteIndex){};
        const ReadWrite& operator=(const ReadWrite& rw){ _currentReadIndex = rw._currentReadIndex;
        _currentWriteIndex = rw._currentWriteIndex;
         return *this;}
    };

template <class TValue>
class CircularAtomicBuffer
{
    public:
		ILockFreeType<ReadWrite> rw;
		std::vector<ILockFreeType<TValue>> _buffer;

		CircularAtomicBuffer(size_t bufferSize) :_buffer(bufferSize),rw() {
			Clear();
		};

		virtual ~CircularAtomicBuffer() {};
        public:
		void Clear()
		{
            rw.Set(ReadWrite());
		};

		void Write(T& value){
            auto rwb = rw.Get();
            size_t write = rwb._currentWriteIndex;
            _buffer[write].Set(value);
            UpdateIndex(write);
            rw._currentWriteIndex = write;
            rw.Set(rwb);
		};

		// note we dispose of the record after we read it
		// Use this Interface for reading
		T* Read() {
            auto rwb = rw.Get();
			if (rwb._currendReadIndex == rwb._currentWriteIndex)
			{
				return NULL;
			}
			size_t read = rwb._currentReadIndex;
			T& ptr = _buffer[read];
			UpdateIndex(read);
			rwb._currentReadindex = read;
			rw.Set(rwb);
			return &ptr;
		};

		private:
		void UpdateIndex(size_t& index) {
			if (index == _buffer.size() -1)
			{
				index = 0;
			}
			else
				++index;
		};
	};
}

#endif
