#ifndef INCLUDE_MACROS_H
#define INCLUDE_MACROS_H
// #define BASE_DIR ../../../ #define and undef in every file you use
#define STRR(x) #x
#define STR__(x) STRR(x)
#define STRB(x) STR__(BASE_DIR##/##x)
// example usuge STRB(algorithm/interface/IType.h)
#endif