#include "../../interface/IJSONSerialize.h"
#include "../../interface/IXMLSerialize.h"
#include "../../interface/IType.h"

class SampleType : public virtual Algorithm::Interface::IType
{
public:
	std::string test;
	size_t i;
	double d;

	// like stl containers returns size of type
	virtual size_t size(void)
	{
		return test.size() + sizeof(i) + sizeof(d);
	};

	// says the maximum size of the type
	virtual size_t max_size(void) { return 100 + sizeof(i) + sizeof(d); };
	// binary Serializer methods
	// method to cast as a pointer may be overridable
	// but also accessor from binary streams
	virtual void* ToBinary(void) { return this; };

	// accessor from binary streams
	virtual void FromBinary(void* address, size_t size)
	{
		memcpy(this, address, (size < max_size() ? size : max_size()));
	};
	// serialize methods

	// must be friended methods
	// istream extraction operators terminated by std::endl for each respective subtype
	// ostream extraction operators terminated by std::endl for each respective subtype

	// encode the stream to stream with variable name + value name. Useful for key value streams;
	virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
		Algorithm::Interface::IPropertyTree pt;
		pt.add(std::string("test"), test);
		pt.add(std::string("i"), i);
		pt.add(std::string("d"), d);
		return pt;
	};

	// method which extracts the values from property tree
	virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt){
		test = pt.get<std::string>(std::string("test"));
		i = pt.get<size_t>(std::string("i"));
		d = pt.get<double>(std::string("d"));
	};

	// call a serializer here
	// method instructs how to write to file by calling the approppriate serializer
	virtual void ToFile(void) {
		std::string testJSON("test.json");
		Algorithm::Interface::IJSONSerialize test(testJSON, ToPropertyTree());
		test.WriteFile();
	};

	virtual void FromFile(void)
	{
		std::string testJSON("test.json");
		Algorithm::Interface::IJSONSerialize test(testJSON, ToPropertyTree());
		test.ReadFile();
		FromPropertyTree(test.GetPropertyTree());
	};

	virtual void ToXMLFile(void) {
		std::string testJSON("test.xml");
		Algorithm::Interface::IXMLSerialize test(testJSON, ToPropertyTree());
		test.WriteFile();
	};

	virtual void FromXMLFile(void)
	{
		std::string testJSON("test.xml");
		Algorithm::Interface::IXMLSerialize test(testJSON, ToPropertyTree());
		test.ReadFile();
		FromPropertyTree(test.GetPropertyTree());
	};
};

int main(int argc, char* argv[])
{
	/*
	SampleType type;
	type.test = "blah blah blah";
	type.d = 5;
	type.i = 3;
	type.ToFile();
	*/
    // Algorithm::Interface::IJSONSerialize.WriteToString()
	SampleType type2;
	type2.FromFile();

	std::cout << type2.test << " " << type2.d << " " << type2.i;
	return 0;
}
