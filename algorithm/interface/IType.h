#ifndef ITYPE_H
#define ITYPE_H
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include "IPropertyTree.h"
#include <fstream>
// macross to simplify streaming property tree
#define __str__(s) #s
#define PADD(s) {\
std::string ss = std::to_string(s);\
std::string key = std::string(__str__(s));\
pt.add(key,ss);\
}
#define PADDS(s) {\
std::string key = std::string(__str__(s));\
pt.add(key,s);\
}
#define PADDBASE(BASE){\
auto st = std::string(__str__(BASE));\
auto pt2 = BASE##ToPropertyTree();\
pt.addPropertyTree(st, pt2);\
}
#define PADDMEMBER(membervar) {\
auto st = std::string(__str__(membervar));\
LOGIT1(st)\
auto _pt = membervar.ToPropertyTree();\
pt.addPropertyTree(st, _pt);\
}
#define PGET(VAR,type) {  std::string s(__str__(VAR));\
VAR = pt.get<type>(s); }

#define _PGET(VAR,type) {  std::string s(__str__(VAR));\
_##VAR = pt.get<type>(s); }

#define PGETBASE(VAR) {\
auto st = std::string(__str__(VAR));\
auto ptBase##VAR = pt.getChild(st); \
VAR##FromPropertyTree(ptBase##VAR);\
}
#define PGETMEMBER(membervar) {\
auto st = std::string(__str__(membervar));\
auto pt2 = pt.getChild(st);\
membervar.FromPropertyTree(pt2);\
}

namespace Algorithm
{
	namespace Interface
	{
		// Class contract that exposes common methods for which to extend
		class IType
		{
			public:
            // causes problems with hiberlite when you derive it
            // from MVC so omitting this
			//	IType(IType& rhs) { *this = rhs; }
			virtual ~IType(){}; // destructor
			// methods don't communicate tho the key just the value

			// like stl containers returns size of type
			virtual size_t size(void){ return sizeof(IType);};

			// says the maximum size of the type
			virtual size_t max_size(void) { return sizeof(IType); };

			virtual void ToString(char* data,size_t& dataSize){ /* not implemented*/ };
			virtual void FromString(char* data,size_t& dataSize){};

			IType& operator=(const IType& rhs){
				std::string s;
				IType& rhsRef = const_cast<IType&>(rhs);
				size_t size = rhsRef.size();
				s.resize(size);
				rhsRef.ToString(const_cast<char*>(s.c_str()), size);
				FromString(const_cast<char*>(s.c_str()),size);
				return *this;
			};


			// must be friended methods
			// istream extraction operators terminated by std::endl for each respective subtype
			// ostream extraction operators terminated by std::endl for each respective subtype

			// encode the stream to stream with variable name + value name. Useful for key value streams;
			virtual IPropertyTree  ToPropertyTree(void){
				IPropertyTree pt;
				return pt;
			};

			// method which extracts the values from property tree
			virtual void FromPropertyTree(boost::property_tree::ptree&  typesEncodedInAPropertyTree){

			};

			// call a serializer here
			// method instructs how to write to file by calling the approppriate serializer
			virtual void ToFile(void){

			};

			virtual void FromFile(void) {};
			virtual std::string TypeName(void) { return ""; };

		protected:
			inline bool exist(const std::string& name)
			{
				std::ifstream file(name);
				if (!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
					return false;    // The file was not found.
				else                 // If the file was found, then file is non-0.
					return true;     // The file was found.
			}
		};
	}
}
#endif
