#ifndef I_FILE_SERIALIZER_H
#define I_FILE_SERIALIZER_H
#include "IJSONSerialize.h"
#include "IType.h"
#include "../../Tools/Diagnostics/Logger/Logger.h"
#include <cstdint>
#include <cstdlib>
#include <string>

namespace Algorithm
{
	namespace Interface
	{
		class IFileSerializer;
		class IFileSerializer : public virtual Algorithm::Interface::IType
		{
		   public:

		   std::string filename;
		   IFileSerializer(void):Algorithm::Interface::IType(),filename(){};

		  virtual void ToFile(void) {
		  	    std::string msg = TypeName() + "::ToFile()";
				LOGIT1(msg)
					std::string testJSON(filename);
					auto pt = ToPropertyTree();
					msg = TypeName() + "::ToFile() calling IJSON serialize";
				LOGIT1(msg)
					Algorithm::Interface::IJSONSerialize test(testJSON, pt);
				msg = TypeName() + "::ToFile() WriteFile";
				LOGIT1(msg)
					test.WriteFile();
		};


			virtual void FromFile(void)
			{
				auto msg = TypeName() + "::FromFile()\n";
				LOGIT1(msg)
				std::string testJSON(filename);
				auto pt = ToPropertyTree();
				Algorithm::Interface::IJSONSerialize test(testJSON, pt);
				test.ReadFile();
				this->FromPropertyTree(test.GetPropertyTree());
			};

		virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) { Algorithm::Interface::IPropertyTree pt; return pt;};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {};

		void ParseServerArgs(char** argv, int argc){
			std::string msg2="IFileSerializer::ParseServerArgs";
			LOGIT1(msg2)
			filename = "config.json";
			if(exist(filename))
			{
				std::string msg = "IFileSerializer::Calling FromFile";
				LOGIT1(msg)
				FromFile();
			}
			else
			{
				std::string msg = "IFileSerializer::Calling ToFile";
				LOGIT1(msg)
				ToFile(); // write it back so next time you can feed in the json
			}
		};
		}; // end class
	}
}
#endif
