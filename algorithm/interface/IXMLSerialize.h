#ifndef IXMLSerialize_H
#define IXMLSerialize_H
#include <string>
#include <vector>
#include <iostream>
#include "IPropertyTree.h"
#include <boost/property_tree/xml_parser.hpp>
namespace Algorithm
{
	namespace Interface
	{
// object that provides facilities to serialize JavaScript Object Notation(xml)
// citation: https://stackoverflow.com/questions/4586768/how-to-iterate-a-boost-property-tree
class IXMLSerialize
{
	IPropertyTree _pt;
	std::string _filename;
	
	public:
	
	IXMLSerialize(std::string& filename, IPropertyTree& pt):_pt(pt),_filename(filename){

	};
	
	virtual void WriteFile(void){
		try
		{
			write_xml(_filename, _pt.GetBoostPropertyTree());
		}
		catch(std::exception ex)
		{
			std::cerr << "can't write xml file " << _filename;
		}
	};
	
	virtual void ReadFile(void){
		 try 
		 {
				read_xml(_filename, _pt.GetBoostPropertyTree()); 
		 }
		 catch(const boost::property_tree::xml_parser_error &jpe)
		 {
				//do error handling
				std::cerr << "can't read xml file " << _filename <<jpe.what();
		 }
	};
	
	virtual void ReadFromString(std::string& s){
		try
		{
		 std::stringstream ss;
		 ss << s;
		 read_xml(ss,_pt.GetBoostPropertyTree());
		}
		catch(std::exception)
		{
			
		}
		 
	};
	virtual std::string WriteToString(void){
		std::stringstream ss;
		write_xml(ss,_pt.GetBoostPropertyTree());
		return ss.str();
	};
	
	// use to retrieve all the values but
	virtual IPropertyTree GetPropertyTree(void){
		return _pt;
	};
	
	private:

		
	
	
};
	}
}
#endif