#ifndef I_THREADING_BUDGET_H
#define I_THREAD_BUDGET_H
#include <map>
#include <string>
class IThreadingBudget
{
	protected:
	std::map<size_t,size_t> _threadBudget;
	public:
	IThreadingBudget(void){};
	size_t GetAllocatedThreadAmount(size_t& key){
		return _threadBudget[key];
	};

	size_t GetNumberOfSystemThreads(void) { return std::thread::hardware_concurrency();};
	
};


#endif