#ifndef IJSONSERIALIZE_H
#define IJSONSERIALIZE_H
#include <string>
#include <vector>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include "IPropertyTree.h"
namespace Algorithm
{
	namespace Interface
	{
// object that provides facilities to serialize JavaScript Object Notation(JSON)
// citation: https://stackoverflow.com/questions/4586768/how-to-iterate-a-boost-property-tree
class IJSONSerialize
{
	IPropertyTree _pt;
	std::string _filename;
	public:
	IJSONSerialize(std::string& filename, IPropertyTree& pt):_pt(pt),_filename(filename){

	};
	
	virtual void WriteFile(void){
		try
		{
			boost::property_tree::json_parser::write_json(_filename, _pt.GetBoostPropertyTree());
		}
		catch(std::exception ex)
		{
			std::cerr << "can't write json file " << _filename;
		}
	};

	virtual void WriteAsAString(std::string& outString)
	{
		std::stringstream ss;
		boost::property_tree::write_json(ss, _pt.GetBoostPropertyTree());
		outString = ss.str();
	};
	
	virtual void ReadFile(void){
		 try 
		 {
			 boost::property_tree::read_json(_filename, _pt.GetBoostPropertyTree());
		 }
		 catch(const boost::property_tree::json_parser_error &jpe)
		 {
				//do error handling
				std::cerr << "can't read json file " << _filename <<jpe.what();
		 }
	};
	
	virtual void ReadFromString(std::string& s){
		try
		{
		 std::stringstream ss;
		 ss << s;
		auto pt = _pt.GetBoostPropertyTree(); boost::property_tree::json_parser::read_json(ss, pt);
		}
		catch(std::exception)
		{
			
		}
	};
	virtual std::string WriteToString(void){
		std::stringstream ss;
    	boost::property_tree::json_parser::write_json(ss,_pt.GetBoostPropertyTree());
		return ss.str();
	};
	
	// use to retrieve all the values but
	virtual IPropertyTree& GetPropertyTree(void){
		return _pt;
	};
	
};
	}
}
#endif
