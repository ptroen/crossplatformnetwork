#ifndef I_HTML_SERIALIZE_H
#define I_HTML_SERIALIZE_H

// algorithm deduced from this example
// https://stackoverflow.com/questions/4586768/how-to-iterate-a-boost-property-tree
// https://theboostcpplibraries.com/boost.propertytree

#include <string>
#include <vector>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include "IPropertyTree.h"
// might be simpler to find and replace the doctypes and piggy back the xml parser
// it's hacky but workable
// find and replace <?xml version="1.0" encoding="UTF-8"?>
// find and replace the doctype to html
// I can defer this to after the project is done
class IHtmlSerilize
{
	/*
	std::string _filename;

		void indent(int level, std::ostringstream& ss) { for (int i=0; i<level; i++) ss << "  ";}; 
	
	
void printTree (boost::property_tree::ptree::ptree &pt, int level, std::ostringstream& ss) {
   ss << "<!DOCTYPE html>" << std::endl;
  if (pt.empty()) {
    ss << pt.data(); // data inside the tag
  }
  else 
  {
	  std::string tagName;
    if (level) 
		ss << std::endl; 

    ss << indent(level)  << std::endl;     

    
	for (ptree::iterator pos = pt.begin(); pos != pt.end();) {
		std::string tagName = pos->first;
      ss << indent(level+1) << "<" << tagName << ">"; 
      printTree(pos->second, level + 1); 
      ss << endl;
	  ss << indent(level+1) << "</" << tagName << ">"; 
		++pos;
    } 
  } 
}
	
public:
    IPropertyTree _pt;
	
	public:
	IHtmlSerilize(std::string& filename, IPropertyTree& pt):_pt(pt),_filename(filename){

	};
	
	virtual void WriteAsAString(std::string& outString)
	{
		std::ostringstream ss;
		printTree(_pt.GetBoostPropertyTree(),0,ss);
		outString = ss.str();
	}
	
	virtual void ReadFile(void){};
	
	virtual void ReadFromString(std::string& s){};
	*/
};

#endif
