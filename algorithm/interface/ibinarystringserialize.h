#ifndef I_BINARY_STRING_SERIALIZE_H
#define I_BINARY_STRING_SERIALIZE_H

namespace Algorithm
{
	namespace Interface
	{
		namespace BinarySerialize
		{
			template <class myType>
			void serializeToString(myType& reference_in, char* stringOut,size_t stringSize)
			{
				const char* charpointer = reinterpret_cast<const char*>(&reference_in); 
				// serialize
				for (int i = 0; i < stringSize; i++,stringOut++,charpointer++)
					*stringOut = *charpointer;
			};

			template <class myType>
			void deserializeFromStringToObject(char* string_in,size_t stringSize, myType& reference_out, size_t maxsize)
			{
				// refuse to serialize strings that are above the max size due to security considerations
				if(stringSize > maxsize)
				{
					return;
				}
				char* characterPointer = reinterpret_cast<char*>(&reference_out);
				for (int i = 0; i < stringSize; i++)
				{
					characterPointer[i] = string_in[i];
				}
			}
		}
	}
}

#endif