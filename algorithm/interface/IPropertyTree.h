#ifndef I_PROPERTY_TREE_H
#define I_PROPERTY_TREE_H
#include <boost/property_tree/ptree.hpp>
#include <memory>
#include <map>
#include <string>
#include <vector>
#include <iostream>
namespace Algorithm
{
	namespace Interface
	{
		class IPropertyTree
		{
			boost::property_tree::ptree _pt; // good reference reading https://theboostcpplibraries.com/boost.propertytree
			const std::string attributePrefix = ".<xmlattr>."; // attribute prefix to reference a attribute within boost property tree
			// https://stackoverflow.com/questions/3690436/how-are-attributes-parsed-in-boost-propertytree
			std::string BuildAttributeInsertionKey(std::string& key, std::string& attributeKey) { return key + attributePrefix + attributeKey; };

			public:
			        IPropertyTree& operator=(IPropertyTree& pt){
			        this->_pt = pt._pt;
			        return *this;};
				IPropertyTree(void) :_pt() {};
				IPropertyTree(boost::property_tree::ptree& pt) : _pt(pt) {};
				// usually only accessed by the serializers don't manually edit this
				boost::property_tree::ptree& GetBoostPropertyTree(void) { return _pt; };
				#ifdef _WIN32
				// key/value get and set
				template <class T>
			        void add(std::string& key, T& value)
			        {
					_pt.put(key, value);
				};
				#else
				template <class T>
			        void add(std::string key, T value)
			        {
					_pt.put(key, value);
				};
				#endif
				template <class T>
				T get(std::string& path) {
					return  _pt.get<T>(path);
				};
				// attribute get/set
				template <class T>
				void addAttribute(std::string& keyName, std::string& attributeKey, T& attributeValue) {
					_pt.add(BuildAttributeInsertionKey(keyName, attributeKey), std::to_string(attributeValue));
				}

				IPropertyTree getChild(std::string& key)
				{
					return IPropertyTree(_pt.get_child(key));
				}

				template <class T>
				T getAttribute(std::string& keyPath, std::string& attributeName) {
					return  _pt.get<T>(BuildAttributeInsertionKey(keyPath, attributeName));
				}


			void addPropertyTree(std::string& keyOfChildTree,IPropertyTree& tree)
		        {
			    _pt.add_child(keyOfChildTree,tree.GetBoostPropertyTree());
			};

			void addAttribute(std::string& keyName,std::string& attributeKey, std::string& attributeValue)
			{
					_pt.add(BuildAttributeInsertionKey(keyName,attributeKey), attributeValue);
			};
		};
	}
}
#endif
