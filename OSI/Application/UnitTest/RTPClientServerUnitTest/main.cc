#include "../../../Transport/RTP/RTPTransport.h"
#include "../../../Transport/RTP/RTPClientTransport.h"
#include "../../../Transport/interface/IRTPClientTransportInitializationParameters.h"
#include "../../../Transport/interface/IRTPTransportInitializationParameters.h"
#include "../../../Transport/UnitTest/LockStepTestHooks.h"
#include <thread>

void ServerMain(int argc,char** argv)
{
	OSI::Transport::Interface::IRTPTransportInitializationParameters server_init_parameters;
	const size_t defaultHTTPPort = 80;
	server_init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPPort, defaultHTTPPort);

	try
	{
		OSI::Transport::RTP::RTPServerTransport<OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IRTPTransportInitializationParameters>,
			OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::IRTPTransportInitializationParameters>,
			OSI::Transport::Test::LockStepServerSession<OSI::Transport::Interface::IRTPTransportInitializationParameters>,
			OSI::Transport::Interface::IRTPTransportInitializationParameters> serverTransport(server_init_parameters);
		std::cout << "running server" << std::endl;
		serverTransport.RunServer();
	}
	catch (std::exception ex)
	{
		std::cout << "server exception " << ex.what() << std::endl;
	}
};

void ClientMain(void)
{
	OSI::Transport::Interface::IRTPClientTransportInitializationParameters client_init_parameters;
	client_init_parameters.ipAddress = "::1";
	client_init_parameters.ipv6 = true;
	client_init_parameters._incomingPortApplicationLayerAddress = 80;
	client_init_parameters._outgoingPortApplicationLayerAddress = 80;
	client_init_parameters.closeConnectionOnZeroOutput = true;
	try
	{
		OSI::Transport::RTP::RTPClientTransport<OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>, OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>,
			OSI::Transport::Test::LockStepClientSession<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>, OSI::Transport::Interface::IRTPClientTransportInitializationParameters> client(client_init_parameters);
		std::cout << "initializing client" << std::endl;

		OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters> incoming(client_init_parameters);
		incoming.msg = "client_sends_flowers._Lots_of love_to_the_server";
		std::string loopbackAddress((char*)client_init_parameters.ipAddress.c_str()); 
		std::cout << "running client" << std::endl;

		client.RunClient(loopbackAddress, incoming);
	}
	catch (std::exception ex)
	{
		std::cout << "client exception" << ex.what() << std::endl;
	}
}

int main(int argc, char* argv[])
{
	std::thread serverThread(ServerMain,argc,&(*argv));

	std::thread clientThread(ClientMain);

	clientThread.join();
	serverThread.join();
	return 0;
}
