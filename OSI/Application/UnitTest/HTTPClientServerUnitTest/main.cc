#include "../../../Transport/HTTP/HTTPClientTransport.h"
#include "../../../Transport/HTTP/HTTPTransport.h"
#include "../../../Transport/interface/IHTTPInitializationParameters.h"
#include "../../../Transport/UnitTest/LockStepTestHooks.h"
#include "../../../Transport/interface/IClientTransportInitializationParameters.h"
#include <thread>

void ServerMain(int argc,char** argv)
{
	OSI::Transport::Interface::IHTTPInitializationParameters server_init_parameters;
	const size_t defaultHTTPPort = 80;
	server_init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPPort, defaultHTTPPort);

	try
	{
		OSI::Transport::HTTP::HTTPServerTransport<OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IHTTPInitializationParameters>,
			OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::IHTTPInitializationParameters>,
			OSI::Transport::Test::LockStepServerSession<OSI::Transport::Interface::IHTTPInitializationParameters>,
			OSI::Transport::Interface::IHTTPInitializationParameters> serverTransport(server_init_parameters);
		std::cout << "running server" << std::endl;
		serverTransport.RunServer();
	}
	catch (std::exception ex)
	{
		std::cout << "server exception " << ex.what() << std::endl;
	}
};

void ClientMain(OSI::Session::HTTP::HTTPRequestType requestType)
{
	OSI::Transport::Interface::IClientTransportInitializationParameters client_init_parameters;
	client_init_parameters.ipAddress = "::1";
	client_init_parameters.ipv6 = true;
	client_init_parameters._incomingPortApplicationLayerAddress = 80;
	client_init_parameters._outgoingPortApplicationLayerAddress = 80;
	client_init_parameters.closeConnectionOnZeroOutput = true;
	try
	{
		OSI::Transport::HTTP::HTTPClientTransport<OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>,
			OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>,
			OSI::Transport::Test::LockStepClientSession<OSI::Transport::Interface::IClientTransportInitializationParameters>, OSI::Transport::Interface::IClientTransportInitializationParameters> client(client_init_parameters);
		std::cout << "initializing client" << std::endl;

		OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters> incoming(client_init_parameters);
		incoming._requestBuilder = new OSI::Session::HTTP::HTTP_Request_Builder();
		incoming._requestBuilder->url = "/incomingPayload/index.html";
		incoming._requestBuilder->keyValueArgs[std::string("client")] = "client_sends_flowers._Lots_of love_to_the_server";
		incoming._requestBuilder->requestType = requestType;
		std::string loopbackAddress((char*)client_init_parameters.ipAddress.c_str()); // loopback address to the main host citation: https://en.wikipedia.org/wiki/IPv6_address
		std::cout << "running client" << std::endl;

		client.RunClient(loopbackAddress, incoming);
	}
	catch (std::exception ex)
	{
		std::cout << "client exception" << ex.what() << std::endl;
	}
}

int main(int argc, char* argv[])
{
	std::thread serverThread(ServerMain,argc,&(*argv));

	std::thread clientThread(ClientMain,OSI::Session::HTTP::HTTPRequestType::GETREQUEST);
	std::thread clientThread2(ClientMain, OSI::Session::HTTP::HTTPRequestType::POSTREQUEST);
	std::thread clientThread3(ClientMain, OSI::Session::HTTP::HTTPRequestType::PUTREQUEST);
	std::thread clientThread4(ClientMain, OSI::Session::HTTP::HTTPRequestType::DELETEREQUEST);
	clientThread.join();
	clientThread2.join();
	clientThread3.join();
	clientThread4.join();
	serverThread.join();
	return 0;
}
