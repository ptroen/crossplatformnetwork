-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

workspace "HTTPClientServerUnitTest"
   configurations { "Debug", "Release" }
   architecture "x86_64"
project "HTTPClientServerUnitTest"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   ",../../../Application/api/*.*",
    "../../../Transport/HTTP/*.*", 
   "../../../Session/HTTP/*.*", 
   "../../../Presentation/HTTP/*.*", 
   "../../../Physical/HTTP/*.*", 
   "../../../Network/HTTP/*.*", 
   "../../../DataLink/HTTP/*.*", 
   "../../../Application/HTTP/*.*",
   "../../../Transport/TCP/*.*", 
   "../../../Session/TCP/*.*", 
   "../../../Presentation/TCP/*.*", 
   "../../../Physical/TCP/*.*", 
   "../../../Network/TCP/*.*", 
   "../../../DataLink/TCP/*.*", 
   ",../../../Application/TCP/*.*"  }
   removefiles { "*.lua", "*.sln","*.v*","*.sh","*.log","*.obj","*.*db","*.suo","*.pch" }
   libdirs {} 
   excludes { "RTP*","UDP*","*rtp*","*udp*","HTTPS*" }
   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"
	  
	  
	  



	