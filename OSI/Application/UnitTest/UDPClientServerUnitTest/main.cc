#include "../../../Transport/UDP/UDPTransport.h"
#include "../../../Transport/UDP/UDPClientTransport.h"
#include "../../../Transport/interface/ITransportInitializationParameters.h"
#include "../../../Transport/UnitTest/LockStepTestHooks.h"
#include "../../../Transport/interface/IClientTransportInitializationParameters.h"
#include <thread>

void ServerMain()
{
	OSI::Transport::Interface::ITransportInitializationParameters server_init_parameters;
	server_init_parameters._incomingPortApplicationLayerAddress = 80;
	server_init_parameters._outgoingPortApplicationLayerAddress = 80;
	try
	{
		OSI::Transport::UDP::UDPServerTransport<OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::ITransportInitializationParameters>,
			OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::ITransportInitializationParameters>,
			OSI::Transport::Test::LockStepServerSession<OSI::Transport::Interface::ITransportInitializationParameters>,
			OSI::Transport::Interface::ITransportInitializationParameters> serverTransport(server_init_parameters);
		std::cout << "running server" << std::endl;
		serverTransport.RunServer();
	}
	catch(std::exception ex)
	{
		std::cout << "server exception " << ex.what() << std::endl;
	}
};

void ClientMain()
{
	OSI::Transport::Interface::IClientTransportInitializationParameters client_init_parameters;
	client_init_parameters.ipAddress = "::1";
	client_init_parameters.ipv6 = true;

	client_init_parameters._incomingPortApplicationLayerAddress = 80;
	client_init_parameters._outgoingPortApplicationLayerAddress = 80;
	try
	{
		OSI::Transport::UDP::UDPClientTransport<OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>,
			OSI::Transport::Test::OutgoingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>,
			OSI::Transport::Test::LockStepClientSession<OSI::Transport::Interface::IClientTransportInitializationParameters>, OSI::Transport::Interface::IClientTransportInitializationParameters> client(client_init_parameters);
		std::cout << "initializing client" << std::endl;


		OSI::Transport::Test::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters> incoming(client_init_parameters);
		incoming.msg = "client sends flowers. Lots of love to the server 3>";
		std::string loopbackAddress((char*)client_init_parameters.ipAddress.c_str()); // loopback address to the main host citation: https://en.wikipedia.org/wiki/IPv6_address
		std::cout << "running client" << std::endl;


		client.RunClient(loopbackAddress, incoming);
	}
	catch(std::exception ex)
	{
		std::cout << "client exception" << ex.what() << std::endl;
	}
}

int main(int argc, char* argv[])
{
	std::thread serverThread(ServerMain);

	std::thread clientThread(ClientMain);


	std::thread clientThread2(ClientMain);
	clientThread.join();
	clientThread2.join();

	serverThread.join();
	return 0;
}
