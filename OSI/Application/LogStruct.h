#ifndef LOG_STRUCT_H
#define LOG_STRUCT_H
// non fatal errors
#include <thread>
#include <memory>
#include <xmmintrin.h>
#include <chrono>
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
/*
using namespace std::chrono;
// not necessary socket errors
// logging to
enum IndividualSocketError
{
	IEPOLLERR='1',
	ECHO='e',
	IP='I',
	TEXT='T'
};
#define LENGTH		64  	// Buffer length
#define LENGTH128    LENGTH/16
#define LENGTH256    LENGTH/32
// lab computers support avx but the compiler does not :(
union LogUnion 
{
   char			     text[LENGTH];
   __attribute__ ((aligned(32))) __m128 m[LENGTH128];
}; 

struct LogStruct
{
	enum IndividualSocketError errorCodePivot;
	high_resolution_clock::time_point looptime; 
	short count;
	LogUnion data;

	struct LogStruct& operator=(const struct LogStruct& o){
		errorCodePivot=o.errorCodePivot;
		looptime=o.looptime;
		count=o.count;
		// manual loop unrolling		
		data.m[0]=o.data.m[0];
		data.m[1]=o.data.m[1];
		data.m[2]=o.data.m[2];
		data.m[3]=o.data.m[3];
		return *this;
	};

	void Write(high_resolution_clock::time_point starttime){
		duration<double> time_span = duration_cast<duration<double>>(looptime - starttime);
		std::cout << "Msg:" 
			  <<(char)errorCodePivot
			  << " "
			  << "time at " << time_span.count() << " "
			  << "Message:" << (char*)data.text;
		std::cout <<"\n";
			  
	};
};

// modified from citation: http://en.cppreference.com/w/cpp/memory/align
struct AlignAllocator
{
    char* data;
    void* p;
    std::size_t sz;
    size_t _alignment;
    AlignAllocator(std::size_t N,std::size_t alignment=32) :  sz(N+alignment) {
	_alignment=alignment;	
	data=new char[sz];
	p=data;
    }
    
    template <typename TProtocolIncoming>
    TProtocolIncoming* aligned_alloc(std::size_t a = alignof(TProtocolIncoming))
    {
        if (std::align(a, sizeof(TProtocolIncoming), p, sz))
        {
            TProtocolIncoming* result = reinterpret_cast<TProtocolIncoming*>(p);
            p = (char*)p + sizeof(TProtocolIncoming);
            sz -= sizeof(TProtocolIncoming);
            return result;
        }
        return nullptr;
    }
};

class Logger
{	
	size_t _current;
	size_t _amount;
	struct LogStruct* _logArray;
	struct LogStruct* _pLogArray;
	AlignAllocator a;
	high_resolution_clock::time_point starttime;
	high_resolution_clock::time_point currenttime;
	void FlushSingleThread(){
		_pLogArray=_logArray;
		for(unsigned int i=0;i<_current;i++)
		{
			_pLogArray->Write(starttime);
			++_pLogArray;
		}
	};
	inline void Log(struct LogStruct& l){
		*_pLogArray=l;
		++_pLogArray;
		++_current;
		if(_current==_amount)
		{
			 currenttime= high_resolution_clock::now();
			Flush();
			_current=0;
			_pLogArray=_logArray;	
		}
	};
	public:
	Logger(size_t amount):a(amount*sizeof(struct LogStruct)){
		_current=0;
		_amount=amount;
		
		_logArray=a.aligned_alloc<LogStruct>();
		_pLogArray=&(*_logArray);
		currenttime=starttime= high_resolution_clock::now();
	};
	

	void Flush(){ FlushSingleThread();};
	
	~Logger(){
		FlushSingleThread();
	};
	
	inline void Log(enum IndividualSocketError errorCodePivot,const char* text,short count)  
	{		
		struct LogStruct l;
		l.errorCodePivot=errorCodePivot;
		l.looptime=currenttime;
		short amt=(count <= LENGTH) ? count: LENGTH ;
		memcpy(l.data.text,text,amt);
		Log(l);
	};

	
};
*/
#endif
