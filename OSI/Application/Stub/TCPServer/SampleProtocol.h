#ifndef SAMPLE_PROTOCOL_H
#define SAMPLE_PROTOCOL_H
#include <iostream>
#include <sstream>
#include "../../../../OSI/Transport/HTTP/HTTPRequest.h"
#include "../../../../OSI/Transport/HTTP/HTTPResponse.h"
#include "../../../../algorithm/interface/ibinarystringserialize.h"
#include "../../../../algorithm/interface/IJSONSerialize.h"
#include "../../../Session/HTTP/ContentRecordType.h"
#include "../../../../algorithm/interface/IType.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"

#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif
//#define PERFBENCMARK 1
namespace SampleProtocol
{
	template<class TInitializationParameters>
	class OutgoingPayload;

	template<class TInitializationParameters>
	class IncomingPayload : public virtual Algorithm::Interface::IType
	{
	public:
		const size_t maxText = 1024;
		std::string msg;
		bool customHTTPUrl;
		OSI::Session::ContentRecordType* _contentType;
		OSI::Session::Interface::IRequestBuilder* _requestBuilder;
		TInitializationParameters& _initialization;
		IncomingPayload(TInitializationParameters& initialization):_initialization(initialization),msg(),customHTTPUrl(false),_contentType(nullptr),_requestBuilder(nullptr) {};
		size_t size(void) { return MIN(max_size(), msg.size()); };
		size_t max_size(void) { return maxText; }
		IncomingPayload& operator=(IncomingPayload& rhs)
		{
			std::string s;
			IncomingPayload& rhsRef = const_cast<IncomingPayload&>(rhs);
			size_t size = rhsRef.size();
			s.resize(size);
			rhsRef.ToString(const_cast<char*>(s.c_str()), size);
			FromString(const_cast<char*>(s.c_str()), size);
			return *this;
		}
		void ToString(char* stringout,size_t dataSize) {
			size_t stringSize = msg.size();
			memcpy(stringout, msg.c_str(), msg.size());


		}
		void FromString(char* dataIn,size_t dataInSize) {
			size_t stringSize = max_size() > dataInSize ? dataInSize : max_size();
			msg.resize(stringSize);
			memcpy(&msg[0], &dataIn[0], stringSize);
		}
		friend std::ostream& operator<<(std::ostream& rhs,  IncomingPayload& o)
		{
			std::string s;
			size_t size = o.size();
			s.resize(size);
			o.ToString(const_cast<char*>(s.c_str()),size);
			rhs << s.c_str();
			return rhs;
		};
		friend std::istream& operator>>(std::istream& rhs, IncomingPayload& o)
		{
			std::string s;
			size_t size = o.size();
			s.resize(size);
			rhs >> s;
			o.FromString(const_cast<char*>(s.c_str()),size);
			return rhs;
		};

		// method to prep before the hook processes
		void IncomingHTTPRequest(OSI::Session::ContentRecordType& contentType, OSI::Session::Interface::IRequestBuilder&  requestBuilder)
		{
			this->_requestBuilder = &requestBuilder;
			if (requestBuilder.url == OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.hat.s")
			{
				customHTTPUrl = true;
				_contentType = &contentType;
				_requestBuilder = &requestBuilder;
			}
			else if(requestBuilder.url== OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.json")
			{
				customHTTPUrl = true;
				_contentType = &contentType;
				_requestBuilder = &requestBuilder;
			}
			else
				customHTTPUrl = false;
		}

		OSI::Transport::HTTP::HTTPRequest< TInitializationParameters> ToHTTPRequest(void)
		{
			OSI::Transport::HTTP::HTTPRequest< TInitializationParameters> httprequest(_initialization);
			if(this->_requestBuilder == nullptr)
			{
				_requestBuilder = new OSI::Session::HTTP::HTTP_Request_Builder();
			}
			_requestBuilder->url = "/incomingPayload";
			_requestBuilder->requestType = OSI::Session::HTTP::HTTPRequestType::GETREQUEST;
			_requestBuilder->keyValueArgs["msg"] = this->msg;
			std::string s = _requestBuilder->ToRequest();
			size_t size = s.size();

			httprequest.FromString(const_cast<char*>(s.c_str()), size);
			return httprequest;
		};
	};

	template<class TInitializationParameters>
	class SampleProtocolServerSession
	{
	public:
		size_t size(void) { return sizeof(SampleProtocolServerSession); };
		size_t max_size(void) { return sizeof(SampleProtocolServerSession); }
		SampleProtocolServerSession(TInitializationParameters& initializationParameters) {};
		SampleProtocolServerSession() {};

	public:
		virtual void OnTimerHook(void)
		{
		}
		virtual void OnIdleHook(void)
		{
		}

		virtual bool IncomingHook(IncomingPayload<TInitializationParameters>& in, OutgoingPayload<TInitializationParameters>& out, std::string& ipadress)
		{
			static size_t userCount = 0;
			++userCount;
			std::string s = std::to_string(userCount);
			LOGIT1(s)
			if(in._requestBuilder != nullptr &&  in.customHTTPUrl)
			{
				if ( in._requestBuilder->url == OSI::Session::HTTP::httpStaticResourceDirectory+"/abc.hat.s")
				{
					out.out = std::string("<HTML><H2> Custom IncomingHook Page made by user</H2> </HTML>");
				}
				else if(in._requestBuilder->url== OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.json")
				{
					in.customHTTPUrl = false;
					out.DumpJSONInObject();
				}
				return true;
			}
			else
			{
				#ifndef PERFBENCMARK
                		    out.out = "server says hello";
                		#else
                		    out.out = "a"; // for benchmarking we will only echo one character
                		#endif
				return true;
			}
			// Session
			in.customHTTPUrl = false;
			return false; // no more data
		}

		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& out)
		{
			out.out = "server says hello";
			return true;
		}
	};

	template<class TInitializationParameters>
	class SampleProtocolClientSession : public virtual SampleProtocolServerSession<TInitializationParameters>
	{
		bool keepTransmitting;
	public:
		SampleProtocolClientSession(TInitializationParameters& parameters):keepTransmitting(false){};
		SampleProtocolClientSession():keepTransmitting(false) {};

		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& in, IncomingPayload<TInitializationParameters>& out, std::string& ipadress)
		{
				// Session
            		std::string msg;
			msg = "client reads from server:";
			msg += in.out;
			LOGIT(msg)
				std::cout << msg << std::endl;
			return keepTransmitting;
		};
		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& out)
		{
            		std::string msg;
			msg = "client reads from server:";
			msg += out.out;
			std::cout << msg << std::endl;
			LOGIT(msg)
			return keepTransmitting;
		}
		virtual void OnTimerHook(void)
		{
			
		}
		virtual void OnIdleHook(void)
		{
    			
		}
	};

	template<class TInitializationParameters>
	class OutgoingPayload : public virtual Algorithm::Interface::IType
	{
	public:
		std::string out;
		OutgoingPayload(TInitializationParameters& initialization) {};
		size_t size(void) { return out.size(); };
		size_t max_size(void) { return 1024; }
		void ToString(char* string, size_t stringSize)
		{
			memcpy(&(*string), out.c_str(), size());
		}
		void FromString(char* data, size_t dataInSize) { out.resize(dataInSize); // we shouldn't be using this method as it's pure output
		memcpy(&out[0], data, dataInSize);
			};
		void FromHTTPResponse(OSI::Transport::HTTP::HTTPResponse< TInitializationParameters> response)
		{
		    std::string msg;
		    msg = "HTTP Response:";
		    msg += response.buffer;
		    LOGIT(msg)

		};
		OutgoingPayload& operator=(OutgoingPayload& rhs)
		{
			std::string s;
			OutgoingPayload& rhsRef = const_cast<OutgoingPayload&>(rhs);
			size_t size = rhsRef.size();
			s.resize(size);
			rhsRef.ToString(const_cast<char*>(s.c_str()), size);
			FromString(const_cast<char*>(s.c_str()), size);
			return *this;
		}
		friend std::ostream& operator<<(std::ostream& rhs, const OutgoingPayload& o) { return rhs; };
		friend std::istream& operator>>(std::istream& rhs, const OutgoingPayload& o) { return rhs; };

		virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
			Algorithm::Interface::IPropertyTree pt;
			int i = 2;
			pt.add(std::string("test"), i);
			return pt;
		};

		// call a serializer here
	// method instructs how to write to file by calling the approppriate serializer
		virtual void ToFile(void) {
			std::string testJSON("test.json");
			auto pt=ToPropertyTree();
			Algorithm::Interface::IJSONSerialize test(testJSON, pt);
			test.WriteFile();
		};

		virtual void DumpJSONInObject(void)
		{
			std::string testJSON("test.json");
			auto pt=ToPropertyTree();
			Algorithm::Interface::IJSONSerialize test(testJSON, pt);
			test.WriteAsAString(out);
		}


		// method which extracts the values from property tree
		virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {

		};

	};
};

#endif
