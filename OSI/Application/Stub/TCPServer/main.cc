#include "../../../../Tools/Diagnostics/Logger/Logger.h"
#include "../../../Transport/TCP/TCPTransport.h"
#include "../../../Transport/interface/ITransportInitializationParameters.h"
#include "SampleProtocol.h"

int main(int argc, char* argv[])
{
	std::string s("TCPServer.main");
	Tools::Diagnostics::Logger::Logger::Get_Instance().Log(s);
	OSI::Transport::Interface::ITransportInitializationParameters init_parameters;
	
	const size_t defaultTCPPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultTCPPort, defaultTCPPort);
	OSI::Transport::TCP::TCP_ServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::ITransportInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::ITransportInitializationParameters>, SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::ITransportInitializationParameters>,  OSI::Transport::Interface::ITransportInitializationParameters> tcpTransport(init_parameters);
	tcpTransport.RunServer();
	return 0;
}
