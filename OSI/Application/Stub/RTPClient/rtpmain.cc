#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/RTP/RTPClientTransport.h"
#include "../../../Transport/interface/IRTPClientTransportInitializationParameters.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"
int main(int argc,char* argv[])
{
	std::string s("RTPClient rtpmain.cc");
	LOGIT1(s)
	OSI::Transport::Interface::IRTPClientTransportInitializationParameters init_parameters;
	short defaultTCPPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultTCPPort, defaultTCPPort);
	SampleProtocol::IncomingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters> request(init_parameters);
	OSI::Transport::RTP::RTPClientTransport<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>, 
	SampleProtocol::IncomingPayload<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>, 
	SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IRTPClientTransportInitializationParameters>, 
	OSI::Transport::Interface::IRTPClientTransportInitializationParameters> client(init_parameters);
	request.msg = init_parameters.initialPayload;
	client.RunClient(init_parameters.ipAddress, request);
}
