#include "../../../Transport/UDPMulticast/UDPMulticastClientTransport.h"
#include "../../../Transport/interface/IMulticastTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"

int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IMulticastInitializationParameters init_parameters;
	short defaultUDPMulticastPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultUDPMulticastPort, defaultUDPMulticastPort);
	OSI::Transport::UDPMulticast::UDPMulticastClientTransport<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IMulticastInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IMulticastInitializationParameters>, OSI::Transport::Interface::IMulticastInitializationParameters  > clientTransport(init_parameters);
	clientTransport.RunClient();
  return 0;
}