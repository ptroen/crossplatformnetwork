#include "../../../Transport/interface/IClientHTTPSInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/HTTPS/HTTPSClientTransport.h"
int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IClientHTTPSInitializationParameters init_parameters;
	short defaultHTTPSPort = 443;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPSPort, defaultHTTPSPort);
	std::string msg = "running server";
	LOGIT1(msg)
	OSI::Transport::HTTPS::HTTPSClientTransport<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IClientHTTPSInitializationParameters>, SampleProtocol::IncomingPayload<OSI::Transport::Interface::IClientHTTPSInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IClientHTTPSInitializationParameters>, OSI::Transport::Interface::IClientHTTPSInitializationParameters> httpsclient(init_parameters);
	SampleProtocol::IncomingPayload<OSI::Transport::Interface::IClientHTTPSInitializationParameters> request(init_parameters);
	msg = "calling httpsclient.RunClient";
	request.msg = init_parameters.initialPayload;
    LOGIT1(request.msg)
	LOGIT1(msg)
	httpsclient.RunClient((char*)init_parameters.ipAddress.c_str(), request);
	return 0;
}
