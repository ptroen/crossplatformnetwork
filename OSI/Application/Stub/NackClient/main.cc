#include "../../../Transport/Nack/NackClientTransport.h"
#include "../../../Transport/interface/INackClientInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"
int main(int argc,char* argv[])
{
	
	OSI::Transport::Interface::INackClientInitializationParameters init_parameters;
	std::string s("NackClient main.cc");
	LOGIT1(s)
	try
	{
		std::string s("NackClient main.cc");
		Tools::Diagnostics::Logger::Logger::Get_Instance().Log(s);
		short defaultNackPort = 80;
		init_parameters.ParseServerArgs(&(*argv), argc, defaultNackPort, defaultNackPort);
		OSI::Transport::NACK::NackClientTransport< SampleProtocol::IncomingPayload<OSI::Transport::Interface::INackClientInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::INackClientInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::INackClientInitializationParameters>, OSI::Transport::Interface::INackClientInitializationParameters> nackClient(init_parameters);
		SampleProtocol::IncomingPayload<OSI::Transport::Interface::INackClientInitializationParameters> request(init_parameters);
		request.msg = init_parameters.initialPayload;
		nackClient.RunClient(const_cast<char*>(init_parameters.ipAddress.c_str()), request);
	}
	catch(std::exception ex)
	{
		std::string msg = ex.what();
		msg += "\n";
		std::cout << msg;	
	}
	return 0;
}
