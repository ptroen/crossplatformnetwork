#include "../../../Transport/interface/ISSLClientTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/SSL/sslclienttransport.h"

int main(int argc,char* argv[])
{
	OSI::Transport::Interface::ISSLClientTransportInitializationParameters init_parameters;
	short defaultSSLPort = 443;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultSSLPort, defaultSSLPort);
	OSI::Transport::SSL::SSLClientTransport<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::ISSLClientTransportInitializationParameters>, SampleProtocol::IncomingPayload<OSI::Transport::Interface::ISSLClientTransportInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::ISSLClientTransportInitializationParameters>, OSI::Transport::Interface::ISSLClientTransportInitializationParameters> sslclient(init_parameters);
	SampleProtocol::IncomingPayload< OSI::Transport::Interface::ISSLClientTransportInitializationParameters> request(init_parameters);
	request.msg = init_parameters.initialPayload;
	sslclient.RunClient(init_parameters.ipAddress.c_str(), request);
	return 0;
}
