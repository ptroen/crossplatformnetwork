#include "../../../Transport/HTTPS/HTTPSTransport.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/interface/IHTTPSInitializationParameters.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"
using namespace SampleProtocol;

int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IHTTPSInitializationParameters init_parameters;
	 auto defaultHTTPSPort = 443;

	std::string s = "httpsserver.cc\n";
	LOGIT(s)
	 try
	 {
		 init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPSPort, defaultHTTPSPort);
		 OSI::Transport::HTTPS::HTTPSTransport<IncomingPayload<OSI::Transport::Interface::IHTTPSInitializationParameters>,
																				OutgoingPayload<OSI::Transport::Interface::IHTTPSInitializationParameters>,
																				SampleProtocolServerSession<OSI::Transport::Interface::IHTTPSInitializationParameters>,
																				OSI::Transport::Interface::IHTTPSInitializationParameters> httpsTransport(init_parameters);
	 	 httpsTransport.RunServer();
	 }
	 catch (std::exception& e)
	 {
		 std::cerr << "Exception: " << e.what() << "\n";
	 }
	 return 0;
}
