#include "../../../Transport/interface/IClientTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/TCP/TCPClientTransport.h"
int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IClientTransportInitializationParameters  init_parameters;
	short defaultTCPPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultTCPPort, defaultTCPPort);
	OSI::Transport::TCP::TCP_ClientTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>, 
		SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IClientTransportInitializationParameters>, OSI::Transport::Interface::IClientTransportInitializationParameters> tcpTransport(init_parameters);;
	SampleProtocol::IncomingPayload< OSI::Transport::Interface::IClientTransportInitializationParameters> request(init_parameters);
	request.msg = init_parameters.initialPayload;
	std::string ipMsg=init_parameters.ipAddress;
	LOGIT1(ipMsg)
	tcpTransport.RunClient(init_parameters.ipAddress, request);
	return 0;
}
