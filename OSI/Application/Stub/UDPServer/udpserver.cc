#include <stdlib.h>
#include "../../../Transport/UDP/udptransport.h"
#include "../../../Transport/interface/ITransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"
int main(int argc, char* argv[])
{
	try
	{
		OSI::Transport::Interface::ITransportInitializationParameters init_parameters;
		std::cout << "parsing config file" << std::endl;
		init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
		std::cout << " calling server constructor" << std::endl;
		OSI::Transport::UDP::UDPServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::ITransportInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::ITransportInitializationParameters>, SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::ITransportInitializationParameters>, OSI::Transport::Interface::ITransportInitializationParameters> serverTransport(init_parameters);
		std::cout << "running server" << std::endl;
		serverTransport.RunServer();
	}
	catch(std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}
	return 0;
}
