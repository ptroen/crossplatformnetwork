-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

solution "UDPServer"
   configurations { "Debug", "Release" }
project "UDPServer"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   flags {}
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   "../../../Application/api/*.*",
   "../../../Transport/UDP/*.*", 
   "../../../Session/UDP/*.*", 
   "../../../Presentation/UDP/*.*", 
   "../../../Physical/UDP/*.*", 
   "../../../Network/UDP/*.*", 
   "../../../DataLink/UDP/*.*", 
   ",../../../Application/UDP/*.*"  }
   libdirs {} 
   excludes { "*tcp*","*http*" }
   configurations "Debug"
      defines { "DEBUG" }

   configurations "Release"
      defines { "NDEBUG" }
