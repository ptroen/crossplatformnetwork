// citation: https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file
(function($)
{
    /*
     * $.import_js() helper (for JavaScript importing within JavaScript code).
     */
    var import_js_imported = [];

    $.extend(true,
    {
        import_js : function(script)
        {
            var found = false;
            for (var i = 0; i < import_js_imported.length; i++)
                if (import_js_imported[i] == script) {
                    found = true;
                    break;
                }

            if (found == false) {
                $("head").append('<script type="text/javascript" src="' + script + '"></script>');
                import_js_imported.push(script);
            }
        }
    });

})(jQuery);
$.import_js('/LikeButton.js');
$.import_js('/LoadDOM.js');
$domElements=['like_button_container','like_button']
var i;
for (i = 0; i < $domElements.length; i++) {
	LoadDOM('#'+$domElements[i])
}