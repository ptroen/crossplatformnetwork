#include "../../../Transport/interface/IAllInitiializationParameters.h"
#include "../../../Transport/All/AllProxy.h"
#include "../TCPProxy/SampleProxyProtocol.h"
#include "../TCPServer/SampleProtocol.h"
using namespace OSI;

int main(int argc, char* argv[])
{
	// replace with Sample Protocol and keeptesting
	Transport::Interface::IAllInitiializationParameters proxy_parameters;
	proxy_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	Transport::All::AllProxyTransport<SampleProxyProtocol::ProxyFlyWeightIncoming<Transport::Interface::IAllInitiializationParameters>,
		SampleProxyProtocol::ProxyFlyWeightOutcoming<Transport::Interface::IAllInitiializationParameters>,
		SampleProxyProtocol::SampleProxyProtocolExternal<Transport::Interface::IAllInitiializationParameters>,
		SampleProxyProtocol::SampleProxyProtocolInternal<Transport::Interface::IAllInitiializationParameters>,
		Transport::Interface::IAllInitiializationParameters> allProxyTransport(proxy_parameters);
	allProxyTransport.RunServer();
	return 0;
}
