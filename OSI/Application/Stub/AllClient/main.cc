#include "../../../Transport/interface/IAllInitiializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/All/AllClient.h"
int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IAllInitiializationParameters  init_parameters;
	short defaultTCPPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultTCPPort, defaultTCPPort);
	OSI::Transport::All::AllClient<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IAllInitiializationParameters >, 
	SampleProtocol::IncomingPayload<OSI::Transport::Interface::IAllInitiializationParameters >, 
	SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IAllInitiializationParameters >, 
		OSI::Transport::Interface::IAllInitiializationParameters > allTransport(init_parameters);
	SampleProtocol::IncomingPayload< OSI::Transport::Interface::IAllInitiializationParameters> request(init_parameters);
	request.msg = init_parameters.initialPayload;
	allTransport.RunClient(init_parameters.ipAddress, request);
	return 0;
}
