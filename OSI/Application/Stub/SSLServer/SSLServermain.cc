#include "../../../Transport/SSL/SSLTransport.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/interface/ISSLTransportInitializationParameters.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"

class SampleLog : public virtual Tools::Diagnostics::Logger::LoggingObject
{
public:
	std::string msg;
	SampleLog(void):msg("hello") {};
	virtual size_t size(void) { return msg.size(); };
	virtual std::string TypeName(void) { return "SampleLog"; };
	// says the maximum size of the type
	virtual size_t max_size(void) { return 80; };

	virtual void ToString(char* data, size_t& dataSize) { memcpy(data, msg.c_str(), size()); };
};

int main(int argc, char* argv[])
{
	
	OSI::Transport::Interface::ISSLTransportInitializationParameters init_parameters;
	 SampleLog sample;
	 size_t sampleSize = sample.size();
	 std::string s;
	 s.resize(sampleSize);
	 sample.ToString(&s[0],sampleSize);
	 LOGIT(s)
	
	const size_t defaultTCPPort = 80;
	init_parameters.ParseServerArgs(&(*argv), argc, defaultTCPPort, defaultTCPPort);
	OSI::Transport::SSL::SSL_ServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::ISSLTransportInitializationParameters>,
		SampleProtocol::OutgoingPayload<OSI::Transport::Interface::ISSLTransportInitializationParameters>,
		SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::ISSLTransportInitializationParameters>,
		OSI::Transport::Interface::ISSLTransportInitializationParameters> SSLTransport(init_parameters);
	SSLTransport.RunServer();
	return 0;
}
