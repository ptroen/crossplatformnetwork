-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

workspace "SSLServer"
   configurations { "Debug", "Release" }
   architecture "x86_64"
project "SSLServer"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   ",../../../Application/api/*.*",
   "../../../Transport/TCP/*.*", 
   "../../../Session/TCP/*.*", 
   "../../../Presentation/TCP/*.*", 
   "../../../Physical/TCP/*.*", 
   "../../../Network/TCP/*.*", 
   "../../../DataLink/TCP/*.*", 
   ",../../../Application/TCP/*.*"  }
   removefiles { "*.lua", "*.sln","*.v*","*.sh","*.log","*.obj","*.*db","*.suo","*.pch" }
   libdirs {} 
   excludes { "*rtp*","*udp*" }
   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"
	  
	  
	  



	