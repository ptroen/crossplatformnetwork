-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

solution "SSLServer"
   configurations { "Debug", "Release" }
project "SSLServer"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   flags {"C++14"}
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   "../../../Application/api/*.*",
   "../../../Transport/TCP/*.*", 
   "../../../Session/TCP/*.*", 
   "../../../Presentation/TCP/*.*", 
   "../../../Physical/TCP/*.*", 
   "../../../Network/TCP/*.*", 
   "../../../DataLink/TCP/*.*", 
   ",../../../Application/TCP/*.*"  }
   libdirs {} 
   excludes { "*rtp*","*udp*" }
   configurations "Debug"
      defines { "DEBUG" }

   configurations "Release"
      defines { "NDEBUG" }
	  
	  
	  



	
