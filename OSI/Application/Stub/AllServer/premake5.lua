-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

workspace "AllServer"
   configurations { "Debug", "Release" }
   architecture "x86_64"
project "AllServer"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   ",../../../Application/api/*.*",
   "../../../Transport/HTTP/*.*", 
   "../../../Session/HTTP/*.*", 
   "../../../Presentation/HTTP/*.*", 
   "../../../Physical/HTTP/*.*", 
   "../../../Network/HTTP/*.*", 
   "../../../DataLink/HTTP/*.*", 
   ",../../../Application/HTTP/*.*",
   "../../../Transport/TCP/*.*", 
   "../../../Session/TCP/*.*", 
   "../../../Presentation/TCP/*.*", 
   "../../../Physical/TCP/*.*", 
   "../../../Network/TCP/*.*", 
   "../../../DataLink/TCP/*.*", 
   ",../../../Application/TCP/*.*",
      "../../../Transport/HTTPS/*.*", 
   "../../../Session/HTTPS/*.*", 
   "../../../Presentation/HTTPS/*.*", 
   "../../../Physical/HTTPS/*.*", 
   "../../../Network/HTTPS/*.*", 
   "../../../DataLink/HTTPS/*.*", 
   ",../../../Application/HTTPS/*.*",
   "../../../Transport/TCP/*.*", 
     }
   removefiles { "*.lua", "*.sln","*.v*","*.sh","*.log","*.obj","*.*db","*.suo","*.pch" }
   libdirs {} 
   excludes { "*rtp*","*udp*" }
   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"
	  
	  
	  



	