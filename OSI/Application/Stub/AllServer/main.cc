#include "../../../Transport/All/AllServer.h"
#include "../../../Transport/interface/IAllInitiializationParameters.h"
#include "../TCPServer/SampleProtocol.h"

int main(int argc,char* argv[])
{
	OSI::Transport::Interface::IAllInitiializationParameters init_parameters;
	auto defaultHTTPSPort = 443;

	try
	{
		init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPSPort, defaultHTTPSPort);
		OSI::Transport::All::AllServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IAllInitiializationParameters>,
		                                        SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IAllInitiializationParameters>,
		                                        SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::IAllInitiializationParameters>,
		OSI::Transport::Interface::IAllInitiializationParameters> allTransport(init_parameters);
		allTransport.RunServer();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}
	return 0;
}
