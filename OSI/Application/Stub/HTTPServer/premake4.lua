-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

solution "HTTPServer"
   configurations { "Debug", "Release" }
project "HTTPServer"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   flags {}
   files {"./*.c*",
   "../../../algorithm/**",
   "../../../OSManagement/**",
   "../../../ThirdParty/**",
   "../../../Tools/**",
   "../../../Transport/interface/*.*", 
   "../../../Session/interface/*.*", 
   "../../../Presentation/interface/*.*", 
   "../../../Physical/interface/*.*", 
   "../../../Network/interface/*.*", 
   "../../../DataLink/interface/*.*", 
   ",../../../Application/interface/*.*",
   "../../../Transport/api/*.*", 
   "../../../Session/api/*.*", 
   "../../../Presentation/api/*.*", 
   "../../../Physical/api/*.*", 
   "../../../Network/api/*.*", 
   "../../../DataLink/api/*.*", 
   "../../../Application/api/*.*",
   "../../../Transport/TCP/*.*", 
   "../../../Transport/HTTP/*.*",
   "../../../Session/TCP/*.*",
   "../../../Session/HTTP/*.*", 
   "../../../Presentation/TCP/*.*", 
"../../../Presentation/HTTP/*.*", 
   "../../../Physical/TCP/*.*",
   "../../../Physical/HTTP/*.*", 
   "../../../Network/TCP/*.*", 
   "../../../Network/HTTP/*.*",
   "../../../DataLink/TCP/*.*", 
   "../../../DataLink/HTTP/*.*", 
   "../../../Application/TCP/*.*",
   "../../../Application/HTTP/*.*"  }
   libdirs {} 
   excludes { "*rtp*","*udp*" }
   configurations "Debug"
      defines { "DEBUG" }

   configurations "Release"
      defines { "NDEBUG" }
	  
	  
	  



	
