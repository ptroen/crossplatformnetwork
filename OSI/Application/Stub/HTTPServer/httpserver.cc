#include "../../../Transport/interface/IHTTPInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../Transport/HTTP/HTTPTransport.h"
#include "../../../../algorithm/CompressionType.h"
int main(int argc, char* argv[])
{
	
	OSI::Transport::Interface::IHTTPInitializationParameters ihttpInitializationParameters;
	//Algorithm::CompressionType<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IHTTPInitializationParameters>, OSI::Transport::Interface::IHTTPInitializationParameters> type(ihttpInitializationParameters);
	const size_t defaultHTTPPort = 80;
	ihttpInitializationParameters.ParseServerArgs(&(*argv), argc, defaultHTTPPort, defaultHTTPPort);

	OSI::Transport::HTTP::HTTPServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IHTTPInitializationParameters>, 
	                                          SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IHTTPInitializationParameters>, 
	                                          SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::IHTTPInitializationParameters>, 
	                                          OSI::Transport::Interface::IHTTPInitializationParameters> httpTransport(ihttpInitializationParameters);
	httpTransport.RunServer();
	return 0;
}
