#ifndef SAMPLE_PROXY_PROTOCOL_H
#define SAMPLE_PROXY_PROTOCOL_H
#include "../../../Transport/HTTP/HTTPRequest.h"
#include "../../../Transport/HTTP/HTTPResponse.h"
namespace SampleProxyProtocol
{
	template<class T>
	class ProxyFlyWeightIncoming
	{
		std::string s;
		bool customHTTPUrl;
		OSI::Session::ContentRecordType* _contentType;
		OSI::Session::HTTP::HTTP_Request_Builder* _requestBuilder;
	public:
		T& init;
		ProxyFlyWeightIncoming(T& t):init(t),s() {};
		ProxyFlyWeightIncoming(void) {};
		 ProxyFlyWeightIncoming& operator=(const ProxyFlyWeightIncoming& rhs) { s = rhs.s; return *this; }

		size_t max_size() { return 1024; };
		size_t size(void) { return s.size(); }
		void ToString(char* data, size_t dataSize)
		{
			memcpy(data, &s[0], dataSize);
		};
		void FromString(char* data, size_t dataSize)
		{
			s.resize(dataSize);
			memcpy(&s[0], data, dataSize);
		};

		OSI::Transport::HTTP::HTTPRequest< T> ToHTTPRequest(void)
		{
			OSI::Transport::HTTP::HTTPRequest< T> httprequest(init);
			//httprequest.url = "/incomingPayload";
			std::ostringstream ss;
			std::string s = ss.str();
			size_t size = s.size();
			httprequest.FromString(const_cast<char*>(s.c_str()), size);
			return httprequest;
		};

		void IncomingHTTPRequest(OSI::Session::ContentRecordType& contentType, OSI::Session::HTTP::HTTP_Request_Builder&  requestBuilder)
		{
			if (requestBuilder.url == OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.hat.s")
			{
				customHTTPUrl = true;
				_contentType = &contentType;
				_requestBuilder = &requestBuilder;
			}
			else if (requestBuilder.url == OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.json")
			{
				customHTTPUrl = true;
				_contentType = &contentType;
				_requestBuilder = &requestBuilder;
			}
			else
				customHTTPUrl = false;
		}
	};

	template <class T>
	class ProxyFlyWeightOutcoming
	{
	public:
		std::string s;
		ProxyFlyWeightOutcoming(void) {};
		ProxyFlyWeightOutcoming(T& t) {};
		const ProxyFlyWeightOutcoming& operator=(const ProxyFlyWeightOutcoming& rhs) { s= rhs.s; return *this; }
		size_t max_size() { return 1024; };
		size_t size(void) { return s.size(); }
		void ToString(char* data, size_t dataSize)
		{
			memcpy( data, &s[0], dataSize);
		};
		void FromString(char* data, size_t dataSize)
		{
			s.resize(dataSize);
			memcpy(&s[0], data, dataSize);
		};
		void FromHTTPResponse(OSI::Transport::HTTP::HTTPResponse< T> response) {};
	};

	template <class T>
	class SampleProxyProtocolExternal
	{
	public:
		T& initialization;
		SampleProxyProtocolExternal(T& parameters):initialization(parameters) {};
		virtual void OnIdleHook(void) {};
		virtual void OnTimerHook(void) {};
		virtual bool IncomingHook(ProxyFlyWeightIncoming<T>& in, ProxyFlyWeightOutcoming<T>& out, std::string& ipadress)
		{
			return true;
		}

		virtual bool IncomingHook(ProxyFlyWeightOutcoming<T>& out)
		{
			return true;
		}

		virtual bool IncomingHook(ProxyFlyWeightIncoming<T>& out)
		{
			return true;
		}


	};

	template <class T>
	class SampleProxyProtocolInternal
	{
	public:
		SampleProxyProtocolInternal(T& parameters) {};
		virtual void OnIdleHook(void) {};
		virtual void OnTimerHook(void) {};
		virtual bool IncomingHook(ProxyFlyWeightOutcoming<T>& in, ProxyFlyWeightIncoming<T>& out, std::string& ipadress)
		{
			// Session
			return true;
		};

		virtual bool IncomingHook(ProxyFlyWeightOutcoming<T>& out)
		{
			return true;
		}

		virtual bool IncomingHook(ProxyFlyWeightIncoming<T>& in) { return true; }
	};
}

#endif
