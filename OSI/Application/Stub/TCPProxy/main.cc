#include "../../../Transport/TCP/TCPProxy.h"
#include "../../../Transport/interface/IProxyTransportInitializationParameters.h"
#include "SampleProxyProtocol.h"
using namespace OSI;

int main(int argc, char* argv[])
{
	Transport::Interface::IProxyTransportInitializationParameters proxy_parameters;
	proxy_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	Transport::Interface::ITransportInitializationParameters internalProxy_parameters;
	Transport::TCP::TCPProxyTransport<SampleProxyProtocol::ProxyFlyWeightIncoming<Transport::Interface::IProxyTransportInitializationParameters>,
		SampleProxyProtocol::ProxyFlyWeightOutcoming<Transport::Interface::IProxyTransportInitializationParameters>,
		SampleProxyProtocol::SampleProxyProtocolExternal<Transport::Interface::IProxyTransportInitializationParameters>,
		SampleProxyProtocol::SampleProxyProtocolInternal<Transport::Interface::IProxyTransportInitializationParameters>,
		Transport::Interface::IProxyTransportInitializationParameters> tcpProxyTransport(proxy_parameters);
	tcpProxyTransport.RunServer();
	return 0;
}