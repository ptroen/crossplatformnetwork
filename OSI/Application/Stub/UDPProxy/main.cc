#include "../../../Transport/interface/IProxyTransportInitializationParameters.h"
#include "../../../Transport/UDP/UDPProxy.h"
#include "../TCPProxy/SampleProxyProtocol.h"
using namespace OSI;

int main(int argc, char* argv[])
{
	Transport::Interface::IProxyTransportInitializationParameters proxy_parameters;
	proxy_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	Transport::UDP::UDPProxyTransport<SampleProxyProtocol::ProxyFlyWeightIncoming<Transport::Interface::IProxyTransportInitializationParameters>,
	SampleProxyProtocol::ProxyFlyWeightOutcoming<Transport::Interface::IProxyTransportInitializationParameters>,
	SampleProxyProtocol::SampleProxyProtocolExternal<Transport::Interface::IProxyTransportInitializationParameters>,
	SampleProxyProtocol::SampleProxyProtocolInternal<Transport::Interface::IProxyTransportInitializationParameters>,
	Transport::Interface::IProxyTransportInitializationParameters> udpProxyTransport(proxy_parameters);
	udpProxyTransport.RunServer();
	return 0;
}