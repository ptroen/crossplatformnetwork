#include "../../../Transport/RTP/RTPTransport.h"
#include "../../../Transport/interface/IRTPTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
using namespace OSI;
using namespace SampleProtocol;
int main(int argc, char* argv[])
{
	Transport::Interface::IRTPTransportInitializationParameters init_parameters;
	init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	Transport::RTP::RTPServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IRTPTransportInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IRTPTransportInitializationParameters>, SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::IRTPTransportInitializationParameters>, OSI::Transport::Interface::IRTPTransportInitializationParameters> rtpTransport(init_parameters);
	rtpTransport.RunServer();
	return 0;
}
