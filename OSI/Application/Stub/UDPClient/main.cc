#include "../../../Transport/UDP/UDPClientTransport.h"
#include "../../../Transport/interface/IClientTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"

int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IClientTransportInitializationParameters init_parameters;
	try
	{
		init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
		OSI::Transport::UDP::UDPClientTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>,
			SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IClientTransportInitializationParameters>, OSI::Transport::Interface::IClientTransportInitializationParameters> client(init_parameters);
		SampleProtocol::IncomingPayload< OSI::Transport::Interface::IClientTransportInitializationParameters> request(init_parameters);
		request.msg = init_parameters.initialPayload;
		client.RunClient(init_parameters.ipAddress, request);
	}
	catch(std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}
	return 0;
}