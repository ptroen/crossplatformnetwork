// https://www.boost.org/doc/libs/1_50_0/doc/html/boost_asio/example/multicast/sender.cpp for how to write multicast servers with boost

#include "../../../Transport/UDPMulticast/UDPMulticastTransport.h"
#include "../../../Transport/interface/IMulticastTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
#include "../../../../Tools/Diagnostics/Logger/Logger.h"
int main(int argc, char* argv[])
{
	std::string msg("main.cc of UDPMulticastServer");
	LOGIT(msg)
	OSI::Transport::Interface::IMulticastInitializationParameters init_parameters;
	init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
		OSI::Transport::UDPMulticast::UDPMulticastServerTransport<SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IMulticastInitializationParameters >,
	SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::IMulticastInitializationParameters>, OSI::Transport::Interface::IMulticastInitializationParameters> udpMulticastTransport(init_parameters);
	udpMulticastTransport.RunServer();
  /*
	try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: sender <multicast_address>\n";
      std::cerr << "  For IPv4, try:\n";
      std::cerr << "    sender 239.255.0.1\n";
      std::cerr << "  For IPv6, try:\n";
      std::cerr << "    sender ff31::8000:1234\n";
      return 1;
    }

    
    sender s(io_context, boost::asio::ip::address::from_string(argv[1]));
    io_context.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }
  */
  return 0;
}
