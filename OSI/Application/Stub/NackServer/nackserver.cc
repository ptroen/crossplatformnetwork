#include "../../../Transport/Nack/NackTransport.h"
#include "../../../Transport/interface/INackServerInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"
int main(int argc, char* argv[])
{
	OSI::Transport::Interface::INackServerInitializationParameters init_parameters;
	init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	OSI::Transport::NACK::NackServerTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::INackServerInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::INackServerInitializationParameters>, SampleProtocol::SampleProtocolServerSession<OSI::Transport::Interface::INackServerInitializationParameters>, OSI::Transport::Interface::INackServerInitializationParameters> nackTransport(init_parameters);
	nackTransport.RunServer();  
	return 0;
}
