#include "../../../Transport/HTTP/HTTPClientTransport.h"
#include "../../../Transport/interface/IClientTransportInitializationParameters.h"
#include "../TCPServer/SampleProtocol.h"

int main(int argc, char* argv[])
{
	OSI::Transport::Interface::IClientTransportInitializationParameters init_parameters;
	init_parameters.ParseServerArgs(&(*argv), argc, 80, 80);
	
	OSI::Transport::HTTP::HTTPClientTransport<SampleProtocol::IncomingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>, SampleProtocol::OutgoingPayload<OSI::Transport::Interface::IClientTransportInitializationParameters>, SampleProtocol::SampleProtocolClientSession<OSI::Transport::Interface::IClientTransportInitializationParameters>, OSI::Transport::Interface::IClientTransportInitializationParameters> client(init_parameters);
	SampleProtocol::IncomingPayload< OSI::Transport::Interface::IClientTransportInitializationParameters> request(init_parameters);
	client.RunClient((char*)init_parameters.ipAddress.c_str(), request);
	return 0;
}