#ifndef HTTP_COMPRESSOR_H
#define HTTP_COMPRESSOR_H
#include "../../../algorithm/GZIP.h"
#include "../../Session/HTTP/HTTP_Response_Builder.h"

class HTTPCompressor
{
public:

	void ZipPayload(OSI::Session::HTTP::HTTP_Response_Builder& response)
	{
		response.customResponsePageCode = Algorithm::Gzip::compress(response.customResponsePageCode);
	};
};

#endif