#ifndef MimeTypeDispatcher_H
#define MimeTypeDispatcher_H
#include <string>
#include <map>
#include <vector>
#include "../interface/IMimeTypeDispatcher.h"
class MimeTypeDispatcher;

class MimeTypeDispatcher : public virtual OSI::Application::Interface::IMimeTypeDispatcher
{
	private:	
	std::string _mimeResponse;
	public:	
	virtual void MakeMimeResponse(std::string& out_resp,
				      std::string mimetype,
                                      std::string& in_url,
                                      std::string& in_siteurl,
					std::map<std::string,std::string>& in_keyValueArgs){
		std::vector<std::string> imgarray= {std::string("bmp"),std::string("tga"),std::string("jpg")};
		for (auto img : imgarray)
			if (mimetype == img )
			{
				MakeImageMimeResponse(out_resp,in_url,in_siteurl,in_keyValueArgs);	
			}

		std::vector<std::string> vidarray[]= {std::string("mpg"),std::string("mpeg"),std::string("ogg")};
		for (auto vid : vidarray)
			if (mimetype == vid)
			{
				MakeVideoMimeResponse(out_resp,in_url,in_siteurl,in_keyValueArgs);
			}
		
	};

        private:
        void MakeImageMimeResponse(std::string& out_resp,std::string& in_url,std::string& in_siteurl,std::map<std::string,std::string>& in_keyValueArgs);
        void MakeVideoMimeResponse(std::string& out_resp,std::string& in_url,std::string& in_siteurl, std::map<std::string,std::string>& in_keyValueArgs);
};
#endif
