#ifndef IMimeTypeDispatcher_H
#define IMimeTypeDispatcher_H
#include <string>
#include <map>
#include <vector>
class IMimeTypeDispatcher;

namespace OSI
{
	namespace Application
	{
		namespace Interface
		{
			class IMimeTypeDispatcher
			{
			private:
				std::string _mimeResponse;
			public:
				virtual std::string& MakeMimeResponse(std::string& in_url, std::map<std::string, std::string>& in_keyValueArgs) {};

			private:
				void MakeImageMimeResponse(std::string& out_resp, std::string& in_url, std::string& in_siteurl, std::map<std::string, std::string>& in_keyValueArgs);
				void MakeVideoMimeResponse(std::string& out_resp, std::string& in_url, std::string& in_siteurl, std::map<std::string, std::string>& in_keyValueArgs);
			};
		}
	}
}
#endif
