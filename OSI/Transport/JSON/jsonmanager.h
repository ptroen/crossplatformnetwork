#ifndef JSON_MANAGER_H
#define JSON_MANAGER_H

#include <sstream>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;

// citation api accessed here https://stackoverflow.com/questions/12394472/serializing-and-deserializing-json-with-boost
class JSON_Manager
{
	public:
	std::map<std::string, std::string> _map;
	
	void from_string(std::string& json){
		  ptree pt2;
          std::istringstream is(json);
          read_json (is, pt2);
		  std::map<std::string, std::string> _readingmap=_map;
		    for (auto& entry: _readingmap) 
			{
              std::string value = pt2.get<std::string> (entry.first);
			  _readingmap[entry.first] = entry.second;
			}
	};
	
	std::string to_string(){
  ptree pt; 
  for (auto& entry: _map) 
      pt.put (entry.first, entry.second);
  std::ostringstream buf; 
  write_json (buf, pt, false); 
  return buf.str();
};
};

#endif