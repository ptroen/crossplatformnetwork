#ifndef OSI_APPLICATION_TEST_LOCKSTEPTESTHOOK_H
#define OSI_APPLICATION_TEST_LOCKSTEPTESTHOOK_H
#include "../../../algorithm/include_macros.h"
#define BASE_DIR ../../../

#include <iostream>
#include <sstream>

#include STRB(algorithm/interface/IType.h)

#include STRB(OSI/Transport/HTTP/HTTPRequest.h)
#include STRB(OSI/Transport/HTTP/HTTPResponse.h)
#include STRB(algorithm/interface/IBinaryStringSerialize.h)
#include STRB(algorithm/interface/IJSONSerialize.h)
#include STRB(Session/HTTP/ContentRecordType.h)
#include STRB(algorithm/interface/IType.h)
namespace OSI
{
	namespace Transport
	{
		namespace Test
		{
			template<class TInitializationParameters>
			class OutgoingPayload;
				
			template<class TInitializationParameters>
			class IncomingPayload : public virtual Algorithm::Interface::IType
			{
			public:
				const size_t maxText = 1024;
				std::string msg;
				bool customHTTPUrl;
				OSI::Session::ContentRecordType* _contentType;
				Session::HTTP::HTTP_Request_Builder* _requestBuilder;
				TInitializationParameters& _initialization;
				IncomingPayload(TInitializationParameters& initialization):_initialization(initialization),_requestBuilder(nullptr),_contentType(nullptr),msg(),customHTTPUrl(false) {};
				size_t size(void) { return msg.size(); };
				size_t max_size(void) { return maxText; }
				IncomingPayload& operator=(IncomingPayload& rhs)
				{
					msg = rhs.msg;
					return *this;
				}
				void ToString(char* stringout,size_t dataSize) { 
					memcpy(stringout, msg.c_str(), dataSize);
				
				}
				void FromString(char* dataIn,size_t dataInSize) { 
					msg.resize(dataInSize);
					memcpy(&msg[0], dataIn, dataInSize);
				}
				friend std::ostream& operator<<(std::ostream& rhs,  IncomingPayload& o)
				{
					std::string s;
					size_t size = o.size();
					s.resize(size);
					o.ToString(const_cast<char*>(s.c_str()),size);
					rhs << s.c_str();
					return rhs;
				};
				friend std::istream& operator>>(std::istream& rhs, IncomingPayload& o)
				{
					std::string s;
					size_t size = o.size();
					s.resize(size);
					rhs >> s;
					o.FromString(const_cast<char*>(s.c_str()),size);
					return rhs;
				};

				// method to prep before the hook processes
				void IncomingHTTPRequest(OSI::Session::ContentRecordType& contentType, Session::HTTP::HTTP_Request_Builder&  requestBuilder)
				{
					_contentType = &contentType;
					_requestBuilder = &requestBuilder;
						if (requestBuilder.url == OSI::Session::HTTP::httpStaticResourceDirectory + "/incomingPayload/index.html")
						{
							customHTTPUrl = true;
						}
						else
							customHTTPUrl = false;
				}

				Session::HTTP::HTTP_Request_Builder&  ToHTTPRequest(void)
				{
					
					if(this->_requestBuilder==nullptr)
					{
						_requestBuilder = new Session::HTTP::HTTP_Request_Builder();
						_requestBuilder->url = "index.html";
					}
					
					return *_requestBuilder;
				};
			};
						
	template<class TInitializationParameters>
	class OutgoingPayload : public virtual Algorithm::Interface::IType
	{
	public:
		std::string out;
		OutgoingPayload(TInitializationParameters& initialization):out() {};
		size_t size(void) { return out.size(); };
		size_t max_size(void) { return 1048576; }
		void ToString(char* string, size_t stringSize)
		{
			memcpy(string, out.c_str(), std::min(stringSize, out.size()));
		}
		void FromString(char* data, size_t dataInSize) { out.resize(dataInSize); memcpy(&out[0],data,dataInSize); };
		void FromHTTPResponse(OSI::Transport::HTTP::HTTPResponse< TInitializationParameters> response)
		{
			std::cout << "http client reads from server " << response.buffer.c_str() <<std::endl;
		}; 
		OutgoingPayload& operator=(OutgoingPayload& rhs)
		{
			std::string s;
			OutgoingPayload& rhsRef = const_cast<OutgoingPayload&>(rhs);
			size_t size = rhsRef.size();
			s.resize(size);
			rhsRef.ToString(const_cast<char*>(s.c_str()), size);
			FromString(const_cast<char*>(s.c_str()), size);
			return *this;
		}
		friend std::ostream& operator<<(std::ostream& rhs, const OutgoingPayload& o) { return rhs; };
		friend std::istream& operator>>(std::istream& rhs, const OutgoingPayload& o) { return rhs; };

		virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
			Algorithm::Interface::IPropertyTree pt;
			int i = 2;
			pt.add(std::string("test"), i);
			return pt;
		};

		// call a serializer here
	// method instructs how to write to file by calling the approppriate serializer
		virtual void ToFile(void) {
			std::string testJSON("test.json");
			Algorithm::Interface::IJSONSerialize test(testJSON, ToPropertyTree());
			test.WriteFile();
		};

		virtual void DumpJSONInObject(void)
		{
			std::string testJSON("test.json");
			Algorithm::Interface::IJSONSerialize test(testJSON, ToPropertyTree());
			test.WriteAsAString(out);
		}


		// method which extracts the values from property tree
		virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
			
		};

	};
	
	template<class TInitializationParameters>
	class LockStepServerSession
	{
	public:
		size_t size(void) { return sizeof(LockStepServerSession); };
		size_t max_size(void) { return sizeof(LockStepServerSession); }
		LockStepServerSession(TInitializationParameters& initializationParameters) {};
		LockStepServerSession(){};

	public:
		virtual void OnTimerHook(void)
		{
			static int i = 0;
			i++;
			std::cout << "Lockstep Timer Server Hook"  << i<< std::endl;
		}
		virtual void OnIdleHook(void)
		{
			static int i = 0;
			std::cout << "LockStep Idle Server Hook" << i << std::endl;
			i++;
		}

		virtual bool IncomingHook(IncomingPayload<TInitializationParameters>& in, OutgoingPayload<TInitializationParameters>& out, std::string& ipadress)
		{
			std::cout << "incoming server session hook" << std::endl;
			std::cout << "server receives the msg " << in.msg << std::endl;
			if(in.customHTTPUrl)
			{
				if (in._requestBuilder != nullptr)
				{
					if (in._requestBuilder->url == OSI::Session::HTTP::httpStaticResourceDirectory + "/incomingPayload/index.html")
					{
						out.out = std::string("<HTML><H2> Greetings from the server</H2>");
						
							switch(in._requestBuilder->requestType)
							{
							case OSI::Session::HTTP::HTTPRequestType::GETREQUEST:
								out.out += "<H1> GET REQUEST </H1>";
								break;
							case OSI::Session::HTTP::HTTPRequestType::DELETEREQUEST:
								out.out += "<P> DELETE REQUEST </P>";
								break;
							case OSI::Session::HTTP::HTTPRequestType::POSTREQUEST:
								out.out += "<P> Post Request </P>";
								break;
							case OSI::Session::HTTP::HTTPRequestType::PUTREQUEST:
								out.out += "<P> Put Request </P>";
								break;
							}
							if (in._requestBuilder->keyValueExists("client"))
							{
								out.out += "<P> client reads:";
								out.out += in._requestBuilder->keyValueArgs["client"];
								out.out += "</P>";
							}
						out.out += "</HTML>";
					}
					else if (in._requestBuilder->url == OSI::Session::HTTP::httpStaticResourceDirectory + "/abc.json")
					{
						out.DumpJSONInObject();
					}
				}
			}
			if (in._requestBuilder == nullptr)
			{
				out.out = "greetings from the server!";
				std::cout << "server sends " << out.out << std::endl;
			}
			else
			{
				
			}
			// Session
			return true; // no more data
		}

		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& out)
		{
			return true;
		}
	};

	template<class TInitializationParameters>
	class LockStepClientSession : public virtual LockStepServerSession<TInitializationParameters>
	{
	public:
		LockStepClientSession(TInitializationParameters& parameters) {};
		LockStepClientSession(void){};

		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& in, IncomingPayload<TInitializationParameters>& out, std::string& ipadress)
		{
			// Session
			std::cout << "incoming client session hook" << std::endl;
			std::cout << "client reads from server:" << in.out << std::endl;
			return false;
		};
		virtual bool IncomingHook(OutgoingPayload<TInitializationParameters>& out)
		{
			std::cout << "incoming client session hook" << std::endl;

			std::cout << "client reads from server:" << out.out << std::endl;
			return true;
		}
		virtual void OnTimerHook(void)
		{
			static int i = 0;
			i++;
			std::cout << "Lock Step Client OnTimer Hook " << i << std::endl;
		}
		virtual void OnIdleHook(void)
		{
			static int i = 0;
			std::cout << "Lock Step Client OnIdle Hook" << i << std::endl;
			i++;
		}
	};
		}
	}
}
#undef BASE_DIR
#endif