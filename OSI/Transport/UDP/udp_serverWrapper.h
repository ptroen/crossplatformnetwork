#ifndef UDP_SERVERWRAPPER_H
#define UDP_SERVERWRAPPER_H
#include "../../Session/IPSecurity/IPSecurity.h"
#include "udp_baseWrapper.h"
#include "../../../algorithm/circularbuffer.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
            template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
            class udp_serverWrapper
			{
			public:
				TServerSession& _session;
				boost::asio::ip::udp::socket socket_;
				boost::asio::ip::udp::endpoint remote_endpoint_;
				typedef udp_baseWrapper< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> udpbase;
				typedef Algorithm::CircularBuffer<udpbase, TInitializationParameters> circular;
				circular readWriteBuffer;
				udp_serverWrapper(boost::asio::io_context& io_context, TInitializationParameters& parameters,TServerSession& session): remote_endpoint_(boost::asio::ip::udp::v6(), parameters._incomingPortApplicationLayerAddress),
					socket_(io_context,boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v6(), parameters._incomingPortApplicationLayerAddress)),_session(session),
					readWriteBuffer(parameters.incomingReadWriteBufferSize, parameters)
				{
					std::string msg = "udp_server_wrapper constructor finished";
					LOGIT1(msg)
				};
				void RunServer(void) { start_receive(); }
			private:
				void start_receive()
				{
                    try
                    {
                        udpbase& udpBase = readWriteBuffer.GrabNewElement();
                        socket_.async_receive_from(boost::asio::buffer(const_cast<char*>(udpBase.incomingString.c_str()), udpBase.incoming.max_size()),
                            remote_endpoint_,
                            boost::bind(&udp_serverWrapper::handle_receive,
                                this,
                                &udpBase,
                                boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred));
					}
					catch(std::exception ex)
					{
                        std::string msg = "udp_serverWrapper::start_receive exception:";
                        msg +=ex.what();
                        LOGIT1(msg)
					}
				}

				static void handle_receive(udp_serverWrapper* that, udpbase* udpBase, const boost::system::error_code& error, size_t bytes_transferred)
				{
                    try
                    {
                        std::string ipaddress = that->remote_endpoint_.address().to_string();
                        if (error)
                        {
                            std::string msg = "error";
                            msg += error.message();
                            LOGIT1(msg)
                            try
                            {
                                auto msg2 = error.message();
                                Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, msg2);
                            }
                            catch(std::exception ex)
                            {
                                std::string msg;
                                msg = " IPSecurity Exception:";
                                msg += ex.what();
                                LOGIT1(msg)
                            }
                        }
                        else
                        {
                            udpBase->incoming.FromString(const_cast<char*>(udpBase->incomingString.c_str()),bytes_transferred);

                            bool packetsToSend = that->_session.IncomingHook(udpBase->incoming, udpBase->outgoing, ipaddress);
                            if (packetsToSend)
                            {
                                size_t objectSize = udpBase->outgoing.size();
                                udpBase->outgoing.ToString(const_cast<char*>(udpBase->outgoingString.c_str()), objectSize);
                                that->socket_.async_send_to(boost::asio::buffer(udpBase->outgoingString, objectSize),
                                  that->remote_endpoint_,
                                boost::bind(&udp_serverWrapper::handle_send, that, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                            }
                        }
                        that->readWriteBuffer.Get(); // so circular read/write are kept in sync
                        that->start_receive();
					}
					catch(std::exception ex)
					{
                        std::string msg;
                        msg = "udp_serverWrapper::handle_receive general exception";
                        msg += ex.what();
                        LOGIT1(msg)
					}
				}
				void handle_send(const boost::system::error_code& error, std::size_t bytes_transferred) {};
			};
		}
	}
}

#endif
