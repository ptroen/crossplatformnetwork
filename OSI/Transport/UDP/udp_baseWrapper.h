#ifndef UDP_BASEWRAPPER_H
#define UDP_BASEWRAPPER_H
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TInitializationParameters>
			class udp_baseWrapper
			{
			public:
				TFlyWeightServerIncoming incoming;
				TFlyWeightServerOutgoing outgoing;
				std::string incomingString;
				std::string outgoingString;
				udp_baseWrapper(TInitializationParameters& t) :incoming(t), outgoing(t),incomingString(),outgoingString()
				{
					incomingString.resize(incoming.max_size());
					outgoingString.resize(outgoing.max_size());
				};
			};
		}
	}
}

#endif