#ifndef UDP_CLIENT_WRAPPER_H
#define UDP_CLIENT_WRAPPER_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "udp_baseWrapper.h"
#include "../../../algorithm/circularbuffer.h"
#include <boost/array.hpp>
#include "../IPType.h"
#include <iostream>
#include <stdexcept>
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			// incoming,outgoing is relative to the server not the client
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TInitializationParameters>
				class udp_clientWrapper
				{
					typedef udp_baseWrapper< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> udpbase;
					typedef Algorithm::CircularBuffer<udpbase, TInitializationParameters> circular;
					boost::asio::ip::udp::socket socket_;
					boost::asio::ip::udp::endpoint remote_endpoint_;
					boost::asio::ip::udp::resolver resolver;
					TSession& _session;
					circular readWriteBuffer;
					public:
					// client constructor
					udp_clientWrapper(boost::asio::io_context& io_context, const std::string& ipAddress, TFlyWeightServerIncoming& outgoing, TInitializationParameters& parameters,TSession& session) :   socket_(io_context),
				_session(session),readWriteBuffer(parameters.incomingReadWriteBufferSize, parameters), resolver(io_context)
					{
						// if you pass in a ipv4 address and set udp::v6() sendto later on will fail or vice versa(ipv6 and udp::v4). Thanks boost for making me spend a whole day figuring out what is wrong. And the exceptions
						// are just as bad just says sendto(). So what does that mean? firewall port? some kind of verification NO you jsut set a ipv4 address with a ipv6 flag. Yes logical NOT!
						bool isipv6 = IsIPV6(ipAddress);
							// citation of how to create a udp client in boost https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/tutorial/tutdaytime4/src.html
							std::ostringstream ss;
							// using the resolver to get the remote endpoint which concides with the ipaddress provided
							ss << parameters._incomingPortApplicationLayerAddress;
							boost::asio::ip::udp::resolver::iterator iter = resolver.resolve(ipAddress.c_str(), ss.str().c_str());
							remote_endpoint_ = *iter;

							if (isipv6) // looks like a bug with boost ipv6 client
							{
								socket_.open(boost::asio::ip::udp::v6()); // ipv6 addresses will work as long as ipv6 is specified. If ipv4 address is past in you get a nasty error
							}
							else
							{
								socket_.open(boost::asio::ip::udp::v4());
							}

							size_t size = outgoing.size();
							//udpbase& udpBase = readWriteBuffer.GrabNewElement();
						try
						{
							std::string incomingString;
							// translate the object to a string and put it in udpBase.incomingString
							incomingString.resize(size);
							outgoing.ToString(const_cast<char*>(incomingString.c_str()), size);
							char* ptr = const_cast<char*>(incomingString.c_str());
							socket_.send_to(boost::asio::buffer(incomingString.c_str(), size), remote_endpoint_);
						}
						catch(std::exception ex)
						{
							std::string msg;
							msg = "can't connect to the server . Inner exception reads:";
							msg+=ex.what();
							char* themsg=const_cast<char*>(msg.c_str());
							throw  std::runtime_error(themsg);
						}
						start_receive();
					};
					
					private:
				  void start_receive()
				{
					udpbase& udpBase = readWriteBuffer.GrabNewElement();
					socket_.async_receive_from(boost::asio::buffer(const_cast<char*>(udpBase.outgoingString.c_str()), udpBase.outgoing.max_size()),
						remote_endpoint_,
						boost::bind(&udp_clientWrapper::handle_receive, 
							this, 
							&udpBase,
												boost::asio::placeholders::error, 
												boost::asio::placeholders::bytes_transferred));
				}

				 static void handle_receive(udp_clientWrapper* that,
					  udpbase* udpBase,
					const boost::system::error_code& error, 
					size_t bytes_transferred)
				{
					 if(error)
					 {
						 std::cout << "client error " << error.message() << std::endl;
					 }
					 else
					{
						 std::string ipaddress = that->remote_endpoint_.address().to_string();
						// translate into a outgoing object which is incoming for a client
						udpBase->outgoing.FromString(const_cast<char*>(udpBase->outgoingString.c_str()), bytes_transferred);
						bool status = that->_session.IncomingHook(udpBase->outgoing, udpBase->incoming, ipaddress);
						if (status)
						{
							size_t size = udpBase->incoming.size();
							if (size > 0)
							{
								// translate to a stream
								udpBase->incoming.ToString(const_cast<char*>(udpBase->incomingString.c_str()), size);
								that->socket_.async_send_to(boost::asio::buffer(const_cast<char*>(udpBase->incomingString.c_str()), size),
									that->remote_endpoint_,
									boost::bind(&udp_clientWrapper::handle_send, udpBase->incomingString, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
							}
						}
						that->start_receive();
					}
				}
				static void handle_send(std::string& outgoing,const boost::system::error_code& error, std::size_t bytes_transferred) {};
				};
		}
	}
}

#endif
