#ifndef UDP_CLIENT_TRANSPORT_H
#define UDP_CLIENT_TRANSPORT_H
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <chrono>
#include "../interface/IClientTransport.h"
#include "../interface/ITransport.h"
#include "udp_clientWrapper.h"
#include "UDPDatagram.h"
#include "../../Session/TimerHooks/TimerHooks.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class UDPClientTransport : public virtual Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>
			{
			public:
				Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters> session;
				UDPClientTransport(TTransportInitializationParameters& parameters) :session(parameters),
				Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>(parameters){};
				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
				{

                        udp_clientWrapper<TFlyWeightServerIncoming,TFlyWeightServerOutgoing,
												Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters> client(Session::IOContextManager::io_context, ipAddress, request,Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters, session);
						Session::IOContextManager::io_context.run();

				};
			}; // end class
		}
	}
}
#endif
