#ifndef UDP_TRANSPORT_H
#define UDP_TRANSPORT_H
#include "udp_serverWrapper.h"
#include "../../Session/TimerHooks/TimerHooks.h"
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class UDPServerTransport :  virtual public Interface::IServerTransport<TFlyWeightServerIncoming,TFlyWeightServerOutgoing, TInitializationParameters >
			{
			public:
				Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters> _session;
				UDPServerTransport(TInitializationParameters& parameters) : Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters >(parameters),_session(parameters){};
				// will invoke a server and wait for it
				virtual void RunServer(void)
				{
                    while(1)
                    {
                        try
                        {
                            udp_serverWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, TInitializationParameters> server(Session::IOContextManager::io_context, Interface::IServerTransport<TFlyWeightServerIncoming,TFlyWeightServerOutgoing, TInitializationParameters >::_parameters,_session);
                            server.RunServer();
                            Session::IOContextManager::io_context.run();
                        }
                        catch(std::exception ex)
                        {
                            std::string msg = "UDPServerTransport Server Crash";
                            LOGIT1(msg)
                        }
                    }
				};
			};
		} // namespace UDP
	} // namespace Transport
} // namespace OSI
#endif
