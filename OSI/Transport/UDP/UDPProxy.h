#ifndef UDP_PROXY_H
#define UDP_PROXY_H
#include "udptransport.h"
#include "UDPDatagram.h"
#include "../interface/iproxytransport.h"
#include <string>
#include "../../../algorithm/Math/Statistics/BellCurve.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TInitializationParameters>
			class ExternalProxySession : Interface::IProxyTransport
			{
				static inline ExternalProxySession* instance = nullptr;
				TExternalSession _session;
				std::vector<std::string> outgoingQueue;
				TInitializationParameters& _parameters;
				Algorithm::Math::Statistics::BellCurve noiseBellCurveGenerator;
			public:
				static ExternalProxySession* GetInstance(void) { return instance; };
				ExternalProxySession(TInitializationParameters& parameters):_session(parameters), _parameters(parameters), noiseBellCurveGenerator(parameters.noiseMin, parameters.noiseMax, parameters.noiseDeviation) { };
				// data
				void SendToExternal(std::string& payload) { outgoingQueue.push_back(payload); };

				virtual bool IncomingHook(TFlyWeightServerIncoming& in, TFlyWeightServerOutgoing& outgoing, std::string& ipadress)
				{
					if (instance == nullptr) { instance = this; }
					// Session
					// all incoming is assumed external

					if (ipadress == _parameters.internalIP)
					{
						if (!_parameters.enableNoise || noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							std::string data = UDPDatagram< TFlyWeightServerIncoming>::SendPacketAndReceive(in, _parameters.externalIP, _parameters._incomingPortApplicationLayerAddress);
							outgoing.FromString(&data[0], data.size());
							return true;
						}
					}
					else if (ipadress == _parameters.externalIP)
					{
						if (!_parameters.enableNoise || noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							std::string data = UDPDatagram< TFlyWeightServerIncoming>::SendPacketAndReceive(in, _parameters.internalIP, _parameters._incomingPortApplicationLayerAddress);
							outgoing.FromString(&data[0], data.size());
							return true;
						}
					}
					return false;
				}
				virtual void OnTimerHook(void)
				{
					_session.OnTimerHook();
				}
				virtual void OnIdleHook(void)
				{
					_session.OnIdleHook();
				}
			};

			template<class TFlyWeightServerIncoming,class TFlyWeightServerOutgoing,class TExternalSession,class TInternalSession, class TTransportInitializationParameters >
			class UDPProxyTransport
			{
				public:
					UDPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, ExternalProxySession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TExternalSession, TInternalSession, TTransportInitializationParameters>, TTransportInitializationParameters> externalServer;

				UDPProxyTransport(TTransportInitializationParameters& parameters):externalServer(parameters){};

				virtual void RunServer()
				{
					externalServer.RunServer();
				};
			};
		}
	}
}
#endif
