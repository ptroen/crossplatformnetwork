#ifndef UDP_DATAGRAM_H
#define UDP_DATAGRAM_H
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <sstream>
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDP
		{
			template <class TDatagram>
			struct UDPDatagram
			{
				// use case when you don't want to respond just send the data.
				static void SendPacketAndClose(TDatagram& datagram,std::string& ipAddress,size_t port){
					boost::system::error_code  ec;
					std::ostringstream ss;
					ss << port;
					boost::asio::ip::udp::resolver resolver(Session::IOContextManager::io_context);

					 // only support ipv6 addresses with the client
					auto ipv6 = boost::asio::ip::make_address_v6(ipAddress.c_str());
					boost::asio::ip::udp::endpoint receiver_endpoint = *resolver.resolve(ipv6.to_string(), ss.str()).begin();
					boost::asio::ip::udp::socket socket(Session::IOContextManager::io_context);
					socket.open(boost::asio::ip::udp::v6()); // ipv6 addresses will work as long as ipv6 is specified. If ipv4 address is past in you get a nasty error
					std::string data;
					size_t size = datagram.size();
					size_t sizeToSend = size;
					data.resize(size);
					datagram.ToString(const_cast<char*>(data.c_str()), size);
					auto buf = boost::asio::buffer(data.c_str(), sizeToSend);
					socket.send_to(buf, receiver_endpoint);
					//socket.close();
				};
			static std::string SendPacketAndReceive(TDatagram& datagram, std::string& ipAddress, size_t port) {
				boost::system::error_code  ec;
				std::ostringstream ss;
				ss << port;
				boost::asio::ip::udp::resolver resolver(Session::IOContextManager::io_context);

				// only support ipv6 addresses with the client
				auto ipv6 = boost::asio::ip::make_address_v6(ipAddress.c_str());
				boost::asio::ip::udp::endpoint receiver_endpoint = *resolver.resolve(ipv6.to_string(), ss.str()).begin();
				boost::asio::ip::udp::socket socket(Session::IOContextManager::io_context);
				socket.open(boost::asio::ip::udp::v6()); // ipv6 addresses will work as long as ipv6 is specified. If ipv4 address is past in you get a nasty error
				std::string data;
				size_t size = datagram.size();
				size_t sizeToSend = size;
				data.resize(size);
				datagram.ToString(const_cast<char*>(data.c_str()), size);
				auto buf = boost::asio::buffer(data.c_str(), sizeToSend);
				socket.send_to(buf, receiver_endpoint);

				data.resize(datagram.max_size());
				size_t len = socket.receive_from(boost::asio::buffer(&data[0], data.size()),receiver_endpoint);
				data.resize(len);
				return data;
				//socket.close();
			};
		};
		}
	}
}
#endif