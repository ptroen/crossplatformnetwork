#ifndef I_RELIABILITY_H
#define I_RELIABILITY_H
#include <cstdint>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IReliabity
			{
			public:
				size_t numberOfPacketsToKeepForRecovery; // this is used during retransmitting to determine how much to store to retransmit
				IReliabity(void) :numberOfPacketsToKeepForRecovery(100) {};
			};
		}
	}
}
#endif