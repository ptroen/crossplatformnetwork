#ifndef SERVER_READ_WRITE_MACRO_H
#define SERVER_READ_WRITE_MACRO_H
#include "../../Session/IPSecurity/IPSecurity.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			/*
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, size_t(*ServerSessionHook)(TFlyWeightServerIncoming&, TFlyWeightServerOutgoing&, TServerSession&, std::string&), class TSocket,class TInitializationParameters>
			class IReadWriteSocket
			{
			protected:
				TSocket _socketRef;
				char* incomingBuffer;
				char* outgoingBuffer;
				size_t incomingBufferSize;
				size_t outgoingBufferSize;
				TFlyWeightServerIncoming incoming;
				TFlyWeightServerOutgoing outgoing;
				TInitializationParameters& _parameters;
				bool _closeOnZeroOutput; // closes connection if ServerSessionHook reported 0
			protected:
				void Init(void)
				{
					session = new TServerSession(_parameters);
					incomingBufferSize = incoming.max_size();
					outgoingBufferSize = outgoing.max_size();
					incomingBuffer = new char[incomingBufferSize];
					outgoingBuffer = new char[outgoingBufferSize];
				};
			public: 
				static TServerSession* session;
				IReadWriteSocket(TSocket& socket,bool closeOnZeroOutput, TInitializationParameters& parameters) :_socketRef(std::move(socket)),incoming(parameters),outgoing(parameters), _closeOnZeroOutput(closeOnZeroOutput),
				_parameters(parameters)
				{
					Init();
				};

				void start(void)
				{
					_socketRef.async_read_some(boost::asio::buffer((void*)(&incomingBuffer[0]), incomingBufferSize), // end buffer  
						boost::bind(&inputHandler,
							this,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred) // end bind
					);  // end read_some 
				}
			protected:
				void inputHandler(const boost::system::error_code& error,
					std::size_t bytes_transferred)
				{
					std::string ipaddress = _socketRef.lowest_layer().remote_endpoint().address().to_string();
					if (error)
					{
						Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, error.message());
					}
					else
					{
						incoming.FromString(&incomingBuffer[0], bytes_transferred);
						size_t outgoingLength = (*ServerSessionHook)(incoming, outgoing, *session, ipaddress);
						if (outgoingLength)
						{
							size_t length = outgoingLength; // ToString can change the length so we cache it
							outgoing.ToString(&outgoingBuffer[0], length);
							boost::asio::async_write(_socketRef,
								boost::asio::buffer(&outgoingBuffer[0], outgoingLength),
								boost::bind(&IReadWriteSocket::outputHandler,
									this,
									boost::asio::placeholders::error,
									boost::asio::placeholders::bytes_transferred)
							);
						}
					}
				};
				void outputHandler(const boost::system::error_code& error,std::size_t bytes_transferred)
				{
					if (_closeOnZeroOutput)
						_socketRef.lowest_layer().close();
				};
			};
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, size_t(*ServerSessionHook)(TFlyWeightServerIncoming&, TFlyWeightServerOutgoing&, TServerSession&, std::string&), class TSocket,class TInitializationParameters> TServerSession* IReadWriteSocket<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, ServerSessionHook,  TSocket, TInitializationParameters>::session;
		}
		*/
	}
}
#endif