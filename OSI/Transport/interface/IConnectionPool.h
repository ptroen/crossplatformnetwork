#ifndef I_CONNECTION_POOL_H
#define I_CONNECTION_POOL_H
#include "../../Transport/UDP/udp_baseWrapper.h"
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TInitializationParameters>
			class LinkListNode
			{
			public:
				LinkListNode* next;
				LinkListNode* prev;
				UDP::udp_baseWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> Type;
			};

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TInitializationParameters>
			class IConnectionPool
			{
				typedef LinkListNode<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> node;
				std::vector<node> pooled;
				node* root;
			public:

				IConnectionPool(TInitializationParameters& init) :pooled() { pooled.resize(init._maxClients *2); };
				void Init(TInitializationParameters& init) {
					for (size_t i = 0; i < pooled.size(); i++)
					{
						pooled[i].Type.Init(init);
						if(i==pooled.size()-1)
						{
							pooled[i].next = &pooled[0];
						}
						else
						{
							pooled[i].next = &pooled[i + 1];
						}
						if (i == 0)
						{
							pooled[i].prev = &pooled[i+1];
						}
						else
						{
							pooled[i].prev = &pooled[i - 1];
						}
					}
					root = &pooled[0];
				};
				node* GetNextAvailable(void)
				{
					if(root != nullptr)
					{
						node* temp = root->prev;
						if (root->prev == root)
							root = nullptr;
						else
						{
							// remove the node from the book keeping
							node* ptr = root->prev->prev;
							ptr->next = root;
							root->prev = ptr;
						}
						return temp;
					}
					return nullptr;
				};

				void ReturnConnectionObjectToPool(node* node)
				{
					// we maintain connections through a double link list
					if(root==nullptr)
					{
						root = &node;
						node->next = root;
						node->prev = root;
					}
					else if(root->next== root)
					{
						root->next = &node;
						root->prev = &node;
						node->next = root;
						node->prev = root;
					}
					else
					{
						auto temp = root->prev;
						temp->next = node;
						node->prev = temp;
						node->next = root;
						root->prev = node;
					}
				};
				
			};
		}
	}
}
#endif
