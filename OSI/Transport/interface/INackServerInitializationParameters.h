#ifndef NACK_SERVER_INITIALIZATION_PARAMETERS_H
#define NACK_SERVER_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class INackServerInitializationParameters : public virtual ITransportInitializationParameters
			{
				public:
					INackServerInitializationParameters(void) :numberOfQueuedNackEntries(1000) {};
				size_t numberOfQueuedNackEntries;
				protected:
					virtual Algorithm::Interface::IPropertyTree INackServerInitializationParametersToPropertyTree(void) {
						Algorithm::Interface::IPropertyTree pt;
						PADD(numberOfQueuedNackEntries)
						return pt;
					};

					// method which extracts the values from property tree
					virtual void INackServerInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
						auto str = std::string("numberOfQueuedNackEntries");
						numberOfQueuedNackEntries = pt.get<size_t>(str);
					};
				public:
					virtual std::string TypeName(void) { return "INackServerInitializationParameters"; };
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt = INackServerInitializationParametersToPropertyTree();
					
					auto pt2 = ITransportInitializationParameters::ToPropertyTree();
					auto str = ITransportInitializationParameters::TypeName();
					pt.addPropertyTree(str, pt2);
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					auto str = std::string("numberOfQueuedNackEntries");
					numberOfQueuedNackEntries = pt.get<size_t>(str);
					auto thetypename = ITransportInitializationParameters::TypeName();
					auto pt2 = pt.getChild(thetypename);
					ITransportInitializationParameters::FromPropertyTree(pt2);
				};
			};
		}
	}
}

#endif
