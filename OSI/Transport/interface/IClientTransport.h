#ifndef I_CLIENT_TRANSPORT_H
#define I_CLIENT_TRANSPORT_H
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			// transport info to the client
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TTransportInitializationParameters>
			class IClientTransport
			{
			protected:
				TTransportInitializationParameters _parameters;
			public:
				IClientTransport(void) :_parameters() {};
				IClientTransport(TTransportInitializationParameters& parameters):_parameters(parameters) {};
				// implement this if you need todo client communication
				virtual void RunClient(const std::string& ipAddress, const TFlyWeightServerIncoming& request){};
			};
		}
	}
}
#endif