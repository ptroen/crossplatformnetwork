#ifndef I_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_TRANSPORT_INITIALIZATION_PARAMETERS_H

#include "../../../algorithm/interface/IFileSerializer.h"
#include "../../../algorithm/interface/IJSONSerialize.h"
#include "../../../algorithm/interface/IType.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
#include <cstdint>
#include <cstdlib>
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class ITransportInitializationParameters;
			class ITransportInitializationParameters : public virtual Algorithm::Interface::IFileSerializer
			{
			public:
				bool ipv6;
				uint16_t _incomingPortApplicationLayerAddress;
				uint16_t _outgoingPortApplicationLayerAddress;
				std::string _listenaddress; // address your listening on. This is a multicast feature. May need to move this around
				bool closeConnectionOnZeroOutput;
				size_t _queue_length;
				size_t _logLength;
				int32_t _timerTimeout;
				int32_t _idleTimeout;
				size_t incomingReadWriteBufferSize;
				size_t _maxClients; // used to determine max number of connections allowed on the server
				ITransportInitializationParameters()
				{
					_incomingPortApplicationLayerAddress = 80;
					_queue_length = 128;
					_logLength = 10000;
					_timerTimeout = 10;
					_idleTimeout = 10;
					_maxClients = 100000;
					incomingReadWriteBufferSize = 100;
					ipv6 = true;
					closeConnectionOnZeroOutput = true;
					_listenaddress = "";
				};
				
				virtual std::string TypeName(void) { return "ITransportInitializationParameters"; };
				virtual void FromFile(void)
				{
					auto msg = ITransportInitializationParameters::TypeName() + "::FromFile()\n";
					LOGIT(msg)
					std::string testJSON(filename);
					auto pt = ToPropertyTree();
					Algorithm::Interface::IJSONSerialize test(testJSON, pt);
					test.ReadFile();
					this->FromPropertyTree(test.GetPropertyTree());
				};
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
auto msg = ITransportInitializationParameters::TypeName() + "::ToPropertyTree()\n";
					Algorithm::Interface::IPropertyTree pt = ITransportInitializationParametersToPropertyTree();
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					ITransportInitializationParametersFromPropertyTree(pt);
				};

				void ParseServerArgs(char** argv, int argc, short defaultIncomingPort,short defaultOutgoingPort)
				{
					std::string msg("ParseServerArgs\n");
					LOGIT(msg)
					_incomingPortApplicationLayerAddress = defaultIncomingPort;
					_outgoingPortApplicationLayerAddress = defaultOutgoingPort;
					filename = "config.json";
					switch (argc)
					{
						case 2: filename = std::string(argv[0]);
						break;
					}
					if(exist(filename))
					{
						msg = "Calling FromFile\n";
						LOGIT(msg)
						FromFile();
					}
					else
					{
						msg = "Calling ToFile\n";
						LOGIT(msg)
						ToFile(); // write it back so next time you can feed in the json
					}
				};

				ITransportInitializationParameters& operator=(ITransportInitializationParameters& rhs)
				{
					_incomingPortApplicationLayerAddress = rhs._incomingPortApplicationLayerAddress;
					_queue_length = rhs._queue_length;
					_logLength = rhs._logLength;
					ipv6 = rhs.ipv6;
					filename = rhs.filename;
					_outgoingPortApplicationLayerAddress = rhs._outgoingPortApplicationLayerAddress;
					_listenaddress = rhs._listenaddress;
					closeConnectionOnZeroOutput = rhs.closeConnectionOnZeroOutput;
					_timerTimeout = rhs._timerTimeout;
					_idleTimeout = rhs._idleTimeout;
					incomingReadWriteBufferSize = rhs.incomingReadWriteBufferSize;;
					_maxClients = rhs._maxClients;
					return *this;
				};

			protected:
				virtual Algorithm::Interface::IPropertyTree  ITransportInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADDS(_listenaddress)
					pt.add(std::string("outgoingPortApplicationLayerAddress"), std::to_string(_outgoingPortApplicationLayerAddress));
					pt.add(std::string("incomingPortApplicationLayerAddress"), std::to_string(_incomingPortApplicationLayerAddress));
					pt.add(std::string("queue_length"), std::to_string(_queue_length));
					pt.add(std::string("logLength"), std::to_string(_logLength));
					pt.add(std::string("timerTimeout"), std::to_string(_timerTimeout));
					pt.add(std::string("idleTimeout"), std::to_string(_idleTimeout));
					pt.add(std::string("MaxClients"), std::to_string(_maxClients));
					PADD(ipv6)
					PADD(closeConnectionOnZeroOutput)
					PADD(incomingReadWriteBufferSize)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void ITransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
				std::string msg = ITransportInitializationParameters::TypeName() + "::ITransportInitializationParametersFromPropertyTree() \n";
					LOGIT(msg)
					msg="_listenaddress\n";
					LOGIT(msg);
					PGET(_listenaddress, std::string)

					msg = "outgoingPortApplicationLayerAddress\n";
					LOGIT(msg)
					_PGET(outgoingPortApplicationLayerAddress,uint16_t)
					
					msg = "incomingPortApplicationLayerAddress\n";
					LOGIT(msg)
					_PGET(incomingPortApplicationLayerAddress,uint16_t)					
					
					msg = "queue_length\n";
					LOGIT(msg)
					_PGET(queue_length,size_t)					
					
					msg = "logLength\n";
					LOGIT(msg)
					_PGET(logLength,size_t)
										
					msg = "timerTimeout\n";
					LOGIT(msg)
					_PGET(timerTimeout,int32_t)

					msg = "idleTimeout\n";	
					LOGIT(msg)			
					_PGET(idleTimeout,int32_t)
					{
					std::string s=	std::string("MaxClients");
					msg = s+"\n";
					LOGIT(msg)				
					_maxClients = pt.get<size_t>(s);
					}
					msg = "ipv6";
					LOGIT(msg)
					PGET(ipv6,bool)
					msg = "closeConnectionOnZeroOutput\n";
					LOGIT(msg)
					PGET(closeConnectionOnZeroOutput,bool)

					msg = "incomingReadWriteBufferSize\n";
					LOGIT(msg)
					PGET(incomingReadWriteBufferSize,size_t)
				};
			
			};
		}
	}
}

#endif
