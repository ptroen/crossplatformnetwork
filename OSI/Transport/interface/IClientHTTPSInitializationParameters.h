#ifndef I_CLIENT_HTTPS_INITIALIZATION_PARAMETERS_H
#define I_CLIENT_HTTPS_INITIALIZATION_PARAMETERS_H
#include "ISSLClientTransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IClientHTTPSInitializationParameters : public virtual ISSLClientTransportInitializationParameters
			{
			public:
				IClientHTTPSInitializationParameters& operator=(IClientHTTPSInitializationParameters& rhs)
				{
					ISSLClientTransportInitializationParameters& sslThis=*this;
					ISSLClientTransportInitializationParameters& sslrhs = rhs;
					sslThis = sslrhs;
					return *this;
				};
			};
		}
	}
}
#endif
