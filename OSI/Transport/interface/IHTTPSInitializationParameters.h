#ifndef I_HTTPS_INITIALIZATION_PARAMETERS_H
#define I_HTTPS_INITIALIZATION_PARAMETERS_H
#include "IHTTPInitializationParameters.h"
#include "ISSLTransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IHTTPSInitializationParameters : public virtual ISSLTransportInitializationParameters, public virtual IHTTPInitializationParameters
			{
			public:
				virtual std::string TypeName(void) { return "IHTTPSInitializationParameters"; };
				const IHTTPSInitializationParameters& operator=(IHTTPSInitializationParameters& rhs)
				{
					(ISSLTransportInitializationParameters&)(*this) = (ISSLTransportInitializationParameters&)rhs;
					return *this;
				};
			};
		}
	}
}
#endif