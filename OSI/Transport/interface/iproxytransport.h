#ifndef I_PROXY_TRANSPORT_H
#define I_PROXY_TRANSPORT_H
#include <boost/bimap.hpp> // citation
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{

			template <class TFlyWeight>
			class IProxyInternalAddress
			{
			    public:
					size_t internalAddress;
					TFlyWeight data;
			};

			class IProxyTransport 
			{
			    protected:
					size_t sessionNumber;
				// we allocate a range of ip's and assign a session number
				boost::bimap<std::string, size_t> routeMappingTable; // external IP address, session number
				public:
				IProxyTransport(void) : routeMappingTable(),sessionNumber(0) {};
				virtual std::string& RouteExternalToInternal(std::string& externaladdress) { return externaladdress; };
			};
		}
	}
}
#endif