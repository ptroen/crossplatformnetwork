#ifndef I_SSL_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_SSL_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H

#include "IClientTransportInitializationParameters.h"
#include "ISSLTransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class ISSLClientTransportInitializationParameters : public virtual IClientTransportInitializationParameters
			{
			public:
				ISSLClientTransportInitializationParameters(void) :IClientTransportInitializationParameters(), _sshEncryptionTypes(SSH_ENCRYPTION_TYPES::TLS){}
				std::string clientCertificateFileName;
				SSH_ENCRYPTION_TYPES _sshEncryptionTypes;
				SSH_ENCRYPTION_TYPES GetClientSSL(void) { return _sshEncryptionTypes; };
				virtual std::string TypeName(void) { return "ISSLClientTransportInitializationParameters"; };
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					auto pt = ISSLClientTransportInitializationParametersToPropertyTree();
					auto thetypename = IClientTransportInitializationParameters::TypeName();
					auto clientpt = IClientTransportInitializationParametersToPropertyTree();
					pt.addPropertyTree(thetypename, clientpt);
					auto transportpt = ITransportInitializationParameters::ToPropertyTree();
					auto transporttypename = ITransportInitializationParameters::TypeName(); 
					pt.addPropertyTree(transporttypename, transportpt);
					return pt;
				};

				ISSLClientTransportInitializationParameters& operator=(ISSLClientTransportInitializationParameters& rhs)
				{
					clientCertificateFileName = rhs.clientCertificateFileName;
					_sshEncryptionTypes = rhs._sshEncryptionTypes;
					(IClientTransportInitializationParameters&)*this = (IClientTransportInitializationParameters&)rhs;
					return *this;
				};
				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					ISSLClientTransportInitializationParametersFromPropertyTree(pt);
					auto thetypename = IClientTransportInitializationParameters::TypeName();
					auto pt1 = pt.getChild(thetypename);
					IClientTransportInitializationParameters::IClientTransportInitializationParametersFromPropertyTree(pt1);
					auto transporttypename = ITransportInitializationParameters::TypeName();
					auto pt2 = pt.getChild(transporttypename);
					ITransportInitializationParameters::FromPropertyTree(pt2);
				};
			protected:
				Algorithm::Interface::IPropertyTree ISSLClientTransportInitializationParametersToPropertyTree(void){
					Algorithm::Interface::IPropertyTree   pt;
					PADDS(clientCertificateFileName)
						std::string s = _sshEncryptionTypes._to_string();
					std::string label = "_sshEncryptionTypes";
					pt.add(label, s);
					return pt;
				};
				void ISSLClientTransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					PGET(clientCertificateFileName,std::string)
						std::string _sshEncryptionTypes;
						PGET(_sshEncryptionTypes, std::string)
						this->_sshEncryptionTypes._from_string(_sshEncryptionTypes.c_str());
				}
			};
		}
	}
}

#endif
