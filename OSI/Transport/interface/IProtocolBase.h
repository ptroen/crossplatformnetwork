#ifndef I_PROTOCOL_BASE_H
#define I_PROTOCOL_BASE_H
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			template <class TSession>
			class IProtocolBase
			{
			public:
				bool markForDestruction; // used this by the protocol state to signal deallocation
				// entry point for a protocol base when timing events are fired
				virtual void TimingHook(TSession& session)
				{
					
				};
			};
		}
	}
}

#endif