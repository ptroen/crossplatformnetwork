#ifndef I_MULTICAST_INITIALIZATION_PARAMETERS_H
#define I_MULTICAST_INITIALIZATION_PARAMETERS_H

#include "ITransportInitializationParameters.h"
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IMulticastInitializationParameters : public virtual ITransportInitializationParameters
			{
				public:
				std::string _multicastaddress;
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt = IMulticastInitializationParametersToPropertyTree();
					PADDBASE(ITransportInitializationParameters)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					IMulticastInitializationParametersFromPropertyTree(pt);
					PGET(_multicastaddress, std::string)
					PGETBASE(ITransportInitializationParameters)
				}

			protected:
				virtual Algorithm::Interface::IPropertyTree  IMulticastInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADDS(_multicastaddress)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void IMulticastInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					PGET(_multicastaddress, std::string)
				}
			};
		}
	}
}
#endif
