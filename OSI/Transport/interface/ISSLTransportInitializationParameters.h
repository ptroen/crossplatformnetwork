#ifndef I_SSL_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_SSL_TRANSPORT_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
#include <string>
#include "../../../ThirdParty/enum.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			BETTER_ENUM(SSH_ENCRYPTION_TYPES, uint16_t, TLS=0,TLS11= 1,TLS12=2,TLS13=3)
			// see this guide: https://nextbigthings.info/secured-tls-connection-using-boost-asio-and-openssl-for-windows/ on how to configure certiifcates
			struct ISSLTransportInitializationParameters : public virtual ITransportInitializationParameters
			{
				public:		ISSLTransportInitializationParameters(void) :ITransportInitializationParameters(),_password(),_certificateChainFilename(),_privateKeyFilename(),_tmpDHFile(),_sshEncryptionTypes(SSH_ENCRYPTION_TYPES::TLS) {};
				std::string _password; // usuage .set_password_callback(boost::bind(&HTTPSTransport::get_password
				std::string _certificateChainFilename; // usuage _context.use_certificate_chain_file
				std::string _privateKeyFilename; // usuage _context.use_private_key_file("TCPServerWrapper.pem", boost::asio::ssl::context::pem);
				std::string _tmpDHFile; // usuage _context.use_tmp_dh_file("dh512.pem");
				SSH_ENCRYPTION_TYPES _sshEncryptionTypes;
				SSH_ENCRYPTION_TYPES& GetServerSSHEncryptionTypes(void)
				{
					return ISSLTransportInitializationParameters::_sshEncryptionTypes;
				};
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt = ISSLTransportInitializationParametersToPropertyTree();
					auto pTree = ITransportInitializationParameters::ToPropertyTree();
					auto thetypename = ITransportInitializationParameters::TypeName();
					pt.addPropertyTree(thetypename, pTree);
					return pt;
				};
				ISSLTransportInitializationParameters& operator= (ISSLTransportInitializationParameters& rhs)
				{
					_password = rhs._password;
					_certificateChainFilename = rhs._certificateChainFilename;
					_privateKeyFilename = rhs._privateKeyFilename;
					_tmpDHFile = rhs._tmpDHFile;
					_sshEncryptionTypes = rhs._sshEncryptionTypes;
					(ITransportInitializationParameters&)*this = (ITransportInitializationParameters&)rhs;
					return *this;
				};
				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					ISSLTransportInitializationParametersFromPropertyTree(pt);
					auto thetypename = ITransportInitializationParameters::TypeName();
					auto pt2 = pt.getChild(thetypename);
					ITransportInitializationParameters::FromPropertyTree(pt2);
				};
			protected:
				virtual Algorithm::Interface::IPropertyTree  ISSLTransportInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADDS(_password)
						PADDS(_certificateChainFilename)
						PADDS(_privateKeyFilename)
						PADDS(_tmpDHFile)
						std::string s = _sshEncryptionTypes._to_string();
					std::string label = "_sshEncryptionTypes";
						pt.add(label, s);
					return pt;
				};
				
				// method which extracts the values from property tree
				virtual void ISSLTransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					std::string msg;
					PGET(_password, std::string)
					msg="password="+_password;
					LOGIT(msg)
					PGET(_certificateChainFilename, std::string)
					msg="_certificateChainFilename="+_certificateChainFilename;
					LOGIT(msg)
					PGET(_privateKeyFilename, std::string)
					VAR_LINE(_privateKeyFilename);
					LOGIT(msg)
					PGET(_tmpDHFile, std::string)
					VAR_LINE(_tmpDHFile)
					LOGIT(msg)
					std::string _sshEncryptionTypes;
					PGET(_sshEncryptionTypes,std::string)
					VAR_LINE(_sshEncryptionTypes)
					LOGIT(msg)
					auto ss = _sshEncryptionTypes.c_str();
					this->_sshEncryptionTypes= SSH_ENCRYPTION_TYPES::_from_string(ss);
				}
			};
		}
	}
}

#endif
