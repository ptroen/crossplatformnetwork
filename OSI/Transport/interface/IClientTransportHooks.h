#ifndef I_CLIENT_HOOKS_H
#define I_CLIENT_HOOKS_H
#include "ITimingHooks.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			// triggers on Client Incoming. non zero incidates send information out	
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing>
			class IClientHooks : public virtual ITimingHooks
			{
				virtual bool IncomingHook(TFlyWeightServerOutgoing& out, TFlyWeightServerIncoming& in,std::string& ipaddress) { return false; };
			};
		}
	}
}
#endif