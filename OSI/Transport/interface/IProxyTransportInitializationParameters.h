#ifndef I_PROXY_TRANSPORT_INIT_PARAMETERS_H
#define I_PROXY_TRANSPORT_INIT_PARAMETERS_H
#include "ITransportInitializationParameters.h"
#include "IClientTransportInitializationParameters.h"
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{

			class IProxyTransportInitializationParameters : public virtual Interface::ITransportInitializationParameters
			{
				public:
				std::string externalIP;
				std::string internalIP;
				double enableNoise;
				double noiseMin;
				double noiseMax;
				double noiseDeviation;
				size_t delayInMilliseconds;
			protected:
				virtual Algorithm::Interface::IPropertyTree  IProxyTransportInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADDS(externalIP)
					PADDS(internalIP)
					PADD(enableNoise)
						PADD(noiseMin)
						PADD(noiseMax)
						PADD(noiseDeviation)
						PADD(delayInMilliseconds)
						return pt;
				};

				// method which extracts the values from property tree
				virtual void IProxyTransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					PGET(externalIP, std::string)
					PGET(internalIP,std::string)
					PGET(enableNoise,bool)
						PGET(noiseMin, double)
						PGET(noiseMax, double)
						PGET(noiseDeviation, double)
						PGET(delayInMilliseconds, size_t)
				};

				public:
					IProxyTransportInitializationParameters(void):noiseMin(1.0),noiseMax(1.0),enableNoise(false), noiseDeviation(1.0), delayInMilliseconds(1) {};

					IProxyTransportInitializationParameters& operator=(IProxyTransportInitializationParameters& rhs)
					{
						externalIP = rhs.externalIP;
						noiseMin = rhs.noiseMin;
						internalIP = rhs.internalIP;
						noiseMax = rhs.noiseMax;
						noiseDeviation = rhs.noiseDeviation;
						delayInMilliseconds = rhs.delayInMilliseconds;
						return *this;
					};

					virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
						Algorithm::Interface::IPropertyTree pt = IProxyTransportInitializationParametersToPropertyTree();
						PADDBASE(ITransportInitializationParameters)
						return pt;
					};

					// method which extracts the values from property tree
					virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
						IProxyTransportInitializationParametersFromPropertyTree(pt);
						PGETBASE(ITransportInitializationParameters)
					};

			};
		}
	};
}

#endif
