#ifndef I_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IClientTransportInitializationParameters;
			class IClientTransportInitializationParameters : public virtual ITransportInitializationParameters
			{
				public:
				std::string ipAddress;
				std::string initialPayload;
				protected:
					 Algorithm::Interface::IPropertyTree  IClientTransportInitializationParametersToPropertyTree(void) {
						auto msg = IClientTransportInitializationParameters::TypeName();
						msg += "::IClientTransportInitializationParametersToPropertyTree\n";
						LOGIT(msg)					
						Algorithm::Interface::IPropertyTree pt;
						 PADDS(ipAddress)
						 PADDS(initialPayload)
						return pt;
					};

					 virtual void IClientTransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
						auto msg = IClientTransportInitializationParameters::TypeName();
						msg += "::IClientTransportInitializationParametersFromPropertyTree\n";
						LOGIT(msg)
						 PGET(ipAddress, std::string)
						 PGET(initialPayload, std::string)
					 };

			public:
				IClientTransportInitializationParameters(void):ipAddress("::1"),initialPayload("client says hello"){}
				IClientTransportInitializationParameters& operator=(IClientTransportInitializationParameters& rhs)
				{
					initialPayload = rhs.initialPayload;
					ipAddress = rhs.ipAddress;
					(ITransportInitializationParameters&)*this = (ITransportInitializationParameters&)rhs;
					return *this;
				};
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					auto msg = IClientTransportInitializationParameters::TypeName();
					msg += "::ToPropertyTree\n";
					LOGIT(msg)
					auto pt = IClientTransportInitializationParametersToPropertyTree();
					auto thetypename = ITransportInitializationParameters::TypeName();
					auto topt = ITransportInitializationParameters::ToPropertyTree();
					pt.addPropertyTree(thetypename, topt);
					return pt;
				};
				virtual std::string TypeName(void) { return "IClientTransportInitializationParameters"; };
				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					auto msg = IClientTransportInitializationParameters::TypeName();
					msg += "::FromPropertyTree\n";
					LOGIT(msg)
					IClientTransportInitializationParametersFromPropertyTree(pt);
					auto thetypename = ITransportInitializationParameters::TypeName();
					std::string msg2="calling get child for \n";
					msg2+=thetypename;
					LOGIT(msg2)
					auto pt2 = pt.getChild(thetypename);
					std::string msg3 = "calling ITransportInitializationParameters::FromPropertyTree\n";				
					LOGIT(msg3)
					ITransportInitializationParameters::FromPropertyTree(pt2);
				};

			};
		}
	}
}
#endif
