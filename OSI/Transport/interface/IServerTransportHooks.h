#ifndef I_TRANSPORT_HOOKS_H
#define I_TRANSPORT_HOOKS_H
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing>
			class IServerTransportHooks
			{
			public:
				// triggers on TCPClientServerWrapper Incoming. Non zero indicates send information out
				virtual bool IncomingHook(TFlyWeightServerIncoming& in, TFlyWeightServerOutgoing& out) { return false; };
			};
		}
	}
}
#endif