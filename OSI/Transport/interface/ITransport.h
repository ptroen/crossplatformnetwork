#ifndef ITCP_TRANSPORT_H
#define ITCP_TRANSPORT_H

namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			// what you want from a ideal tcp transport Interface? The incoming out outgoing data as flyweight objects(unions of data).
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TInitializationParameters>
			class IServerTransport
			{
			protected:
				TInitializationParameters _parameters;
			public:
				IServerTransport(TInitializationParameters& parameters) :_parameters(parameters) {};
				IServerTransport(void) :_parameters() {};
			protected:
				virtual void RunServer(void) {};
			};
		}
	}
}
#endif
