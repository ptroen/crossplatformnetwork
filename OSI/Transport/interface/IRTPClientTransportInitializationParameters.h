#ifndef I_RTP_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_RTP_CLIENT_TRANSPORT_INITIALIZATION_PARAMETERS_H
#include "IRTPTransportInitializationParameters.h"
#include "IClientTransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IRTPClientTransportInitializationParameters : public virtual IRTPTransportInitializationParameters, public virtual IClientTransportInitializationParameters, public virtual ITransportInitializationParameters
			{
				public:
					virtual std::string TypeName(void) { return "IRTPClientTransportInitializationParameters"; };
					virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
						Algorithm::Interface::IPropertyTree pt;
						std::string msg = "ITransportInitializationParameters\n";
						LOGIT1(msg)
						PADDBASE(ITransportInitializationParameters)
						msg = "IRTPTransportInitializationParameters\n";
						LOGIT1(msg)
						PADDBASE(IRTPTransportInitializationParameters)
						msg = "IClientTransportInitializationParameters\n";
						LOGIT1(msg)
						PADDBASE(IClientTransportInitializationParameters)
						return pt;
					};

					// method which extracts the values from property tree
					virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
						std::string msg = "ITransportInitializationParameters\n";
						LOGIT1(msg)
						PGETBASE(ITransportInitializationParameters)
						msg = "IRTPTransportInitializationParameters\n";
						LOGIT1(msg)
						PGETBASE(IRTPTransportInitializationParameters)
						LOGIT1(msg)
						PGETBASE(IClientTransportInitializationParameters)
					};
			};
		}
	}
}

#endif
