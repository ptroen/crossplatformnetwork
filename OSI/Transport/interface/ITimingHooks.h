#ifndef I_TIMING_HOOKS_H
#define I_TIMING_HOOKS_H
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class ITimingHooks
			{
				virtual void OnIdleHook(void){};
				virtual void OnTimerHook(void){};
			};
		}
	}
}
#endif