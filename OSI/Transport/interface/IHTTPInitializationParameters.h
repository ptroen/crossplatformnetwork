#ifndef I_HTTP_INITIALIZATION_PARAMETERS_H
#define I_HTTP_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IHTTPInitializationParameters : public virtual ITransportInitializationParameters
			{
			public:
				virtual std::string TypeName(void) { return "IHTTPInitializationParameters"; };
			};
		}
	}
}

#endif