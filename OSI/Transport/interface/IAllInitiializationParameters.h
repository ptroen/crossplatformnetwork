#ifndef I_INITIALIZATION_PARAMETERS_H
#define I_INITIALIZATION_PARAMETERS_H
#include "IHTTPInitializationParameters.h"
#include "IHTTPSInitializationParameters.h"
#include "INackServerInitializationParameters.h"
#include "IRTPTransportInitializationParameters.h"
#include "ISSLTransportInitializationParameters.h"
#include "ISSLClientTransportInitializationParameters.h"
#include "../../../OSI/Transport/All/protocolselection.h"
#include "IMulticastTransportInitializationParameters.h"
#include "IProxyTransportInitializationParameters.h"

namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IAllInitiializationParameters : public virtual IHTTPInitializationParameters, 
																	public virtual IHTTPSInitializationParameters,
																	public virtual INackServerInitializationParameters,
																	public virtual IRTPTransportInitializationParameters,
																	public virtual ISSLTransportInitializationParameters,
																	public virtual ISSLClientTransportInitializationParameters,
																	public virtual IMulticastInitializationParameters,
																	public virtual IProxyTransportInitializationParameters,
																	public virtual ITransportInitializationParameters,
																	public virtual IClientTransportInitializationParameters
			{
			public:
				IAllInitiializationParameters(void) :protocol(All::ProtocolSelection::TCP) {};
				All::ProtocolSelection protocol;
				virtual std::string TypeName(void) { return "IAllInitiializationParameters"; };
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt = IAllInitiializationParametersToPropertyTree();
					PADDBASE(INackServerInitializationParameters)
					PADDBASE(ISSLTransportInitializationParameters)
					PADDBASE(IMulticastInitializationParameters)
					PADDBASE(IRTPTransportInitializationParameters)
					PADDBASE(IProxyTransportInitializationParameters)
					PADDBASE(ITransportInitializationParameters)
					PADDBASE(IClientTransportInitializationParameters)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					IAllInitiializationParametersFromPropertyTree(pt);
					PGETBASE(INackServerInitializationParameters)
					PGETBASE(ISSLTransportInitializationParameters)
					PGETBASE(IMulticastInitializationParameters)
					PGETBASE(IRTPTransportInitializationParameters)
					PGETBASE(IProxyTransportInitializationParameters)
					PGETBASE(ITransportInitializationParameters)
					PGETBASE(IClientTransportInitializationParameters)
				};
			protected:
				virtual Algorithm::Interface::IPropertyTree  IAllInitiializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					pt.add(std::string("ProtocolSelection"), std::string(protocol._to_string()));
					return pt;
				};

				// method which extracts the values from property tree
				virtual void IAllInitiializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					auto sss = std::string("ProtocolSelection");
					auto s = pt.get<std::string>(sss);
					protocol = protocol._from_string(s.c_str());
				};
			};
		}
	};
}

#endif
