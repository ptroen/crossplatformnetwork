#ifndef I_RTP_TRANSPORT_INITIALIZATION_PARAMETERS_H
#define I_RTP_TRANSPORT_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
#include "IReliability.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class IRTPTransportInitializationParameters : public virtual OSI::Transport::Interface::ITransportInitializationParameters, public virtual OSI::Transport::Interface::IReliabity
			{
				public:
					uint16_t csrc; // contributing source(csrc). This is an additional part of the sender
					uint16_t ssrcOfPacketSender;
					std::string cname;
					std::string name;
					std::string email;
					std::string phone;
					std::string location;
					std::string tool;
					std::string note;
					std::string priv;
					std::string byeWhy;
					IRTPTransportInitializationParameters(void) : OSI::Transport::Interface::ITransportInitializationParameters(),cname("\0"),
				name("\0"),email("\0"),phone("\0"),location("\0"),tool("\0"),note("\0"),priv("\0"),csrc(0),byeWhy("\0"), ssrcOfPacketSender(0){};

					virtual std::string TypeName(void) { return "IRTPTransportInitializationParameters"; };

					virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
						Algorithm::Interface::IPropertyTree pt = IRTPTransportInitializationParametersToPropertyTree();
						PADDBASE(ITransportInitializationParameters)
							return pt;
					};

					// method which extracts the values from property tree
					virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
						IRTPTransportInitializationParametersFromPropertyTree(pt);
						PGETBASE(ITransportInitializationParameters)
					};
			protected:
				virtual Algorithm::Interface::IPropertyTree  IRTPTransportInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADDS(csrc)
					PADDS(ssrcOfPacketSender)
					PADDS(cname)
					PADDS(name)
					PADDS(email)
					PADDS(phone)
					PADDS(location)
					PADDS(tool)
					PADDS(note)
					PADDS(priv)
					PADDS(byeWhy)
					PADD(numberOfPacketsToKeepForRecovery)
					return pt;
				}

				virtual void IRTPTransportInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					PGET(csrc,uint16_t)
						PGET(ssrcOfPacketSender,uint16_t)
						PGET(cname,std::string)
						PGET(name, std::string)
						PGET(email, std::string)
						PGET(phone, std::string)
						PGET(location, std::string)
						PGET(tool, std::string)
						PGET(note, std::string)
						PGET(priv, std::string)
						PGET(byeWhy, std::string)
					PGET(numberOfPacketsToKeepForRecovery,size_t)
				}
			};
		}
	}
}
#endif
