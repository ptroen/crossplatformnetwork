#ifndef I_CLIENT_READ_WRITE_SOCKET_H
#define I_CLIENT_READ_WRITE_SOCKET_H
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			/*
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
			class IClientReadWriteSocket
			{
			protected:
				TSocket& _socketRef;
				TFlyWeightServerIncoming outgoing;
				TFlyWeightServerOutgoing incoming;
				bool _closeOnZeroOutput; // closes connection if ServerSessionHook reported 0
			public: 
				 TSession session;
				IClientReadWriteSocket(TSocket& socket,bool closeOnZeroOutput, TInitializationParameters& parameters) :_socketRef(socket),incoming(parameters),outgoing(parameters), _closeOnZeroOutput(closeOnZeroOutput) {};
			protected:
				void async_write(size_t outgoingLength)
				{
					boost::asio::async_write(_socketRef,
						boost::asio::buffer(&outgoing, outgoingLength),
						boost::bind(&IClientReadWriteSocket::outputHandler,
							this,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred)
					);
				};
				void inputHandler(const boost::system::error_code& error, std::size_t bytes_transferred)
				{
					std::string ipaddress = _socketRef.lowest_layer().remote_endpoint().address().to_string();
					if(!error)
					{
						size_t outgoingLength = (*ClientReadWriteHook)(outgoing,incoming, session, ipaddress);
						if (outgoingLength)
						{
							outgoing.ToString(const_cast<char*>(outgoingString.c_str()), outgoingString.size());
							async_write(outgoingLength);
						}
					}
				};
				void outputHandler(
					const boost::system::error_code& error,
					std::size_t bytes_transferred)
				{
					if (_closeOnZeroOutput)
						_socketRef.lowest_layer().close();
				};
			};
			*/
		}
	}
}
#endif