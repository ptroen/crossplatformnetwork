#ifndef I_NACK_CLIENT_INITIALIZATION_PARAMETERS_H
#define I_NACK_CLIENT_INITIALIZATION_PARAMETERS_H
#include "ITransportInitializationParameters.h"
#include "IClientTransportInitializationParameters.h"
#include "INackServerInitializationParameters.h"
namespace OSI
{
	namespace Transport
	{
		namespace Interface
		{
			class INackClientInitializationParameters : public virtual INackServerInitializationParameters, public virtual IClientTransportInitializationParameters
			{
				public:
			protected:
				virtual Algorithm::Interface::IPropertyTree IINackClientInitializationParametersToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
						return pt;
				};

				// method which extracts the values from property tree
				virtual void INackClientInitializationParametersFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
				};
			public:
				virtual std::string TypeName(void) { return "INackServerInitializationParameters"; };
				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt = IINackClientInitializationParametersToPropertyTree();
					std::string msg = "INackServerInitializationParameters\n";
					LOGIT1(msg)
					PADDBASE(INackServerInitializationParameters)
					LOGIT1(msg)
					msg = "IClientTransportInitializationParameters\n";
					PADDBASE(IClientTransportInitializationParameters)
					msg = "ITransportInitializationParameters\n";
					LOGIT1(msg)
					PADDBASE(ITransportInitializationParameters)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					std::string msg = "INackServerInitializationParameters\n";
					LOGIT(msg)
					PGETBASE(INackServerInitializationParameters)
					msg = "IClientTransportInitializationParameters\n";
					LOGIT(msg)
					PGETBASE(IClientTransportInitializationParameters)
					msg = "ITransportInitializationParameters\n";
					LOGIT(msg)
					PGETBASE(ITransportInitializationParameters)
				};
			};
		}
	}
}
#endif
