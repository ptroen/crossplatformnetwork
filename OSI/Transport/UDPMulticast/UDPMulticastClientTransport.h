#ifndef UDP_MULTICAST_CLIENT_TRANSPORT_H
#define UDP_MULTICAST_CLIENT_TRANSPORT_H
#include "../interface/ITransportInitializationParameters.h"
#include "../interface/IClientTransport.h"
#include "../../Session/TimerHooks/TimerHooks.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDPMulticast
		{
			template <class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class UDPMulticastClientTransport
			{
			private:
				boost::asio::ip::udp::socket socket_;
				boost::asio::ip::udp::endpoint sender_endpoint_;
				TFlyWeightServerOutgoing data;
				std::string dataString;
				size_t dataSize;
			    public:
					Session::TimerHooks::ClientMulticastTimerHooks<TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters> _session;
					TTransportInitializationParameters& _parameters;
					UDPMulticastClientTransport(TTransportInitializationParameters& parameters) : socket_(Session::IOContextManager::io_context),
					_session(parameters),_parameters(parameters),data(parameters)
					{
						dataSize = data.max_size();
						dataString.resize(dataSize);
					};
		

				void handle_receive_from(const boost::system::error_code& error,size_t bytes_recvd)
				{
					data.FromString(const_cast<char*>(dataString.c_str()), bytes_recvd);
					_session.IncomingHook(data);
					LOGIT(dataString)
					if (!error)
					{
						ReceiveData();
						
					}
				}
				void ReceiveData(void) {
					socket_.async_receive_from(boost::asio::buffer(const_cast<char*>(dataString.c_str()),dataSize), sender_endpoint_,
						boost::bind(&UDPMulticastClientTransport::handle_receive_from, this,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred));
					
				};
			public:

					virtual void RunClient()
					{
						try
						{
							// citation: based on this tutorial https://www.boost.org/doc/libs/1_50_0/doc/html/boost_asio/example/multicast/receiver.cpp
							// Create the socket so that multiple may be bound to the same address.
							boost::asio::ip::udp::endpoint listen_endpoint(
								boost::asio::ip::address::from_string(_parameters._listenaddress), _parameters._incomingPortApplicationLayerAddress);
							socket_.open(listen_endpoint.protocol());
							socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
						socket_.bind(listen_endpoint);

							// Join the multicast group.
							socket_.set_option(boost::asio::ip::multicast::join_group(boost::asio::ip::address::from_string(_parameters._multicastaddress)));
							ReceiveData();
							Session::IOContextManager::io_context.run();
						}
						catch(std::exception ex)
						{
							std::cout << ex.what() << std::endl;
						}
					};
			};
		}
	}
}
#endif