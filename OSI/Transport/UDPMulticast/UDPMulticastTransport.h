#ifndef UDP_MULTICAST_TRANSPORT_H
#define UDP_MULTICAST_TRANSPORT_H
#include <sstream>
#include <string>
#include <boost/asio.hpp>
#include "boost/bind.hpp"
#include "../interface/ITransport.h"
#include "../../Session/TimerHooks/TimerHooks.h"
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace UDPMulticast
		{
			template<class TFlyWeightServerOutgoing, class TServerSession,class TInitializationParameters>
			class UDPMulticastServerTransport
			{
				boost::asio::ip::udp::endpoint endpoint_;
				boost::asio::ip::udp::socket socket_;
				int message_count_;
				std::string outgoingString;
				TFlyWeightServerOutgoing outgoing;
				OSI::Transport::Interface::ITransportInitializationParameters& _parameters;
			public:
				Session::TimerHooks::ServerMulticastTimerHooks<TFlyWeightServerOutgoing, TServerSession, TInitializationParameters> _session;
				UDPMulticastServerTransport(TInitializationParameters& parameters) : endpoint_(boost::asio::ip::address::from_string(parameters._multicastaddress.c_str()),parameters._outgoingPortApplicationLayerAddress),
					socket_(Session::IOContextManager::io_context, endpoint_.protocol()),
				_parameters(parameters),
				outgoing(parameters),
				_session(parameters)
				{
					outgoingString.resize(outgoing.max_size());
				};
				virtual void RunServer(void)
				{
					if (KeepSending())
					{
						size_t outSize = outgoing.size();
						LOGIT(outgoingString)
						socket_.async_send_to(
							boost::asio::buffer(const_cast<char*>(outgoingString.c_str()), outSize),
							endpoint_,
							boost::bind(&UDPMulticastServerTransport::handle_send_to, this,
								boost::asio::placeholders::error));
						OSI::Session::IOContextManager::io_context.run();
					}
				};
			private:
				bool KeepSending(void)
				{
					bool status = _session.IncomingHook(outgoing);
					size_t outSize = outgoing.size();
					outgoing.ToString(const_cast<char*>(outgoingString.c_str()),outSize);
					return status;
				};
				void handle_send_to(const boost::system::error_code& error)
				{
                    try
                    {
                        if (!error)
                        {
                            if(KeepSending())
                            {
                                size_t outSize = outgoing.size();
								LOGIT(outgoingString)
                                socket_.async_send_to(
                                    boost::asio::buffer(const_cast<char*>(outgoingString.c_str()),outSize), endpoint_,
                                    boost::bind(&UDPMulticastServerTransport::handle_send_to, this,
                                        boost::asio::placeholders::error));
                            }
                        }
					}
					catch(std::exception ex)
					{
                        std::string msg="UDPMulticastTransport::handle_send_to exception:";
                        msg += ex.what();
                        LOGIT(msg)
					}
				}
			};
		}
	}
}
#endif
