#ifndef HTTPS_TRANSPORT_H
#define HTTPS_TRANSPORT_H

#include "../SSL/SSLTransport.h"
#include "../HTTP/HTTPTransport.h"
#include <cstdlib>
#include <iostream>

// citation: https://www.boost.org/doc/libs/1_39_0/doc/html/boost_asio/example/ssl/TCPServerWrapper.cpp
#include "../HTTP/httpserverresponder.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTPS
		{
			// https rfc https://tools.ietf.org/html/rfc2660
			// basically the 
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class HTTPSTransport : public virtual SSL::SSL_ServerTransport <HTTP::HTTPRequest<TInitializationParameters>,
																														HTTP::HTTPResponse<TInitializationParameters>,
																														HTTP::HTTPServerResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>,
																														TInitializationParameters>
			{
			public:
				HTTPSTransport(TInitializationParameters& params) :SSL::SSL_ServerTransport <HTTP::HTTPRequest<TInitializationParameters>,
																																						HTTP::HTTPResponse<TInitializationParameters>,
						HTTP::HTTPServerResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, 
					TInitializationParameters>(params) {};
			};
		}
	}
}
#endif