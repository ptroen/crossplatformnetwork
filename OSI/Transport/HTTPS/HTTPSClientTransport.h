#ifndef HTTPS_CLIENT_TRANSPORT_H
#define HTTPS_CLIENT_TRANSPORT_H
#include "../HTTP/HTTPClientTransport.h"
#include "../SSL/sslclienttransport.h"
#include "../HTTP/HTTPRequest.h"
#include "../HTTP/HTTPResponse.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTPS
		{
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
			class HTTPSClientTransport : public virtual OSI::Transport::SSL::SSLClientTransport<HTTP::HTTPResponse<TTransportInitializationParameters>, HTTP::HTTPRequest<TTransportInitializationParameters>,
				HTTP::HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters>,
			public virtual Interface::IClientTransport<HTTP::HTTPRequest<TTransportInitializationParameters>, HTTP::HTTPResponse<TTransportInitializationParameters>, TTransportInitializationParameters>
			{
			public:
				HTTPSClientTransport(TTransportInitializationParameters& parameters) :OSI::Transport::SSL::SSLClientTransport<HTTP::HTTPResponse<TTransportInitializationParameters>, HTTP::HTTPRequest<TTransportInitializationParameters>,
					HTTP::HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters>(parameters), Interface::IClientTransport<HTTP::HTTPRequest<TTransportInitializationParameters>, HTTP::HTTPResponse<TTransportInitializationParameters>, TTransportInitializationParameters>(parameters) {};
				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
				{
					HTTP::HTTPRequest<TTransportInitializationParameters> requestToServer(Interface::IClientTransport<HTTP::HTTPRequest<TTransportInitializationParameters>, HTTP::HTTPResponse<TTransportInitializationParameters>, TTransportInitializationParameters>::_parameters);
					auto _request = request.ToHTTPRequest();
					requestToServer = _request;
					SSL::SSLClientTransport<HTTP::HTTPResponse<TTransportInitializationParameters>, 
																HTTP::HTTPRequest<TTransportInitializationParameters>,
																HTTP::HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>,
																TTransportInitializationParameters>::RunClient(ipAddress, _request);

				};
			};
		}
	}
}
#endif
