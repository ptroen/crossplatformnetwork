#ifndef SOCKET_ERROR_H
#define SOCKET_ERROR_H
enum SocketError
{
	NoError=0,
	FailToSetSIGINTHANDLER=1,
	FailToCreateListeningSocket=2,
	FailToReusePort=3,
	FailToSetNonBlocking=4,
	FailToBindToListeningPort=5,
	FailToListenForMaxConServer=6,
	FailToSetEPOLLCTL=7,
	FailErrorInEpollWait=8,
	FailEventsEqualEpollIn=9,
	FailToCreateEpollFileDescriptor=10,
	FailToSetNewFDNonBlocking=11,
	FailToSetNewFdFCTL=12,
	FailEpollCreate=13
};
#endif
