#ifndef OSI_TRANSPORT_HANDLER_MEMORY_H
#define OSI_TRANSPORT_HANDLER_MEMORY_H

#include <boost/asio.hpp>
#include <boost/aligned_storage.hpp>
#include <boost/array.hpp>
#include <boost/bind/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace OSI
{
	namespace Transport
	{
            // Class to manage the memory to be used for handler-based custom allocation in boost::asio. This was grabbed from the boost example code. Remaining code and comments are copy pasted.
			// It contains a single block of memory which may be returned for allocation
			// requests. If the memory is in use when an allocation request is made, the
			// allocator delegates allocation to the global heap.
			class handler_memory
			  : private boost::noncopyable
			{
			public:
			  handler_memory()
			    : in_use_(false)
			  {
			  }

			  void* allocate(std::size_t size)
			  {
			    if (!in_use_ && size < storage_.size)
			    {
			      in_use_ = true;
			      return storage_.address();
			    }
			    else
			    {
			      return ::operator new(size);
			    }
			  }

			  void deallocate(void* pointer)
			  {
			    if (pointer == storage_.address())
			    {
			      in_use_ = false;
			    }
			    else
			    {
			      ::operator delete(pointer);
			    }
			  }

			private:
			  // Storage space used for handler-based custom memory allocation.
			  boost::aligned_storage<1024> storage_;

			  // Whether the handler-based custom allocation storage has been used.
			  bool in_use_;
			};

			// The allocator to be associated with the handler objects. This allocator only
			// needs to satisfy the C++11 minimal allocator requirements, plus rebind when
			// targeting C++03.
			template <typename T>
			class handler_allocator
			{
			public:
			  typedef T value_type;

			  explicit handler_allocator(handler_memory& mem)
			    : memory_(mem)
			  {
			  }

			  template <typename U>
			  handler_allocator(const handler_allocator<U>& other)
			    : memory_(other.memory_)
			  {
			  }

			  template <typename U>
			  struct rebind
			  {
			    typedef handler_allocator<U> other;
			  };

			  bool operator==(const handler_allocator& other) const
			  {
			    return &memory_ == &other.memory_;
			  }

			  bool operator!=(const handler_allocator& other) const
			  {
			    return &memory_ != &other.memory_;
			  }

			  T* allocate(std::size_t n) const
			  {
			    return static_cast<T*>(memory_.allocate(sizeof(T) * n));
			  }

			  void deallocate(T* p, std::size_t /*n*/) const
			  {
			    return memory_.deallocate(p);
			  }

			//private:
			  // The underlying memory.
			  handler_memory& memory_;
			};

			// Wrapper class template for handler objects to allow handler memory
			// allocation to be customised. The allocator_type typedef and get_allocator()
			// member function are used by the asynchronous operations to obtain the
			// allocator. Calls to operator() are forwarded to the encapsulated handler.
			template <typename Handler>
			class custom_alloc_handler
			{
			public:
			  typedef handler_allocator<Handler> allocator_type;

			  custom_alloc_handler(handler_memory& m, Handler h)
			    : memory_(m),
			      handler_(h)
			  {
			  }

			  allocator_type get_allocator() const
			  {
			    return allocator_type(memory_);
			  }

			  template <typename Arg1>
			  void operator()(Arg1 arg1)
			  {
			    handler_(arg1);
			  }

			  template <typename Arg1, typename Arg2>
			  void operator()(Arg1 arg1, Arg2 arg2)
			  {
			    handler_(arg1, arg2);
			  }

			private:
			  handler_memory& memory_;
			  Handler handler_;
			};

			// Helper function to wrap a handler object to add custom allocation.
			template <typename Handler>
			inline custom_alloc_handler<Handler> make_custom_alloc_handler(
			    handler_memory& m, Handler h)
			{
			  return custom_alloc_handler<Handler>(m, h);
			}
	}
}

#endif
