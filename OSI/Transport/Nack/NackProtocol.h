#ifndef NACK_PROTOCOL_H
#define NACK_PROTOCOL_H
#include "nackcontents.h"
#include "../PayloadValidation/CRCValidatePayload.h"
#include "../../../algorithm/GZIP.h"
#include "../../../algorithm/interface/IType.h"
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
				template<class TPayload, class TInitializationParameters>
				class NackProtocol : public virtual Algorithm::Interface::IType
				{
					public:
					TPayload payload;
					NackContents NackContents;
					private:
					std::string compressedData;
					public:
					NackProtocol(TInitializationParameters& parameters):payload(parameters),NackContents(){};

					size_t size(void)
					{
						size_t n = NackContents.size();
						if (NackContents.flags.test(CompressGZIP))
							n+= compressedData.size();
						else
							n += payload.size();
						return n;
					};

					void ToString(char* stringout, size_t thesize)
					{
						
						size_t nacksize = NackContents.size();
						NackContents.ToString(&(*stringout), nacksize);
						if (NackContents.flags.test(CompressGZIP))
						{
							thesize = size() - nacksize;
							memcpy(&stringout[nacksize], const_cast<char*>(compressedData.c_str()), thesize);
						}
						else
						{
							size_t payloadSize = payload.size();
							payload.ToString(&stringout[nacksize], payloadSize);
						}
						// need to make the CRC before you turn it into a string
//						this->MakeCrc();
						
					}
					void FromString(char* data,size_t dataSize)
					{
						size_t nacksize = NackContents.size();
						// boundary testing
						if (dataSize < nacksize) return;
						if(dataSize > max_size()) return;

						// end boundary testing
						NackContents.FromString(&data[0], nacksize);
						if (NackContents.flags.test(CompressGZIP))
						{
							
							compressedData.resize(dataSize - nacksize );
							memcpy(&compressedData[0],&data[nacksize], dataSize -  nacksize);
						}
						else
						{
							size_t size = dataSize - nacksize;
							payload.FromString(&data[nacksize],size);
						}
					};

					void CheckIfWeCanCompress(void)
					{
						std::string compressionContainer;
						size_t payloadsize = payload.size();
						compressionContainer.resize(payloadsize);
						payload.ToString(const_cast<char*>(compressionContainer.c_str()), payloadsize);
						compressedData = Algorithm::Gzip::compress(compressionContainer);
						if(compressedData.size() < compressionContainer.size())
						{
							NackContents.flags.set(CompressGZIP);
						}
						else
						{
							compressedData.clear();
						}
					}

					void UnCompressIfCompressed(void)
					{
						if (checkIfCompressed())
						{
							std::string uncompressedData = Algorithm::Gzip::decompress(compressedData);
							size_t size = uncompressedData.size();
							payload.FromString(const_cast<char*>(uncompressedData.c_str()),size);
						}
					}
					size_t max_size(void) { return NackContents.size() + payload.max_size(); };
					/*
					bool ValidatePayload(void)
					{

						std::string s;
						size_t payloadsize = payload.size();
						s.resize(payload.size());
						payload.ToString(const_cast<char*>(s.c_str()),payloadsize);
						return OSI::Transport::PayloadValidation::CRC::ValidatePayload(NackContents.checksum, s);
					}
					*/
					void SetNackAndInstructToRetransmit()
					{
						NackContents.flags.reset();
						NackContents.flags.set(NackFlags::NackPleaseRetransmit);
					}

				//	void SetSize(size_t sizeOfPayload) { nack_size = sizeOfPayload; };
				//	size_t PayloadSize() { return (nack_size >= TPayload) ? TPayload : nack_size; }
					/* no need checksum in udp header
					void MakeCrc(void)
					{
						if (checkIfCompressed())
						{
							PayloadValidation::CRC::MakeCRC(NackContents.checksum, compressedData);
						}
						else
						{
							std::string payloadString;
							size_t payloadStringSize = payload.size();
							payloadString.resize(payloadStringSize);
							payload.ToString(&payloadString[0], payloadStringSize);
							//PayloadValidation::CRC::MakeCRC(NackContents.checksum, payloadString);
						};
					};*/
				private:
					bool checkIfCompressed(void)
					{
						return (NackContents.flags.test(CompressGZIP));
					}
					//size_t sizeWithoutCrc(void) { return size() - sizeof(NackContents.checksum)};			
				};

				// this is a general CreateNack
				template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing,class TInitializationParameters>
				size_t CreateNack(NackProtocol<TFlyWeightServerIncoming, TInitializationParameters>& incoming, NackProtocol<TFlyWeightServerOutgoing, TInitializationParameters>& outgoing)
				{
					// nack the sequence number hence we copy it
					outgoing.NackContents = incoming.NackContents;
					outgoing.SetNackAndInstructToRetransmit();
					outgoing.MakeCrc();
					return outgoing.NackContents.size();
				};
		} // end NACK
	} // end Transport
} // end OSI
#endif
