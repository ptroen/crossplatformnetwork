#ifndef NACK_CONTENTS_H
#define NACK_CONTENTS_H

#include <boost/crc.hpp>
#include <bitset>
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			// Nack Flags are position use 2^8 pow to extract value or bit shift
			enum NackFlags : uint8_t
			{
				SYN = 0,
				SYNACK = 1,
				ACK = 2,
				RegularPayload = 3,
				NackPleaseRetransmit = 4,
				CompressGZIP = 5,
				// reserve 6,7 bits for future
				MaxValue = 7
			};

			
			// pure transport details for nack based protocol
			class NackContents
			{
			public:
				std::bitset<8> flags; // flags could be considered session
				const size_t NackFlagNumberOfDigits = 8;
				NackContents(void) :flags(0) { Reset(); };
				
				uint8_t source_sequenceNumber; // counting packets only
				
				void Reset(void)
				{
					flags = 0;
					flags.set(RegularPayload);
				}
				//boost::crc_32_type checksum;
				NackContents& operator=(NackContents&rhs)
				{
					// we swap destination and source number
					source_sequenceNumber = rhs.source_sequenceNumber;
					flags = rhs.flags;
					//checksum = rhs.checksum;
					return *this;
				};
				size_t size(void)
				{
					size_t thesize = 0;
					thesize += sizeof(uint8_t) + sizeof(unsigned char);
					return thesize;
				}

				void ToString(char* data, size_t size)
				{
					if (size < this->size()) return;
					data[0] = static_cast<unsigned char>(flags.to_ulong());
					data[1] = source_sequenceNumber;
				};

				void FromString(char* data, size_t size)
				{
					if (size < this->size()) return;
					flags.reset(0);
					flags = std::bitset<8>(data[0]);
					source_sequenceNumber = data[1];
				};
			};
		}
	}
}

#endif