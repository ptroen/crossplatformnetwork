#ifndef NACK_TRANSPORT_H
#define NACK_TRANSPORT_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <string>
#include <map>
#include "../UDP/udptransport.h"
#include "../../../algorithm/circularbuffer.h"
#include "../../../Tools/Diagnostics/Benchmark.h"
#include "nackcontents.h"
#include "NackProtocol.h"
#include "NackProtocolBase.h"
#include "NackProtocolStatus.h"
#include "NackServerProtocolState.h"
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class NackServerSession
			{
			public:
				TServerSession _session;
				TInitializationParameters& _parameters;
				NackServerProtocolState<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession,  TInitializationParameters> serverUserPool;
				NackServerSession(TInitializationParameters& parameters): _session(_parameters),serverUserPool(_parameters,_session), _parameters(parameters){
					serverUserPool._port = _parameters._outgoingPortApplicationLayerAddress; 
				};
				void OnTimerHook()
				{
					for (auto it = serverUserPool.userMap.cbegin(); it != serverUserPool.userMap.cend();)
					{
						it->second.get()->TimingHook();
						// citation: https://stackoverflow.com/questions/8234779/how-to-remove-from-a-map-while-iterating-it
						if (it->second.get()->markForDestruction)
						{
							serverUserPool.userMap.erase(it++);
						}
						else
						{
							++it;
						}
					} // end for
					_session.OnTimerHook();
				};

				virtual void OnIdleHook(void)
				{
					_session.OnIdleHook();
				}

				virtual bool IncomingHook(NackProtocol<TFlyWeightServerIncoming, TInitializationParameters>& in, NackProtocol<TFlyWeightServerOutgoing, TInitializationParameters>& out, std::string& ipadress)
				{
					// pack details are filled in by Process Incoming
					return  serverUserPool[ipadress]->ProcessIncoming(in, out, ipadress);
				}
			};

			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class NackServerTransport : public virtual UDP::UDPServerTransport<NackProtocol<TFlyWeightServerIncoming, TInitializationParameters>, NackProtocol<TFlyWeightServerOutgoing, TInitializationParameters>, NackServerSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, TInitializationParameters>
			{
			public:
				NackServerTransport(TInitializationParameters& parameters) : UDP::UDPServerTransport<NackProtocol<TFlyWeightServerIncoming, TInitializationParameters>, NackProtocol<TFlyWeightServerOutgoing, TInitializationParameters>, NackServerSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, TInitializationParameters>(parameters){
					
				}
			}; // end NackTransport
		} // namespace NACK
	} // namespace Transport
} // namespace OSI
#endif
