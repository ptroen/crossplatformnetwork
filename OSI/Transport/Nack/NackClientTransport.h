#ifndef NACK_CLIENT_TRANSPORT_H
#define NACK_CLIENT_TRANSPORT_H

#include "../UDP/UDPClientTransport.h"
#include "NackProtocolBase.h"
#include "NackProtocol.h"
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession,class TInitializationParameters>
			class NackClientSession : public virtual NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>
			{
			public:
				NackClientSession(TInitializationParameters& parameters) :NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>(parameters.ipAddress, parameters, session),
				session(parameters){};
				TSession session;
				inline static  TFlyWeightServerIncoming* request = nullptr;
				 virtual void OnTimerHook()
				{
					 NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>::TimingHook();
					session.OnTimerHook();
				};
				virtual void OnIdleHook(void)
				{
					session.OnIdleHook();
				}

				bool IncomingHook(NackProtocol<TFlyWeightServerOutgoing, TInitializationParameters>& incoming, NackProtocol<TFlyWeightServerIncoming, TInitializationParameters>& outgoing, std::string& ipaddress)
				{
					// server says its ok so we can finally send our initial packet that we initialized with
					if (incoming.NackContents.flags.test(NackFlags::SYNACK))
					{
						NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>::transmitFirstPacket = true;
						NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>::sourceSequenceNumber = incoming.NackContents.source_sequenceNumber;
						std::string s;
						size_t requestSize = request->size();
						request->ToString(const_cast<char*>(s.c_str()), requestSize);
						outgoing.payload.FromString(const_cast<char*>(s.c_str()), requestSize);
						outgoing.NackContents.flags.set(NackFlags::RegularPayload);
						outgoing.NackContents.flags.set(NackFlags::ACK);
						AdjustOutgoingSequenceNumber(NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>::destinationSequenceNumber, outgoing);
						return true;
					}
					return NackProtocolBase<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>::ProcessIncoming(incoming, outgoing, ipaddress);
				};
			};

			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class NackClientTransport : public virtual UDP::UDPClientTransport< NackProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, NackProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>, NackClientSession< TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,TTransportInitializationParameters>
			{
			public:
				NackClientTransport(TTransportInitializationParameters& parameters) : UDP::UDPClientTransport< NackProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, NackProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
				NackClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
				TTransportInitializationParameters>(parameters){};

				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
				{
					NackProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters> wrapped_request(this->_parameters);
					this->session.session.request = &request;
					wrapped_request.NackContents.flags.set(NackFlags::SYN);
					UDP::UDPClientTransport< NackProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, NackProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
				NackClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
				TTransportInitializationParameters>::RunClient(ipAddress, wrapped_request);
				}
			};
		}
	}
}
#endif
