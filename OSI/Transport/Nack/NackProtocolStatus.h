#ifndef NACK_PROTOCOL_STATE_STATUS_H
#define NACK_PROTOCOL_STATE_STATUS_H
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			class NackProtocolStateStatus
			{
			    public:
				bool confirmed;
			};
		}
	}
}
#endif