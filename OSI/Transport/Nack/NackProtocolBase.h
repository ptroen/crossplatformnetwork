#ifndef NACK_PROTOCOL_BASE_H
#define NACK_PROTOCOL_BASE_H
#include <string>
#include "../../../OSManagement/timer.h"
#include "../../../OSI/Transport/UDP/UDPClientTransport.h"
#include "../../../algorithm/circularbuffer.h"
#include "NackProtocol.h"
#include "../interface/IProtocolBase.h"
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			template<class TFlyWeightServerIncoming, class  TFlyWeightServerOutgoing, class TSession, class TInitializtionParameters>
			class NackProtocolBase : public Interface::IProtocolBase<TSession>
			{
			public:
				const size_t defaultTimerDelayInMilliseconds = 10;
				size_t maximumRoundTripTime;
				size_t roundTripTimeInMilliseconds;
				std::string _ipaddress;
				TInitializtionParameters& _parameters;
				const size_t maxTimeout = 1000;
				size_t maxPackets;
				TSession& _session;
			protected:
				OSManagement::Timer nackRTTBenchmark;
				uint8_t sourceSequenceNumber;
				uint8_t destinationSequenceNumber;
				bool transmitFirstPacket;
			private:
				size_t _numberOfQueuedNackEntries;
				Algorithm::CircularBuffer<NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>, TInitializtionParameters> destinationNackBuffer;
			public:
				NackProtocolBase(const std::string& ipaddress, TInitializtionParameters& parameters, TSession& session) :roundTripTimeInMilliseconds(defaultTimerDelayInMilliseconds), transmitFirstPacket(false),
				_ipaddress(ipaddress),
					nackRTTBenchmark(roundTripTimeInMilliseconds, OSManagement::Milliseconds), 
				sourceSequenceNumber(0),
					destinationSequenceNumber(0),
				_parameters(parameters),
					destinationNackBuffer(parameters.numberOfQueuedNackEntries,parameters),
				_numberOfQueuedNackEntries(parameters.numberOfQueuedNackEntries), 
				maximumRoundTripTime(2000),
				_session(session){};
				virtual void TimingHook(void)
				{
					if (nackRTTBenchmark.Expired())
					{
						DeRegisterNackTimer();
						NackEvent();
						// check if we need to bail. That is terminate the connection
						if (this->roundTripTimeInMilliseconds == maximumRoundTripTime)
						{
							Interface::IProtocolBase<TSession>::markForDestruction = true;
						}
						else
						{
							RecalculateRoundTripTime();
							RegisterNackTimer(this->_ipaddress);
						}
					}
				};

				// only call this if you confirmed that it's a nacked item
				size_t SendNackedPacketCorrespondingToIncomingSequenceNumber(NackProtocol<TFlyWeightServerIncoming, TInitializtionParameters>& incoming, NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& outgoing)
				{
					size_t readIndex = destinationNackBuffer._currendReadIndex;
					NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>* it = destinationNackBuffer.GetButDontUpdateIndex(readIndex);
					while (it != nullptr)
					{
						if (it->NackContents.source_sequenceNumber == incoming.NackContents.source_sequenceNumber)
						{
							// we found our packet. Retransmit it(parent will do it)
							// need to remove the size_t and rely on tostring and fromstring
							std::string s;
							size_t size = it->size();
							s.resize(size);
							size_t backupSize = size;
							it->ToString(const_cast<char*>(s.c_str()), size);
							outgoing.FromString(const_cast<char*>(s.c_str()), backupSize);
							return 1;
						}
						destinationNackBuffer.UpdateIndex(readIndex);
						it = destinationNackBuffer.GetButDontUpdateIndex(readIndex);
					}
					return 0;
				};


				bool ProcessIncoming(NackProtocol<TFlyWeightServerIncoming, TInitializtionParameters>& incoming, NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& outgoing, std::string& ipAddress)
				{
					if (incoming.NackContents.flags.test(NackFlags::SYN))
					{
						sourceSequenceNumber = incoming.NackContents.source_sequenceNumber;
						SetNackFlag(NackFlags::SYNACK, outgoing);
						return true;
					}
					else if (incoming.NackContents.flags.test(NackFlags::SYNACK))
					{
						sourceSequenceNumber = incoming.NackContents.source_sequenceNumber;
						SetNackFlag(NackFlags::ACK, outgoing);
						transmitFirstPacket = true;
						return true;
					}
					else if (incoming.NackContents.flags.test(NackFlags::ACK))
					{
						transmitFirstPacket = true; // used to determine when nacks are appropriate
					}
					else if (!transmitFirstPacket) // packet may be out of order but other side could retransmit via nack mechanism since we track sequence numbers. So don't respond ie don't process
					{
						return false;
					}
					else if (incoming.NackContents.flags.test(NackFlags::NackPleaseRetransmit)) // if it's a nack
					{
						// no need for crc check since it's a nack and udp handles this
						SendNackedPacketCorrespondingToIncomingSequenceNumber(incoming, outgoing);
						return true;
					}
					if (incoming.NackContents.flags.test(NackFlags::RegularPayload))
					{
						// reset timer immediately to prevent false nack requests
						DeRegisterNackTimer();
						incoming.UnCompressIfCompressed();
						sourceSequenceNumber = incoming.NackContents.source_sequenceNumber;
						RecalculateRoundTripTime();

						// activate the session hook
						bool hookStatus = _session.IncomingHook(incoming.payload, outgoing.payload, ipAddress);

						// if something will be sent back
						if (hookStatus)
						{
							SetNackFlag(NackFlags::RegularPayload, outgoing);
						}
						// all bookkeeping done add the nack timer back in
						RegisterNackTimer(ipAddress);

						return hookStatus;
					}
				}
			protected:
				void SetNackFlag(NackFlags flag, NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& outgoing)
				{
					outgoing.NackContents.flags.reset();
					outgoing.NackContents.flags.set(flag);
					AdjustOutgoingSequenceNumber(destinationSequenceNumber, outgoing);
				};
			private:
				// called when we missing the next sequence number and we have to notify remote to issue a nack
				void NackEvent(void)
				{
					NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters> nackDatagram(_parameters);
					CreateNackBadSequenceNumber(nackDatagram);
					SendPacketAndClose(nackDatagram);
				};

				void RegisterNackTimer(std::string& ipaddress)
				{
					_ipaddress = ipaddress;
					nackRTTBenchmark.UpdateTimerResolution(roundTripTimeInMilliseconds, OSManagement::TimerResolution::Milliseconds);
					nackRTTBenchmark.StartTimer();
				};
				private:
					// citation https://stackoverflow.com/questions/9218724/get-random-element-and-remove-it
					template <typename T>
					void remove_at(std::vector<T>& v, typename std::vector<T>::size_type n)
					{
						std::swap(v[n], v.back());
						v.pop_back();
					}

				void DeRegisterNackTimer(void){nackRTTBenchmark.ManuallyStopTimer();};
				void RecalculateRoundTripTime(void)
				{
					DeRegisterNackTimer();
					AdjustSlidingWindow();
				};
				void AdjustSlidingWindow(void){ roundTripTimeInMilliseconds = roundTripTimeInMilliseconds + (nackRTTBenchmark.TimeInMilliSeconds() /2) ;}

				// nacks are always source sequence number
				void CreateNackBadSequenceNumber(NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& outgoing)
				{
					if (!this->transmitFirstPacket) return;
					// nack the sequence number we were expecting hence we copy it
					outgoing.NackContents.source_sequenceNumber = sourceSequenceNumber;
					outgoing.SetNackAndInstructToRetransmit();
				};

				void SendPacketAndClose(NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& it)
				{
					if (!this->transmitFirstPacket) return;
					it.CheckIfWeCanCompress();
					UDP::UDPDatagram< NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>>::SendPacketAndClose(it, _ipaddress, _parameters._incomingPortApplicationLayerAddress);
				};
			protected:
				void AdjustOutgoingSequenceNumber(uint8_t& outgoing_seqNumber, NackProtocol<TFlyWeightServerOutgoing, TInitializtionParameters>& outgoing)
				{
					outgoing.NackContents.source_sequenceNumber = outgoing_seqNumber;
					outgoing_seqNumber += outgoing.size();
					destinationNackBuffer.Assign(outgoing);
				}
			};
		}
	}
}
#endif
