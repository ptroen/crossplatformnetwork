#ifndef NACK_SERVER_PROTOCOL_STATE_H
#define NACK_SERVER_PROTOCOL_STATE_H
#include "NackProtocolBase.h"
#include <string>
#include <memory>
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace NACK
		{
			template<class TFlyWeightIncoming, class  TFlyWeightOutgoing, class TSession, class TInitializationParameters>
			class NackServerProtocolState
			{
				typedef NackProtocolBase<TFlyWeightIncoming, TFlyWeightOutgoing, TSession,TInitializationParameters> nackProtocolState;
			public:
				std::map<std::string, std::shared_ptr<nackProtocolState>> userMap;
			private:
				TInitializationParameters& _parameters;
				TSession& _session;
			public:
				uint16_t _port;
				NackServerProtocolState(TInitializationParameters& parameters,TSession& session) :userMap(), _parameters(parameters),_session(session)
				{
					
				};
				std::shared_ptr <NackProtocolBase<TFlyWeightIncoming, TFlyWeightOutgoing, TSession,  TInitializationParameters>>& operator[] (const std::string& ipAddress)
				{
					if (userMap.find(ipAddress) == userMap.end())
					{
						userMap.insert(std::pair<std::string, std::shared_ptr<nackProtocolState>>(ipAddress, std::make_shared<nackProtocolState>(ipAddress, _parameters,_session)));
					}
					return userMap[ipAddress];
				};
			}; // end NackServerProtocolState
		} // end NACK
	} // end Transport
} // end OSI
#endif