#ifndef TCP_SESSION_WRAPPER_H
#define TCP_SESSION_WRAPPER_H
#ifndef _WIN32
#define BOOST_ASIO_HAS_EPOLL
#endif
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <memory>
#include <utility>
#include "../../Session/IPSecurity/IPSecurity.h"
#include "../interface/IConnectionPool.h"
#include "../handler_memory.h"


namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class TCPSessionWrapper //: public virtual IConnectionPoolObject< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>
			{
                // The memory to use for handler-based custom memory allocation.
                OSI::Transport::handler_memory handler_memory_;
				boost::asio::ip::tcp::socket _socket;
				char* incomingBuffer;
				char* outgoingBuffer;
				size_t incomingBufferSize;
				size_t outgoingBufferSize;
				TFlyWeightServerIncoming incoming;
				TFlyWeightServerOutgoing outgoing;
				TInitializationParameters& _parameters;
				bool _closeOnZeroOutput; // closes connection if ServerSessionHook reported 0
			public:
				TServerSession& _session;

				TCPSessionWrapper(boost::asio::ip::tcp::socket socket, bool closeOnZeroInput,TInitializationParameters& parameters, TServerSession& session): _socket(std::move(socket)),
				_session(session),
				 _parameters(parameters),
				   incoming(parameters),outgoing(parameters),
				   _closeOnZeroOutput(closeOnZeroInput),
				   handler_memory_() {
					Init();
				};
			private:
				void Init(void)
				{
					incomingBufferSize = incoming.max_size();
					outgoingBufferSize = outgoing.max_size();
					incomingBuffer = new char[incomingBufferSize];
					outgoingBuffer = new char[outgoingBufferSize];
				};
			public:
				static void start(std::shared_ptr<TCPSessionWrapper> wrapper)
				{
				    try
				    {
                        auto that = wrapper.get();
                        that->_socket.async_read_some(boost::asio::buffer((void*)(&that->incomingBuffer[0]), that->incomingBufferSize), // end buffer
                           make_custom_alloc_handler(that->handler_memory_, boost::bind(inputHandler,
                                wrapper,
                                boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred)) // end bind
                        );  // end read_some
					}
					catch(std::exception ex)
					{
						std::string msg = "TCPSessionWrapper::start() exception reads:";
						msg += ex.what();
						LOGIT1(msg)
					}
				}
			protected:
				static void inputHandler(std::shared_ptr<TCPSessionWrapper> wrapper,const boost::system::error_code& error, std::size_t bytes_transferred)
				{
                    try
                    {
                        auto that = wrapper.get();
                        std::string ipaddress = that->_socket.remote_endpoint().address().to_string();
                        if (error)
                        {
                            try
                            {
                                auto errormessage = error.message();
                                Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, errormessage);
                            }
                            catch(std::exception ex)
                            {
                                std::string msg = "TCPSessionWrapper::InputHandler Exception ";
                                msg+=ex.what();
                                msg +=" Error code reads:";
                                msg += error.message();
                                LOGIT1(msg)
                            }
                        }
                        else
                        {
                            try
                            {
								char* ptr= that->incomingBuffer;
                                that->incoming.FromString(ptr, bytes_transferred);
                                bool status = that->_session.IncomingHook(that->incoming, that->outgoing, ipaddress);
                                if (status)
                                {
                                    size_t length = that->outgoing.size(); // ToString can change the length so we cache it
                                    if (!length) return;
                                    that->outgoing.ToString(&that->outgoingBuffer[0], length);
                                    boost::asio::async_write(that->_socket, boost::asio::buffer(&that->outgoingBuffer[0], length),
                                        make_custom_alloc_handler(that->handler_memory_, boost::bind(&outputHandler,
                                            wrapper,
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred))
                                    );
                                }
                            }
                            catch(std::exception ex)
                            {
                                std::string msg = "TCPSessionWrapper::InputHandler Exception ";
                                msg += ex.what();
                                LOGIT1(msg)
                            }
                        }
					}
					catch(std::exception ex)
					{
						std::string msg = "TCPSessionWrapper::InputHandler General Exception ";
						msg += ex.what();
						LOGIT1(msg)
					}
				};
				static void outputHandler(std::shared_ptr<TCPSessionWrapper> wrapper,const boost::system::error_code& error, std::size_t bytes_transferred)
				{
                    try
                    {
                        auto that = wrapper.get();
                        if (that->_closeOnZeroOutput)
                            that->_socket.lowest_layer().close();
                    }
                    catch(std::exception ex)
                    {
                        std::string msg = "TCPSessionWrapper::outputHandler General Exception ";
                        msg += ex.what();
                        msg += "bytes transfered";
                        msg += bytes_transferred;
                        LOGIT1(msg)
                    }
				};
			}; // end class
		}
	}
}
#endif
