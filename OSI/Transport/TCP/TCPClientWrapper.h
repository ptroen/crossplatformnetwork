#ifndef TCP_CLIENT_WRAPPER_H
#define TCP_CLIENT_WRAPPER_H
#include "../../Session/IOContextManager.h"
#include "../../../algorithm/circularbuffer.h"
#include "../UDP/udp_baseWrapper.h"
#include <iostream>
#include "../../Session/IPSecurity/IPSecurity.h"
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			// citation: How to perform a nonblocking read with boost asio https://stackoverflow.com/questions/456042/how-do-i-perform-a-nonblocking-read-using-asio
			template < class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TInitializationParameters>
			class TCPClientWrapper
			{
				boost::asio::ip::tcp::acceptor* _acceptor;
				TInitializationParameters& _parameters;
				TSession& _session;
				typedef UDP::udp_baseWrapper< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> tcpbase;
				typedef Algorithm::CircularBuffer<tcpbase, TInitializationParameters> circular;
				boost::asio::ip::tcp::socket _socket;
				circular readWriteBuffer;
				boost::system::error_code  ec;
				boost::asio::ip::tcp::resolver::results_type endpoints;
			public:
				TCPClientWrapper(const std::string& ipaddress, std::string& request,TInitializationParameters& parameters, TSession& session) : _acceptor(nullptr), _parameters(parameters), _session(session),
					readWriteBuffer(parameters.incomingReadWriteBufferSize, parameters),
					_socket(Session::IOContextManager::io_context)
				{
					boost::asio::ip::tcp::resolver resolver(Session::IOContextManager::io_context);
					std::ostringstream ss;
					ss << _parameters._incomingPortApplicationLayerAddress;

					boost::asio::ip::tcp::resolver::query query(ipaddress.c_str(), ss.str());
					endpoints = resolver.resolve(query);

					boost::asio::connect(_socket, endpoints);
					_socket.write_some(boost::asio::buffer(const_cast<char*>(request.c_str()), request.size()), ec);

					read_some();
				}
				void read_some()
				{
					tcpbase& tcpBase = readWriteBuffer.GrabNewElement();
					_socket.async_read_some(boost::asio::buffer(const_cast<char*>(tcpBase.outgoingString.c_str()), tcpBase.outgoingString.size()), // end buffer  
						boost::bind(inputHandler,
							this,
							&tcpBase,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred) // end bind
					);
				};

			protected:
				static void inputHandler(TCPClientWrapper* that,tcpbase* tcpBase, const boost::system::error_code& error, std::size_t bytes_transferred)
				{
					try
					{
						std::string ipaddress = that->_socket.remote_endpoint().address().to_string();
						if (error)
						{
							auto msg = error.message();
							Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, msg);
						}
						else
						{
							tcpBase->outgoing.FromString(const_cast<char*>(tcpBase->outgoingString.c_str()), bytes_transferred);
							bool writeData = that->_session.IncomingHook(tcpBase->outgoing, tcpBase->incoming, ipaddress);
							if (writeData)
							{
								size_t length = tcpBase->incoming.size(); // ToString can change the length so we cache it
								if (length)
								{
									tcpBase->incoming.ToString(const_cast<char*>(tcpBase->incomingString.c_str()), length);
									boost::asio::async_write(that->_socket, boost::asio::buffer(const_cast<char*>(tcpBase->incomingString.c_str()), length),
										boost::bind(&outputHandler,
											boost::asio::placeholders::error,
											boost::asio::placeholders::bytes_transferred)
									);
								}
							}
							if (that->_parameters.closeConnectionOnZeroOutput)
								that->_socket.close();
							else
							{
								that->read_some();
							}
						}
					}
					catch (std::exception ex)
					{
						std::string msg = ex.what();
						LOGIT(msg)
					}
				}

				static void outputHandler(const boost::system::error_code& error, std::size_t bytes_transferred)
				{
					
				};
			};
		}
	}
}

#endif
