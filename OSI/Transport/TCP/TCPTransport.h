#ifndef TCP_Transport_H
#define TCP_Transport_H
#include "TCPClientServerWrapper.h"
#include "../../Session/TimerHooks/TimerHooks.h"
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class TCP_ServerTransport : protected virtual Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>
			{
			public:
				Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters> _session;
				TCP_ServerTransport(void) : Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>(TInitializationParameters()), _session(TInitializationParameters()) {};
				TCP_ServerTransport(TInitializationParameters& params) : Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>(params), _session(params){};
				// use boost asio
				virtual void RunServer(void) {
                    while(1)
                    {
                       try
                       {
                            TCPClientServerWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>,TInitializationParameters> server(Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>::_parameters, _session);
                            server.RunServer(Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>::_parameters._incomingPortApplicationLayerAddress);
                            Session::IOContextManager::io_context.run();
					   }
					   catch(std::exception ex)
					   {
					      std::string msg = "Exception on RunServer. Exception reads:";
					      msg += ex.what();
					      msg += ".Rerunning server.";
					      LOGIT1(msg)
					      msg = "Rebooting IOContext";
					      Session::IOContextManager::Reboot(); // make new object to stop immediately transactions
					   }
					} // end while(1)
				};
			}; // end class
		}
	}
}
#endif
