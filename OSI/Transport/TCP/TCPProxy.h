#ifndef TCP_PROXY_H
#define TCP_PROXY_H
#include "TCPTransport.h"
#include <string>
#include "../../../algorithm/Math/Statistics/BellCurve.h"
#include "../interface/iproxytransport.h"
#include "TCPDatagram.h"
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TInitializationParameters>
			class ExternalProxySession : Interface::IProxyTransport
			{
				static inline ExternalProxySession* instance = nullptr;
				TExternalSession _session;
				TInitializationParameters& _parameters;
				Algorithm::Math::Statistics::BellCurve noiseBellCurveGenerator;
			public:
				static ExternalProxySession* GetInstance(void) { return instance; };
				ExternalProxySession(TInitializationParameters& parameters) :_session(parameters),
				_parameters(parameters),
				noiseBellCurveGenerator(parameters.noiseMin, parameters.noiseMax, parameters.noiseDeviation) { };

				virtual bool IncomingHook(TFlyWeightServerIncoming& incoming, TFlyWeightServerOutgoing& outgoing, std::string& ipadress)
				{
					if (instance == nullptr) { instance = this; }
					if (ipadress == _parameters.externalIP)
					{
						if (!_parameters.enableNoise ||  noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							std::string reply = TCPDatagram<TFlyWeightServerIncoming>::SendPacketAndReceive(incoming, _parameters.internalIP, _parameters._incomingPortApplicationLayerAddress);
							size_t size = reply.size();
							outgoing.FromString(const_cast<char*>(reply.c_str()), size);
							return true;
						}
					}
					else if(ipadress == _parameters.internalIP)
					{
						if (!_parameters.enableNoise || noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							std::string reply = TCPDatagram<TFlyWeightServerIncoming>::SendPacketAndReceive(incoming, _parameters.externalIP, _parameters._incomingPortApplicationLayerAddress);
							size_t size = reply.size();
							outgoing.FromString(const_cast<char*>(reply.c_str()), size);
							return true;
						}
					}
					return false;
				}
				virtual void OnTimerHook(void)
				{
					_session.OnTimerHook();
				}
				virtual void OnIdleHook(void)
				{
					_session.OnIdleHook();
				}
			};

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TTransportInitializationParameters >
			class TCPProxyTransport
			{
			public:
				TCP_ServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, ExternalProxySession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TExternalSession, TInternalSession, TTransportInitializationParameters>, TTransportInitializationParameters> externalServer;

				TCPProxyTransport(TTransportInitializationParameters& parameters) :externalServer(parameters) {};

				virtual void RunServer()
				{
					externalServer.RunServer();
				};
			};
		
		}
	}
}

#endif
