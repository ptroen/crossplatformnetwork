#ifndef TCP_CLIENT_TRANSPORT_H
#define TCP_CLIENT_TRANSPORT_H
#include "../interface/ITransportInitializationParameters.h"
#include "../interface/IClientTransport.h"
#include "TCPClientWrapper.h"
#include "../../Session/TimerHooks/TimerHooks.h"
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			template < class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class TCP_ClientTransport : public virtual Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>
			{
				public:
				Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters> session;
				TCP_ClientTransport(TTransportInitializationParameters& parameters): Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>(parameters),
				session(parameters){};
				// use boost asio
				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request) {
					
					std::string requestString;
					size_t payloadSize = request.size();
					requestString.resize(payloadSize);
					request.ToString(&requestString[0], payloadSize);
					TCPClientWrapper<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters> client(ipAddress, requestString,Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters,session);
					Session::IOContextManager::io_context.run();
				};
			}; // end class
		} // end TCP
	} // end Transport
} // end OSI
#endif
