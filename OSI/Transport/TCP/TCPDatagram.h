#ifndef TCP_DATAGRAM_H
#define TCP_DATAGRAM_H
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <sstream>
#include "../../Session/IOContextManager.h"
#include <boost/asio.hpp>
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			template <class TDatagram>
			class TCPDatagram
			{
				public:
				static std::string SendPacketAndReceive(TDatagram& datagram, std::string& ipAddress, size_t port) {
							boost::asio::ip::tcp::resolver resolver(Session::IOContextManager::io_context);
							std::ostringstream ss;
							ss << port;
							boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v6(), ipAddress, ss.str().c_str());
							boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

							boost::asio::ip::tcp::socket socket(Session::IOContextManager::io_context);
							boost::asio::connect(socket, iterator);

							std::string request;
							request.resize(datagram.size());
							datagram.ToString(const_cast<char*>(request.c_str()), datagram.size());
							boost::asio::write(socket, boost::asio::buffer(&request[0], request.size()));

							std::string reply;
							size_t replysize = datagram.max_size();
							reply.resize(replysize);
							
							size_t reply_length = boost::asio::read(socket,boost::asio::buffer(&reply[0], replysize));
							reply.resize(reply_length);
							return reply;
				};
			};
		}
	}
}
#endif