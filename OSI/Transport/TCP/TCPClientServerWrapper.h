#ifndef TCP_CLIENT_SERVER_WRAPPER_H
#define TCP_CLIENT_SERVER_WRAPPER_H
#include "TCPSessionWrapper.h"
#include "../../Session/IOContextManager.h"
#include <iostream>
namespace OSI
{
	namespace Transport
	{
		namespace TCP
		{
			// citation: How to perform a nonblocking read with boost asio https://stackoverflow.com/questions/456042/how-do-i-perform-a-nonblocking-read-using-asio
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class TCPClientServerWrapper : public std::enable_shared_from_this<TCPSessionWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>>
			{
				boost::asio::ip::tcp::acceptor* _acceptor;
				TInitializationParameters& _parameters;
				TServerSession& _session;
			public:
				TCPClientServerWrapper(TInitializationParameters& parameters, TServerSession& session) : _acceptor(nullptr), _parameters(parameters),_session(session)
				{

				}

				void RunServer(uint16_t port)
				{

					_acceptor = new boost::asio::ip::tcp::acceptor(Session::IOContextManager::io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port));
					 do_accept();
				};
			private:
				void do_accept(void)
				{
					try
					{
                        _acceptor->async_accept(
						[this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket)
                        {
						if (!ec)
						{
							std::shared_ptr<TCPSessionWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>> shared =std::make_shared<TCPSessionWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>>(std::move(socket), false,_parameters,_session);
							shared->start(shared);
						}
						else
						{
                            std::string msg = "Error code line 46:";
                            msg += ec.message();
                            LOGIT1(msg)
						}
						do_accept();
                        });
                    }
                    catch (std::exception ex)
                    {
                        std::string msg = "RunServer Exception in TCPClientServerWrapper line 53:";
                        msg += ex.what();
                        LOGIT1(msg)
                    }
				}
			};
		}
	}
}

#endif
