#ifndef ALL_PROXY_H
#define ALL_PROXY_H
#include "AllServer.h"
#include "AllClient.h"
#include "../interface/iproxytransport.h"
#include <string>
#include "../../../algorithm/Math/Statistics/BellCurve.h"
namespace OSI
{
	namespace Transport
	{
		namespace All
		{
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TInitializationParameters>
				class ExternalProxySession;

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TInitializationParameters>
			class InternalProxySession
			{
			public:
				std::vector<std::string> incomingQueue;
				TInternalSession session;
				TInitializationParameters& _parameters;
				Algorithm::Math::Statistics::BellCurve noiseBellCurveGenerator;
				InternalProxySession(TInitializationParameters& parameters) :session(parameters), _parameters(parameters), noiseBellCurveGenerator(parameters.noiseMin, parameters.noiseMax, parameters.noiseDeviation) { };
				virtual bool IncomingHook(TFlyWeightServerOutgoing& in, TFlyWeightServerIncoming& out, std::string& ipadress)
				{
					// Session
					bool status = false;
					std::string payload;
					if (ipadress == _parameters.internalIP)
					{
						if (noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							ExternalProxySession< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TExternalSession, TInternalSession, TInitializationParameters>::GetInstance()->SendToExternal(payload);
						}
						// if we have a message to route
						if (!incomingQueue.empty())
						{
							// process the next by sending it out that is just return outgoing
							std::string packet = incomingQueue.back();
							incomingQueue.pop_back();
							size_t size = packet.size();
							out.FromString(const_cast<char*>(packet.c_str()), size);
						}
						status = true;
					}

					// only accept from internal ip
					return status; // no more data close the connection
				}
				// multicast hook
				virtual bool IncomingHook(TFlyWeightServerOutgoing& outgoing) { return true; }
				virtual bool IncomingHook(TFlyWeightServerIncoming& outgoing) { return true; }
				void SendToInternal(std::string& flyweight) {};
				virtual void OnTimerHook(void)
				{
					session.OnTimerHook();
				}
				virtual void OnIdleHook(void)
				{
					session.OnIdleHook();
				}
			};

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TInitializationParameters>
			class ExternalProxySession : Interface::IProxyTransport
			{
				static inline ExternalProxySession* instance = nullptr;
				TExternalSession _session;
				std::vector<std::string> outgoingQueue;
				TInitializationParameters& _parameters;
				Algorithm::Math::Statistics::BellCurve noiseBellCurveGenerator;
			public:
				static ExternalProxySession* GetInstance(void) { return instance; };
				ExternalProxySession(TInitializationParameters& parameters) :_session(parameters), _parameters(parameters), internalConnection(parameters), noiseBellCurveGenerator(parameters.noiseMin, parameters.noiseMax, parameters.noiseDeviation) { };
				AllClient<TFlyWeightServerOutgoing,
					TFlyWeightServerIncoming,
					InternalProxySession<TFlyWeightServerIncoming,TFlyWeightServerOutgoing, TExternalSession, TInternalSession, TInitializationParameters>,
					TInitializationParameters>  internalConnection;

				// data
				void SendToExternal(std::string& payload) { outgoingQueue.push_back(payload); };

				virtual bool IncomingHook(TFlyWeightServerOutgoing& outgoing) { return true; }
				virtual bool IncomingHook(TFlyWeightServerIncoming& outgoing) { return true; }
				virtual bool IncomingHook(TFlyWeightServerIncoming& incoming, TFlyWeightServerOutgoing& outgoing, std::string& ipadress)
				{
					if (instance == nullptr) { instance = this; }
					// Session
					// all incoming is assumed external

					// internal is transmitted on another port
					bool status = false;
					// finally run the hook to see if anything else todo(like custom reporting)
					// no need to lookup just use the custom router internal as it will always find a connection
					if (ipadress == _parameters.externalIP)
					{
						if (noiseBellCurveGenerator.GenerateBool())
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(_parameters.delayInMilliseconds));
							std::string s;
							size_t dSize = incoming.size();
							s.resize(dSize);
							incoming.ToString(const_cast<char*>(s.c_str()), dSize);
							internalConnection.GetSession().SendToInternal(s);
						}
						if (!outgoingQueue.empty())
						{
							std::string packet = outgoingQueue.back();
							outgoingQueue.pop_back();
							size_t size = packet.size();
							outgoing.FromString(const_cast<char*>(packet.c_str()), size);
						}
						return true;
					}
					return status;
				}
				virtual void OnTimerHook(void)
				{
					_session.OnTimerHook();
				}
				virtual void OnIdleHook(void)
				{
					_session.OnIdleHook();
				}
			};

			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TExternalSession, class TInternalSession, class TTransportInitializationParameters >
			class AllProxyTransport
			{
			public:
				AllServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, ExternalProxySession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TExternalSession, TInternalSession, TTransportInitializationParameters>, TTransportInitializationParameters> externalServer;

				AllProxyTransport(TTransportInitializationParameters& parameters) :externalServer(parameters) {};

				virtual void RunServer()
				{
					externalServer.RunServer();
				};
			};
		}
	}
}
#endif
