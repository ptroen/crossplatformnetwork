#ifndef ALL_SERVER_H
#define ALL_SERVER_H
#include "../HTTP/HTTPTransport.h"
#include "../HTTPS/HTTPSTransport.h"
#include "../Nack/NackTransport.h"
#include "../UDP/udptransport.h"
#include "../RTP/RTPTransport.h"
#include "protocolselection.h"
#include "../UDPMulticast/UDPMulticastTransport.h"
#include <stdexcept>
namespace OSI
{
	namespace Transport
	{
		namespace All
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class AllServerTransport
			{
				public:
					TInitializationParameters& _parameters;
					
					struct ProtocolTypes
					{
						ProtocolSelection protocol;
						TCP::TCP_ServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* tcp;
						HTTP::HTTPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* http;
						HTTPS::HTTPSTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* https;
						NACK::NackServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* nack;
						UDP::UDPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* udp;
						RTP::RTPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* rtp;
						UDPMulticast::UDPMulticastServerTransport<TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* udp_multicast;
						SSL::SSL_ServerTransport<TFlyWeightServerIncoming,TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>* ssl;
					};
					ProtocolTypes protocol;
					AllServerTransport(TInitializationParameters& parameters):protocol(),_parameters(parameters)
					{
						protocol.protocol = parameters.protocol;
						ProtocolSelection p = protocol.protocol;
					};

					virtual void RunServer(void)
					{
						ProtocolSelection p = protocol.protocol;
						switch (p)
						{
						case ProtocolSelection::UDP: protocol.udp = new UDP::UDPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case	ProtocolSelection::TCP: protocol.tcp = new TCP::TCP_ServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case ProtocolSelection::HTTP: protocol.http = new HTTP::HTTPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case ProtocolSelection::HTTPS: protocol.https = new HTTPS::HTTPSTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case ProtocolSelection::NACK: protocol.nack = new NACK::NackServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case ProtocolSelection::RTP: protocol.rtp = new RTP::RTPServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						case ProtocolSelection::UDPMulticast: protocol.udp_multicast = new UDPMulticast::UDPMulticastServerTransport<TFlyWeightServerOutgoing, TServerSession, TInitializationParameters >(_parameters); break;
						case ProtocolSelection::SSL: protocol.ssl = new SSL::SSL_ServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>(_parameters); break;
						}
						switch (p)
						{
						case ProtocolSelection::UDP: protocol.udp->RunServer(); break;
						case	ProtocolSelection::TCP: protocol.tcp->RunServer(); break;
						case ProtocolSelection::HTTP: protocol.http->RunServer(); break;
						case ProtocolSelection::HTTPS: protocol.https->RunServer(); break;
						case ProtocolSelection::NACK: protocol.nack->RunServer(); break;
						case ProtocolSelection::RTP: protocol.rtp->RunServer(); break;
						case ProtocolSelection::UDPMulticast: protocol.udp_multicast->RunServer(); break;
						case ProtocolSelection::SSL: protocol.ssl->RunServer(); break;
						}
					}

					TServerSession& GetSession(void)
					{
						ProtocolSelection p = protocol.protocol;
						switch (p)
						{
						case ProtocolSelection::UDP: return protocol.udp->_session;
						case	ProtocolSelection::TCP: return protocol.tcp->_session;
						case ProtocolSelection::HTTP: return protocol.http->_session;
						case ProtocolSelection::HTTPS: return protocol.https->_session; break;
						case ProtocolSelection::NACK: return protocol.nack->_session; break;
						case ProtocolSelection::RTP: return protocol.rtp->_session; break;
						case ProtocolSelection::UDPMulticast:  return protocol.udp_multicast->_session; break;
						case ProtocolSelection::SSL: return protocol.ssl->_session; break;
						default: throw new std::runtime_error(std::string("no protocol"));
						}
					};
			};

		}
	};
};

#endif
