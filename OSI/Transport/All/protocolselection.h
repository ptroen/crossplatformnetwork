#ifndef PROTOCOL_SELECTION_H
#define PROTOCOL_SELECTION_H
#include "../../../ThirdParty/enum.h"
namespace OSI
{
	namespace Transport
	{
		namespace All
		{
			BETTER_ENUM(ProtocolSelection, uint16_t, NOSELECTION = 0,UDP = 1,TCP = 2,HTTP = 3,HTTPS = 4,NACK = 5,RTP = 6,UDPMulticast = 7,SSL = 8)
		}
	}
}
#endif