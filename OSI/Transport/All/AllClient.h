#ifndef ALL_CLIENT_H
#define ALL_CLIENT_H
#include "../TCP/TCPClientTransport.h"
#include "../HTTP/HTTPClientTransport.h"
#include "../HTTPS/HTTPSClientTransport.h"
#include "../UDP/UDPClientTransport.h"
#include "../Nack/NackClientTransport.h"
#include "../RTP/RTPClientTransport.h"
#include "../SSL/sslclienttransport.h"
#include "../UDPMulticast/UDPMulticastClientTransport.h"
#include "protocolselection.h"
#include <stdexcept>
namespace OSI
{
	namespace Transport
	{
		namespace All
		{
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
			class AllClient
			{
				TTransportInitializationParameters& _parameters;
				public:
					struct ClientProtocolType
					{
						ProtocolSelection protocol;
						TCP::TCP_ClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>* tcp;
						HTTP::HTTPClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>* http;
						HTTPS::HTTPSClientTransport<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>* https;
						NACK::NackClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>* nack;
						UDP::UDPClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>* udp;
						RTP::RTPClientTransport< TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>* rtp;
						UDPMulticast::UDPMulticastClientTransport<TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>* udp_multicast;
						SSL::SSLClientTransport<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>* ssl;
					};

					ClientProtocolType protocol;

					AllClient(TTransportInitializationParameters& parameters) :_parameters(parameters),protocol(){
						protocol.protocol = parameters.protocol;
						ProtocolSelection p = protocol.protocol;
						switch (p)
						{
						case ProtocolSelection::UDP: protocol.udp = new UDP::UDPClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>(_parameters); break;
						case	ProtocolSelection::TCP: protocol.tcp = new TCP::TCP_ClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::HTTP: protocol.http = new HTTP::HTTPClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::HTTPS: protocol.https = new HTTPS::HTTPSClientTransport<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::NACK: protocol.nack = new NACK::NackClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::RTP: protocol.rtp = new RTP::RTPClientTransport<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::UDPMulticast: protocol.udp_multicast = new UDPMulticast::UDPMulticastClientTransport<TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>(_parameters); break;
						case ProtocolSelection::SSL: protocol.ssl = new SSL::SSLClientTransport<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>(_parameters); break;
						}
					};

					virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request) {
						ProtocolSelection p = protocol.protocol;
						switch (p)
						{
						case ProtocolSelection::UDP: protocol.udp->RunClient(ipAddress, request); break;
						case	ProtocolSelection::TCP: protocol.tcp->RunClient(ipAddress, request); break;
						case ProtocolSelection::HTTP: protocol.http->RunClient(ipAddress, request); break;
						case ProtocolSelection::HTTPS: protocol.https->RunClient(ipAddress, request); break;
						case ProtocolSelection::NACK: protocol.nack->RunClient(ipAddress, request); break;
						case ProtocolSelection::RTP: protocol.rtp->RunClient(ipAddress, request); break;
						case ProtocolSelection::UDPMulticast: protocol.udp_multicast->RunClient(); break;
						case ProtocolSelection::SSL: protocol.ssl-> RunClient(ipAddress, request); break;
						}
					};

					TSession& GetSession(void)
				{
					ProtocolSelection p = protocol.protocol;
					
					switch (p)
					{
					case ProtocolSelection::UDP: return protocol.udp->session.session;
					case	ProtocolSelection::TCP: return protocol.tcp->session.session;
					case ProtocolSelection::HTTP: return protocol.http->session.session.session;
					case ProtocolSelection::HTTPS: return protocol.https->_session.session.session;
					case ProtocolSelection::NACK: return protocol.nack->session.session.session;
					case ProtocolSelection::RTP: return protocol.rtp->session.session.session;
					case ProtocolSelection::UDPMulticast:  return protocol.udp_multicast->_session.session;
					case ProtocolSelection::SSL: return protocol.ssl->_session.session;
					default:
						throw new std::runtime_error("no protocol");
					}
				};
				};
			};
		}
	}

#endif
