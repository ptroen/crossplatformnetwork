#include "IPType.h"

bool OSI::Transport::IsIPV6(const std::string& ip){
				auto ad=boost::asio::ip::make_address(ip.c_str());
				return ad.is_v6();
			}
			
			bool OSI::Transport::ISIPV4(const std::string& ip){
				auto ad=boost::asio::ip::make_address(ip.c_str());
				return ad.is_v4();
			}