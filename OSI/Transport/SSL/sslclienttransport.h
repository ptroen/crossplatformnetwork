#ifndef SSL_CLIENT_TRANSPORT_H
#define SSL_CLIENT_TRANSPORT_H
#include "../interface/ITransportInitializationParameters.h"
#include "../interface/ISSLTransportInitializationParameters.h"
#include "../interface/IClientTransport.h"
#include "../../Session/TimerHooks/TimerHooks.h"
#include "../../Session/IOContextManager.h"
#include "../SSL/sslclientsessionwrapper.h"
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <string>
#ifdef _WIN32 // visual C++ directives for linking
#pragma comment(lib,"libcryptoMD.lib")
#pragma comment(lib,"libsslMD.lib")
#endif
namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
			class SSLClientTransport : public virtual Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>
			{
				public:
					Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters> _session;
				SSLClientTransport(TTransportInitializationParameters& parameters): Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>(parameters),
				_session(parameters){};
				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
				{
					// worked from this example: https://www.boost.org/doc/libs/1_52_0/doc/html/boost_asio/example/ssl/client.cpp and placed it within SSLSessionWrapper(see below)
					boost::asio::ip::tcp::resolver resolver(Session::IOContextManager::io_context);
					std::ostringstream port;
					port << Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters._incomingPortApplicationLayerAddress;
					boost::asio::ip::tcp::resolver::query query(ipAddress, port.str());
					boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
					boost::asio::ssl::context* sslcontext = nullptr;
					switch (Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters.GetClientSSL())
					{
					
					case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS11:
						sslcontext = new boost::asio::ssl::context(boost::asio::ssl::context::tlsv11_client);
						break;
					case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS12:
						sslcontext = new boost::asio::ssl::context(boost::asio::ssl::context::tlsv12_client);
						break;
					case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS13:
						sslcontext = new  boost::asio::ssl::context(boost::asio::ssl::context::tlsv13_client);
						break;
					case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS:
					default:
						sslcontext = new  boost::asio::ssl::context(boost::asio::ssl::context::tls_client);
						break;
					}
					//boost::asio::ssl::context sslcontext(boost::asio::ssl::context::tls);
					sslcontext->load_verify_file(Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters.clientCertificateFileName);
					SSLClientSessionWrapper< TFlyWeightServerOutgoing, TFlyWeightServerIncoming, Session::TimerHooks::ClientTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters> ssl_client(*sslcontext, Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters,_session);
					ssl_client.RunClient(iterator, Interface::IClientTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters>::_parameters._incomingPortApplicationLayerAddress,request);
					Session::IOContextManager::io_context.run();
				};
			};
		}
	}
}

#endif
