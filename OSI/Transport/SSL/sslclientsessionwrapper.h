#ifndef SSL_CLIENT_SESSION_WRAPPER_H
#define SSL_CLIENT_SESSION_WRAPPER_H
#include "SSLSessionWrapper.h"
#include "../UDP/udp_baseWrapper.h"
#include "../../../algorithm/circularbuffer.h"
namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
			class SSLClientSession
			{
			private:
				ssl_socket _socket;
				typedef UDP::udp_baseWrapper< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TTransportInitializationParameters> sslbase;
				typedef Algorithm::CircularBuffer<sslbase, TTransportInitializationParameters> sslcircular;
				sslcircular readWriteBuffer;
				TSession& _session;
				TTransportInitializationParameters& _parameters;
				bool first;
			public:
				SSLClientSession(boost::asio::io_context& io_context, boost::asio::ssl::context& context, TTransportInitializationParameters& parameters, TSession& session)
					: _socket(io_context, context),_session(session),_parameters(parameters), readWriteBuffer(parameters.incomingReadWriteBufferSize, parameters)
				{
					first = true;
				}
				bool client_verify_certificate(bool preverified, boost::asio::ssl::verify_context& ctx)
				{
					// The verify callback can be used to check whether the certificate that is
					// being presented is valid for the peer. For example, RFC 2818 describes
					// the steps involved in doing this for HTTPS. Consult the OpenSSL
					// documentation for more details. Note that the callback is called once
					// for each certificate in the certificate chain, starting from the root
					// certificate authority.

					// In this example we will simply print the certificate's subject name.
					char subject_name[256];
					X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
					X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
					std::cout << "Verifying Cerificate subject name: " << subject_name << "\n";

					return true;
				}

				void ClientConnect(boost::asio::ip::tcp::resolver::iterator& endpoint_iterator,TFlyWeightServerIncoming& request)
				{
					_socket.set_verify_mode(boost::asio::ssl::verify_peer);
					_socket.set_verify_callback(boost::bind(&SSLClientSession::client_verify_certificate, this, _1, _2));
					boost::asio::async_connect(_socket.lowest_layer(), endpoint_iterator, boost::bind(&SSLClientSession::handleConnect, this,request, boost::asio::placeholders::error));
				}

				void handleConnect(TFlyWeightServerIncoming& request,const boost::system::error_code& error)
				{
					if (!error)
					{
						_socket.async_handshake(boost::asio::ssl::stream_base::client,
							boost::bind(&SSLClientSession::handle_handshake, this, request,
								boost::asio::placeholders::error));
					}
					else
					{
						std::cout << error << std::endl;
					}
				};

				ssl_socket::lowest_layer_type& socket()
				{
					return _socket.lowest_layer();
				}
			private:
				void async_write(sslbase* s)
				{
					sslbase& ref = *s;
					boost::asio::async_write(_socket,
						boost::asio::buffer(const_cast<char*>(ref.incomingString.c_str()), ref.incoming.size()),
						boost::bind(&SSLClientSession::outputHandler,
							this,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred)
					);
				};

				void read(void)
				{
					sslbase& rw = readWriteBuffer.GrabNewElement();
						_socket.async_read_some(boost::asio::buffer(const_cast<char*>(rw.outgoingString.c_str()), rw.outgoing.max_size()),
							boost::bind(&SSLClientSession::inputHandler, this, &rw,
								boost::asio::placeholders::error,
								boost::asio::placeholders::bytes_transferred));
				}

				void inputHandler(sslbase* sslbase, const boost::system::error_code& error, std::size_t bytes_transferred)
				{
					std::string ipaddress = _socket.lowest_layer().remote_endpoint().address().to_string();
					if (!error && bytes_transferred > 0)
					{
						sslbase->outgoing.FromString(const_cast<char*>(sslbase->outgoingString.c_str()), bytes_transferred);
						bool isOutgoing = _session.IncomingHook(sslbase->outgoing, sslbase->incoming, ipaddress);
						if (isOutgoing)
						{
							size_t incomingsize = sslbase->incoming.size();
							sslbase->incoming.ToString(const_cast<char*>(sslbase->incomingString.c_str()), incomingsize);
							async_write(&(*sslbase));
						}
					}
					else
						std::cout << " input Handler error:" << error.message() << std::endl;
				};
				void outputHandler(const boost::system::error_code& error,
					std::size_t bytes_transferred)
				{
					if (_parameters.closeConnectionOnZeroOutput && !first)
						_socket.lowest_layer().close();
					else
					{
						first = false;
						read();
					}
				};

					void handle_handshake(TFlyWeightServerIncoming& request,const boost::system::error_code& error)
					{
						if (!error)
						{
							std::string s;
							s.resize(request.size());
							size_t requestsize = request.size();
							request.ToString(const_cast<char*>(s.c_str()), requestsize);
							boost::asio::async_write(_socket, boost::asio::buffer(const_cast<char*>(s.c_str()), request.size()),
								boost::bind(&SSLClientSession::outputHandler,
									this,
									boost::asio::placeholders::error,
									boost::asio::placeholders::bytes_transferred));
						}
						else
						{
							std::cout << "client_handshake had a problem:" << error.message().c_str() << std::endl;
						}
					}
			};

			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
				class SSLClientSessionWrapper
				{
					public:
						boost::asio::ip::tcp::acceptor* acceptor_;
						boost::asio::ssl::context& context_;
						bool _closeOnZeroInput;
						TTransportInitializationParameters& _parameters;
						SSLClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>* _sslsession;
						TSession& _session;
					SSLClientSessionWrapper(boost::asio::ssl::context& context, TTransportInitializationParameters& parameters, TSession& session):acceptor_(nullptr),context_(context),_parameters(parameters),
				_session(session){};
					virtual void RunClient(boost::asio::ip::tcp::resolver::iterator endpoint_iterator,uint16_t port, TFlyWeightServerIncoming& incoming)
					{
						acceptor_ = new boost::asio::ip::tcp::acceptor(Session::IOContextManager::io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port));
						_sslsession = new SSLClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>(Session::IOContextManager::io_context, context_,  _parameters,_session);
						_sslsession->ClientConnect(endpoint_iterator,incoming);
					}
				};
		}
	}
}

#endif