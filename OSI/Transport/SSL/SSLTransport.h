#ifndef SSL_TRANSPORT_H
#define SSL_TRANSPORT_H
#include <string>
#include "SSLSessionWrapper.h"
#include "../../Session/TimerHooks/TimerHooks.h"
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "../interface/ISSLTransportInitializationParameters.h"
#ifdef _WIN32 // visual C++ directives for linking
#pragma comment(lib,"libcryptoMD.lib")
#pragma comment(lib,"libsslMD.lib")
#endif
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class SSL_ServerTransport : public virtual Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>
			{
				public:
					Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters> _session;
					SSL_ServerTransport(void) :Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>(), _session(TInitializationParameters()) {};
					SSL_ServerTransport(TInitializationParameters& params) : Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>(params), _session(params)
					{
						std::string msg="SSL_ServerTransport::SSL_ServerTransport";
						LOGIT(msg)
						Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>
			::_parameters = params;
					};
				virtual void RunServer(void) {
					std::string msg="SSL_ServerTransport::RunServer";
					LOGIT(msg)
					//while(1)
					{
						SSLSessionWrapper<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, Session::TimerHooks::ServerTimerHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, TInitializationParameters> server(Interface::IServerTransport<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters>
				::_parameters,_session);

						try
						{
							server.RunServer(this->_parameters._incomingPortApplicationLayerAddress);
						}
						catch(std::exception ex)
						{
							msg="SSL_ServerTransport::ex.what()=";
							msg+=ex.what();
							LOGIT1( msg)
							Session::IOContextManager::io_context.restart();
						}
					} // end while()
				};
			};
		}
	}
}
#endif
