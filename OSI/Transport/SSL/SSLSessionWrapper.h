#ifndef SSL_SESSION_WRAPPER_H
#define SSL_SESSION_WRAPPER_H
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/context.hpp>
#include "SSLSession.h"
#include "../../Session/IOContextManager.h"
#include "../interface/ISSLTransportInitializationParameters.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
// to support tls 1.3

namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
				// reworked from class server from this url https://www.boost.org/doc/libs/1_69_0/doc/html/boost_asio/example/cpp11/ssl/server.cpp. Use as reference if implementation is wrong
				template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
				class SSLSessionWrapper
				{
				private:
					TInitializationParameters& _parameters;
					boost::asio::ip::tcp::acceptor* acceptor_;
					boost::asio::ssl::context* _context;
					TServerSession& _session;
				public:
					SSLSessionWrapper(TInitializationParameters& parameters, TServerSession& session) : acceptor_(nullptr), 
					 _parameters(parameters), _session(session), _context(nullptr)
					{
						std::string msg = "SSLSessionWrapper::SSLSessionWrapper";
						LOGIT(msg)
						switch(parameters.GetServerSSHEncryptionTypes())
						{
							case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS13:
								msg = "TLS13";
								_context = new boost::asio::ssl::context(boost::asio::ssl::context_base::method::tlsv13);
								break;
							case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS12:
								msg = "TLS12";
								_context = new boost::asio::ssl::context(boost::asio::ssl::context_base::method::tlsv12);
								break;
							case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS11:
								msg = "TLS11";
								_context = new boost::asio::ssl::context(boost::asio::ssl::context_base::method::tlsv11);
								break;
							case Transport::Interface::SSH_ENCRYPTION_TYPES::TLS:
							default:
								msg = "TLS";
								_context = new boost::asio::ssl::context(boost::asio::ssl::context_base::method::tls);
						}
						LOGIT(msg)
					};
					virtual void RunServer(uint16_t port)
					{
						std::string msg = "SSLSessionWrapper::RunServer";
						LOGIT(msg)
						acceptor_ = new boost::asio::ip::tcp::acceptor(Session::IOContextManager::io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port));
						SetSSLOptions(_parameters);
						start_accept();
						Session::IOContextManager::io_context.run();
					}

				
					void start_accept()
					{
							std::string msg = "SSLSessionWrapper::start_accept()";
							LOGIT(msg)
							std::shared_ptr<SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>> new_session = std::make_shared <SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>>( *_context, _parameters, _session);
							acceptor_->async_accept(new_session->socket(), // extract tcp socket
								boost::bind(&SSLSessionWrapper::handle_accept, this, new_session,
									boost::asio::placeholders::error));
					}

					void handle_accept(std::shared_ptr<SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>>  new_session, const boost::system::error_code& error)
					{
						if (!error)
						{
							std::string msg="SSLSessionWrapper::handle_accept::calling handshake";
							LOGIT(msg)
							new_session.get()->handshake(new_session);
						}
						else
						{
							std::string msg="SSLSessionWrapper::handle_accept::errorInHandshake";
							LOGIT(msg)							
						}
						start_accept();
					}
					std::string get_password() const { return _parameters._password; }

					void SetSSLOptions(TInitializationParameters& ssl_parameters)
					{
						std::string msg("SSLSessionWrapper::SetSSLOptions");
						LOGIT(msg)
						msg = "set_options";
						LOGIT(msg)
						_context->set_options(
							boost::asio::ssl::context::default_workarounds
							//| boost::asio::ssl::context::no_sslv2
							| boost::asio::ssl::context::single_dh_use);
						msg = "set_password_callback";
						LOGIT(msg)
						_context->set_password_callback(boost::bind(&SSLSessionWrapper::get_password, this));
						msg = "use_certificate_chain_file";
						LOGIT(msg)
						_context->use_certificate_chain_file(ssl_parameters._certificateChainFilename);
						msg = "use_private_key_file";
						LOGIT(msg)
						_context->use_private_key_file(ssl_parameters._privateKeyFilename, boost::asio::ssl::context::pem);
						msg = "use_tmp_dh_file";
						LOGIT(msg)
						_context->use_tmp_dh_file(ssl_parameters._tmpDHFile);
					};
				};
			};
		}
	}
#endif
