#include "SSLSessionWrapper.h"
namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			// reworked from class server from this url https://www.boost.org/doc/libs/1_52_0/doc/html/boost_asio/example/ssl/server.cpp. Use as reference if implementation is wrong
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, size_t(*ServerSessionHook)(TFlyWeightServerIncoming&, TFlyWeightServerOutgoing&, TServerSession&, std::string&), void(*OnIdleHook)(TServerSession&)>
				class SSLServerSessionWrapper : public virtual SSLSessionWrapper<TFlyWeightServerIncoming,TFlyWeightServerIncoming,ServerSessionHook,OnIdleHook>
				{
					public:
					SSLServerSessionWrapper(boost::asio::ssl::context& context, bool closeOnZeroInput):SSLSessionWrapper<TFlyWeightServerIncoming,TFlyWeightServerIncoming,ServerSessionHook,OnIdleHook>(context,closeOnZeroInput){};
							void start_accept()
					{
						std::string msg ="SSLServerSessionWrapper::SSLServerSessionWrapper";
						LOGIT(msg)
						SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, ServerSessionHook, OnIdleHook>* new_session = new SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, ServerSessionHook, OnIdleHook>(Session::IOContextManager::io_context, context_, _closeOnZeroInput);
						acceptor_->async_accept(new_session->socket(), boost::bind(&SSLSessionWrapper::handle_accept, this, new_session, boost::asio::placeholders::error));
					}
					void handle_accept(SSLSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, ServerSessionHook, OnIdleHook>* new_session, const boost::system::error_code& error)
					{
                        try
                        {
                            std::string msg ="SSLServerSessionWrapper::handle_accept";
                            LOGIT(msg)
                            if (!error)
                            {
                                msg = "invoking new session handshake";
                                LOGIT1(msg)
                                new_session->handshake();
                            }
                            else
                            {
                                msg ="deleting new session";
                                LOGIT1(msg)
                                delete new_session;
                            }
                            start_accept();
						}
						catch(std::exception ex)
						{
						   std::string msg = "SSLServerSessionWrapper::handle_accept exception ";
						   msg +=ex.what();
						   LOGIT1(msg)
						}
					}
				public:
					virtual void RunServer(uint16_t port)
					{
						std::string msg ="SSLServerSessionWrapper::RunServer";
						LOGIT(msg)
						acceptor_ = new boost::asio::ip::tcp::acceptor(Session::IOContextManager::io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port));
						start_accept();
					}
				}
		}
	}
}
