#ifndef SSL_SESSION_H
#define SSL_SESSION_H
#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/context.hpp>
#include "../../Session/IPSecurity/IPSecurity.h"
#include "../../Session/IOContextManager.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
#include <string>
// based off session class at https://www.boost.org/doc/libs/1_52_0/doc/html/boost_asio/example/ssl/server.cpp
namespace OSI
{
	namespace Transport
	{
		namespace SSL
		{
			typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class SSLSession
			{
				char* incomingBuffer;
				char* outgoingBuffer;
				size_t incomingBufferSize;
				size_t outgoingBufferSize;
				TFlyWeightServerIncoming incoming;
				TFlyWeightServerOutgoing outgoing;
				TServerSession& _session;
				ssl_socket _socket;
				TInitializationParameters& _parameters;
			public:
				// using this example https://www.boost.org/doc/libs/1_58_0/doc/html/boost_asio/example/cpp03/ssl/server.cpp Don't use the C+11 examples they don't compile???!!
				SSLSession(boost::asio::ssl::context& context,
									TInitializationParameters& parameters,
									TServerSession& session): _session(session),
				incoming(parameters),
				outgoing(parameters),
				_parameters(parameters),
				_socket(OSI::Session::IOContextManager::io_context,context)
				{
					Init();
				}
				ssl_socket::lowest_layer_type& socket()
				{
					return _socket.lowest_layer();
				}
				void handshake(std::shared_ptr < SSLSession> session)
				{
                    try
                    {
					_socket.async_handshake(boost::asio::ssl::stream_base::server,
						boost::bind(&SSLSession::handle_handshake, this, session,
							boost::asio::placeholders::error));
                    }
                    catch(std::exception ex)
                    {
                       std::string msg;
                       msg = "SSLSession::handshake exception:";
                       msg+=ex.what();
                       LOGIT1(msg)
                    }
				}

				void handle_handshake(std::shared_ptr < SSLSession> session,const boost::system::error_code& error)
				{
                    try
                    {
                        if (!error)
                        {
                            SSLSession* that = session.get();

                            _socket.async_read_some(boost::asio::buffer(&that->incomingBuffer[0], that->incomingBufferSize),
                                boost::bind(&SSLSession::inputHandler, this, session,
                                    boost::asio::placeholders::error,
                                    boost::asio::placeholders::bytes_transferred));

                        }
                        else
                        {
                            std::string msg = "handle handshake error code:";
                            msg += error.message();
                            LOGIT1(msg)
                        }
                    }
                    catch(std::exception ex)
                    {
                       std::string msg = "SSLSession::handle_handshake exception :";
                       msg+=ex.what();
                       LOGIT1(msg)
                    }
				}

				 void inputHandler(std::shared_ptr<SSLSession> wrapper, const boost::system::error_code& error, std::size_t bytes_transferred)
				{
                    try
                    {
                        std::string msg = " server input handler triggered with number of bytes transferred ";
                        msg+= std::to_string( bytes_transferred);
                        LOGIT(msg);
                        auto that = wrapper.get();
                        std::string ipaddress = that->socket().remote_endpoint().address().to_string();
                        if (error)
                        {
                            auto errormsg = error.message();
                            Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, errormsg);
                        }
                        else
                        {
                            that->incoming.FromString(&that->incomingBuffer[0], bytes_transferred);
                            bool outgoingLength = that->_session.IncomingHook(that->incoming, that->outgoing, ipaddress);
                            if (outgoingLength)
                            {
                                size_t length = that->outgoing.size(); // ToString can change the length so we cache it
                                that->outgoing.ToString(&that->outgoingBuffer[0], length);
                                boost::asio::async_write(that->_socket, boost::asio::buffer(&that->outgoingBuffer[0], length),
                                    boost::bind(&outputHandler,
                                        wrapper,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred)

                                );
                            }
                        }
					}
					catch(std::exception ex)
					{
					  std::string msg = "SSLSession::inputhandler exception ";
					  msg+=ex.what();
					  LOGIT1(msg)
					}
				};
				static void outputHandler(std::shared_ptr<SSLSession> wrapper, const boost::system::error_code& error, std::size_t bytes_transferred)
				{
                    try
                    {
                        auto that = wrapper.get();
                        if (that->_parameters.closeConnectionOnZeroOutput)
                            that->_socket.lowest_layer().close();
                        else
                        {
                            that->handle_handshake(wrapper,error);
                        }
					}
					catch(std::exception ex)
					{
                        std::string msg;
                        msg = "SSLSession::outputHandler exception:";
                        msg+= ex.what();
                        LOGIT1(msg)
					}
				};
				void Init(void)
				{
					incomingBufferSize = incoming.max_size();
					outgoingBufferSize = outgoing.max_size();
					incomingBuffer = new char[incomingBufferSize];
					outgoingBuffer = new char[outgoingBufferSize];
				};
			};
		};
	}
}
#endif
