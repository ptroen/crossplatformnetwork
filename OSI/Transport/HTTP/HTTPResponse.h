#ifndef HTTP_FLY_WEIGHT_SERVER_OUTGOING_H
#define HTTP_FLY_WEIGHT_SERVER_OUTGOING_H
#include "../../Session/HTTP/HTTP_Request_Builder.h"
#include "../../Session/HTTP/HTTP_Response_Builder.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template <class TInitializationParameters>
			class HTTPResponse
			{
			public:
				std::string url;
				std::string buffer;
				size_t bufferLength;
				const size_t maxSize = 1048576;
				HTTPResponse(TInitializationParameters& parameters) :buffer(){}


				size_t max_size(void) { return maxSize; };
				size_t size(void) { return bufferLength; }

				HTTPResponse<TInitializationParameters>& operator=(HTTPResponse<TInitializationParameters>& rhs)
				{
					url = rhs.url;
					buffer = rhs.buffer;
					bufferLength = rhs.bufferLength;
					return *this;
				};
				void FromString(char* pointer, size_t& data)
				{
					bufferLength = (data >= maxSize ? maxSize : data);
					buffer.resize(bufferLength);
					memcpy(&buffer[0], &(*pointer), bufferLength);
				};

				void ToString(char* pointer,size_t& data)
				{
                    data  = bufferLength;
					memcpy( &(*pointer), &buffer[0], bufferLength);
				}
			};
		}
	}
}

#endif
