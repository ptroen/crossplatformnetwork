#ifndef HTTP_SERVER_RESPONDER_H
#define HTTP_SERVER_RESPONDER_H
#include "../../../MVC/Model/ModelContentLoader.h"
#include "../../Session/HTTP/HTTP_Response_Builder.h"
#include <sstream>
#include "../../Presentation/HTTP/HTTPCompressor.h"
#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "../../../algorithm/circularbuffer.h"
#include "../../../OSI/Transport/UDP/udp_baseWrapper.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class HTTPServerResponder
			{
				MVC::Model::ModelContentLoader _content;
				typedef UDP::udp_baseWrapper< TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TInitializationParameters> udpbase;
				typedef Algorithm::CircularBuffer<udpbase, TInitializationParameters> circular;
				circular readWriteBuffer;
				TServerSession session;
			public:
				HTTPServerResponder(TInitializationParameters& parameters) :readWriteBuffer(parameters.incomingReadWriteBufferSize, parameters), session(parameters)
				{
					// this is to service static content only. Dynamic code content is through the hooks
					_content.ReadFileSystemAtRoot();

				}; // this could be moved to the sample protocol

				void OnIdleHook() { session.OnIdleHook(); }

				void OnTimerHook() { session.OnTimerHook(); };

				bool IncomingHook(HTTPRequest<TInitializationParameters>& in, HTTPResponse<TInitializationParameters>& out, std::string& ipAddress) {
					std::string serverResponse;
					std::stringstream lines(const_cast<char*>(in.buffer.c_str()));

					// This stage is strictly parsing. 1. identify the parts of the request and extract it
					size_t line_number = 0;
					std::string line;
					std::getline(lines, line);
					line_number++;
					Session::HTTP::HTTP_Request_Builder request;
					request.ParseHTTPRequestLine(line);

					// scanning the rest of the http body coming in
					for (; std::getline(lines, line); line_number++)
					{
						line_number++;
						size_t acceptEncodingLocation = line.find("Accept-Encoding:");
						if (acceptEncodingLocation != -1)
						{
							size_t zipLocation = line.find("gzip");
							if (zipLocation != -1)
							{
								request.gzipEnabled = true;
							}
						}

						// Code to find post or put location
						//Keep-Alive: timeout=15
						//Connection: Keep-Alive
						//X-Forwarded-For: 192.168.10.1
						// how to find post body
						std::string minimumLine = "\n\r";
						if (line.length() <= minimumLine.length() && (request.requestType != OSI::Session::HTTP::HTTPRequestType::GETREQUEST))
						{
							std::string post; // extracts the post
							for (; std::getline(lines, line);)
							{
								post += line;
							}
							// identify POST https://stackoverflow.com/questions/14551194/how-are-parameters-sent-in-an-http-post-request
							request.ParsePostOrPut(post);
							break;
						}
					}
					// Ok we parsed it now we need to prepare a response
					Session::HTTP::HTTP_Response_Builder response = PrepareResponse(request, ipAddress);
					std::string& resp = response.GetResponse();
					size_t size = resp.size();
					// assigning the response from the response builder just a string copy here
					out.FromString(const_cast<char*>(resp.c_str()), size);
					return true;
				};

				// This is the responder
				Session::HTTP::HTTP_Response_Builder PrepareResponse(Session::HTTP::HTTP_Request_Builder& request, std::string& ipaddress) {
					Session::HTTP::HTTP_Response_Builder resp;
					OSI::Session::ContentRecordType content;
					udpbase& udpBase = readWriteBuffer.GrabNewElement();
					if (MVC::Model::ModelContentLoader::foundContent(request.url))
					{
						// found
						content = MVC::Model::ModelContentLoader::GetContent(request.url);
						// deploy the content
						resp.customResponsePageCode = MVC::Model::ModelContentLoader::GetContent(request.url).contentAsAString;
						
					}
					else
					{
						
						// load into the sample protocol that it's a http request!
						udpBase.incoming.IncomingHTTPRequest(content,request);
						bool responseFromHook = session.IncomingHook(udpBase.incoming, udpBase.outgoing, ipaddress);
						if (responseFromHook)
						{
							// we processed something so render it
							std::string response;
							size_t length = udpBase.outgoing.size();
							response.resize(length);
							// convert our flyweightType to a string
							udpBase.outgoing.ToString(const_cast<char*>(response.c_str()), length);
							// now our application flyweight type is a customResponsePageCode
							resp.customResponsePageCode = response;
						}
						else // render something on the page that says it wasn't found. This could be a http error later
						{
							std::ostringstream res;
							if (Algorithm::String::IsFileExtension(request.url, "html"))
							{
								res << "<HTML>";
								res << "url was ";
								res << request.url << "<br/>";
								res << "key value args are:<br/>";
								for (auto it = request.keyValueArgs.cbegin();
									it != request.keyValueArgs.cend(); ++it)
								{
									res << it->first << " " << it->second << " <br/>";
								}
								res << "</HTML>";
								
							}
							else if(Algorithm::String::IsFileExtension(request.url, "json"))
							{
								res << "{\n\r";
								res << "url=" << request.url << "\n\r";
									res << "}\n\r";
							}
							resp.customResponsePageCode = res.str();
						}
					}
                    // selects proper mime type from the string and sets it internally
					content.PickProperMimeType(request.url);
					resp.ConstructHTTPResponsePacket(request, content);
					
					// check if we can compress page
					if (request.gzipEnabled && content.CompressAble())
					{
						HTTPCompressor compress;
						compress.ZipPayload(resp); // works on local host but not for client/server unknown reason
					}
					return resp;
				};
			}; // end class
		} // end HTTP
	} // end Transport
} // end OSI

#endif
