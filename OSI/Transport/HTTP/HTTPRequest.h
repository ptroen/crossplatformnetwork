#ifndef HTTP_FLY_WEIGHT_SERVER_INCOMING_H
#define HTTP_FLY_WEIGHT_SERVER_INCOMING_H
#include <cstring> // for memcpy
#include <string>
//#include "../../Session/HTTP/HTTPRequestType.h"
#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template<class TInitializationParameters>
			class HTTPRequest
			{
			public:
				std::string buffer;
				size_t dataSize;
				const size_t maxSize = 1048576;
				HTTPRequest(void) : buffer(""),dataSize(0){
				    buffer.resize(maxSize); // preallocation
				 };
				HTTPRequest(TInitializationParameters& parameters) : buffer(""),dataSize(0)
				{
				    buffer.resize(maxSize); // preallocation
				}
				HTTPRequest& operator=(HTTPRequest& rhs)
				{
					dataSize = rhs.dataSize;
					buffer = rhs.buffer;

					return *this;
				};
				size_t max_size(void) { return maxSize; };
				size_t size(void) {
				  if(dataSize < maxSize)
				      return dataSize;
				  else
				 return maxSize;
				 }

				void ToString(char* pointer,size_t data)
				{
				     //dataSize = MIN(data,maxSize);
					memcpy(&(*pointer), buffer.c_str(), dataSize);
				}

				void FromString(char* pointer, size_t data)
				{
					this->dataSize = MIN(data, maxSize);
					memcpy(const_cast<char*>(buffer.c_str()), &(*pointer), dataSize);
				}
			};
		}
	}
}
#endif
