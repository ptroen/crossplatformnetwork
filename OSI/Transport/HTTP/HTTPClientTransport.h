#ifndef HTTP_CLIENT_TRANSPORT_H
#define HTTP_CLIENT_TRANSPORT_H

#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "../TCP/TCPClientTransport.h"
#include "HTTPClientTransportClientHook.h"
#include "../../../OSI/Session/HTTP/HTTP_Request_Builder.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class HTTPClientTransport : public virtual Interface::IClientTransport<HTTPRequest<TTransportInitializationParameters>,HTTPResponse<TTransportInitializationParameters>,TTransportInitializationParameters>,
				public TCP::TCP_ClientTransport <HTTPRequest<TTransportInitializationParameters>,
				HTTPResponse<TTransportInitializationParameters>,
			HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters>
			{
				public:
				HTTPClientTransport(TTransportInitializationParameters& parameters) :Interface::IClientTransport<HTTPRequest<TTransportInitializationParameters>, HTTPResponse<TTransportInitializationParameters>, TTransportInitializationParameters>(parameters),
					TCP::TCP_ClientTransport <HTTPRequest<TTransportInitializationParameters>, HTTPResponse<TTransportInitializationParameters>,
					HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters>(parameters){};
				virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
				{
					HTTPRequest<TTransportInitializationParameters> requestToServer(Interface::IClientTransport<HTTPRequest<TTransportInitializationParameters>,HTTPResponse<TTransportInitializationParameters>,TTransportInitializationParameters>::_parameters);
					
					
					auto _requestToServer = request.ToHTTPRequest();
					requestToServer = _requestToServer;
					TCP::TCP_ClientTransport< HTTPRequest<TTransportInitializationParameters>, HTTPResponse<TTransportInitializationParameters>,
					HTTPClientResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TSession, TTransportInitializationParameters>, TTransportInitializationParameters>::RunClient(ipAddress, requestToServer);
					
				};
			}; // end class
		}
	}
}
#endif
