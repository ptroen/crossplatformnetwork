#ifndef HTTP_CLIENT_TRANSPORT_CLIENT_HOOK_H
#define HTTP_CLIENT_TRANSPORT_CLIENT_HOOK_H
#include "../../Transport/interface/IClientTransportHooks.h"
#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "HTTPClientTransportClientHook.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template < class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class HTTPClientResponder : public virtual Interface::IClientHooks<TFlyWeightServerIncoming,TFlyWeightServerOutgoing>
			{
			public:
				TSession session;
				TFlyWeightServerIncoming outgoing;
				TFlyWeightServerOutgoing incoming;
				HTTPClientResponder(TTransportInitializationParameters& parameters):outgoing(parameters),incoming(parameters),
				session(parameters){};
				// triggers on Client Incoming. non zero incidates send information outServer

				virtual void OnIdleHook() { session.OnIdleHook(); }

				virtual void OnTimerHook() { session.OnTimerHook(); };

				virtual bool IncomingHook(HTTPResponse< TTransportInitializationParameters>& outServer, HTTPRequest< TTransportInitializationParameters>& inServer, std::string& ipadddress)
				{
					// extract the information from the http response. That is the http meta information(i.e url is one of the meta information)
					incoming.FromHTTPResponse(outServer);
					// set the TFlyweight with the information 
					
					// handle the response
					bool write = session.IncomingHook(incoming, outgoing, ipadddress);
					if (write)
					{
						
						auto _inServer  = outgoing.ToHTTPRequest();
						inServer = _inServer;						
						return true;
					}
					return 0;
				};
			};
		}
	}
}
#endif
