#ifndef HTTP_TRANSPORT_H
#define HTTP_TRANSPORT_H
#include "../TCP/TCPTransport.h"
#include "../HTTP/httpserverresponder.h"
namespace OSI
{
	namespace Transport
	{
		namespace HTTP
		{
			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class HTTPServerTransport;
			
			// This interface glues the TCP Transport which is the server code with the HTTPServerTransportParserAndResponder which is the parsing and request code
			template <class TFlyWeightServerIncoming,class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters >
			class HTTPServerTransport  : public virtual TCP::TCP_ServerTransport<HTTPRequest<TInitializationParameters>,
																																HTTPResponse<TInitializationParameters>,
																																HTTPServerResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>,
																																TInitializationParameters>
			{
				public:
				HTTPServerTransport(TInitializationParameters& params) : TCP::TCP_ServerTransport<HTTPRequest<TInitializationParameters>,
					HTTPResponse<TInitializationParameters>,
					HTTPServerResponder<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>,
					TInitializationParameters>(params) {};
			}; // end class
		}
	}
}
#endif