#ifndef CRC_VALIDATE_PAYLOAD_H
#define CRC_VALIDATE_PAYLOAD_H

#include <boost/crc.hpp>
#include <string>
namespace OSI
{
	namespace Transport
	{
		namespace PayloadValidation
		{
			namespace CRC
			{
				    static void MakeCRC(boost::crc_32_type& result,std::string& s)
				    {
						result.process_bytes(&s[0], s.size());
					};
					static bool ValidatePayload(boost::crc_32_type& checksum,std::string& s)
					{
						boost::crc_32_type result;
						MakeCRC(result, s);
						return (result.checksum() == checksum.checksum());
					}
			}
		}
	}
}
#endif