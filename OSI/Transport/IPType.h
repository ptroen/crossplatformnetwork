#ifndef ISIPV6_H
#define ISIPV6_H
#include <string>
#include <boost/asio/ip/address.hpp>
namespace OSI
{
		namespace Transport
		{
			bool IsIPV6(const std::string& ip);
			
			bool ISIPV4(const std::string& ip);
		}
}

#endif