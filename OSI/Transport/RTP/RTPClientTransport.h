#ifndef RTP_CLIENT_TRANSPORT_H
#define RTP_CLIENT_TRANSPORT_H
#include <ctime>
#include "../UDP/UDPClientTransport.h"
#include <boost/asio.hpp>
#include "RTPProtcolState.h"
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{
			template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession,class TInitializationParameters>
			class RTPClientSession
			{
				 RTPProtocolState<TFlyWeightServerOutgoing, TFlyWeightServerIncoming,TSession,TInitializationParameters>* clientState;
				 TInitializationParameters& _parameters;
			public:
				TSession session;
				RTPClientSession(TInitializationParameters& parameters) :clientState(nullptr),_parameters(parameters),
				session(parameters){};
				void OnTimerHook()
				{
					clientState->TimingHook();
					session.OnTimerHook();
				};
				virtual void OnIdleHook(void) { session.OnIdleHook(); };
				 void Init(std::string& ipaddress)
				{
					clientState = new RTPProtocolState<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TInitializationParameters>(ipaddress, _parameters,true,session);
				};
				bool IncomingHook(RTPProtocol<TFlyWeightServerOutgoing, TInitializationParameters>& incoming, RTPProtocol<TFlyWeightServerIncoming, TInitializationParameters>& outgoing, std::string& ipaddress)
				{
					if (incoming.ValidatePayload())
					{
						// pack details are filled in by Process Incoming
						return clientState->ProcessIncoming(incoming, outgoing, ipaddress);
					}
					return true;
				};
			};
		
		template <class TFlyWeightServerOutgoing, class TFlyWeightServerIncoming, class TSession, class TTransportInitializationParameters>
		class RTPClientTransport : public virtual UDP::UDPClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 
			RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
			RTPClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
			TTransportInitializationParameters>,
			public virtual Interface::IClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 								RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>, 
							TTransportInitializationParameters>
		{
			public:
			RTPClientTransport(TTransportInitializationParameters& parameters) :UDP::UDPClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 
			RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
				RTPClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
				TTransportInitializationParameters>(parameters) {};
			
			virtual void RunClient(const std::string& ipAddress, TFlyWeightServerIncoming& request)
			{
				// need to start changing inherited stuff
				RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters> wrapped_request(Interface::IClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 								RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>, 
							TTransportInitializationParameters>::_parameters);
				wrapped_request.packetTypes._AppPayload._payload = request;
				wrapped_request.MakeApplicationPacket();
				UDP::UDPClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 
			RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
			RTPClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
			TTransportInitializationParameters>::session._session.Init(const_cast<std::string&>(ipAddress));
				UDP::UDPClientTransport<RTPProtocol<TFlyWeightServerIncoming, TTransportInitializationParameters>, 
			RTPProtocol<TFlyWeightServerOutgoing, TTransportInitializationParameters>,
			RTPClientSession<TFlyWeightServerOutgoing, TFlyWeightServerIncoming, TSession, TTransportInitializationParameters>,
			TTransportInitializationParameters>::RunClient(ipAddress, wrapped_request);
			}
		}; // end RTPClientTransport
		} // end RTP
	} // END Transport
} // end OSI
#endif
