#ifndef RTP_PROTOCOL_H
#define RTP_PROTOCOL_H
#include <algorithm>
#include <cstdint>
#include "RTPRFCCode/RTPRFCFunctions.h"
#include "../../../algorithm/interface/IType.h"
#include "../interface/IRTPTransportInitializationParameters.h"
#ifndef _MAX
#define _MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{		
			template<class TPayload, class TInitializationParameters>
			class RTPProtocol : public virtual Algorithm::Interface::IType
			{
				const size_t maxSDESItemSize = 10;
				const size_t maxSDESRTCP = 10;
				std::string constructedPayload;
				struct AppPayload
				{
					AppPayload(TInitializationParameters& parameters) :_payload(parameters) {};
					OSI::Transport::RTP::RTPRFCCode::SSRC_CSRC ssrc_csrc;
					std::string appMessage;
					TPayload _payload;
					size_t size()
					{
						/*
					*  0                   1                   2                   3
				   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				   |V=2|P| subtype |   PT=APP=204  |             length            |
				   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				   |                           SSRC/CSRC                           |
					Header
				   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				   |                          name (ASCII)    = appMessage                     |
				   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				   |                   application-dependent data (_payload)               ...
				   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				*/
						size_t packetInBytes;
						packetInBytes = ssrc_csrc.size();
						if (appMessage.size() % 2 != 0)
						{
							appMessage += '\0';
						}
						else if(appMessage.size()==0)
						{
							appMessage += '\0';
							appMessage += '\0';
						}
						packetInBytes += appMessage.size();
						packetInBytes += _payload.size();
						return packetInBytes;
					}

					void ToString(char* stringout, size_t size)
					{
						ssrc_csrc.ToString(&(*stringout), size);
						stringout += ssrc_csrc.size();
						memcpy(&(*stringout), appMessage.c_str(), appMessage.size());
						stringout += appMessage.size();
						size_t payloadSize = _payload.size();
						_payload.ToString(&(*stringout), payloadSize);
					}

					void FromString(char* data, size_t size)
					{
						ssrc_csrc.FromString(&(*data), size);
						data += ssrc_csrc.size();
						appMessage.clear();
						size_t appMsgSize = 0;
						do
						{
							appMessage += *data;
							++data;
							--size;
							++appMsgSize;
							if (size == 0) return;
						} while (*data != '\0');
						if(appMsgSize %2 !=0)
						{
							appMessage += "\0";
							++data;
						}
						_payload.FromString(&(*data), size);
					}
				};

				struct SDESType
				{
					SDESType() :sdes(OSI::Transport::RTP::RTPRFCCode::maxSDESRTCP) {};
					std::vector<OSI::Transport::RTP::RTPRFCCode::rtcp_sdes> sdes;
					size_t size(void)
					{
						size_t sum =0;
						for (auto it : sdes)
						{
							sum += it.size();
						}
						return sum;
					};
					size_t max_size(void)
					{
						return sdes[0].max_size() * OSI::Transport::RTP::RTPRFCCode::maxSDESRTCP;
					}

					void ToString(char* stringout, size_t& size)
					{
						for (auto it : sdes)
						{
							it.ToString(&(*stringout), size);
							stringout += it.size();
						}
					}

					void FromString(char* data, size_t& dataSize)
					{
						sdes.clear();
						size_t index = 0;
						while (dataSize > 0 && index < OSI::Transport::RTP::RTPRFCCode::maxSDESRTCP)
						{
							OSI::Transport::RTP::RTPRFCCode::rtcp_sdes entry;
							entry.FromString(&(*data), dataSize);
							sdes.push_back(entry);
							++index;
						}
					}
				};

				//std::string constructedPayload;
				class rtpPacketTypes
				{
				public:
					// sender report (SR) 
					struct OSI::Transport::RTP::RTPRFCCode::senderReport sr;

					//reception report (RR) 
					struct OSI::Transport::RTP::RTPRFCCode::receptionReport rr;
					
					// source description (SDES) 
					SDESType sdes;
					
					// App packet payload goes here
					AppPayload _AppPayload;
					
					// BYE 
					struct OSI::Transport::RTP::RTPRFCCode::BYEStructure bye;
					size_t max_size(void){ return _MAX(_MAX(bye.max_size(), sdes.max_size()), _MAX(rr.max_size(), sr.max_size()));};

					size_t size(void)
					{
						return sizeof(rtpPacketTypes);
					};
					rtpPacketTypes(TInitializationParameters& parameters) :_AppPayload(parameters),sr(),rr(),sdes(),bye(){};
					~rtpPacketTypes(void) {};
				};
			public:
					OSI::Transport::RTP::RTPRFCCode::rtcp_common_t common;    // common header 
					rtpPacketTypes packetTypes;
					uint16_t ssrcOfPacketSender;
					uint16_t ccsrc;
					//time_t time;
				TInitializationParameters _parameters;
				RTPProtocol(TInitializationParameters& parameters) : _parameters(parameters),common(),packetTypes(parameters),ssrcOfPacketSender(0),ccsrc(0)
				{
					uint16_t* c = (uint16_t *)&common;
					*c = RTCP_VALID_MASK;
				};
				~RTPProtocol(void){}
				RTPProtocol(const RTPProtocol& rhs) : _parameters(), 
				common(), 
				packetTypes(rhs.packetTypes)
				{
					ccsrc = 0;
					ssrcOfPacketSender = 0;
					uint16_t* c =(uint16_t *)&common;
					*c = RTCP_VALID_MASK;
					std::string s;
					RTPProtocol& r = const_cast<RTPProtocol&>(rhs);
					size_t rhssize = r.size();
					if (!rhssize) return;
					s.resize(rhssize);
					r.ToString(&s[0], rhssize);
					FromString(&s[0], rhssize);
				}
				// grab the current data
				void FromString(char* data,size_t dataSize)
				{
					// copy the header first
					char* remainingData = BuildCommonPacketHeaderFromStream(&(*data), dataSize);
					RTPRFCCode::rtcp_type_t rr = common.pt;
					// 2. switch on the different string types
					switch(rr)
					{
					case RTPRFCCode::rtcp_type_t::RTCP_SR:
						packetTypes.sr.FromString(&(*remainingData),dataSize);
						break;
					case RTPRFCCode::rtcp_type_t::RTCP_RR:
						packetTypes.rr.FromString(&(*remainingData), dataSize);
						break;
					case	 RTPRFCCode::rtcp_type_t::RTCP_SDES: // source description we are description SDES Types
						packetTypes.sdes.FromString(&(*remainingData), dataSize);
						break;
					case	 RTPRFCCode::rtcp_type_t::RTCP_BYE:
						packetTypes.bye.FromString(&(*remainingData), dataSize);
						break;
					case	 RTPRFCCode::rtcp_type_t::RTCP_APP:
						packetTypes._AppPayload.FromString(&(*remainingData), dataSize);
						break;
					}
					// 3. copy the different string types
				}
				size_t size(void)
				{
					RTPRFCCode::rtcp_type_t pt=common.pt;
					switch (pt)
					{
					case RTPRFCCode::rtcp_type_t::RTCP_SR:
						return packetTypes.sr.size() + common.size();
					case RTPRFCCode::rtcp_type_t::RTCP_RR:
						return packetTypes.rr.size() + common.size();
					case	 RTPRFCCode::rtcp_type_t::RTCP_SDES:
							return packetTypes.sdes.size() + common.size();
						case	 RTPRFCCode::rtcp_type_t::RTCP_BYE:
							return packetTypes.bye.size() + common.size();
						case	 RTPRFCCode::rtcp_type_t::RTCP_APP:
							return packetTypes._AppPayload.size() + common.size();
					}
					return 0; // don't know which type
				};

				void ToString(char* stringout, size_t size)
				{
					common.ToString(&(*stringout), size);
					size -= common.size();
					stringout += common.size();
					RTPRFCCode::rtcp_type_t pt=common.pt;
					switch (pt)
					{
						case RTPRFCCode::rtcp_type_t::RTCP_SR:
							packetTypes.sr.ToString(&(*stringout), size);
							break;
						case RTPRFCCode::rtcp_type_t::RTCP_RR:
							packetTypes.rr.ToString(&(*stringout), size);
							break;
						case	 RTPRFCCode::rtcp_type_t::RTCP_SDES:
							packetTypes.sdes.ToString(&(*stringout), size);
								break;
						case	 RTPRFCCode::rtcp_type_t::RTCP_BYE:
							packetTypes.bye.ToString(&(*stringout), size);
							break;
						case	 RTPRFCCode::rtcp_type_t::RTCP_APP:
							packetTypes._AppPayload.ToString(&(*stringout), size);
							break;
					}
				}
				size_t max_size(void) {
					return sizeof(OSI::Transport::RTP::RTPRFCCode::rtcp_common_t) + packetTypes.max_size();
				};
				bool ValidatePayload(void)
				{
						/* only validate for SR
						if ((*(uint16_t *)&common & RTCP_VALID_MASK) != RTCP_VALID_VALUE)
					    {
							/* something wrong with packet format * /
							return false;
						}
				*/
					return true;
						size_t lengthInBytes = (common.length * 2) + common.size();
						size_t byteSize = size();
					if (lengthInBytes == byteSize)
						{
							return true;
						}
						return false;
				};

				void MakeGoodBye(OSI::Transport::RTP::RTPRFCCode::source& src,std::string& reasonForLeaving)
				{
					/*
					 *        0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |V=2|P|    SC   |   PT=BYE=203  |             length            |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                           SSRC/CSRC                           |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      :                              ...                              :
      +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
(opt) |     length    |               reason for leaving            ...
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   The BYE packet indicates that one or more sources are no longer
   active.

   version (V), padding (P), length:
      As described for the SR packet (see Section 6.4.1).

   packet type (PT): 8 bits
      Contains the constant 203 to identify this as an RTCP BYE packet.

   source count (SC): 5 bits
      The number of SSRC/CSRC identifiers included in this BYE packet.
      A count value of zero is valid, but useless.

   The rules for when a BYE packet should be sent are specified in
   Sections 6.3.7 and 8.2.
					 */
					size_t packetInBytes;
					packetInBytes = sizeof(common);
					packetInBytes += sizeof(_parameters.ssrcOfPacketSender);
					packetInBytes += sizeof(_parameters.csrc);
					packetInBytes += sizeof(this->packetTypes.bye);
					packetTypes.bye.src =  _parameters.ssrcOfPacketSender;

					FillCommonPacketInHeader(OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_BYE, packetInBytes);
					FillSSRCSRC(packetTypes.bye.srcccsrc);
				};

				void MakeApplicationPacket(void)
				{
					// now build the packet;
					size_t size = this->size();
					FillCommonPacketInHeader(RTPRFCCode::rtcp_type_t::RTCP_APP, size);
					FillSSRCSRC(packetTypes._AppPayload.ssrc_csrc);
				};
				


				// called at  the beginning
				void MakeSDESPackets(TInitializationParameters& initParameters)
				{
					/*
					 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					|V=2|P|    SC   |  PT=SDES=202  |             length            | header
					+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
					|                          SSRC/CSRC_1                          | chunk
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+   1
					|                           SDES items                          |
					|                              ...                              |
					+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
					|                          SSRC/CSRC_2                          | chunk
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+   2
					|                           SDES items                          |
					|                              ...                              |
					+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
					 *
					 *
					 *
					 */
					size_t packetInBytes;
					packetInBytes = sizeof(common);
					packetInBytes += sizeof(ssrcOfPacketSender);
					packetInBytes += sizeof(ccsrc);
				
					// now build the packet;
					char* ptr = FillCommonPacketInHeader(RTPRFCCode::rtcp_type_t::RTCP_APP, packetInBytes);
					
					 Transport::Interface::IRTPTransportInitializationParameters& par = _parameters;
					 size_t index = 0;
					 size_t sdesTotalItemByteCount = 0;
					if(par.cname.size()>0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_CNAME, par.cname, index, sdesTotalItemByteCount);
						++index;
					}
					if(par.name.size()>0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_NAME, par.name, index, sdesTotalItemByteCount);
						++index;
					}
					if(par.email.size()>0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_EMAIL, par.email, index, sdesTotalItemByteCount);
						++index;
					}
					if (par.phone.size() > 0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_PHONE, par.phone, index, sdesTotalItemByteCount);
						++index;
					}
					if(par.location.size() > 0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_LOC, par.location, index, sdesTotalItemByteCount);
						++index;
					}
					if(par.tool.size() > 0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_TOOL, par.tool, index, sdesTotalItemByteCount);
						++index;
					}
					if(par.note.size() > 0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_NOTE, par.note, index, sdesTotalItemByteCount);
						++index;
					}
					if (par.priv.size() > 0)
					{
						FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_PRIV, par.priv, index, sdesTotalItemByteCount);
						++index;
					}
					FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_END, par.byeWhy, index, sdesTotalItemByteCount);
					packetInBytes += sdesTotalItemByteCount;
					constructedPayload.clear();
					constructedPayload.resize(packetInBytes);
					ptr = FillCommonPacketInHeader(OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_SDES, packetInBytes);
					ptr = FillSSRCSRC(&(*ptr));
					// now put all the data in
					for (size_t i = 0; i < index; i++)
					{
						CopySDESItemToConstructedPayload(index, ptr);
					}
				};

				void AssignSDESPacket(void)
				{
					Transport::Interface::IRTPTransportInitializationParameters& par = _parameters;
					RTPRFCCode::rtcp_sdes_item_t* sdesItem = &packetTypes.sdes.sdes[0].item[0];
					size_t index = 0;
					do
					{
						std::string stringPayload;
						stringPayload.resize(sdesItem->length);
						memcpy(&stringPayload[0], (char*)sdesItem->data, sdesItem->length);
						auto pt = sdesItem->type;
						switch (pt)
						{
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_END:
							par.byeWhy = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_CNAME:
							par.cname = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_NAME:
							par.name = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_EMAIL:
							par.email = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_PHONE:
							par.phone = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_LOC:
							par.location = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_TOOL:
							par.tool = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_NOTE:
							par.note = stringPayload;
							break;
						case 	OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_PRIV:
							par.priv = stringPayload;
							break;
							++index;
						}
						++sdesItem;
					} while (sdesItem->type != OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t::RTCP_SDES_END && index != OSI::Transport::RTP::RTPRFCCode::maxsdesItemTypes);
				};

			private:
				void FillSDESItem(OSI::Transport::RTP::RTPRFCCode::rtcp_sdes_type_t& type,std::string value,size_t index)
				{
					struct rtcp_sdes_item_t* item = &packetTypes.sdes[0].item[index];
					item->type = type;
					unsigned int n = value.size();
					item->length = std::min(255U, n);
					memcpy(&item->data[0], value.c_str(), item->length);
				}
				void FillSSRCSRC(OSI::Transport::RTP::RTPRFCCode::SSRC_CSRC& sscCSRC)
				{
					sscCSRC.src = ssrcOfPacketSender;
					sscCSRC.ccsrc = ccsrc;
				}

				char* ReadSSRCCSRC(char* ptr, size_t& remainingBytes)
				{
					memcpy(&ssrcOfPacketSender,&(*ptr), sizeof(_parameters.ssrcOfPacketSender));
					ptr += sizeof(ssrcOfPacketSender);
					memcpy(&_parameters.csrc, ptr, sizeof(_parameters.csrc));
					ptr += sizeof(_parameters.csrc);
					return ptr;
				};

				char* MakeTimeStamp(char* ptr)
				{
					time = std::time(nullptr);
					memcpy(ptr, &time, sizeof(time_t));
					ptr += sizeof(time_t);
					return ptr;
				}

				char* BuildCommonPacketHeaderFromStream(char* string, size_t& remainingBytes)
				{
					size_t commonSize = sizeof(common);
					memcpy(&common, &(*string), commonSize);
					remainingBytes -= commonSize;
					string += commonSize;
					return &(*string);
				};
			public:
				void FillCommonPacketInHeader(OSI::Transport::RTP::RTPRFCCode::rtcp_type_t packetType,uint16_t packetLengthInBytes)
				{
					packetLengthInBytes -= sizeof(common); // we don't count common
					/*
					0                   1                   2                   3
						0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
						+ -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
						| V = 2 | P | X | CC | M | PT | sequence number |
						+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
						*/
					common.version = 2;// protocol version
					common.p = 0; // padding flag
					common.pt = packetType;   // RTCP packet type
					common.length = packetLengthInBytes / 2; // pkt len in words, w/o this word
				}
			};
			
		}
	}
}
#endif
