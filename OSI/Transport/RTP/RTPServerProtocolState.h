#ifndef RTP_SERVER_PROTOCOL_STATE_H
#define RTP_SERVER_PROTOCOL_STATE_H
#include "RTPProtcolState.h"

#include <string>
#include <memory>
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{
			template<class TFlyWeightIncoming, class  TFlyWeightOutgoing, class TSession, class TInitializationParameters>
			class RTPServerProtocolState
			{
				public:
				std::map<std::string, std::shared_ptr<RTPProtocolState<TFlyWeightIncoming,TFlyWeightOutgoing,TSession,TInitializationParameters>>> userMap;
				TInitializationParameters& _parameters;
				TSession& _session;
				RTPServerProtocolState(TInitializationParameters& parameters,TSession& session) :_parameters(parameters),userMap(),_session(session){};
				std::shared_ptr <RTPProtocolState<TFlyWeightIncoming, TFlyWeightOutgoing, TSession, TInitializationParameters>>& operator[] ( std::string& ipAddress)
				{
					if (userMap.find(ipAddress) == userMap.end())
					{
						userMap.insert(std::pair<std::string, std::shared_ptr<RTPProtocolState<TFlyWeightIncoming, TFlyWeightOutgoing, TSession,TInitializationParameters>>>(ipAddress, std::make_shared<RTPProtocolState<TFlyWeightIncoming, TFlyWeightOutgoing, TSession,TInitializationParameters>>(ipAddress,_parameters,true,_session)));
					}
					return userMap[ipAddress];
				};
			}; // end RTPServerProtocolState
		}
	}
}
#endif