#ifndef SENDER_REPORT_H
#define SENDER_REPORT_H
#include <cstdint>
#include <string>
#include <vector>
#include "rtcp_rr_t.h"
#include "source.h"
#include "../../../../OSManagement/timer.h"
namespace OSI
{
	namespace Transport
	{
		// ported code from https://tools.ietf.org/html/rfc3550#page-21
		namespace RTP
		{
			namespace RTPRFCCode
			{
				struct senderReport;
				struct senderReport
				{
					/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
header |V=2|P|    RC   |   PT=SR=200   |             length            |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         SSRC of sender                        |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
sender |              NTP timestamp, most significant word             |
info   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |             NTP timestamp, least significant word             |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         RTP timestamp                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                     sender's packet count                     |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      sender's octet count                     |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_1 (SSRC of first source)                 |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  1    | fraction lost |       cumulative number of packets lost       |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           extended highest sequence number received           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      interarrival jitter                      |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         last SR (LSR)                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                   delay since last SR (DLSR)                  |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_2 (SSRC of second source)                |onl
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  2    :                               ...                             :
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	   |                  profile-specific extensions                  |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

					uint32_t ssrc;     // sender generating this report 
					#ifdef _WIN32
					__time64_t ntp_sec;  // NTP timestamp
					#else
					uint64_t ntp_sec;
					#endif
					uint32_t rtp_ts;   // RTP timestamp 
					uint32_t psent;    // packets sent 
					uint32_t osent;    // octets sent 
					rtcp_rr_t rr;  // report block 
					size_t max_size(void) { return sizeof(senderReport); }
					STATIC_SIZE(senderReport)
						senderReport(void) :ssrc(0), ntp_sec(0), rtp_ts(0), psent(0), osent(0), rr() {};
				private:
					void FillTimeStamps(void)
					{
						ntp_sec = std::time(nullptr);
						rtp_ts = std::time(nullptr);
					};

					void FillPacketStatistics(size_t packetNumber, size_t bytesSent)
					{
						psent = packetNumber;
						osent = ConvertBytesToOctet(bytesSent);
					};
				public:
					void FillSenderReport(senderReport& lastSenderReport,uint32_t ssrcOfRTPPacket, size_t packetNumber, size_t bytesSent,uint32_t ssrcOfDatasource,OSManagement::Timer beforeTimer, OSManagement::Timer afterTimer)
					{
						FillTimeStamps();
						FillPacketStatistics(packetNumber, bytesSent);
						ssrc = ssrcOfRTPPacket;
						// this could be updated in the receiver report?
						rr.fraction = 0; // I don't understand the fraction functon
						rr.lost = 0;
						rr.last_seq = packetNumber;
						rr.lsr = lastSenderReport.rr.last_seq;
						// fill in the delay
						rr.FillDlSR(beforeTimer, afterTimer);
						// 	CalculateReceptionJitter(otherEndSource, otherEnd);
					};
					/*
					void FillReceptionReports(OSI::Transport::RTP::RTPRFCCode::source& otherEndSource, OSI::Transport::RTP::RTPRFCCode::rtcp_rr_t& otherEnd,uint16_t currentSequenceNumber, uint32_t ssrc)
					{
						/*
						 * 	uint32_t ssrc;             // data source being reported 
					uint32_t fraction : 8; // fraction lost since last SR/RR 
					uint32_t lost : 24;              // cumul. no. pkts lost (signed!) 
					uint32_t last_seq;       // extended last seq. no. received 
					uint32_t jitter;         //interarrival jitter 
					uint32_t lsr;             // last SR packet from this source 
					uint32_t dlsr;             //delay since last SR packet 
						 *
						 *
						 * 
						otherEnd.ssrc = ssrc;

					
						rr = otherEnd;
					};*/
					size_t ConvertBytesToOctet(size_t n) { return n; }

					size_t GetBytesSent(void)
					{
						return osent;
					}
				private:
					void CalculateReceptionJitter(source& otherEndSource, rtcp_rr_t& otherEnd)
					{
						otherEnd.jitter = (uint32_t)otherEndSource.jitter;
					}

				};

			}
		}
	}
}
#endif
