#ifndef STATIC_SIZE_H
#define STATIC_SIZE_H

#define STATIC_SIZE(TYPE) size_t size() \
{\
return sizeof(TYPE);\
}\
void ToString(char* stringout, size_t& stringSize)\
{\
	memcpy(&stringout[0], this, stringSize);\
}\
void FromString(char* data,size_t dataSize)\
{\
	memcpy(this, &data[0], dataSize);\
}

#endif