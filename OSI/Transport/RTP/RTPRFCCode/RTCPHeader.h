#ifndef RTCP_HEADER_H
#define RTCP_HEADER_H
#include <cstdint>
#include <string>
#include <vector>
#include "senderReport.h"
#include "ReceptionReport.h"
#include "static_size.h"
#include "source.h"
namespace OSI
{
	namespace Transport
	{
		// ported code from https://tools.ietf.org/html/rfc3550#page-21
		namespace RTP
		{
			namespace RTPRFCCode
			{
				enum class rtcp_type_t : uint8_t
				{
					RTCP_SR = 200,
					RTCP_RR = 201,
					RTCP_SDES = 202, // source description we are description SDES Types
					RTCP_BYE = 203,
					RTCP_APP = 204
				};

				enum class rtcp_sdes_type_t : uint8_t // configure this as static assignment from the client and assigned to the server
				{
					RTCP_SDES_END = 0,
					RTCP_SDES_CNAME = 1,
					RTCP_SDES_NAME = 2,
					RTCP_SDES_EMAIL = 3,
					RTCP_SDES_PHONE = 4,
					RTCP_SDES_LOC = 5,
					RTCP_SDES_TOOL = 6,
					RTCP_SDES_NOTE = 7,
					RTCP_SDES_PRIV = 8
				};
				const size_t maxsdesItemTypes = 9;
				// SDES item
				struct rtcp_sdes_item_t
				{
					rtcp_sdes_type_t type;              // type of item (rtcp_sdes_type_t) 
					uint8_t length;            // length of item (in octets) 
					int8_t data[255];             //text, not null-terminated 
					size_t max_size(void) { return sizeof(rtcp_sdes_item_t); }

					size_t size(void) { return sizeof(type) + sizeof(length) + length; }

					void ToString(char* stringout, size_t dataSize)
					{
						stringout[0] = (char)type;
						stringout[1] = (char)length;
						dataSize -= sizeof(type) + sizeof(length) + length;
						memcpy(&stringout[2], (char*)data[0], length);
					}
					void FromString(char* stringIn, size_t dataSize)
					{
						type = (rtcp_sdes_type_t)stringIn[0];
						length = static_cast<uint8_t>(stringIn[1]);
						dataSize -= sizeof(type) + sizeof(length) + length;
						memcpy(data, &stringIn[2], length);
					}
				};
				const size_t maxSDESItemSize = 10;
				const size_t maxSDESRTCP = 10;

				struct SSRC_CSRC
				{
					uint16_t src;
					uint16_t ccsrc;
					size_t size(void) { return sizeof(SSRC_CSRC); }
					void ToString(char* stringout, size_t size)
					{
						memcpy(&(*stringout), this, sizeof(SSRC_CSRC));
					}

					void FromString(char* data, size_t dataSize)
					{
						memcpy(this,&(*data), sizeof(SSRC_CSRC));
					}
					SSRC_CSRC() :src(), ccsrc(){};
				};

				struct rtcp_sdes
				{
					SSRC_CSRC srcccsrc;
					rtcp_sdes_item_t item[maxSDESItemSize]; // list of SDES items possible which is terminated by end
					rtcp_sdes(void) :srcccsrc(), item() {};
					size_t max_size(void) { return srcccsrc.size() + (sizeof(rtcp_sdes_item_t) * maxsdesItemTypes); };
					size_t size(void)
					{
						size_t size = srcccsrc.size();
						for (int i = 0; i < maxSDESItemSize; i++)
						{
							size += item[i].size();
							if (item[i].type == rtcp_sdes_type_t::RTCP_SDES_END)
							{
								return size;
							}
						}
						return size;
					}
					void ToString(char* stringout, size_t& stringSize)
					{
						memcpy(&stringout[0], &srcccsrc, srcccsrc.size());
						for (int i = 0; i < maxSDESItemSize; i++)
						{
							item[i].ToString(&(*stringout), stringSize);
							if (item[i].type == rtcp_sdes_type_t::RTCP_SDES_END)
							{

								return;
							}
						}
					}
					void FromString(char* data, size_t dataSize)
					{
						memcpy(&srcccsrc, &data[0], srcccsrc.size());
						for (int i = 0; i < maxSDESItemSize; i++)
						{
							item[i].FromString(&(*data), dataSize);
							if (item[i].type == rtcp_sdes_type_t::RTCP_SDES_END)
							{
								return;
							}
						}
					}
				};

				

				//One RTCP packet
				//RTCP common header word
				struct rtcp_common_t
				{
					uint8_t version : 2;   // protocol version 
					uint8_t p : 1;         // padding flag 
					uint8_t count : 5;     // varies by packet type 
					rtcp_type_t pt;        // RTCP packet type 
					uint16_t length;           // pkt len in words, w/o this word 

					STATIC_SIZE(rtcp_common_t)

						size_t max_size(void) { return sizeof(rtcp_common_t); }
				};


				struct BYEStructure
				{
					SSRC_CSRC srcccsrc;
					uint32_t src;   // list of sources (but we only do 1 because no multicast)
					//can't express trailing text for reason 
					size_t max_size(void) { return sizeof(BYEStructure); }
					STATIC_SIZE(BYEStructure)
						size_t max_items(void) { return 1; }
					BYEStructure() :srcccsrc(), src(0) {};
				};
			}
		}
	}
}

#endif
