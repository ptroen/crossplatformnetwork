#ifndef RTCP_RR_H
#define RTCP_RR_H
#include <cstdint>
#include <string>
#include <vector>
#include "static_size.h"
#include <chrono>
#include "../../../../OSManagement/timer.h"
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{
			namespace RTPRFCCode
			{
				// Reception report block
				struct rtcp_rr_t
				{
					uint32_t ssrc;             // data source being reported 
					uint32_t fraction : 8; // fraction lost since last SR/RR 
					uint32_t lost : 24;              // cumul. no. pkts lost (signed!) 
					uint32_t last_seq;       // extended last seq. no. received 
					uint32_t jitter;         //interarrival jitter 
					uint32_t lsr;             // last SR packet from this source 
					uint32_t dlsr;             //delay since last SR packet in time32_t time stamps
					rtcp_rr_t& operator=(rtcp_rr_t& r)
					{
						ssrc = r.ssrc;
						fraction = r.fraction;
						lost = r.lost;
						last_seq = r.last_seq;
						jitter = r.jitter;
						lsr = r.lsr;
						dlsr = r.dlsr;
						return *this;
					}
					size_t max_size(void)
					{
						return sizeof(rtcp_rr_t);
					};
					rtcp_rr_t(void)
					{
						last_seq = 0;
						lost = 0;
						fraction = 0;
						jitter = 0;
						lsr = 0;
						dlsr = 0;
					}

					void FillDlSR(OSManagement::Timer& beforeTimer, OSManagement::Timer& afterTimer)
					{
						size_t beforeInMillisecods = beforeTimer.TimeInMilliSeconds();
						size_t afterInMilliseconds = afterTimer.TimeInMilliSeconds();
						size_t diff = afterInMilliseconds - beforeInMillisecods;
						dlsr = (uint32_t)diff;
					};

					STATIC_SIZE(rtcp_rr_t)
				};
			}
		}
	}
}

#endif