#ifndef SOURCE_H
#define SOURCE_H
#include <cstdint>
namespace OSI
{
	namespace Transport
	{
		// ported code from https://tools.ietf.org/html/rfc3550#page-21
		namespace RTP
		{
			namespace RTPRFCCode
			{
				struct source
				{
					uint16_t max_seq;        //highest seq. number seen
					uint32_t cycles;         // shifted count of seq. number cycles
					uint32_t base_seq;      //base seq number
					uint32_t bad_seq;      //last 'bad' seq number + 1
					uint32_t probation;      // sequ. packets till source is valid
					uint32_t received;       //packets received
					uint32_t expected_prior;//packet expected at last interval
					uint32_t received_prior; //packet received at last interval
					uint32_t transit;        // relative trans time for prev pkt
					uint32_t jitter;         // estimated jitter
					source(void) :max_seq(0), cycles(0), bad_seq(0), probation(0), received(0), expected_prior(0), received_prior(0), transit(0), jitter(0) {};
				};
			}
		}
	}
}
#endif