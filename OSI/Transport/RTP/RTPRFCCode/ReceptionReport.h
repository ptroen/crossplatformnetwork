#ifndef RECEPTION_REPORT_H
#define RECEPTION_REPORT_H
#include "rtcp_rr_t.h"
namespace OSI
{
	namespace Transport
	{
		// ported code from https://tools.ietf.org/html/rfc3550#page-21
		namespace RTP
		{
			namespace RTPRFCCode
			{
				struct receptionReport
				{
					/*
				0                   1                   2                   3
					0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
					+ -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					header | V = 2 | P | RC | PT = RR = 201 | length |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					| SSRC of packet sender |
					+= += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += +
					report | SSRC_1(SSRC of first source) |
					block + -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					1 | fraction lost | cumulative number of packets lost |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					| extended highest sequence number received |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					| interarrival jitter |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					| last SR(LSR) |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					| delay since last SR(DLSR) |
					+= += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += +
					report | SSRC_2(SSRC of second source) |
					block + -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					2    :                               ... :
					+= += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += += +
					| profile - specific extensions |
					+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					*/
					void FillReceptionReport(receptionReport& lastReport,size_t packetNumber,OSManagement::Timer beforeTimer, OSManagement::Timer afterTimer ,uint32_t ssrcOfRTPPacket)
					{
						rr.FillDlSR(beforeTimer, afterTimer);
						ssrc = ssrcOfRTPPacket;
						// this could be updated in the receiver report?
						rr.fraction = 0; // I don't understand the fraction functon
						rr.lost = 0;
						rr.last_seq = packetNumber;
						rr.lsr = lastReport.rr.last_seq;
					}

					uint32_t ssrc;    //receiver generating this report 
					rtcp_rr_t rr;  // variable-length list 
					STATIC_SIZE(receptionReport)
						receptionReport(void) :rr(), ssrc() {};
						size_t max_size(void) { return sizeof(receptionReport); }
					size_t max_items(void) { return 1; };
				};
			}
		}
	}
}

#endif