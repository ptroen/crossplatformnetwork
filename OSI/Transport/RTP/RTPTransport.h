#ifndef RTP_TRANSPORT_H
#define RTP_TRANSPORT_H
#include <ctime>
#include <cstdint>
#include "../interface/ITransport.h"
#include "../UDP/udptransport.h"
#include <boost/asio.hpp>
#include "RTPProtcolState.h"
#include "RTPServerProtocolState.h"
#include "../../Session/IOContextManager.h"
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{
			
			template<class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class RTPServerSession 
			{
				TInitializationParameters& _parameters;
				TServerSession session;
			public:
				RTPServerSession(TInitializationParameters& parameters):_parameters(parameters),
				serverUserPool(parameters,session),
				session(parameters) {};
				RTPServerProtocolState<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters> serverUserPool;
				 void OnTimerHook()
				{
					for (auto it = serverUserPool.userMap.cbegin(); it != serverUserPool.userMap.cend();)
					{
						it->second.get()->TimingHook();
						// citation: https://stackoverflow.com/questions/8234779/how-to-remove-from-a-map-while-iterating-it
						if (it->second.get()->markForDestruction)
						{
							serverUserPool.userMap.erase(it++);
						}
						else
						{
							++it;
						}
					} // end for
					session.OnTimerHook();
				};

				 virtual void OnIdleHook(void)
				 {
					 session.OnIdleHook();
				 }

				 virtual bool IncomingHook(RTPProtocol<TFlyWeightServerIncoming, TInitializationParameters>& incoming, RTPProtocol<TFlyWeightServerOutgoing, TInitializationParameters>& outgoing, std::string& ipAddress)
				 {
					 if (incoming.ValidatePayload())
					 {
						 // pack details are filled in by Process Incoming
						 return serverUserPool[ipAddress]->ProcessIncoming(incoming, outgoing, ipAddress);
					 }
					 return false;
				 }
			};

			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TServerSession, class TInitializationParameters>
			class RTPServerTransport : public virtual UDP::UDPServerTransport<RTPProtocol<TFlyWeightServerIncoming, TInitializationParameters>, RTPProtocol<TFlyWeightServerOutgoing, TInitializationParameters>, RTPServerSession<TFlyWeightServerIncoming,TFlyWeightServerOutgoing,TServerSession,TInitializationParameters>, TInitializationParameters>
		{
		public:
			RTPServerTransport(TInitializationParameters& parameters) : UDP::UDPServerTransport<RTPProtocol<TFlyWeightServerIncoming, TInitializationParameters>, RTPProtocol<TFlyWeightServerOutgoing, TInitializationParameters>, RTPServerSession<TFlyWeightServerIncoming, TFlyWeightServerOutgoing, TServerSession, TInitializationParameters>, TInitializationParameters>(parameters){};
		}; // end RTPServerTransport
		} // end RTP
	} // END Transport
} // end OSI
#endif
