#ifndef RTP_PROTOCOL_STATE_H
#define RTP_PROTOCOL_STATE_H
#include "RTPProtocol.h"
//#include "RTCPHeader.h"
#include <string>
#include <cstdint>
#include "../interface/IProtocolBase.h"
#include "./RTPRFCCode/RTPRFCFunctions.h"
#include "../UDP/UDPDatagram.h"
namespace OSI
{
	namespace Transport
	{
		namespace RTP
		{
			template<class TFlyWeightIncoming, class  TFlyWeightOutgoing, class TSession,class TInitializationParameters>
			class RTPProtocolState:  public virtual Interface::IProtocolBase<TSession>
			{
				std::string _ipaddress;
				TInitializationParameters& _parameters;
				OSI::Transport::RTP::RTPRFCCode::source sourceofOtherEnd;
				OSI::Transport::RTP::RTPRFCCode::receptionReport otherEndLastReceivedReceptionReport;
				OSI::Transport::RTP::RTPRFCCode::receptionReport currentReceptionReport;
				OSI::Transport::RTP::RTPRFCCode::senderReport otherEndSenderReport;
				OSI::Transport::RTP::RTPRFCCode::senderReport currentSenderReport;
				OSManagement::Timer lastTimer;
				OSManagement::Timer currentTimer;
				uint16_t seq;
				size_t packetNumber;
				size_t bytesTransmitted;
				bool senderOrReceiver;
				TSession& _session;
			public:
				RTPProtocolState(std::string& ipaddress, TInitializationParameters& parameters, bool senderOrReceiver,TSession& session) :_ipaddress(ipaddress), _parameters(parameters),_session(session),
					senderOrReceiver(senderOrReceiver), packetNumber(0),lastTimer(0,OSManagement::TimerResolution::Milliseconds),currentTimer(0, OSManagement::TimerResolution::Milliseconds)
				{
					srand(NULL);
					seq = rand();
					// when a new connection is heard for the first time
					OSI::Transport::RTP::RTPRFCCode::init_seq(&sourceofOtherEnd, seq);
					sourceofOtherEnd.max_seq = seq - 1;
					const int MIN_SEQUENTIAL = 2;
					sourceofOtherEnd.probation = MIN_SEQUENTIAL;
					lastTimer.StartTimer();
					lastTimer.ManuallyStopTimer();
					currentTimer.StartTimer();
				
				};
				void SendToIP(RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>& outgoing)
				{
					UDP::UDPDatagram<RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>>::SendPacketAndClose(outgoing, this->_ipaddress, this->_parameters._incomingPortApplicationLayerAddress);
				};
			private:
				void CheckIfSenderPacketsDidNotArrive(void)
				{
					//  does our reception report and sender report differ?
					// Do we still have the packets in the circular buffer?
					// if so send them out again!
				}
				void ProcessReceiverReport(RTPProtocol<TFlyWeightIncoming, TInitializationParameters>& incoming)
				{
					otherEndLastReceivedReceptionReport = incoming.packetTypes.rr; // update our statistics of the other end
					
					CheckIfSenderPacketsDidNotArrive();
				};

				// these are the packets the other end sent in
				void ProcessSenderReport(RTPProtocol<TFlyWeightIncoming, TInitializationParameters>& incoming)
				{
					/*
					 *struct rtcp_rr_t
				{
					uint32_t ssrc;             // data source being reported
					uint32_t fraction : 8; // fraction lost since last SR/RR
					uint32_t lost : 24;              // cumul. no. pkts lost (signed!)
					uint32_t last_seq;       // extended last seq. no. received
					uint32_t jitter;         //interarrival jitter
					uint32_t lsr;             // last SR packet from this source
					uint32_t dlsr;             //delay since last SR packet
				};
					 * 	uint32_t ssrc;    //receiver generating this report
						rtcp_rr_t rr[1];  // variable-length list
					 */
					// compare our current copy(otherEndSenderReport) and see if we need to request for lost packets based on policy
					// CheckIfSenderPacketsDidNotArrive(incoming); // not needed will go out in next report
					// we received a sender Report so update the sender statistics
					otherEndSenderReport = incoming.packetTypes.sr;
					
				};

				void SendSenderReport(void)
				{
					RTPProtocol<TFlyWeightOutgoing, TInitializationParameters> outgoing(_parameters);
					FillSenderReportPacket(outgoing);
					SendToIP(outgoing);
				};

				void SendReceiverReport(void)
				{
					RTPProtocol<TFlyWeightOutgoing, TInitializationParameters> outgoing(_parameters);
					FillReceiverReportPacket(outgoing);
					SendToIP(outgoing);
				};

				void SendSDESPacket(void)
				{
					RTPProtocol<TFlyWeightOutgoing, TInitializationParameters> outgoing(_parameters);
					outgoing.MakeSDESPackets(_parameters);
					SendToIP(outgoing);
				}

				size_t MakeApplicationPacket(RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>& outgoing)
				{
					outgoing.MakeApplicationPacket();
					return outgoing.size();
				}

				void FillSenderReportPacket(RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>& outgoing)
				{
					RTPRFCCode::senderReport& senderReport = outgoing.packetTypes.sr;
					size_t packetInBytes = senderReport.size();
					outgoing.FillCommonPacketInHeader(RTPRFCCode::rtcp_type_t::RTCP_SR, packetInBytes);
					
					currentTimer.ManuallyStopTimer();
					// void FillSenderReport(senderReport& lastSenderReport,uint32_t ssrcOfRTPPacket, size_t packetNumber, size_t bytesSent,uint32_t ssrcOfDatasource,uint32_t lastSenderReportSource,OSManagement::Timer beforeTimer, OSManagement::Timer afterTimer)
					senderReport.FillSenderReport(currentSenderReport, _parameters.ssrcOfPacketSender,packetNumber, bytesTransmitted, _parameters.ssrcOfPacketSender,lastTimer,currentTimer);
					lastTimer = currentTimer;
					currentSenderReport = outgoing.packetTypes.sr;
					currentTimer.StartTimer();
				};
				void FillReceiverReportPacket(RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>& outgoing)
				{
					size_t packetInBytes = outgoing.packetTypes.rr.size();
					outgoing.packetTypes.rr.FillReceptionReport(currentReceptionReport, packetNumber, currentTimer, lastTimer, _parameters.ssrcOfPacketSender);
				};

				void MakeScheduledReports(void)
				{
					if (this == nullptr) return;
					RTPProtocol<TFlyWeightOutgoing, TInitializationParameters> outgoing(_parameters);
					FillSenderReportPacket(outgoing);
					SendToIP(outgoing);
					SendReceiverReport();
				}
			public:
				virtual void TimingHook()
				{
					// send SenderRepor
					MakeScheduledReports();
				}

				bool ProcessIncoming(RTPProtocol<TFlyWeightIncoming, TInitializationParameters>& incoming, RTPProtocol<TFlyWeightOutgoing, TInitializationParameters>& outgoing, std::string& ipAddress)
				{
					// if sequence is out of sequenc
					// nack back all missed packets
					OSI::Transport::RTP::RTPRFCCode::update_seq(&sourceofOtherEnd, seq);
					bool i;
					std::string leavingMessage = "goodbye";
				 	switch (incoming.common.pt)
					{
					case OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_BYE:
						this->markForDestruction = true;
						return 0;
					case OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_APP: //  not defined it's user defined you will need to derive class
						// just process payload 
						i =_session.IncomingHook(incoming.packetTypes._AppPayload._payload, outgoing.packetTypes._AppPayload._payload, ipAddress);
						if (i)
							return this->MakeApplicationPacket(outgoing);
						else
						 outgoing.MakeGoodBye(sourceofOtherEnd, leavingMessage);
						return 0;
					case OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_RR:
						ProcessReceiverReport(incoming);
						break;
					case OSI::Transport::RTP::RTPRFCCode::rtcp_type_t::RTCP_SDES:
						incoming.AssignSDESPacket();
						break;
					}
					outgoing.MakeGoodBye(sourceofOtherEnd, leavingMessage);
					return false;
				}
			};
		}
	}
}

#endif
