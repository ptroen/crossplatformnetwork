#ifndef I_REQUEST_BUILDER_H
#define I_REQUEST_BUILDER_H
#include <map>
#include <string>
#include "../HTTP/HTTPRequestType.h"
#include <sstream>
#include <vector>
namespace OSI
{
	namespace Session
	{
		namespace Interface
		{
			// request is a request for resource. This is analagous to events that is it's same thing
			class IRequestBuilder
			{
			public:
				std::string url;
				std::string siteurl;
				std::map<std::string, std::string> keyValueArgs;
				bool keyValueExists(const std::string& key)
				{
					try
					{
						bool resp = (keyValueArgs.find(key) != keyValueArgs.end());
						return resp;
					}
					catch(std::exception ex)
					{
						return false;
					}
				}
				size_t requestId; // internal id never to be shared which tells internal controllers what resource belongs to whom
				HTTP::HTTPRequestType requestType; // this indicates what type of request. Post,Put,Delete
				IRequestBuilder(void) : requestType(HTTP::HTTPRequestType::GETREQUEST), keyValueArgs(), url(), siteurl() {};
				friend std::ostream & operator << (std::ostream &out, const OSI::Session::Interface::IRequestBuilder &request)
				{
					out << request.siteurl;
					out << request.url;
					for (auto it : request.keyValueArgs)
					{
						out << it.first << " " << it.second << std::endl;
					}
					
					return out;
				};
				 friend std::istream & operator >> (std::istream &in, const OSI::Session::Interface::IRequestBuilder &request)
				{

					 std::string& siteurl = const_cast<std::string&>(request.siteurl);
					std::getline(in, siteurl,'\n');
					std::string& url = const_cast<std::string&>( request.url);
					std::getline(in, url,'\n');
					// citation: https://stackoverflow.com/questions/18855128/is-there-a-way-to-stream-in-a-map
					for (std::string line; std::getline(in, line,'\n'); )
					{
						std::stringstream theline(line);
						std::string segment;
						std::vector<std::string> seglist;

						while (std::getline(theline, segment, ':'))
						{
							seglist.push_back(segment);
						}
						if (seglist.size() >= 2) // requested for key value
						{
							std::map<std::string, std::string>& keyVal = const_cast<std::map<std::string, std::string>&>(request.keyValueArgs);
							keyVal[seglist[0]] = seglist[1];
						}
						else
							break;
					}
					return in;
				};
				 virtual std::string ToRequest(void) { return std::string(); };
			};
		}
	}
}
#endif