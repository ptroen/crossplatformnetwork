#ifndef I_POST_HTTP_REQUEST_HOOK_H
#define I_POST_HTTP_REQUEST_HOOK_H
#include <map>
#include <string>
class IPostHTTPRequestHook
{
   public:
   virtual void RunRequest(std::map<std::string,std::string>& kvp_request,unsigned char* postblob, size_t postblobSize){};
};
#endif