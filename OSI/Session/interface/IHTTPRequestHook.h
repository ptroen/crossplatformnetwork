#ifndef I_HTTP_REQUEST_HOOK_H
#define I_HTTP_REQUEST_HOOK_H

#include <map>
#include <string>

namespace OSI
{
	namespace Session
	{
		namespace Interface
		{
			class IHTTPRequestHook
			{
			public:
				virtual void RunRequest(std::map<std::string, std::string>& kvp_request) {};
			};
		}
	}
}
#endif