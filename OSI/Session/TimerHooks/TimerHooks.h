#ifndef TIMER_HOOKS__H
#define TIMER_HOOKS__H
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <sstream>
#include <cstdio>
#include <ctime>
#include <time.h>
#define BOOST_ASIO_HAS_STD_CHRONO
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "../IOContextManager.h"
#include <chrono>
#include "../../Transport/interface/ITransport.h"
#include "../../Transport/interface/ITransportInitializationParameters.h"
#include "../../../OSManagement/timer.h"
#include "../../Transport/interface/IClientTransportHooks.h"
#include "../../Transport/interface/IServerTransportHooks.h"
#include "../../Session/IPSecurity/IPSecurity.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
namespace OSI
{
	namespace Session
	{
		namespace TimerHooks
		{
			template <class TSession, class TTransportInitializationParameters>
			class TimerHooks
			{
			     inline void logTime(void) {
                    #ifndef ANDROID
                    std::string msg;
					auto elapsed = boost::chrono::thread_clock::now() - start;
					std::chrono::system_clock::time_point p = std::chrono::system_clock::now();
					auto t = std::chrono::system_clock::to_time_t(p);
					msg = std::ctime(&t);
					msg += " ";
					msg +=  "duration=";
					msg += std::to_string(boost::chrono::duration_cast<boost::chrono::microseconds>(elapsed).count());
					msg += " microseconds. ";
					LOGIT1(msg)
                    #endif
				};
			protected:
				OSManagement::Timer onTimerTimer;
				OSManagement::Timer onIdleTimer;
				TTransportInitializationParameters& _parameters;
				boost::chrono::thread_clock::time_point start;
			public:
				TSession session;
				TimerHooks(TTransportInitializationParameters& parameters) :session(parameters),onTimerTimer(parameters._timerTimeout,OSManagement::TimerResolution::Milliseconds),_parameters(parameters),
				onIdleTimer(parameters._idleTimeout, OSManagement::TimerResolution::Milliseconds)
				{
                    #ifndef ANDROID
                    start = boost::chrono::thread_clock::now();
                    #endif
					onTimerTimer.StartTimer();
					onIdleTimer.StartTimer();
				};

			public:
				virtual void OnIdleHook(void)
				{
					IPSecurity::IPSecurity::Get_Instance().ReviewIPsForExpirationOfBan(); // IP Security does this in its own thread now
					session.OnIdleHook();
				}
				virtual void OnTimerHook(void){
                    logTime();
                    session.OnTimerHook();
				};

				 void CheckTimers(void)
				 {
					 if(onTimerTimer.Expired())
					 {
						 OnTimerHook();
					 }
					 if(onIdleTimer.Expired())
					 {
						 OnIdleHook();
					 }
				 }
				 void TurnOnTimers(void)
				 {
					 if (onTimerTimer.Expired())
					 {
						 onTimerTimer.ResetTimer();
						 onTimerTimer.StartTimer();
					 }
					 if(onIdleTimer.Expired())
					 {
						 onIdleTimer.ResetTimer();
						 onIdleTimer.StartTimer();
					 }
				 };
			};

			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class ClientTimerHooks : public virtual TimerHooks<TSession, TTransportInitializationParameters>
			{
			public:
				TSession _session;
				ClientTimerHooks(TTransportInitializationParameters& t) :TimerHooks<TSession, TTransportInitializationParameters>(t),_session(t){};
				virtual bool IncomingHook(TFlyWeightServerOutgoing& in, TFlyWeightServerIncoming& out, std::string& ipadress)
				{
					TimerHooks<TSession, TTransportInitializationParameters>::CheckTimers();
					bool ret;
					ret = _session.IncomingHook(in, out, ipadress);
					TimerHooks<TSession, TTransportInitializationParameters>::TurnOnTimers();
					return ret;
				};
			};

			template <class TFlyWeightServerIncoming, class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class ServerTimerHooks : public virtual TimerHooks<TSession,TTransportInitializationParameters>,
				public virtual Transport::Interface::IServerTransportHooks<TFlyWeightServerIncoming, TFlyWeightServerOutgoing>
			{
				public:
					ServerTimerHooks(TTransportInitializationParameters& t) :TimerHooks<TSession,TTransportInitializationParameters>(t) {};
					virtual bool IncomingHook(TFlyWeightServerIncoming& in, TFlyWeightServerOutgoing& out, std::string& ipadress)
					{
						TimerHooks<TSession,TTransportInitializationParameters>::CheckTimers();
						bool ret;
						ret = TimerHooks<TSession,TTransportInitializationParameters>::session.IncomingHook(in, out, ipadress);
						TimerHooks<TSession,TTransportInitializationParameters>::TurnOnTimers();
						return ret;
					};
			};

			template <class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class ServerMulticastTimerHooks : public virtual TimerHooks<TSession, TTransportInitializationParameters>
			{
			public:
				ServerMulticastTimerHooks(TTransportInitializationParameters& t) :TimerHooks<TSession, TTransportInitializationParameters>(t) {};
				virtual bool IncomingHook(TFlyWeightServerOutgoing& out)
				{
					TimerHooks<TSession, TTransportInitializationParameters>::CheckTimers();
					bool ret;
					ret = TimerHooks<TSession, TTransportInitializationParameters>::session.IncomingHook(out);
					TimerHooks<TSession,TTransportInitializationParameters>::TurnOnTimers();
					return ret;
				};
			};

			template <class TFlyWeightServerOutgoing, class TSession, class TTransportInitializationParameters>
			class ClientMulticastTimerHooks : public virtual TimerHooks<TSession, TTransportInitializationParameters>
			{
			public:
				TSession _session;
				ClientMulticastTimerHooks(TTransportInitializationParameters& t) :TimerHooks<TSession, TTransportInitializationParameters>(t),_session(t) {};
				virtual bool IncomingHook(TFlyWeightServerOutgoing& in)
				{
					TimerHooks<TSession, TTransportInitializationParameters>::CheckTimers();
					bool ret;
					ret = _session.IncomingHook(in);
					TimerHooks<TSession,TTransportInitializationParameters>::TurnOnTimers();
					return ret;
				};
			};
		}
	}
}

#endif
