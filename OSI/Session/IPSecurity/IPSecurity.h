#ifndef IP_SECURITY_H
#define IP_SECURITY_H
#include <string>
#include "../../../OSManagement/Firewall/FirewallHeader.h"
#include "../../../OSManagement/timer.h"
#include "../../Transport/IPType.h"
#include "../../../algorithm/icriticalthread.h"
namespace OSI
{
	namespace Session
	{
		namespace IPSecurity
		{
			// not thread safe
			class IPSecurity : protected virtual Algorithm::ICriticalThread
			{
				size_t packetThrottlingPolicyInSeconds;
				static IPSecurity* _instance;
                size_t banLengthInSeconds;
                OSManagement::Firewall::FirewallConfiguration configuration;
                OSManagement::Firewall::PreroutingChain* preroutingChain;
                std::pair<std::string,std::string> ipAndMessage;
                std::vector<std::pair<OSManagement::Timer,std::string>> flaggedIps;

				IPSecurity(void):banLengthInSeconds(0), packetThrottlingPolicyInSeconds(0),configuration(),preroutingChain(nullptr),flaggedIps(){
                    std::string blank("");
                    ipAndMessage = std::make_pair(blank,blank);
                    configuration.flushExistingRules = false;
					configuration.allowDHCP = false;
					configuration.allowSSH = false;
					configuration.blockPortZero = true;
					configuration.ipAddressesToBlock.clear();
					configuration.ipAddressesToUnBlock.clear();
					preroutingChain = new OSManagement::Firewall::PreroutingChain(configuration);

				};
				public:

				//checkIP(std::string& ip); not needed IPTables will flag the IP
				// maybe pass string into shell script

				void FlagIP(std::string& ip, std::string& message)
				{
                    IPSecurity::_instance->lock();
                    IPSecurity::_instance->ipAndMessage.first = ip;
                    IPSecurity::_instance->ipAndMessage.second = message;
                    IPSecurity::_instance->unlock();
				}

				static void FirewallLoop(void)
				{
					while(1)
					{
						_instance->lock();
						if (IPSecurity::_instance->ipAndMessage.first.size() > 0)
						{
                            std::string ip = _instance->ipAndMessage.first;
                            std::string message = _instance->ipAndMessage.second;
							//_instance->FlagIPWorkerThread(ip,message);
						}
                        _instance->unlock();
                        _instance->ReviewIPsForExpirationOfBan();
					}
				};

				void FlagIPWorkerThread(std::string& ip, std::string& message)
				{
					// process the Ban
					configuration.ipAddressesToBlock.clear();
					configuration.ipAddressesToBlock.push_back(ip);
					preroutingChain->Run();
					std::pair<OSManagement::Timer, std::string> flaggedEntry(OSManagement::Timer(banLengthInSeconds,OSManagement::TimerResolution::seconds), ip);
					flaggedEntry.first.StartTimer();
					flaggedIps.push_back(flaggedEntry);
				};
				static IPSecurity& Get_Instance(void) {
					if (_instance == nullptr)
					{
						_instance = new IPSecurity();
					}
					return *_instance;
				};
				    // should be called on Idle
					void ReviewIPsForExpirationOfBan(void)
					{
						for(auto it =flaggedIps.begin();it!=flaggedIps.end();)
						{
							if(it->first.Expired())
							{
                                std::string msg = "ip ban expired:";
                                msg +=it->second;
                                LOGIT1(msg)

                                configuration.ipAddressesToUnBlock.clear();
								configuration.ipAddressesToUnBlock.push_back(it->second);
								preroutingChain->Run();
								flaggedIps.erase(it);
							}
							else
							{
								++it;
							}
						}
					};

				};

			}
	}
}
#endif
