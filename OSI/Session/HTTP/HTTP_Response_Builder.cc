#include "HTTP_Response_Builder.h"
#include "HTTP_STATUS_CODE.h"
#include <chrono>
#include <iomanip>
#include <sstream>

using namespace OSI::Session;

HTTP::HTTP_Response_Builder::HTTP_Response_Builder() :status(OSI::Session::HTTP_STATUS_CODE::OK),
customResponsePageCode()
{

}

std::string OSI::Session::HTTP::HTTP_Response_Builder::MakeDate(void)
{
    // this could be a year 2031 problem. maybe refactor to the
    // new date object(see OSMananagement::DateTime)
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	std::stringstream ss;
	struct tm newtime;
	#ifdef _WIN32
	localtime_s(&newtime, &in_time_t); 
	#else 
	localtime(&in_time_t);
	#endif
	ss << std::put_time(&newtime, "%Y-%m-%d %X");
	return ss.str();
}

void HTTP::HTTP_Response_Builder::BuildHTTPContentType(ContentRecordType& contentType)
{
	// https://www.geeksforgeeks.org/http-headers-content-type/
    // which is the discretion of the server to choose
	out_resp += "Content-Type:";
	out_resp += contentType.GetHtmlContentType();
	out_resp += newline;
}

void HTTP::HTTP_Response_Builder::ConstructHTTPResponsePacket(OSI::Session::HTTP::HTTP_Request_Builder& requestInfo, ContentRecordType& contentType)
{
	//_contentType = contentType;
	out_resp.clear();
	std::string& customResponse = customResponsePageCode;

	// status line
	int i = status;

	// make a TCPServerWrapper requestInfo ie HTTP/1.1 200 OK
	out_resp += std::string("HTTP/1.1 ") + std::to_string(i) + " " + status._to_string() + newline;

	// encode the date
	out_resp += std::string("DATE: ") + MakeDate() + newline;

	// declare TCPServerWrapper type
	out_resp += "Server: Custom" + newline;

	// encode when the data was last modified
	out_resp += "Last Modified:" + MakeDate() + newline;

	// Post Response	
	out_resp += "Content-Length:" + std::to_string(customResponse.length()) + newline;

	BuildHTTPContentType(contentType);

	// if zip make it zip
	if (requestInfo.gzipEnabled && contentType.CompressAble())
		out_resp += "Content-Encoding: zip" + newline;
	out_resp += "Connection: Closed" + newline;

	// end of message
	out_resp += newline;
	out_resp += customResponse;
};
