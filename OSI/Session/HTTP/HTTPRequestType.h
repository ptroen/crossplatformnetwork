#ifndef HTTP_REQUEST_TYPE_H
#define HTTP_REQUEST_TYPE_H
namespace OSI
{
	namespace Session
	{
		namespace HTTP
		{
			enum class HTTPRequestType : char 
			{
				NOREQUESTTYPE,
				GETREQUEST,
				POSTREQUEST,
				PUTREQUEST,
				DELETEREQUEST
			};
		}
	}
}
#endif