#ifndef HTTP_REQUEST_BUILDER_H
#define HTTP_REQUEST_BUILDER_H
#include <map>
#include <string>
#ifndef WINDOWS
#include <cstring> // for strncpy with g++
#endif
#include <vector>
#include "../../../algorithm/stringtokenizer.h"
#include "HTTPRequestType.h"
#include <iostream>
#include "../interface/IRequestBuilder.h"
namespace OSI
{
	namespace Session
	{
		namespace HTTP
		{
			class HTTP_Request_Builder;
			// Builds the HTTP Request into in memory

			const std::string httpStaticResourceDirectory = "/www";
			class HTTP_Request_Builder : public virtual Interface::IRequestBuilder
			{
			public:
				std::string data;
				bool gzipEnabled;
			private:
				 const char* defaultPage = "index.html";
				 const char* newline = "\r\n";
			public:
				HTTP_Request_Builder(void) : Interface::IRequestBuilder(), gzipEnabled(false),data() {};
				void ParsePostOrPut(std::string& PostLine)
				{
					// sample post
					// key1=value1&key2=value2
					std::vector<std::string> argumentResults = Algorithm::String::split(PostLine, '&');
					for (auto line : argumentResults)
					{
						std::vector<std::string> kvP = Algorithm::String::split(line, '=');
						if (kvP.size() > 1)
						{
							// extracting key value pair kvp 0 is the key 1 is the value
							keyValueArgs[kvP[0]] = kvP[1];
						}
					}
				};
			private:
				void LoadPageOnDirectoryPath(void)
				{
					if (url == httpStaticResourceDirectory)// if we got a blank page
					{
						url += "/" + std::string(defaultPage); // load index
					}
					else if (url[url.size() - 1] == '/' || url[url.size() - 1]=='\\')// if we got a directory load index
					{
						url += std::string(defaultPage); // load index
					}
					else if(url.find(".") == std::string::npos)
					{
						url+= +"/" + std::string(defaultPage); // if no extension found we assume its a directory
					}
				}
				void ParseHTTPUrl(std::string& url, std::map<std::string, std::string>& get_keyValueArgs, bool postOrPut = false)
				{
					// Parse the Get Line and it's respective Get Value arguments
					// GETREQUEST URL.html?arg1=value&arg2=value HTTP/1.1

					std::vector<std::string> questionMarkResults;

					std::vector<std::string> urlResults = Algorithm::String::split(url, ' ');
					if (!postOrPut)
						url = urlResults[0];
					if (postOrPut == false)
					{
						questionMarkResults = Algorithm::String::split(url, '?');
					}
					if (questionMarkResults.size() >= 2)
					{
						std::vector<std::string> argumentResults = Algorithm::String::split(questionMarkResults[1], '&');
						for (auto line : argumentResults)
						{
							std::vector<std::string> kvP = Algorithm::String::split(line, '=');
							if (kvP.size() > 1)
							{
								// extracting key value pair kvp 0 is the key 1 is the value
								get_keyValueArgs[kvP[0]] = kvP[1];
							}
						}
					}
				};
				bool ProtocolRequest() { return requestType != HTTPRequestType::NOREQUESTTYPE; };
			public:
				void ParseHTTPRequestLine(std::string& line)
				{
					const std::string httpTag("HTTP/");
					size_t httpTagLocation = line.find(httpTag); // could be HTTP/1.1 or HTTP/1.0 if really old. Heck maybe HTTP/2.0 if some new standard?
					const size_t notFound = -1;
					if (httpTagLocation == notFound) return;
					const std::string Get = "GET";
					const std::string Post = "POST";
					const std::string Put = "PUT";
					const std::string Delete = "DELETE";
					size_t getLocation = line.find(Get);
					size_t postLocation = line.find(Post);
					size_t putLocation = line.find(Put);
					size_t deleteLocation = line.find(Delete);
					size_t endHTTPLength = httpTagLocation - httpTag.size();
					url.resize(endHTTPLength);
					if (getLocation != notFound && httpTagLocation != notFound)
					{
						size_t GetSize = Get.size();
						size_t getLineSize = endHTTPLength + 1;
						url.resize(getLineSize);
						strncpy(&url[0], &line[getLocation + Get.size() +1], getLineSize);
						requestType = HTTPRequestType::GETREQUEST;
						// extract the substring
						
						// remove the ? and get the true url
						ParseHTTPUrl(url, keyValueArgs);
					}
					else if (postLocation != notFound && httpTagLocation != notFound)
					{
						size_t postLineSize = endHTTPLength - 1;
						url.resize(postLineSize);;
						strncpy(&url[0], &line[postLocation + Post.size() + 1], postLineSize);
						requestType = HTTPRequestType::POSTREQUEST;
						/*
						* POSTREQUEST /test/demo_form.php HTTP/1.1
						Host: w3schools.com

						name1=value1&name2=value2
						*/
					}
					else if (putLocation != notFound && httpTagLocation != notFound)
					{
						size_t putSize = Put.size();
						size_t putLineSize = endHTTPLength;
						url.resize(putLineSize);
						strncpy(&url[0], &line[putLocation+putSize +1], putLineSize);
						requestType = HTTPRequestType::PUTREQUEST;
						// PUTREQUEST /new.html HTTP/1.1 
						// citation: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUTREQUEST
					}
					else if (deleteLocation != notFound && httpTagLocation != notFound)
					{
						size_t stringLength = endHTTPLength - 3;
						url.resize(stringLength);
						strncpy(&url[0], &line[deleteLocation+Delete.size()+1], stringLength);
						requestType = HTTPRequestType::DELETEREQUEST;
					}
					size_t questionMarkLocation = url.find('?');

					if (questionMarkLocation != -1)
					{
						url = url.substr(0, questionMarkLocation);
					}
					url = httpStaticResourceDirectory + url;
					LoadPageOnDirectoryPath();
				};
			protected:
				std::string DumpKeyValuePairs(bool get)
				{
					std::string str;
					// post format name1=value1&name2=value2 
					if (!this->keyValueArgs.empty())
					{
						if (get)
						{
							str += "?";
						}
						auto iter = keyValueArgs.begin();
						str += iter->first;
						str += "=";
						str += iter->second;
						++iter;
						for (; iter != keyValueArgs.end(); ++iter)
						{
							str += "&";
							str += iter->first;
							str += "=";
							str += iter->second;
						}
						if(!get)
						str += newline;
					}
					return str;
				};
				void MakeProtocolLine(std::string& currentLine)
				{
					const std::string HTTPONE = " HTTP/1.1";
					//"GET / hello.htm ? parameters HTTP / 1.1"
					//POST / test / demo_form.php HTTP / 1.1
					switch (this->requestType)
					{
					case HTTPRequestType::NOREQUESTTYPE:
					case HTTPRequestType::GETREQUEST:
						currentLine = "GET";
						break;
					case HTTPRequestType::PUTREQUEST:
						currentLine = "PUT";
						break;
					case HTTPRequestType::POSTREQUEST:
						currentLine = "POST";
						break;
					case HTTPRequestType::DELETEREQUEST:
						currentLine = "DELETE";
						break;
					}
					currentLine += " ";
					if(url[0] !='\\' && url[0] !='/')
						currentLine += "/";
					currentLine += url;
					switch (this->requestType)
					{
					case HTTPRequestType::GETREQUEST:
						currentLine += DumpKeyValuePairs(true);
					}
					currentLine += HTTPONE;
					currentLine += newline;
				};
				void MakeHostLine(std::string& currentLine)
				{
					currentLine += "Host: ";
					currentLine += siteurl;   // Host: w3schools.com
					currentLine += newline;
				};

				void AcceptEncoding(std::string& currentLine)
				{
					if (gzipEnabled)
					{
						currentLine += "Accept-Encoding: gzip"; // Accept-Encoding: gzip, deflate
						currentLine += newline;
					}
				};
				void AcceptLanguage(std::string& currentLine)
				{
					currentLine += "Accept-Language: en-us"; // Accept-Language: en-us
					currentLine += newline;
				};
				void ConnectionState(std::string& currentLine)
				{
					currentLine += "Connection: Keep-Alive";
					currentLine += newline;
				};
				void UserAgent(std::string& currentLine)
				{
                    // currently only makes one user agent
                    // but this should really be expanded for
                    // the client request
					currentLine += "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)";
					currentLine += newline;
				};
				void ExtraDataAfterBody(std::string& currentLine)
				{
                    // for client request dumps appropriate
                    // request info
					switch (this->requestType)
					{
					case HTTPRequestType::GETREQUEST:
					case HTTPRequestType::NOREQUESTTYPE:

						return;
					}
					switch (this->requestType)
					{
					case HTTPRequestType::POSTREQUEST:
					case HTTPRequestType::PUTREQUEST:
					case HTTPRequestType::DELETEREQUEST:
						currentLine += DumpKeyValuePairs(false);
						currentLine += newline;
					}
					// otherwise get and put which we dump the data
					currentLine += data;
				};
			public:
				// converts to a string based http format
				virtual std::string ToRequest(void)
				{
                    // how client dumps the request
					std::string request;
					MakeProtocolLine(request);
					MakeHostLine(request);
					AcceptEncoding(request);
					AcceptLanguage(request);
					ConnectionState(request);
                    // make user agent?
					request += newline;
					request += newline;
					ExtraDataAfterBody(request);
					return request;
				};
				
			};
		}
	}
}

#endif
