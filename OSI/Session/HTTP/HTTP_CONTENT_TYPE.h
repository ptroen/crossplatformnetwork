#ifndef HTTP_CONTENT_TYPE_H
#define HTTP_CONTENT_TYPE_H

#include "../../../ThirdParty/enum.h"
namespace OSI
{
	namespace Session
	{
		BETTER_ENUM(HTTP_CONTENT_TYPE, int32_t, MIME, text_html,text_xml, APPLICATION_JAVASCRIPT, text_plain,AUDIO_MPEG,image_gif,image_jpeg,image_png,image_tiff, VIDEO, JSON, CONSOLE, NOTCONTENTTYPE)
	}
}

#endif
