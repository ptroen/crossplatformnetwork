#include "ContentRecordType.h"

std::map<std::string, OSI::Session::HTTP_CONTENT_TYPE> OSI::Session::ContentRecordType::mimes = { {"jpeg",HTTP_CONTENT_TYPE::image_jpeg},
																																		{"jpg",HTTP_CONTENT_TYPE::image_jpeg},
																																		{"png",HTTP_CONTENT_TYPE::image_png},
																																		{"tiff",HTTP_CONTENT_TYPE::image_tiff},
																																		{"xml",HTTP_CONTENT_TYPE::text_xml},
																																		{"html",HTTP_CONTENT_TYPE::text_html},
{"json",HTTP_CONTENT_TYPE::JSON},
{"js",HTTP_CONTENT_TYPE::APPLICATION_JAVASCRIPT}};