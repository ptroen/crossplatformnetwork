#ifndef HTTP_CONTENT_RECORD_TYPE_H
#define HTTP_CONTENT_RECORD_TYPE_H
#include <string>
#include "HTTP_CONTENT_TYPE.h"
#include "../../../algorithm/stringtokenizer.h"
#include <map>
namespace OSI
{
	namespace Session
	{
		struct ContentRecordType
		{
			std::string contentAsAString; // this is the actual text data read from the file
			static std::map<std::string, HTTP_CONTENT_TYPE> mimes;
			HTTP_CONTENT_TYPE _content;
			ContentRecordType(void) :_content(HTTP_CONTENT_TYPE::text_html)
			{

			};

			ContentRecordType(std::string& s) :contentAsAString(s), _content(HTTP_CONTENT_TYPE::text_html) {};
            
            // just converts to a html type
			std::string GetHtmlContentType()
			{
				// put mime types HERE!
				std::string contentString;
                // putting the mime type since it's a better enum type
				contentString = _content._to_string();
				// replaces underscore with / to construct the proper mime type in HTML speak
				::Algorithm::String::replace(contentString, "_", "/");
				return contentString;
			};

			// some content types we don't compress
			bool CompressAble(void)
			{
				
				switch(_content)
				{
				case HTTP_CONTENT_TYPE::text_html:
				case HTTP_CONTENT_TYPE::text_xml:
				case HTTP_CONTENT_TYPE::text_plain:
					return true;
				default:
					return false;
				}
				return true;
			}

			void PickProperMimeType(std::string& filenameWithExtension)
			{
				// citation: https://stackoverflow.com/questions/51949/how-to-get-file-extension-from-string-in-c
				std::string substring = filenameWithExtension.substr(filenameWithExtension.find_last_of(".") + 1);
				
				if (mimes.find(substring) != mimes.end())
				{
					_content = mimes[substring];
				}
				else
				{
					// looking for a extension
					//_content = OSI::Session::HTTP_CONTENT_TYPE::text_plain;
				}
			}
			virtual ~ContentRecordType(void) {};
		}; // end class
	}
}
#endif
