#ifndef HTTP_RESPONSE_BUILDER_H
#define HTTP_RESPONSE_BUILDER_H
#include <string>
#include "ContentRecordType.h"
#include "HTTP_Request_Builder.h"
#include "HTTP_STATUS_CODE.h"
//#include "../../../../../MVC/View/interface/IView.h"
//#include "../../../../../MVC/Controller/interface/IController.h"
#include "../../../MVC/Model/interface/IModel.h"
#include "../../../MVC/Model/ModelContentLoader.h"
namespace OSI
{
	namespace Session
	{
		namespace HTTP
		{
			class HTTP_Response_Builder // : public virtual MVC::View::Interface::IView
			{
				const std::string newline = "\r\n";
				std::string out_resp;
				std::string MakeDate(void);

				// this could be html or it could be text or one of many mime types. Render this as the standard http ContentType tag.
				void BuildHTTPContentType(ContentRecordType& contentType);
			public:
				HTTP_Response_Builder();
				virtual ~HTTP_Response_Builder() {};
				std::string customResponsePageCode;
				HTTP_STATUS_CODE status;
			public:
				std::string& GetResponse(void) { return out_resp; }
				void ConstructHTTPResponsePacket(HTTP::HTTP_Request_Builder& request, ContentRecordType& contentType);
				//virtual void Render(MVC::Model::Interface::IModel& currentModel);
			};
		}
	}

}

#endif