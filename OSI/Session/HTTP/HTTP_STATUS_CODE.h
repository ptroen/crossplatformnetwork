#ifndef HTTP_STATUS_CODE_H
#define HTTP_STATUS_CODE_H
#include "../../../ThirdParty/enum.h"
namespace OSI
{
	namespace Session
	{
		BETTER_ENUM(HTTP_STATUS_CODE, int32_t, OK = 200, CREATED = 201, ACCEPTED = 202, MovedPermenatly = 301, Forbidden = 403, TooManyRequests = 429, InternalServerError = 500)
	}	
}


#endif