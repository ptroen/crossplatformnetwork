#ifndef IO_CONTEXT_MANAGER_H
#define IO_CONTEXT_MANAGER_H
#include <boost/asio.hpp>
#include <boost/bind.hpp>
namespace OSI
{
	namespace Session
	{
		class IOContextManager
		{
			public:
			static boost::asio::io_context io_context;



			static void Reboot(){io_context.restart();};
		};
	}
}
#endif
