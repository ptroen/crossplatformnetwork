#ifndef TCP_UTIL_H
#define TCP_UTIL_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>

#include <errno.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>

#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include "LogStruct.h"
#define TCP_SERVER_PORT 7000;

struct socket_info
{
	int sd;				// socket  descriptor
	struct hostent*   _hostent;
	struct sockaddr_in  _server;
	int		  _port;
	char  _UserDesiredHost[160];
};

struct IOBuffers
{
	char receive[LENGTH];
	char send[LENGTH];
};

enum ErrorCode
{
	Success = 0,
	CantConnectToServer = 1,
	UnknownServerAddressEnd = 2,
	ARGCEND = 3,
	END = 4,
	SOCKETCREATIONERROR = 5,
	TOOLONGHOSTNAME=6
};

// maybe put in assembly http://stackoverflow.com/questions/14930830/can-gcc-compile-x86-assembly-or-just-link-it
enum ErrorCode inline client_connect(struct socket_info* _info)
{
	char str[128];
	// Create the socket
	if ((_info->sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		return SOCKETCREATIONERROR;
	}

	bzero((char *)&_info->_server, sizeof(struct sockaddr_in));
	_info->_server.sin_family = AF_INET;
	_info->_server.sin_port = htons(_info->_port);
	if ((_info->_hostent = gethostbyname(_info->_UserDesiredHost)) == NULL)
	{
		return UnknownServerAddressEnd;
	}
	bcopy(_info->_hostent->h_addr, (char *)&_info->_server.sin_addr, _info->_hostent->h_length);

	// Connecting to the TCPClientServerWrapper
	if (connect(_info->sd, (struct sockaddr *)&_info->_server, sizeof(_info->_server)) == -1)
	{
		return CantConnectToServer;
	}
	printf("Connected:    Server Name: %s\n", _info->_hostent->h_name);
	char **pptr;
	pptr = _info->_hostent->h_addr_list;
	printf("\t\tIP Address: %s\n",
		inet_ntop(_info->_hostent->h_addrtype,
		*pptr,
		str,
		sizeof(str)));

	// end initialize socket and connect
	return Success;
}

int64_t GetTime(){
	struct timespec tms;
	if(clock_gettime(CLOCK_REALTIME,&tms))
		return -1;
	int64_t micros= tms.tv_sec*1000000;
	micros+= tms.tv_nsec/1000;
	if(tms.tv_nsec %1000>=500)
		++micros;
	return micros;
}

void inline ClientChatSession(struct socket_info* _info,
			char* stringtosend,
			int number_of_times_to_send,
			struct IOBuffers* buffer)
{
	int i ;
	for (i = 0; i < number_of_times_to_send; i++)
	{
		int len = strlen(stringtosend);
		int bytes_to_copy = (len < LENGTH) ? len : LENGTH;
		memcpy(&buffer->send, stringtosend, bytes_to_copy);
		printf("Sended %s \n", buffer->send);
		send(_info->sd,
			buffer->send,
			bytes_to_copy,
			0);
		// blocking wait for the TCPClientServerWrapper to give you your echo
		recv(_info->sd, buffer->receive, LENGTH, 0);
		printf("Received %s \n", buffer->receive);
	}
}

int inline ServerHttpSession(int sd,
			     char* buffer,Logger& l,size_t& totalbytes)
{
		recv(sd, buffer, LENGTH, 0);
		int len = strlen(buffer);
		int bytes_to_copy = (len < LENGTH) ? len : LENGTH;
		totalbytes+=bytes_to_copy;
		l.Log(ECHO, buffer,bytes_to_copy);
		return send(sd, buffer, bytes_to_copy, 0);
}

int inline ServerHttpSession(int sd,
			     char* buffer)
{
		recv(sd, buffer, LENGTH, 0);
		printf("Received and echoing back %s \n", buffer);
		int len = strlen(buffer);
		int bytes_to_copy = (len < LENGTH) ? len : LENGTH;
		return send(sd, buffer, bytes_to_copy, 0);
}

#endif
