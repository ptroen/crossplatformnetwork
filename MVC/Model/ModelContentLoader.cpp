#include "ModelContentLoader.h"
#include "../../OSManagement/FileSystem/FILEIOFacade.h"
#include "../../OSI/Session/HTTP/ContentRecordType.h"
#include "../../algorithm/stringtokenizer.h"
#include <iostream>
#include <boost/filesystem/operations.hpp>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdio.h>
#include "../../OSI/Presentation/HTTP/HTTPCompressor.h"

std::map <std::string, std::unique_ptr<OSI::Session::ContentRecordType>> MVC::Model::ModelContentLoader::contentLoadedIn;

void MVC::Model::ModelContentLoader::contentLoadingHandler(void* httpHook, const boost::filesystem::path& pathFound)
{
	std::wcout << pathFound.string().c_str() << std::endl;

	// citation reading  a contentAsAString into a string
	std::string currentPath = boost::filesystem::current_path().string();
	// get the contentAsAString size
	
	std::string relativePath = pathFound.string();
	bool replaced = Algorithm::String::replace(relativePath, currentPath, "");
	replaced = Algorithm::String::replace(relativePath, "\\", "/");
	Algorithm::String::trim(relativePath);
	Algorithm::String::replace(relativePath, "\\", "/");
	std::string absPath = currentPath + relativePath;
	std::string contentStr;
	FILE * pFile;
	pFile = fopen(absPath.c_str(), "rb");
	fseek(pFile, 0, SEEK_END);
	size_t filesize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	contentStr.resize(filesize);
	fread(const_cast<char*>(contentStr.c_str()),1, filesize, pFile);
	fclose(pFile);
	// optimize the string
	contentLoadedIn[relativePath] = std::make_unique<OSI::Session::ContentRecordType>();
	contentLoadedIn[relativePath].get()->contentAsAString = contentStr;
}

void MVC::Model::ModelContentLoader::ReadFileSystemAtRoot(void)
{
	// works cited: https://beta.boost.org/doc/libs/1_45_0/libs/filesystem/v3/doc/tutorial.html
	std::wstring directoryPrefix(L"/www/");
	boost::filesystem::path path(boost::filesystem::current_path().wstring() + directoryPrefix);
	OSManagement::FileSystem::FileIOFacade::ReadFileSystem(true, path, this, &contentLoadingHandler);
};