#ifndef MODEL_UTILS_H
#define MODEL_UTILS_H
#include <sstream>
#include <vector>
#include <string>
#include <stdlib.h>     /* atoi */
// if no error in the error object create
#define NO_ERROR_CREATE(modelobj)try\
{\
if (modelobj->errorMessages.size() == 0)\
modelobj->create();\
}\
catch (std::exception ex)\
{\
auto what = ex.what();\
auto msg = std::string(what);\
LOGIT(msg)\
}

// to remove the controller boiler plate code of 1. if(key) => model->key = key else error
#define KEY_FIND_ASSIGN(keyname,modelobj,modelkeyname,errormsg) if (request.keyValueExists(keyname))\
{\
modelobj->modelkeyname = request.keyValueArgs[keyname];\
}\
else\
{\
modelobj->errorMessages.push_back(errormsg);\
}
/*
else\
{\
modelobj->errorMessages.push_back(errormsg);\
}
*/
#define KEY_FIND_ASSIGN_INTEGER(keyname,modelobj,modelkeyname,errormsg) if (request.keyValueExists(keyname))\
{\
modelobj->modelkeyname = atoi(request.keyValueArgs[keyname].c_str());\
}
/*
else\
{\
modelobj->errorMessages.push_back(errormsg);\
}*/

#define KEY_FIND_ASSIGN_MULTILINE(keyname,modelobj,modelkeyname,errormsg) if (request.keyValueExists(keyname))\
{\
auto ss = std::stringstream(request.keyValueArgs[keyname]);\
for (std::string line; std::getline(ss, line, '\n');)\
modelobj->modelkeyname.push_back(line);\
}\
else\
{\
modelobj->errorMessages.push_back(errormsg);\
}

#endif
