#ifndef FORUM_MENU_BAR_MODEL_H
#define FORUM_MENU_BAR_MODEL_H
#include "../interface/IModel.h"
namespace MVC
{
	namespace Model
	{
		namespace Menus
		{
			class ForumMenuBarModel : public virtual Interface::IModel
			{
				public:
				std::string userid;
				std::string fk_forumid;
				//std::string postid;
				ForumMenuBarModel(void) :userid(""), fk_forumid("") {};
			};
		}
	}
}
#endif