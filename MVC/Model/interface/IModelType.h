#ifndef I_MODEL_TYPE_H
#define I_MODEL_TYPE_H
//#include "IVariant.h"
//#include "IVariantType.h"
namespace MVC
{
	namespace Model
	{
		namespace Interface
		{
			// a single atomic type. Analogy is a row type in a database but this is in memory
			struct IModelType
			{
				IModelType() {};
				virtual ~IModelType() {};
				//MVC::Model::Interface::IVariant variant;
				//IVariantType variantType;
			};
		}
	}
}
#endif