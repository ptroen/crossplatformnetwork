#ifndef I_VARIANT_H
#define I_VARIANT_H
#include <cstdint>
#include <string>
namespace MVC
{
	namespace Model
	{
		namespace Interface
		{
			union IVariant
			{
				IVariant() {};
				std::int32_t int32t;     // occupies 4 bytes
				std::uint16_t uint16t; // occupies 4 bytes
				std::uint8_t uint8t;     // occupies 1 byte
				std::string std_string;
				std::wstring std_wstring;
				 ~IVariant() {};
			};
		}
	}
}

#endif