#ifndef IMODEL_H
#define IMODEL_H
#include <map>
#include <vector>
#include <string>
#include "IModelInitializer.h"
#include "../../../algorithm/interface/IJSONSerialize.h"
#include "../../../algorithm/interface/IType.h"

namespace MVC
{
	namespace Model
	{
		namespace Interface
		{
			class IModel : public virtual Algorithm::Interface::IType
			{
			public:
				size_t id;
                // used to queue up error messages by the controller
                std::vector<std::string> errorMessages;
				virtual void RegisterIntoDatabase(Interface::IModelInitializer& initializer) {};
				
				virtual void create(void) {};
				//virtual void readOnPrimaryKey(size_t pk) {};
				// search on non zero attributes in object
				virtual void update(void) {};
				virtual void erase(void) {};
				virtual size_t size(void) { return 0; }
				IModel(void) :errorMessages(), id() {};
				IModel& operator=(IModel& rhs)
				{
					id = rhs.id;
					errorMessages = rhs.errorMessages;
					return *this;
				};

				bool AreTheirErrorMessages(void) {
					return (errorMessages.size() == 0 ? true : false);
				}
			};
		}
	}
}
#endif
