#ifndef I_MODEL_INITIALIZER_H
#define I_MODEL_INITIALIZER_H
#include <boost/filesystem.hpp>
#include <string>

namespace MVC
{
	namespace Model
	{
		namespace Interface
		{
			class IModelInitializer
			{
				public:
					bool setCreate;
					virtual void CheckIfWeNeedToCreateDB(void) {};
					virtual void CheckSetCreate() {};
				
			};
		}
	}
}
#endif