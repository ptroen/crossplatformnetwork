#ifndef FORUM_H
#define FORUM_H
#include "../User/User.h"
#include "ForumModel.h"
#include "PostModel.h"
namespace MVC
{
	namespace Model
	{
		namespace Forum
		{
			const std::string adminBarController = "/menus/adminbar/index.html";
			const std::string menuBar = "menus/menubar/index.html";
			const std::string forumAddAForumPage = "/forum/addaforumpage/index.html";
			const std::string forumAddAPostPage = "/forum/addapost/index.html";
			const std::string forumDisplayPost = "/forum/displaypost/index.html";
			const std::string forumDisplayAllForums = "/forum/viewforums/index.html";
			const std::string forumDisplayAForum = "/forum/viewforum/index.html";
		}
	}
	
}
#endif