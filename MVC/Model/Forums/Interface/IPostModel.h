#ifndef I_POST_MODEL_H
#define I_POST_MODEL_H
#include <vector>
#include <string>
#include "../../DefaultColumns/Interface/IDefaultColumns.h"

class IPostModel : public virtual MVC::Model::DefaultColumns::Interface::IDefaultColumns
{
public:
	size_t FK_userID;
	size_t FK_forumID;
	std::string Posttitle;
	std::vector<std::string> Postlines;
    
    virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
                           std::string u= "userid";
                            std::string f="fk_forumid";
                            std::string p="posttitle";
                            std::string pl="postlines";
                            Algorithm::Interface::IPropertyTree pt;
                            pt.add(u, FK_userID);
                            pt.add(f, FK_forumID);
                            pt.add(p, Posttitle);
                            std::string joinedString = boost::algorithm::join(Postlines, " ");
                            pt.add(pl, FK_userID);
                           return pt;
                       };

                       // method which extracts the values from property tree
                       virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt){
                           std::string u= "userid";
                            std::string f="fk_forumid";
                            std::string p="posttitle";
                            std::string pl="postlines";
                            FK_userID =pt.get<size_t>(u);
                            FK_forumID = pt.get<size_t>(f);
                            Posttitle = pt.get<std::string>(p);
                            std::string joinedString = pt.get<std::string>(pl);
                           Postlines.clear();
                            std::istringstream iss(joinedString);
                                                 for(std::string sss; iss >> sss; )
                                                     Postlines.push_back(sss);
                           
                       };
    
};


#endif
