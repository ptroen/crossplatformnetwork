#ifndef I_FORUM_MODEL_H
#define I_FORUM_MODEL_H
#include "header.h"
#include "../../interface/IModel.h"
#include <boost/algorithm/string/join.hpp>
namespace MVC
{
	namespace Model
	{
		namespace Forum
		{
			namespace Interface
			{
				class IForumModel : public virtual MVC::Model::DefaultColumns::DefaultColumns,public virtual MVC::Model::Interface::IModel
				{
				public:
					std::string forumName;
					std::vector<uint64_t> FK_userGroups;
					uint64_t fk_Owner;
                    
                    virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
                        std::string f= "forumname";
                        std::string owner ="owner";
                        Algorithm::Interface::IPropertyTree pt;
                        pt.add(f, forumName);
                        pt.add(owner,fk_Owner);
                        return pt;
                    };

                    // method which extracts the values from property tree
                    virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt){
                        std::string f= "forumname";
                        std::string owner ="owner";
                        forumName =pt.get<std::string>(f);
                       fk_Owner =pt.get<size_t>(owner);
                        
                    };
                    
				};
			}
		}
	}
}

#endif
