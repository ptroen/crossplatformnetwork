#ifndef POSTS_FOR_A_FORUM_H
#define POSTS_FOR_A_FORUM_H
#include "PostModel.h"
#include "ForumModel.h"
namespace MVC
{
	namespace Model
	{
		namespace Forums
		{
			class PostsForAForum : public virtual MVC::Model::Interface::IModel
			{
				public:
				std::vector< hiberlite::bean_ptr<MVC::Model::Forum::PostModel>> posts;
				// id is the forum id
				size_t userid;
				virtual void create()
				{
					posts[0]->readAllByForumId(id, posts);
				};
				PostsForAForum(void) :posts(), userid() {};
			};
		}
	}	
}
#endif