#ifndef MODEL_CONTENT_LOADER_H
#define MODEL_CONTENT_LOADER_H

#include <map>
#include <string>
#include "../../OSI/Session/HTTP/ContentRecordType.h"
#include <boost/filesystem/path.hpp>
namespace MVC
{
	namespace Model
	{
		class ModelContentLoader
		{
			static void contentLoadingHandler(void* hook, const boost::filesystem::path& pathFound);
			static std::map <std::string, std::unique_ptr<OSI::Session::ContentRecordType>> contentLoadedIn;
		public:
			static bool foundContent( std::string& filename)
			{
				Algorithm::String::trim(filename);
				
				return (MVC::Model::ModelContentLoader::contentLoadedIn.find(filename) != MVC::Model::ModelContentLoader::contentLoadedIn.end());
			};
			static OSI::Session::ContentRecordType& GetContent(std::string& filename)
			{
				return *(contentLoadedIn[filename].get());
			};
			void ReadFileSystemAtRoot(void); // Application reads the filesystem
		};
	}
}

#endif