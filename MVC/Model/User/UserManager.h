#ifndef USER_MANAGER_H
#define USER_MANAGER_H
#include "User.h"
#include <unordered_map>
// not needed as we just load all the users #include <unordered_map>
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			class UserManager
			{
				UserManager() {};
				// users need to input the user number when they login
				
				std::unordered_map<std::string,hiberlite::bean_ptr < MVC::Model::User::User>> allUsersIndexedByUserName; // need to insert ip address to monitor logged in
				User user;
				// std::string // username, 
				public:
					static UserManager& Get_Instance()
					{
						static UserManager instance;
						return instance;
					};

				// to ensure the database is registered
					void Register(Model::ModelInitializer& init)
					{
						user.Register(init);
					}

					void LoadAllUsers(void)
					{
						std::vector< hiberlite::bean_ptr<MVC::Model::User::User>> allUsers;
						user.readAll(allUsers);
						allUsersIndexedByUserName.clear();
						for (auto u : allUsers)
						{
							std::pair<std::string, hiberlite::bean_ptr < MVC::Model::User::User>> record;
							record.first = u->name;
							record.second = u;
							record.second->id = u.get_id();
							allUsersIndexedByUserName.insert(record);
						}
					};

					MVC::Model::User::User* FindUser(std::string userID)
					{
						auto it = allUsersIndexedByUserName.find(userID);
						if (it != allUsersIndexedByUserName.end())
						{
							MVC::Model::User::User* ptr = it->second.get_object()->get();
							return ptr;
						}
						return nullptr;
					};

					void AddUser(hiberlite::bean_ptr < MVC::Model::User::User>& user)
					{
						if (FindUser(user->name) == nullptr)
						{
							user->create(); // create in database
							std::string& u = user->name;
							std::pair<std::string, hiberlite::bean_ptr < MVC::Model::User::User>> kvp(u, user);
							allUsersIndexedByUserName.emplace(kvp); // add to our memory indexed users
						}
					};
				
			};
		}
	}
}
#endif