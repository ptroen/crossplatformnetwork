#ifndef USER_H
#define USER_H
#include <string>
#include "interface/IUser.h"
#include "orm.h"
// General Header for user model subsystem
#include "UserGroupModel.h"
#include "UserCredentials.h"
#include "../interface/IModel.h"
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			const std::string addUserUrl = "/user/adduser/index.html";
			const std::string edituser = "/user/edituser/index.html";
			const std::string addUserGroup = "/user/usergroup/index.html";
			const std::string loginPage = "/user/login/index.html";
			const std::string permissionDenied = "/user/denied.html";
			//
			const std::string KeyUsername = "username";
			const std::string KeyPassword = "password";
			const std::string KeyAge = "age";
			const std::string KeyBio = "bio";
			class User : public virtual MVC::Model::User::Interface::IUser, public virtual DefaultColumns::DefaultColumns,public virtual MVC::Model::Interface::IModel
			{
				Model::ModelInitializer* _initializer;
			public:
				void SetInitializer(Model::ModelInitializer& initializer) { _initializer = &initializer; }
				void Register(Model::ModelInitializer& init)
				{
					_initializer = &init;
					if(_initializer->setCreate)
                        _initializer->db->registerBeanClass<MVC::Model::User::User>();
				};
				virtual void create(void)
				{
					if(_initializer->db !=nullptr)
					{
						SetDateStamps();
						hiberlite::bean_ptr<MVC::Model::User::User> p = _initializer->db->copyBean(*this); //create a managed copy of the object
					}
				};
				virtual void readAll(std::vector< hiberlite::bean_ptr<MVC::Model::User::User>>& v)
				{
					v = _initializer->db->getAllBeans<MVC::Model::User::User>();
					// v.size();
				};
				virtual void readOnPrimaryKey(size_t pk)
				{
					//db->getBeanIdsByCondition<MVC::Model::User::User>("WHERE pk="+atoi(pk));
				};
				
				// search on non zero attributes in object
				
				virtual void update(hiberlite::bean_ptr<MVC::Model::User::User>& user)
				{
						
						//std::cout << user.get_id() << " " << std::endl;
						//v[0]->age += 1;
				
				};
				virtual void erase(hiberlite::bean_ptr<MVC::Model::User::User>& user)
				{
						//std::vector< hiberlite::bean_ptr<MVC::Model::User::User> > v = db->getAllBeans<MVC::Model::User::User>();
						//std::cout << v[0].get_id() << " " << std::endl;
						user->deleted = 1;

				};

				friend class hiberlite::access;
				template<class Archive>
				void hibernate(Archive & ar)
				{
					ar & HIBERLITE_NVP(name);
					ar & HIBERLITE_NVP(age);
					ar & HIBERLITE_NVP(bio);
					ar & HIBERLITE_NVP(password);
					ar & HIBERLITE_NVP(FK_userGroups);
					ar & HIBERLITE_NVP(loggedOn);
					archive(ar);
				}
			};
		} // end User
	} // end Model
} // end MVC

HIBERLITE_EXPORT_CLASS(MVC::Model::User::User)
#endif