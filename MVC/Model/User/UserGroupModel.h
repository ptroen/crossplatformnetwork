#ifndef MVC_MODEL_USER_USERGROUPMODEL_H
#define MVC_MODEL_USER_USERGROUPMODEL_H
#include "orm.h"
#include "interface/iusergroupmodel.h"
#include "../interface/IModel.h"
#include "../ModelInitializer.h"
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			class UserGroupModel : public virtual MVC::Model::User::Interface::IUserGroupModel,
        public virtual MVC::Model::DefaultColumns::DefaultColumns,
        public virtual MVC::Model::Interface::IModel
			{
				Model::ModelInitializer* _initializer;
			public:
				UserGroupModel(void):IModel() { };
				void Register(Model::ModelInitializer& init) 
				{
					_initializer = &init;
					if (_initializer->setCreate)
						_initializer->db->registerBeanClass<MVC::Model::User::UserGroupModel>();
				};
				virtual void create(void)
				{

					if (_initializer->db != nullptr)
					{
						SetDateStamps();
						hiberlite::bean_ptr<MVC::Model::User::UserGroupModel> p = _initializer->db->copyBean(*this); //create a managed copy of the object
					}
				};
				virtual void readAll(std::vector< hiberlite::bean_ptr<MVC::Model::User::UserGroupModel>>& v)
				{
					v = _initializer->db->getAllBeans<MVC::Model::User::UserGroupModel>();
					// v.size();
				};
				friend class hiberlite::access;
				template<class Archive>
				void hibernate(Archive & ar)
				{
					ar & HIBERLITE_NVP(FK_userID);
					ar & HIBERLITE_NVP(userGroupName);
					archive(ar);
				}
			};
		}
	}
}
HIBERLITE_EXPORT_CLASS(MVC::Model::User::UserGroupModel)

#endif
