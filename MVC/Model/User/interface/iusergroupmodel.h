#ifndef I_USER_GROUP_MODEL_H
#define I_USER_GROUP_MODEL_H
#include "../../DefaultColumns/Interface/IDefaultColumns.h"
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			namespace Interface
			{
				class IUserGroupModel : public virtual MVC::Model::DefaultColumns::Interface::IDefaultColumns
				{
				public:
					uint64_t FK_userID;
					std::string userGroupName;
				};
			}
		}
	}
}
#endif
