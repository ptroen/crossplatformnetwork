#ifndef I_USER_CREDENTIALS_H
#define I_USER_CREDENTIALS_H

#include "header.h"
// General Header for user model subsystem
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			namespace Interface
			{
				// a user can have many user credentials with the last one being the one currently active
				class IUserCredentials
				{
				public:
					uint64_t FK_userid;
					std::string saltedPassword;
					std::string otpKey; // used if we want to encrypt with otp
				};
			}
		}
	}
}

#endif