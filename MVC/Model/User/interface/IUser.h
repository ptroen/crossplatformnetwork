#ifndef I_USER_H
#define I_USER_H
#include "iusergroupmodel.h"
#include "../../interface/IModel.h"
#include "../../DefaultColumns/DefaultColumns.h"
#include <boost/algorithm/string/join.hpp>
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			namespace Interface
			{
				// the more public user table
				class IUser : public virtual MVC::Model::DefaultColumns::DefaultColumns,
            public virtual MVC::Model::Interface::IModel
				{
				public:
					std::string name;
					double age;
					std::vector<size_t> FK_userGroups;
					std::vector<std::string> bio;
					uint8_t loggedOn;
                    std::string password;
                    
                    virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
                        std::string n= "name";
                        std::string a= "age";
                        std::string p= "password";
                        std::string b ="bio";
                        Algorithm::Interface::IPropertyTree pt;
                        pt.add(n, name);
                        pt.add(a, age);
                        pt.add(p, password);
                        std::string joinedString = boost::algorithm::join(bio, " ");
                        pt.add(b, joinedString);
                        return pt;
                    };

                    // method which extracts the values from property tree
                    virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt){
                        std::string n= "name";
                        std::string a= "age";
                        std::string p= "password";
                        std::string b ="bio";
                        name = pt.get<std::string>(n);
                        age = pt.get<double>(a);
                        std::string joinedString = pt.get<std::string>(b);
                        bio.clear();
                        std::istringstream iss(joinedString);
                        for(std::string sss; iss >> sss; )
                            bio.push_back(sss);
                        password = pt.get<std::string>(p);
                        
                    };
				};
			}
		}
	}
}

#endif
