#ifndef MVC_MODEL_USER_USER_CREDENTIALS_H
#define MVC_MODEL_USER_USER_CREDENTIALS_H
#include "orm.h"
#include "./interface/IUserCredentials.h"
#include "../DefaultColumns/DefaultColumns.h"
namespace MVC
{
	namespace Model
	{
		namespace User
		{
			class UserCredentials : public virtual MVC::Model::User::Interface::IUserCredentials, public virtual DefaultColumns::DefaultColumns
			{
				friend class hiberlite::access;
				template<class Archive>
				void hibernate(Archive & ar)
				{
					ar & HIBERLITE_NVP(FK_userid);
					ar & HIBERLITE_NVP(saltedPassword);
					ar & HIBERLITE_NVP(otpKey);
					archive(ar);
				}
				Model::ModelInitializer* _initializer;
			public:
				void Register(Model::ModelInitializer& init)
				{
					_initializer = &init;
					if (_initializer->setCreate)
						_initializer->db->registerBeanClass<MVC::Model::User::UserCredentials>();
				};
				virtual void create(void)
				{

					if (_initializer->db != nullptr)
					{
						SetDateStamps();
						hiberlite::bean_ptr<MVC::Model::User::UserCredentials> p = _initializer->db->copyBean(*this); //create a managed copy of the object
					}
				};
				virtual void readAll(std::vector< hiberlite::bean_ptr<MVC::Model::User::UserCredentials>>& v)
				{
					v = _initializer->db->getAllBeans<MVC::Model::User::UserCredentials>();
					// v.size();
				};
			};
		}
	}
}
HIBERLITE_EXPORT_CLASS(MVC::Model::User::UserCredentials)
#endif
