#ifndef MODEL_INITIALIZER_H
#define MODEL_INITIALIZER_H
#include "./interface/IModelInitializer.h"
#include "../../ThirdParty/hiberlite-master/include/hiberlite.h"
#include <string>
#include <boost/filesystem.hpp>
#include "../../Tools/Diagnostics/Logger/Logger.h"
namespace MVC
{
	namespace Model
	{
		class ModelInitializer : public virtual Interface::IModelInitializer
		{
			public:
				hiberlite::Database* db;
				const std::string databasefilenamewithdbextension = "forum.db";
				ModelInitializer(void) :db(nullptr)
				{
					CheckIfWeNeedToCreateDB();
				};
				virtual ~ModelInitializer(void) { delete db; };
				virtual void CheckIfWeNeedToCreateDB(void)
				{
					// CreateDB
					try
					{
						bool setCreate = false;
						if (!boost::filesystem::exists(boost::filesystem::path(databasefilenamewithdbextension)))
						{
							setCreate = true;
						}
						db = new hiberlite::Database(databasefilenamewithdbextension);
					}
					catch (std::exception ex)
					{
						auto msg = std::string(ex.what()); 
						LOGIT(msg);
					}
				};
				virtual void CheckSetCreate()
				{
					// Create DDL
					if (setCreate && !boost::filesystem::exists(boost::filesystem::path(databasefilenamewithdbextension)))
					{
						//drop all tables beans will use
						db->dropModel();
						//create those tables again with proper schema
						db->createModel();
					}
				};
		};
	}
}

#endif
