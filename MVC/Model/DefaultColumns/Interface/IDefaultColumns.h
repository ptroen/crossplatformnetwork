#ifndef IDEFAULT_COLUMNS_H
#define IDEFAULT_COLUMNS_H
#include <string>
#include <cstdint>
#include <vector>
#include <map>
#ifndef _WIN32
#include <cstring> // for memcpy
#endif
namespace MVC
{
	namespace Model
	{
		namespace DefaultColumns
		{
			namespace Interface
			{
				class IDefaultColumns
				{
						public:
							virtual size_t size(void) { return sizeof(IDefaultColumns); };
							virtual size_t max_size(void) { return size(); };
							virtual void ToString(char* stringout, size_t& dataSize) {
								if (dataSize >= size())
									memcpy(stringout, this, size());
							};
							virtual void FromString(char* dataIn, size_t& dataInSize) {
								if (dataInSize >= size())
									memcpy(this,dataIn, size());
							};
						uint64_t dateCreated;
						uint64_t dateModified;
						uint64_t dateAccessed;
						uint8_t deleted; // 1 is considered deleted, 0 is not. This is to support soft delete and not a hard delete(DELETE from where).
						// pk autonumber is provided by hiberlite
						virtual uint64_t GetPk() { return NULL; }; // this is fetched manually
				}; // end IDefaultColumns
			} // end Interface
		} // end DefaultColumns
	} // end Model
} // end MVC

#endif
