#ifndef MVC_REQUEST_H
#define MVC_REQUEST_H
#include "../../../OSI/Transport/HTTP/HTTPRequest.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
namespace MVC
{
	
	template <class TInit>
	class MVCRequest : public OSI::Transport::HTTP::HTTPRequest<TInit>
	{
	public:
		inline static OSI::Session::ContentRecordType* _contentType = nullptr;
		inline static OSI::Session::Interface::IRequestBuilder* _requestBuilder = nullptr;
		MVCRequest(TInit& init): OSI::Transport::HTTP::HTTPRequest<TInit>(init){};
		MVCRequest(void):OSI::Transport::HTTP::HTTPRequest<TInit>(TInit()) {};
		// method to prep before hook processes
			void IncomingHTTPRequest(OSI::Session::ContentRecordType& contentType, OSI::Session::Interface::IRequestBuilder&  requestBuilder)
			{
				std::string icontroll("Incoming HTTP Request \n");
				LOGIT(icontroll)
					_contentType = &contentType;
				_requestBuilder = &requestBuilder;
				std::string ss;
				ss += requestBuilder.url;
				ss += "\n";
				LOGIT(ss)
			_contentType = &contentType;
			_requestBuilder = &requestBuilder;
			};
	};
}

#endif
