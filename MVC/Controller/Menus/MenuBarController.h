#ifndef MENU_BAR_CONTROLLER_H
#define MENU_BAR_CONTROLLER_H
#include "../interface/IController.h"
#include "../../../MVC/Model/ModelUtils.h"
#include "../../../MVC/Model/Menus/ForumMenuBarModel.h"
#include "../../Model/Forums/PostModel.h"
#include "../../Model/Forums/ForumModel.h"
namespace MVC
{
	namespace Controller
	{
		namespace Menus
		{
			class MenuBarController : public virtual MVC::Controller::Interface::IController
			{
				public:
				virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
					model = new MVC::Model::Menus::ForumMenuBarModel();
					MVC::Model::Menus::ForumMenuBarModel* menuBarModel = dynamic_cast<MVC::Model::Menus::ForumMenuBarModel*>(model);
					try
					{
						KEY_FIND_ASSIGN(MVC::Model::Forum::FK_userID, menuBarModel, userid, " no user id specified")
						KEY_FIND_ASSIGN(MVC::Model::Forum::FK_forumID, menuBarModel, fk_forumid, "no forum id specifiied")
							model->errorMessages.clear();
					}
					catch (std::exception ex)
					{
						std::cout << "MenuBarController exception:" << ex.what() << std::endl;
					}
					
				}
				virtual void OnTimerHook(void)
				{
					// check db key integrity
				}
				virtual void OnIdleHook(void)
				{
					// check db integrity for this table
				}
			};
		}
	}
}

#endif