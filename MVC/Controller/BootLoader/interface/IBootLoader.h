#ifndef I_BOOT_LOADER_H
#define I_BOOT_LOADER_H

class IBootLoader
{
	public:
	IBootLoader(void){}; // boot things via constructor
	virtual void Boot(void) = 0;
	protected:
	virtual void RealTimeEventLoop(void) = 0;
};

#endif