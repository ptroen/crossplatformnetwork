#include <SDL.h>
#undef main // https://stackoverflow.com/questions/6847360/error-lnk2019-unresolved-external-symbol-main-referenced-in-function-tmainc
#include "../../View/Graphics/HardwareManagement/ViewSDLTexture.h"
#include <SDL_image.h>
#include "SDLDevicePointers.h"
#include "../../View/Graphics/GraphicsDirector.h"
#include "../../View/Graphics/GraphicsCommandList.h"
#include "SDLBootLoader.h"
//Scene textures

const int DEFAULT_SCREEN_WIDTH = 1024;
const int DEFAULT_SCREEN_HEIGHT = 768;
const int DEFAULT_BITS_PER_PIXEL = 32;

void SDLBootLoader::Boot(void)
{
	currentDirector = new MVC::View::Graphics::GraphicsDirector();
	currentGraphicscommandList = new GraphicsCommandList();

	int screenWidth = DEFAULT_SCREEN_WIDTH;
	int screenHeight = DEFAULT_SCREEN_HEIGHT;
	int bitsperpixel = DEFAULT_BITS_PER_PIXEL;
	_imageBuffer = new ImageBuffer(screenWidth, screenHeight, bitsperpixel);


	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		throw new std::exception(SDL_GetError());
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Seed random
		srand(SDL_GetTicks());

		//Create window
		SDL_Device_Pointers::_SDLWindow = SDL_CreateWindow("RTS Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, SDL_WINDOW_FULLSCREEN);
		if (SDL_Device_Pointers::_SDLWindow == nullptr)
		{
			throw new std::exception(SDL_GetError());
		}
		else
		{
			//Create renderer for window
			SDL_Device_Pointers::_SDLRenderer = SDL_CreateRenderer(SDL_Device_Pointers::_SDLWindow,
				-1,
				SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (SDL_Device_Pointers::_SDLRenderer == nullptr)
			{
				throw new std::exception(SDL_GetError());
			}

			//Initialize renderer color
			SDL_SetRenderDrawColor(SDL_Device_Pointers::_SDLRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		}
	}
	RealTimeEventLoop();
}

// Need a Real time Processing loop
SDLBootLoader::SDLBootLoader(void): _backBufferTexture(nullptr), currentDirector(nullptr), currentGraphicscommandList(nullptr),_imageBuffer(nullptr)
{
	SDL_Device_Pointers::_SDLWindow = nullptr;

	//The window renderer
	SDL_Device_Pointers::_SDLRenderer = nullptr;

}

void SDLBootLoader::CreateSDLRendererSurface()
{
	//Load blank texture
	_backBufferTexture = new ViewSDLTexture(_imageBuffer->_width, _imageBuffer->_height);
	_backBufferTexture->createBlank();
}

SDLBootLoader::~SDLBootLoader(void)
{
	//Free loaded images
	if(_backBufferTexture != nullptr)
	delete _backBufferTexture;

	//Destroy window	
	SDL_DestroyRenderer(SDL_Device_Pointers::_SDLRenderer);
	SDL_DestroyWindow(SDL_Device_Pointers::_SDLWindow);
	SDL_Device_Pointers::_SDLWindow = nullptr;
	SDL_Device_Pointers::_SDLRenderer = nullptr;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}


void SDLBootLoader::SDLUpdateBuffer()
{
	// get current command list(make a singleton)
	currentDirector->RenderSceneDispatcher(*currentGraphicscommandList, *_imageBuffer);
	//Clear screen
	SDL_SetRenderDrawColor(SDL_Device_Pointers::_SDLRenderer, 0, 0, 0, 0);
	SDL_RenderClear(SDL_Device_Pointers::_SDLRenderer);

	// copying the backbuffer from the image buffer
	SDL_UpdateTexture
	(
		_backBufferTexture->_SDLTexture,
		NULL,
		static_cast<void*>(_imageBuffer->_buffer),
		_imageBuffer->_width * _imageBuffer->_bpp / 8
	);
	SDL_RenderCopy(SDL_Device_Pointers::_SDLRenderer,
		_backBufferTexture->_SDLTexture, NULL, NULL);
	SDL_RenderPresent(SDL_Device_Pointers::_SDLRenderer);
}

void SDLBootLoader::RealTimeEventLoop(void)
{
	//Start up SDL and create window
	//Load media
	CreateSDLRendererSurface();

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	//While application is running
	//Handle events on queue
	while (!quit)
	{
		SDL_PollEvent(&e);
		//User requests quit
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}
		SDLUpdateBuffer();
	}
}
