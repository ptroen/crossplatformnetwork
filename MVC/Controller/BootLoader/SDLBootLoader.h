#ifndef SDL_RESOURCE_LOADER_H
#define SDL_RESOURCE_LOADER_H
#include "../../View/Graphics/HardwareManagement/ViewSDLTexture.h"
#include "../../View/Graphics/Primitives/ImageBuffer.h"
#include "../../View/Graphics/IGraphicsDirector.h"
#include "./interface/IBootLoader.h"

// BootLoader stays active until the user quits
class SDLBootLoader : public virtual IBootLoader
{ 
    public:	
	SDLBootLoader(void);
	//Frees media and shuts down SDL
	virtual ~SDLBootLoader(void);
	//Starts up SDL and creates window
	virtual void Boot(void);
    protected:
	//call this to render
	virtual void RealTimeEventLoop(void);
	private:
	ImageBuffer* _imageBuffer;

	//Scene textures
	 ViewSDLTexture* _backBufferTexture;

	// low level book keeping
	IGraphicsDirector* currentDirector;
	IGraphicsCommandList* currentGraphicscommandList;
	void SDLUpdateBuffer();

    //Loads media
	void CreateSDLRendererSurface(void);

};
#endif