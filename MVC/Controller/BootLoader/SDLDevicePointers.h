#ifndef SDL_DEVICE_POINTERS_H
#define SDL_DEVICE_POINTERS_H
#include <SDL.h>

class SDL_Device_Pointers
{
public:
	//The window we'll be rendering to
	static SDL_Window* _SDLWindow;

	//The window renderer
	static SDL_Renderer* _SDLRenderer;
	//Screen dimension constants
};

#endif
