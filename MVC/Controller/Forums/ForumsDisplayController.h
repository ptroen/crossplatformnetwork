#ifndef FORUMS_DISPLAY_CONTROLLER_H
#define FORUMS_DISPLAY_CONTROLLER_H
#include "../interface/IController.h"
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			// display all the forums
			class ForumsDisplayController : public virtual MVC::Controller::Interface::IController
			{
				public:
					
					virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
						// display all forums no key necessary
						// we may want to filter on user if we wanted to be more secure
						model = new MVC::Model::Forum::ForumModel();
						
						MVC::Model::Forum::ForumModel* forumModel = dynamic_cast<MVC::Model::Forum::ForumModel*>(model);
						forumModel->SetInitializer(initializer);
						if (request.keyValueExists(MVC::Model::Forum::FK_userID))
						{
							std::string id = request.keyValueArgs[MVC::Model::Forum::FK_userID].c_str();
							Algorithm::String::replace(id, "%0", "");
							MVC::Model::Forum::ForumModel* forumModel = dynamic_cast<MVC::Model::Forum::ForumModel*>(model);
							forumModel->fk_Owner = atoi(id.c_str());
						}
					}
					virtual void OnTimerHook(void)
					{
						// not necessary
					}
					virtual void OnIdleHook(void)
					{
						// not necessary
					}
			};
		}
	}
}
#endif