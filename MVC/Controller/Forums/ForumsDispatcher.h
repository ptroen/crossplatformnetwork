#ifndef FORUMS_MODEL_DISPATCHER_H
#define FORUMS_MODEL_DISPATCHER_H

#include "../User/userdispatcher.h"
#include "../../Model/Forums/Forum.h"

#include "../../Model/Forums/PostModel.h"
#include "../../Controller/interface/IController.h"
#include "../../View/interface/ViewVMWidget.h"
#include "../../Model/interface/IModel.h"
// controllers

// Menus
#include "../Menus/AdminMenuBarController.h"
#include "../Menus/MenuBarController.h"
// Forums
#include "AddAForumPageController.h"
#include "AddAForumPostController.h"
#include "DisplayAForumPostController.h"
#include "ForumsDisplayController.h"
#include "ForumDisplayController.h"
// end controllers
// views
// Menus
#include "../../View/Menus/MenuBarView.h"
#include "../../View/Menus/AdminMenuBarView.h"

// Forums
#include "../../View/Forums/AddAForumPageView.h"
#include "../../View/Forums/AddAForumPostView.h"
#include "../../View/Forums/DisplayAForumPostView.h"
#include "../../View/Forums/ForumsDisplayView.h"
#include "../../View/Forums/ForumDisplayView.h"
// end views
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			// selects appropriate model based on url or type provided
			// it also decides if caching is needed and happens on the server
			template<class TInitializationParameters>
			class ForumsDispatcher : public virtual User::UserDispatcher<TInitializationParameters>,
						public virtual Interface::IControllerDispatcher< TInitializationParameters>
			{				
				virtual void LoadViewsAndControllers(void)
				{
					auto msg = std::string("Forum Dispatcher Load Views And Controllers\n");
					LOGIT(msg)
					User::UserDispatcher< TInitializationParameters>::LoadViewsAndControllers();
					LoadVC(Model::Forum::forumAddAForumPage, std::make_shared<Controller::Forum::AddAForumPageController>(), std::make_shared<View::Forums::AddAForumPageView>());
					LoadVC(Model::Forum::forumAddAPostPage, std::make_shared<Controller::Forum::AddAForumPostController>(), std::make_shared<View::Forums::AddAForumPostView>());
					LoadVC(Model::Forum::forumDisplayAllForums, std::make_shared<Controller::Forum::ForumsDisplayController>(), std::make_shared<View::Forums::ForumsDisplayView>());
					LoadVC(Model::Forum::forumDisplayAForum, std::make_shared<Controller::Forum::ForumDisplayController>(), std::make_shared<View::Forums::ForumDisplayView>());
					LoadVC(Model::Forum::forumDisplayPost, std::make_shared<Controller::Forum::DisplayAForumPostController>(), std::make_shared<View::Forums::DisplayAForumPostView>());
					auto msg2 = std::string("Forum Dispatcher end Load Views And Controllers\n");
					LOGIT(msg2)
				};
				virtual void InitializeModels(Model::ModelInitializer& initializer) {
					initializer.CheckIfWeNeedToCreateDB();
					User::UserDispatcher<TInitializationParameters>::InitializeModels(initializer);
					auto msgg = std::string("Forum Dispatcher InitializeModels\n"); 
					LOGIT(msgg)
					Model::Forum::ForumModel frm;
					frm.Register(initializer);
					Model::Forum::PostModel pst;
					pst.Register(initializer);
					auto msg = std::string("Forum Dispatcher InitializeModels end\n"); 
					LOGIT(msg)
					initializer.CheckSetCreate();
				};
			public:
				ForumsDispatcher(TInitializationParameters& init = TInitializationParameters()):User::UserDispatcher<TInitializationParameters>(init)
				{
					LoadViewsAndControllers();
					InitializeModels(Interface::IControllerDispatcher< TInitializationParameters>::_initializer);
				};
			};
		}
	}
}
#endif
