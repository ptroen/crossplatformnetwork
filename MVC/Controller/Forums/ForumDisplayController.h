#ifndef FORUM_DISPLAY_CONTROLLER_H
#define FORUM_DISPLAY_CONTROLLER_H
#include "../interface/IController.h"
#include "../../../MVC/Model/Forums/PostsForAForum.h"
#include "../../../MVC/Model/ModelUtils.h"
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			// display all the forums
			class ForumDisplayController : public virtual MVC::Controller::Interface::IController
			{
			public:
					virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder& request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
						// display all forums no key necessary
						// we may want to filter on user if we wanted to be more secure
						
						model = new MVC::Model::Forums::PostsForAForum();
						MVC::Model::Forums::PostsForAForum* forumModel = dynamic_cast<MVC::Model::Forums::PostsForAForum*>(model);
						KEY_FIND_ASSIGN_INTEGER(MVC::Model::Forum::FK_forumID, forumModel, id, " no forum id specified")
							if (request.keyValueExists(MVC::Model::Forum::FK_userID))
							{
								std::string id = request.keyValueArgs[MVC::Model::Forum::FK_userID].c_str();
								Algorithm::String::replace(id, "%0", "");
								MVC::Model::Forum::PostModel post;
								post.SetInitializer(initializer);
								post.readAllByForumId(forumModel->id, forumModel->posts);
							}
					}
					virtual void OnTimerHook(void)
					{
						// not necessary
					}
					virtual void OnIdleHook(void)
					{
						// not necessary
					}
			};
		}
	}
}
#endif