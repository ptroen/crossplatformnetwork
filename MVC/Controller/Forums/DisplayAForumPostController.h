#ifndef DISPLAY_A_FORUM_POST_CONTROLLER_H
#define DISPLAY_A_FORUM_POST_CONTROLLER_H

#include "../interface/IController.h"
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			class DisplayAForumPostController : public virtual MVC::Controller::Interface::IController
			{
				virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder& request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
					model = new MVC::Model::Forum::PostModel();
					MVC::Model::Forum::PostModel* postModel = reinterpret_cast<MVC::Model::Forum::PostModel*>(model);
					KEY_FIND_ASSIGN_INTEGER(MVC::Model::Forum::PostId, postModel, id, " no post id specified")
					postModel->readOnId();
				}
			};
		}
	}
}

#endif