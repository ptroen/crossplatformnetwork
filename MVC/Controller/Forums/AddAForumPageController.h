#ifndef ADD_A_FORUM_PAGE_CONTROLLER_H
#define ADD_A_FORUM_PAGE_CONTROLLER_H

#include "../interface/IController.h"
#include "../../../MVC/Model/Forums/ForumModel.h"
#include "../../../OSI/Session/IPSecurity/IPSecurity.h"
#include "../../../MVC/Model/ModelUtils.h"
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			class AddAForumPageController : public virtual MVC::Controller::Interface::IController
			{
			public:
				virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder& request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
					model = new MVC::Model::Forum::ForumModel();
					MVC::Model::Forum::ForumModel* forumModel = dynamic_cast<MVC::Model::Forum::ForumModel*>(model);
					forumModel->SetInitializer(initializer);
					KEY_FIND_ASSIGN(MVC::Model::Forum::forumName, forumModel, forumName, " no forum name specified")
					if(request.keyValueExists(MVC::Model::Forum::FK_userID))
					{
						std::string id = request.keyValueArgs[MVC::Model::Forum::FK_userID].c_str();
						Algorithm::String::replace(id, "%0", "");
						forumModel->fk_Owner = atoi(id.c_str());
					}
					else
					{
						forumModel->errorMessages.push_back(" userid not provided");
						auto msg = std::string("userid not provided for addaforumpage controller");
						OSI::Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, msg);
					}
					NO_ERROR_CREATE(forumModel)
				}
				virtual void OnTimerHook(void)
				{
					// not necessary
				}
				virtual void OnIdleHook(void)
				{
					// not necessary
				}
			};
		}
	}
}
#endif
