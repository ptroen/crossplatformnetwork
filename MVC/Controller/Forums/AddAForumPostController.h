#ifndef ADD_A_FORUM_POST_CONTROLLER_H
#define ADD_A_FORUM_POST_CONTROLLER_H

#include "../interface/IController.h"
#include "../../../MVC/Model/Forums/PostModel.h"
#include "../../../MVC/Model/ModelUtils.h"
namespace MVC
{
	namespace Controller
	{
		namespace Forum
		{
			class AddAForumPostController : public virtual MVC::Controller::Interface::IController
			{
			public:
				virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    request, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
					model = new MVC::Model::Forum::PostModel();
					MVC::Model::Forum::PostModel* post = dynamic_cast<MVC::Model::Forum::PostModel*>(model);
					
					post->SetInitializer(initializer);
					MVC::Model::Forum::PostModel* postModel = dynamic_cast<MVC::Model::Forum::PostModel*>(model);
					KEY_FIND_ASSIGN_INTEGER(MVC::Model::Forum::FK_userID, postModel, FK_userID, " no user id specified")
					KEY_FIND_ASSIGN_INTEGER(MVC::Model::Forum::FK_forumID, postModel, FK_forumID, "no forum id specifiied")
					KEY_FIND_ASSIGN(MVC::Model::Forum::PostTitle, postModel, Posttitle, "No Post Title specified")
					KEY_FIND_ASSIGN_MULTILINE(MVC::Model::Forum::PostLines, postModel, Postlines, "Nothing in the post specified. Write something in the post lines.")
					NO_ERROR_CREATE(postModel)
				}
				virtual void OnTimerHook(void)
				{
					// check db key integrity
				}
				virtual void OnIdleHook(void)
				{
					// check db integrity for this table
				}
			};
		}
	}
}

#endif