#ifndef MESSAGE_PUMP_TYPE_H
#define MESSAGE_PUMP_TYPE_H

namespace controller
{
	enum class MessagePumpType : uint16_t
	{
		IDLE = 0
	};
}

#endif