#ifndef PERMISSION_DENIED_CONTROLLER_H
#define PERMISSION_DENIED_CONTROLLER_H

#include "../interface/IController.h"
#include "../../../OSI/Session/IPSecurity/IPSecurity.h"
namespace MVC
{
	namespace Controller
	{
		namespace User
		{
			class PermissionDeniedController : public virtual MVC::Controller::Interface::IController
			{
				// no need to process anything
				virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    request, std::string& ipaddress) {
					// Flag the IP
					auto msg = "Permission Denied " + request.url;
					OSI::Session::IPSecurity::IPSecurity::Get_Instance().FlagIP(ipaddress, msg);
				};
				virtual void OnTimerHook(void)
				{
					// not necessary
				}
				virtual void OnIdleHook(void)
				{
					// not necessary
				}
			};
		}
	}
}

#endif
