#ifndef ADD_A_USER_CONTROLLER_H
#define ADD_A_USER_CONTROLLER_H
#include "../interface/IController.h"
#include "../../Model/User/User.h"
#include <string>
#include <vector>
#include <cstdlib> // for atoi
namespace MVC
{
	namespace Controller
	{
		namespace User
		{
			class AddAUserController : public virtual MVC::Controller::Interface::IController
			{
            protected:
                std::string username;
                std::string password;
                std::vector<std::string> description;
			public:
                virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    requestBuilder, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
                        model = new MVC::Model::User::User();
                        MVC::Model::User::User* usr=dynamic_cast<MVC::Model::User::User*>(model);
                        if(requestBuilder.keyValueExists(MVC::Model::User::KeyUsername))
                        {
                            usr->name = requestBuilder.keyValueArgs[MVC::Model::User::KeyUsername];
                        }
                        else
                        {
                            usr->errorMessages.push_back(" no user name");
                        }
                        if(requestBuilder.keyValueExists("age"))
                        {
                            usr->age = atoi(requestBuilder.keyValueArgs[MVC::Model::User::KeyAge].c_str());
                        }
                        else
                        {
                            usr->errorMessages.push_back("no age inputted");
                        }
                        usr->loggedOn = 0;
                        if(requestBuilder.keyValueExists(MVC::Model::User::KeyPassword))
                        {
                            usr->password = requestBuilder.keyValueArgs[MVC::Model::User::KeyPassword];
                        }
                        else
                        {
                            usr->errorMessages.push_back("no password inputted");
                        }
                        if(usr->AreTheirErrorMessages())
                        {
							usr->SetInitializer(initializer);
                            usr->create(); // populate in the db
                        }
                    };
                    virtual void OnTimerHook(void)
                    {
                            // not necessary
                    }
                    virtual void OnIdleHook(void)
                    {
                            // not necessary
                    }
			};
		}
	}
}
#endif