#ifndef LOGIN_PAGE_CONTROLLER_H
#define LOGIN_PAGE_CONTROLLER_H

#include "../interface/IController.h"
#include "../../Model/User/UserManager.h"
namespace MVC
{
	namespace Controller
	{
		namespace User
		{
			class LoginPageController : public virtual MVC::Controller::Interface::IController
			{
				// add a login manager
				// the model has a attribute called 'logged on' if it's not set we aren't logged on!
				bool loadedAllUsers;
				public:
					LoginPageController(): IController(), loadedAllUsers(false){
					
					};
					virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder& requestBuilder, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {
						// check if we need to login in that's it
						if(requestBuilder.keyValueExists(MVC::Model::User::KeyUsername))
						{
							if (!loadedAllUsers)
							{
								Model::User::UserManager::Get_Instance().LoadAllUsers();
								loadedAllUsers = true;
							}

							// search the user in the User Manager
							auto username = requestBuilder.keyValueArgs[MVC::Model::User::KeyUsername];
							MVC::Model::User::User* ptr = Model::User::UserManager::Get_Instance().FindUser(username);
							if (ptr == nullptr)
							{
								model = new MVC::Model::User::User();
								return;
							}
							// check also if the password matches
							if(requestBuilder.keyValueExists(MVC::Model::User::KeyPassword))
							{
								if(requestBuilder.keyValueArgs[MVC::Model::User::KeyPassword] != ptr->password)
								{
									ptr->errorMessages.push_back("invalid password or invalid login. please login properly.");
								}
								else
								{
									// if found in the user manager
									model = ptr;
									ptr->loggedOn = 1;
									return;
								}
							}
							// if password doesn't match log a error
						}
					};
					virtual void OnTimerHook(void)
					{

					};
					virtual void OnIdleHook(void)
					{

					};
			};
		}
	}
}

#endif