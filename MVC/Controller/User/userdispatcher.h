#ifndef USER_DISPATCHER_H
#define USER_DISPATCHER_H

#include "../../Model/User/User.h"
#include "../interface/IControllerDispatcher.h"
// User based Security
#include "../User/AddAUserController.h"
#include "../User/AddAUserGroupController.h"
#include "../User/EditAUserController.h"
#include "../User/EditAUserGroupController.h"
#include "../User/LoginPageController.h"
#include "../User/PermissionDeniedController.h"

// User
#include "../../View/User/AddAUserGroupView.h"
#include "../../View/User/AddAUserView.h"
#include "../../View/User/EditAUserGroupView.h"
#include "../../View/User/EditAUserView.h"
#include "../../View/User/LoginPageView.h"
#include "../../View/User/PermissionDeniedView.h"

#include "../../View/Menus/MenuBarView.h"
#include "../../Controller/Menus/MenuBarController.h"
namespace MVC
{
	namespace Controller
	{
		namespace User
		{
			template <class TInitialization>
			class UserDispatcher : public virtual Interface::IControllerDispatcher< TInitialization>
			{
			protected:
				virtual void InitializeModels(Model::ModelInitializer& initializer) {
					initializer.CheckIfWeNeedToCreateDB();
					MVC::Model::User::UserManager::Get_Instance().Register(initializer);
				};
				public:
					UserDispatcher(TInitialization& init) : Interface::IControllerDispatcher<TInitialization>(init){};
					
					virtual void LoadViewsAndControllers(void)
					{
						auto msgn = std::string("User Dispatcher LoadViewsAndControllers\n");
						LOGIT(msgn)
						LoadVC(Model::User::addUserUrl, std::make_shared<Controller::User::AddAUserController>(), std::make_shared<View::User::AddAUserView>());
						LoadVC(Model::User::addUserGroup, std::make_shared<Controller::User::AddAUserGroupController>(), std::make_shared<View::User::AddAUserGroupView>());
						LoadVC(Model::User::loginPage, std::make_shared<Controller::User::LoginPageController>(), std::make_shared<View::User::LoginPageView>());
						LoadVC(Model::User::permissionDenied, std::make_shared<Controller::User::PermissionDeniedController>(), std::make_shared<View::User::PermissionDeniedView>());
						LoadMenuBar(std::make_shared<Controller::Menus::MenuBarController>(), std::make_shared<View::Menus::MenuBarView>());
						auto msg = std::string("User Dispatcher end LoadViewsAndController\n");
						LOGIT(msg)
					};
			};
		}
	}
}

#endif
