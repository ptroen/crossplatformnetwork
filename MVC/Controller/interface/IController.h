#ifndef ICONTROLLER_H
#define ICONTROLLER_H
#include "../../Model/interface/IModel.h"
#include "../../../OSI/Session/interface/IRequestBuilder.h"
#include "../../../OSI/Session/HTTP/ContentRecordType.h"
#include "../../../OSI/Transport/HTTP/HTTPRequest.h"
#include "../../../OSI/Transport/HTTP/HTTPResponse.h"
#include "../../../OSI/Transport/interface/IHTTPSInitializationParameters.h"
#include <string>
#include "../../Model/interface/IModel.h"
namespace MVC
{
	namespace Controller
	{
		namespace Interface
		{
			// all manipulation goes here. This is basically a frontend to access the vm
			class IController
			{
			protected: 
				MVC::Model::Interface::IModel* model;
			public:

				virtual MVC::Model::Interface::IModel* GetModel(void) { return model; };
                virtual void ProcessEvents(OSI::Session::Interface::IRequestBuilder&    requestBuilder, std::string& ipaddress, MVC::Model::ModelInitializer& initializer) {};
				virtual void OnTimerHook(void){};
				virtual void OnIdleHook(void){};
			}; // end class
		}
	}
} // end MVC
#endif
