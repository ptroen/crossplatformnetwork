#ifndef I_CONTROLLER_DISPATCHER_H
#define I_CONTROLLER_DISPATCHER_H
#include "../../../OSI/Transport/HTTP/HTTPRequest.h"
#include "../../../OSI/Transport/HTTP/HTTPResponse.h"
#include "IController.h"
#include <map>
#include <memory>
#include "../../../MVC/View/interface/ViewVMWidget.h"
#include "../../../OSI/Session/HTTP/ContentRecordType.h"
#include "../../../OSI/Session/interface/IRequestBuilder.h"
#include "../../../OSI/Transport/interface/IAllInitiializationParameters.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
#include "../../../MVC/MVCRequest.h"
#include "../../../algorithm/stringtokenizer.h"
#include "../../../MVC/Model/ModelInitializer.h"
namespace MVC
{
	namespace Controller
	{
		namespace Interface
		{
			class MVCContainer
			{
				public:
					std::shared_ptr<IController> controller;
					std::shared_ptr<MVC::View::Interface::IViewVMWidget> view;
			};

			template <class TInitialization>
			class IControllerDispatcher
			{
			public:
				IControllerDispatcher(TInitialization& init) :_inStaticRequest(init),_initializer()
				{

				};
				IControllerDispatcher(void) :_inStaticRequest(),_initializer() {};
			protected:
				Model::ModelInitializer _initializer;
				OSI::Transport::HTTP::HTTPRequest<TInitialization> _inStaticRequest;
				std::map<std::string, MVCContainer> mvc;
				OSI::Session::ContentRecordType* _contentType;
				OSI::Session::Interface::IRequestBuilder*  _requestBuilder;
				MVCContainer menuBar;
				std::string viewResponse;

				virtual void InitializeModels(Model::ModelInitializer& initializer)
				{
				
				};
				void LoadVC(const std::string& url, std::shared_ptr<IController> controller, std::shared_ptr<MVC::View::Interface::IViewVMWidget> view)
				{
					MVCContainer mv;
					mv.controller = controller;
					mv.view = view;
					mvc[url] = mv;
				};

				void LoadMenuBar(std::shared_ptr<IController> controller, std::shared_ptr<MVC::View::Interface::IViewVMWidget> view)
				{
					menuBar.controller = controller;
					menuBar.view = view;
				};
			public:
				virtual void LoadViewsAndControllers(void) {};

				virtual void OnTimerHook(void)
				{
					for (auto iter = mvc.begin(); iter != mvc.end(); ++iter)
					{
						iter->second.controller->OnTimerHook();
					}
				}
				virtual void OnIdleHook(void)
				{
					for (auto iter = mvc.begin(); iter != mvc.end(); ++iter)
					{
						iter->second.controller->OnIdleHook();
					}
				}
				virtual bool IncomingHook(OSI::Transport::HTTP::HTTPRequest<TInitialization>& in, OSI::Transport::HTTP::HTTPResponse<TInitialization>& out, std::string& ipadress)
				{
					_requestBuilder = MVC::MVCRequest< TInitialization>::_requestBuilder;
					_contentType = MVC::MVCRequest< TInitialization>::_contentType;
					std::string icontroll("iController.h \n");
					LOGIT(icontroll)

					if (_requestBuilder != nullptr)
					{
						std::string attemptedurl = this->_requestBuilder->url;
						Algorithm::String::replace(attemptedurl,"/www", "");
						Algorithm::String::trim(attemptedurl);
						if (mvc.find(attemptedurl.c_str()) != mvc.end())
						{
							auto entry = mvc[attemptedurl];
							menuBar.controller.get()->ProcessEvents(*_requestBuilder, ipadress, _initializer);
							entry.controller.get()->ProcessEvents(*_requestBuilder, ipadress, _initializer);

							std::ostringstream output;
							menuBar.view.get()->ProcessRequest(menuBar.controller.get()->GetModel(), _contentType->_content, output);
							entry.view.get()->ProcessRequest(entry.controller.get()->GetModel(), _contentType->_content, output);
							const std::string& resp = output.str();
							size_t size = resp.size();
							out.FromString(const_cast<char*>(resp.c_str()), size);
							return true;
						}
					}
					else
					{
						auto msg = std::string("nullptr in IController for request Handler!");
						LOGIT(msg)
					}
					return false;
				}

				virtual bool IncomingHook(OSI::Transport::HTTP::HTTPResponse<TInitialization>& out)
				{
					
					return true;
				}
			};
		}
	}
}

#endif
