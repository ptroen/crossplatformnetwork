#ifndef TerrainTypes_H
#define TerrainTypes_H
#include "AttackAndDefence.h"
#include <cstdint>

enum class TerrainType : uint16_t
{
	NoTerrain,
	Grassland,
	Farmland,
	HillGoingUp,
	HillGoingDown,
	RockyTerrain,
	MountainTop,
	River,
    Air,
	Waterfall,
	EarthGround,
	GoldPile,
	StonePile,
	IronPile,
	FoodPile

}; 
class TerrainTypes : public AttackAndDefence
{
	public:
	TerrainType _terrainType;
	TerrainTypes(void) :AttackAndDefence(){};

	const TerrainTypes& operator=(const TerrainTypes& rhs){
		AttackAndDefence::Assign(rhs);
		return *this; };
};

#endif