#ifndef TERRAIN_TYPE_H
#define TERRAIN_TYPE_H

enum class terraintype
{
	plain,
	water,
};

#endif