#ifndef GRID_TILE_MODEL_H
#define GRID_TILE_MODEL_H

class UnitModel;
class TerrainModel;
class SkyModel;
class WaterModel;

class GridTileModel
{
public:
	TerrainModel * _terrain;
	UnitModel* _unit;
	SkyModel* _sky;
	WaterModel* _water;
};

#endif