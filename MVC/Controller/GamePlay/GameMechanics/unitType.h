#ifndef UNIT_TYPE_H
#define UNIT_TYPE_H
#include <cstdint>

enum class unitCategoryType
{
	military,
	intellignce,
	economic,
	morale,
	religious
};

enum class unitType : uint8_t
{
	StoneClubman=2,
	StoneSpearman=3,
	StoneBowmen=4,
	StoneSlinger=5,
	StoneRam=6
};

enum class stoneWorkerUnitType : uint8_t
{
	Worker = 1,
	Merchant =2,
	MilitaryConscript,
	IntelligenceConscript,

};

enum class stoneIntelligenceUnitType : uint8_t
{
	messenger,
	socialite,
	loner,
	spy
};

enum class stoneReligionUnitType : uint8_t
{
	priest,
	believer,
	heretic
};

enum class stoneMoraleUnitType : uint8_t
{
	warchief,
	realist,
	cynic,
};

#endif