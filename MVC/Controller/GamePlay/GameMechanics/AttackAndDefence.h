#ifndef ATTACKANDDEFENCE_H
#define ATTACKANDDEFENCE_H
#include "PaperRockScissors.h"
class AttackAndDefence
{
	public:
	PaperRockScissors _attack;
	PaperRockScissors _defence;

	protected:
	void Assign(const AttackAndDefence& rhs){
		_attack = rhs._attack;
		_defence = rhs._defence;
	};
	public:
	AttackAndDefence(){};
	AttackAndDefence(AttackAndDefence& rhs){
		Assign(rhs);
	};

	const AttackAndDefence& operator=(const AttackAndDefence& rhs){
		Assign(rhs);
		return *this;
	};

	public:
	int battle(AttackAndDefence& opponent){
		return _attack.battle(opponent._defence);
	};
};
#endif