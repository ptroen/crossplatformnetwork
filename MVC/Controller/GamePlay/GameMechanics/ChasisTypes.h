#ifndef CHASISTYPES_H
#define CHASISTYPES_H
#include <cstdint>

enum class ChasisType : uint16_t
{
	NoChasis,
	Onfoot,
	horse,
	elephant,
	raft,
	tireme,
	galleon,
	helocopter
};

class ChasisTypes
{
public:
	ChasisType type;
	short _totalhp;
	short _remainhp;
	short _topspeed;
	short _currentspeed;
	short _acceleration;
	ChasisTypes(){ _totalhp = 0;
		                     _remainhp = 0; 
							 _topspeed = 0;
							 _currentspeed = 0;
							 _acceleration = 0;
							 type = ChasisType::NoChasis;
	};

};


#endif