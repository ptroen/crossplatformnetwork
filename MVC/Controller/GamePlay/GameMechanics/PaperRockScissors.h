#ifndef PAPERROCKSCISSORS_H
#define PAPERROCKSCISSORS_H
#include "IPaperRockScissors.h"
class PaperRockScissors;

class PaperRockScissors : public virtual IPaperRockScissors
{
public:


	PaperRockScissors(void) {};
	PaperRockScissors(PaperRockScissors& rhs) {
		Assign(rhs);
	};

	PaperRockScissors(char scoord, unsigned short  techtree) :_positionInTable(scoord), _techlevel(techtree) {};

	const PaperRockScissors& operator=(const PaperRockScissors& rhs) {
		Assign(rhs);
		return *this;
	};

	/*
	 *Put this on the server
	 *
	 */
	int battle(PaperRockScissors& opponent) {
		return std::max(_techlevel - opponent._techlevel, 1)*std::max(1, _positionInTable - opponent._positionInTable);
	};


};

#endif