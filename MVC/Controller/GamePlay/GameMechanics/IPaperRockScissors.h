#ifndef I_PAPER_ROCK_SCISSORS_H
#define I_PAPER_ROCK_SCISSORS_H
static long double operator "" _w(long double i) { return i; }; // user literal

class IPaperRockScissors
{
public:
	char _positionInTable;
	unsigned short _techlevel;

protected:
	__inline void Assign(const IPaperRockScissors& rhs) {
		_positionInTable = rhs._positionInTable;
		_techlevel = rhs._techlevel;
	};
};

#endif