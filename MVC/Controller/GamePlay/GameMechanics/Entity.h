#ifndef ENTITY_H
#define ENTITY_H
#include <algorithm>
#include "Projectile.h"
using namespace std;
#include "Attribute.h"
class Entity : public Projectile
{
	public:
	Attribute _militaryWeapon;
	Attribute _militaryArmour;
	Attribute _weatherWeapon;
	Attribute _weatherArmour;
	Attribute _intelligenceWeapon;
	Attribute _intelligenceArmour;
	Attribute _religiousWeapon;
	Attribute _religousArmour;

	Entity(void){ vehicle._totalhp = 0; vehicle._remainhp = 0;  };
	void battle(Projectile& opponent){
		int damage_inflicted = 0;
		damage_inflicted  = military.battle(opponent.military);
		damage_inflicted += terrain.battle(opponent.terrain);
		damage_inflicted += weather.battle(opponent.weather);
		damage_inflicted += intelligence.battle(opponent.intelligence);
		damage_inflicted += religion.battle(opponent.religion);
		damage_inflicted += morale.battle(opponent.morale);
		opponent.vehicle._remainhp = std::max(0, damage_inflicted)*std::min(opponent.vehicle._remainhp,(short)1);	// check against remaining damage

	};
};

#endif