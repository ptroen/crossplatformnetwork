#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "TerrainTypes.h"
#include "WeatherTypes.h"
#include "ChasisTypes.h"
#include "MilitaryTypes.h"
#include "ReligionTypes.h"
#include "MoraleTypes.h"
#include "IntelligenceTypes.h"

class Projectile 
{
	public:
	Projectile(void){};
	Projectile(const Projectile& prj){  };
	TerrainTypes terrain;
	WeatherTypes weather;
	ChasisTypes vehicle;
	MilitaryTypes military;
	IntelligenceTypes intelligence;
	ReligionTypes religion;
	MoraleTypes  morale;

};

#endif