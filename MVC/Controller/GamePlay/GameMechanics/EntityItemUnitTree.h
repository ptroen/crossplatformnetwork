#ifndef ENTITYITEMUNITTREE_H
#define ENTITYITEMUNITTREE_H
#include "wordcode.h"
#include <map>
#include "ChasisTypes.h"
#include "TerrainTypes.h"
#include "WeatherTypes.h"
#include "IntelligenceTypes.h"
#include "ReligionTypes.h"
#include "MoraleTypes.h"

class EntityItemUnitTree
{
	public:
	std::map<wordcode, ChasisTypes>			availablechasis;
	std::map<wordcode, TerrainTypes>			availableterrain;
	std::map<wordcode, WeatherTypes>		availableweather;
	std::map<wordcode, IntelligenceTypes>	availableintelligence;
	std::map<wordcode, ReligionTypes>			availablereligion;
	std::map<wordcode, MoraleTypes>			availablemorale;


};
#endif