#ifndef I_EVENT_H
#define I_EVENT_H

namespace controller
{
	class IEvent
	{
	public:
		virtual void RunEvents() {};
	};
}

#endif