#ifndef VIEW_H
#define VIEW_H

class View: interface::IView
{
	public:
    static IView currentView;
    
    static void ChangeView(ICurrentView);
};

#endif