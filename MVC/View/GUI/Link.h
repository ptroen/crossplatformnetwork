#ifndef LINK_H
#define LINK_H
#include <string>
#include <map>
namespace MVC
{
	namespace View
	{
		namespace GUI
		{
			class Link : public virtual Interface::IViewVMWidget
			{
				std::string GetUrl(void){
					std::string u;
					u = url;
					 std::map<std::string, std::string>::iterator it;
					 bool first = true;
				   for (it = getMap.begin(); it != getMap.end(); it++)
				   {
					   if (first)
					   {
						   u += "?";
						   first = false;
					   }
					   else
						   u+="&";
						u+=it->first+"="+it->second;
				   }
					return u;
					
				}
				public: 
				std::string url;
				std::string description;
				std::map<std::string, std::string> getMap;
				bool inactive;
				virtual bool RenderConsoleDisplay(std::ostringstream& o)
				{
					if (inactive) return true;
					o << "url: "<< GetUrl()<<" description:"<<description << std::endl;
					return true;
				}
				
				virtual bool RenderHTMLDisplay(std::ostringstream& o)
				{
					if (inactive) return true;
					o << " <a href=\"" << GetUrl()<<"\">"<<description <<"</a>";
					return true;
				}
				
			};
		}
	}
}


#endif