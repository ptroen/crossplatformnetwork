#ifndef MVC_VIEW_GUI_FORM_H
#define MVC_VIEW_GUI_FORM_H
#include <vector>
#include <string>
namespace MVC
{
	namespace View
	{
		namespace GUI
		{
			class FormInvisibleTags
			{
				public:
				std::string _labelName;
				std::string _value;
				std::string _key;
			};
			
			class FormLabelAndfieldName : public virtual FormInvisibleTags
			{
				public:
					FormLabelAndfieldName(void) :_fieldName(), _multiline(false) {};
					FormLabelAndfieldName(const std::string& fieldName, bool multiline, const std::string& labelName, const std::string& value) :_fieldName(fieldName), _multiline(multiline)
					{
						_labelName = labelName;
						_value = value;
					};
				std::string _fieldName;
				bool _multiline;
			};
			
			class Form : public virtual Interface::IViewVMWidget
			{
			protected:
				
			public:
				std::vector<FormInvisibleTags> _invisibleTags;
				std::vector<FormLabelAndfieldName> _visibleFormsAndLabels;
				std::string submitLink;
				std::string successMessage;
				Form(void) :successMessage(), submitLink(), _visibleFormsAndLabels(), _invisibleTags(), Interface::IViewVMWidget() {};
			virtual bool RenderConsoleDisplay(std::ostringstream& o)
				{
					if (Interface::IViewVMWidget::RenderConsoleDisplay(o))
					{
						o << successMessage << std::endl;
						return true;
					}
					else
					{
						// render the forum
						for (auto it : _invisibleTags)
						{
							o << "<input type=\"hidden\" id=\"" << it._key << "\" name=\"" << it._labelName << "\" value=\"" << it._value << "\"><br/>";
						}
						for (auto it2 : _visibleFormsAndLabels)
						{
							o << "<label for=\"" << it2._key << "\">"<< it2._labelName <<"</label>";
							o << "<input type = \"text\" id = \"" << it2._key<< "\" name = \"" << it2._fieldName << "\" value=\"" << it2._value << "\"><br/>";
						}
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& o)
				{
					if (Interface::IViewVMWidget::RenderHTMLDisplay(o))
					{
						o << "<H1>"+successMessage+"</H1>" << std::endl;
					}
					
					// ForumID
					// UserID
					// render the post form
					o << "<form action=\"" << submitLink << "\"" <<"target=\"_blank\" method=\"post\">";
					for(auto it : _invisibleTags)
					{
						o << "<input type=\"hidden\" id=\"" << it._key << "\" name=\"" << it._labelName << "\" value=\"" << it._value << "\"><br>";
					}
					for (auto it2 : _visibleFormsAndLabels)
					{
						o << "<label for=\"" << it2._key<< "\">" << it2._labelName <<"</label>";
						o << "<input type = \"text\" id=\"" << it2._key << "\" name=\"" << it2._key << "\" value=\"" << it2._value << "\"><br>";
					}
					// wrap up the form with a submit
					o << "<input type = \"submit\" value = \"Submit\"><br>";
					// close the form
					o << "</form><br/>";
					return true;
				};
			};
		}
	}
}
#endif