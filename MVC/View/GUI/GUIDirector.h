#ifndef VIEW_GUI_DIRECTOR_H
#define VIEW_GUI_DIRECTOR_H

#include "Texture.h"
#include <utility>
#include <map>
#include <vector>

#include "IGUIWidget.h"
using namespace std;

namespace MVC
{
	namespace View
	{
		/*
		 * This is a view object that directs other View objects
		 */
		class GUIDirector
		{
			std::map <controller::MessagePumpType, bool> _eventTriggerTable;
			public:
			std::vector <IGUIWidget> _guiwidgets;

			void RunEvents()
			{
				for (IGUIWidget widget : _guiwidgets)
					for (std::pair<controller::MessagePumpType, controller::IEvent> eventType : widget._messagePump)
					{
						// if we have a eventTrigger of this type
						if (_eventTriggerTable[eventType.first])
						{
							eventType.second.RunEvents();
							_eventTriggerTable[eventType.first] = false;
						}
					}
			}

			void Draw()
			{
				for (IGUIWidget widget : _guiwidgets)
				{
					widget.Draw();
				}
			}
		};
	}
}
#endif