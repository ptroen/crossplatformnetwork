#ifndef VIEW_TAG_H
#define VIEW_TAG_H
include <map>
namespace MVC
{
	namespace View
	{

class ViewTag : interface::IIdentiferTag
{
  ViewStyleSheet styleSheet;
  std::vector<ViewAttributes> viewAttributes;
 std::map<std::string,std::shared_ptr<ViewTag>>* childTags;  
  std::map<std::string,std::shared_ptr<ViewTag>>* parentTags; 
}
	}
}


#endif