#ifndef I_GUI_WIDGET_H
#define I_GUI_WIDGET_H
//#include "../Controller/MessagePumpType.h"
//#include "../Controller/IEvent.h"
#include <utility>
#include <map>
#include <vector>
namespace MVC
{
	namespace View
	{
		// This is a low level graphic canvas concept
		class IGUIWidget
		{
		public:
			//std::vector <std::pair<controller::MessagePumpType, controller::IEvent> > _messagePump;
			size_t x;
			size_t y;
			size_t z;
			size_t width;
			size_t height;
			size_t depth;
			virtual void ProcessMessages(){};
			virtual void Draw() {};
		};
	}
}
#endif