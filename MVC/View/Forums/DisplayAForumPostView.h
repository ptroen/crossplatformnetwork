#ifndef DISPLAY_A_FORUM_POST_VIEW_H
#define DISPLAY_A_FORUM_POST_VIEW_H

#include "../interface/ViewVMWidget.h"
#include "../../Model/Forums/PostModel.h"
namespace MVC
{
	namespace View
	{
		namespace Forums
		{
			class DisplayAForumPostView : public virtual View::Interface::IViewVMWidget
			{
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (View::Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						MVC::Model::Forum::PostModel* post = reinterpret_cast<MVC::Model::Forum::PostModel*>(_model);
						os << post->Posttitle << std::endl;
						for(auto it : post->Postlines)
							os << it;
						return true;
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					if (View::Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						MVC::Model::Forum::PostModel* post = reinterpret_cast<MVC::Model::Forum::PostModel*>(_model);
						os << "<H1>" << post->Posttitle << "</H1>" << std::endl;
						os << "<P>";
						for (auto it : post->Postlines)
							os <<   it;
						os << "</P>";
						return true;
					}
					return false;
				};
			};
		}
	}
}
#endif
