#ifndef FORUM_DISPLAY_VIEW_H
#define FORUM_DISPLAY_VIEW_H
#include "../interface/ViewVMWidget.h"
#include "../../Model/Forums/PostsForAForum.h"
namespace MVC
{
	namespace View
	{
		namespace Forums
		{
			class ForumDisplayView : public virtual Interface::IViewVMWidget
			{
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (View::Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						MVC::Model::Forums::PostsForAForum* posts = reinterpret_cast<MVC::Model::Forums::PostsForAForum*>(_model);
						for (auto post : posts->posts)
						{
							os << post->Posttitle << std::endl;
							for (auto it : post->Postlines)
								os << it;
						}
						return true;
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
						MVC::Model::Forums::PostsForAForum* posts = dynamic_cast<MVC::Model::Forums::PostsForAForum*>(_model);
						for (auto post : posts->posts)
						{
							os << "<H1>" << post->Posttitle << "</H1>" << std::endl;
							os << "<br/>";
							os << "<P>";
							for (auto it : post->Postlines)
								os << it;
							os << "</P>";
							os << "<br/>";
						}
						os << "<br/>";
						GUI::Link link;
						link.inactive = false;
						link.url = Model::Forum::forumAddAPostPage;
						link.description = "Make A Post";
						link.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_userID, std::to_string(posts->userid)));
						link.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_forumID, std::to_string(posts->id)));
						link.RenderHTMLDisplay(os);
						os << "<br/>";
						return true;
				};
			};
		}
	}
}
#endif