#ifndef ADD_A_FORUM_PAGE_VIEW_H
#define ADD_A_FORUM_PAGE_VIEW_H
#include "../interface/ViewVMWidget.h"
#include "../../../MVC/Model/Forums/ForumModel.h"
#include "../../../MVC/View/GUI/Form.h"
namespace MVC
{
	namespace View
	{
		namespace Forums
		{
			class AddAForumPageView : public virtual GUI::Form
			{
			public:
				//MVC::View::GUI::Form createAForumForm;
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (GUI::Form::RenderConsoleDisplay(os))
					{
						// need to populate the forummodel and add
						os << "Your Forum was added!" << std::endl;
						return true;
					}
					else
					{
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					if (GUI::Form::RenderHTMLDisplay(os) && _model !=nullptr && _model->errorMessages.size()==0)
					{
						os << "<H1> Your Forum was added </H1>" << std::endl;
					}
					return true;
				};
				void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					
					_model = &(*model);
					if (model != nullptr)
					{
						//successMessage = "forum page has been done succesfully";
						MVC::Model::Forum::ForumModel* forumModel = dynamic_cast<MVC::Model::Forum::ForumModel*>(_model);
						_visibleFormsAndLabels.clear();
						/*
						createAForumForm._visibleFormsAndLabels.clear();
						createAForumForm._invisibleTags.clear();
						MVC::View::GUI::FormLabelAndfieldName forumtilelabel;
						forumtilelabel._labelName = "Forum Name";
						forumtitlelabel.
						MVC::View::GUI::FormInvisibleTags userId;
						userId._key = MVC::Model::Forum::FK_userID;
						userId._value = forumModel->fk_Owner;
						*/
						View::GUI::FormInvisibleTags inv;
						inv._labelName = Model::Forum::FK_userID;
						inv._value = forumModel->fk_Owner;
						_invisibleTags.push_back(inv);
						MVC::View::GUI::FormLabelAndfieldName body;
						body._labelName = forumModel->forumName;
						body._key = MVC::Model::Forum::forumName;
						_visibleFormsAndLabels.push_back(GUI::FormLabelAndfieldName(body));
					}
					GUI::Form::IViewVMWidget::ProcessRequest(model, content, outViewAsBinaryString);
				};
			};
		}
	}
}
#endif
