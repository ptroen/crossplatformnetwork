#ifndef ADD_A_FORUM_POST_VIEW_H
#define ADD_A_FORUM_POST_VIEW_H
#include "../interface/ViewVMWidget.h"
#include "../../Model/Forums/PostModel.h"
#include "../../View/GUI/Form.h"

namespace MVC
{
	namespace View
	{
		namespace Forums
		{
			class AddAForumPostView : public virtual View::GUI::Form
			{
			public:
				// Model::Forum::PostTitle

				void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					if(model !=nullptr)
					{
						// prep the form
						// ok let's fill in the form info	
						//
						//const std::string PostTitle("PostTitle");
						//const std::string PostLines("PostLines");
							// forum will resolve if no errors are posted
						_visibleFormsAndLabels.clear();
						_invisibleTags.clear();
						if (model->errorMessages.size() > 0)
						{
							successMessage = "";
						}
						else
							successMessage = "forum post has been done succesfully";
							MVC::Model::Forum::PostModel* _post = reinterpret_cast<MVC::Model::Forum::PostModel*>(model);
							View::GUI::FormInvisibleTags inv;
							inv._labelName = Model::Forum::FK_userID;
							inv._key = MVC::Model::Forum::FK_userID;
							inv._value = _post->FK_userID;
							_invisibleTags.push_back(inv);
							View::GUI::FormInvisibleTags inv2;
							inv2._labelName = Model::Forum::FK_forumID;
							inv2._key = Model::Forum::FK_forumID;
							inv2._value = _post->FK_forumID;
							_invisibleTags.push_back(inv2);
							MVC::View::GUI::FormLabelAndfieldName title;
							title._key = Model::Forum::PostTitle;
							title._labelName = "Title of your post:";
							_visibleFormsAndLabels.push_back(title);
							MVC::View::GUI::FormLabelAndfieldName body; 
							body._key = Model::Forum::PostLines;
							body._labelName = "body:";
							body._multiline = true;
							_visibleFormsAndLabels.push_back(body);
						
					}
					View::Interface::IViewVMWidget::ProcessRequest(model, content, outViewAsBinaryString);
				};
			};
		}
	}
}
#endif