#ifndef FORUMS_DISPLAY_VIEW_H
#define FORUMS_DISPLAY_VIEW_H
#include <utility>
#include <string>
#include "../interface/ViewVMWidget.h"
#include "../../Model/Forums/PostModel.h" // this is for reading all posts
#include "../../Model/Forums/ForumModel.h"
namespace MVC
{
	namespace View
	{
		namespace Forums
		{
			class ForumsDisplayView : public virtual Interface::IViewVMWidget
			{
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						MVC::Model::Forum::ForumModel* model = reinterpret_cast<MVC::Model::Forum::ForumModel*>(_model);
						std::vector< hiberlite::bean_ptr<MVC::Model::Forum::ForumModel>> v;
						model->readAll(v);
						for(auto it : v)
						{
							os << it->forumName << " " << it->GetPk() << std::endl;
						}
						return true;
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
						// Display all forum posts here
					MVC::Model::Forum::ForumModel* frm = dynamic_cast<MVC::Model::Forum::ForumModel*>(_model);
						std::vector< hiberlite::bean_ptr<MVC::Model::Forum::ForumModel>> v;
						frm->readAll(v);
						os << "<BR/>";
						for (auto it : v)
						{
							GUI::Link link;
							link.inactive = false;
							link.url = Model::Forum::forumDisplayAForum;
							link.description = it->forumName;
							link.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_userID, std::to_string(frm->fk_Owner)));
							link.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_forumID, std::to_string(it->GetPk())));
							link.RenderHTMLDisplay(os);
							os << "<BR/>";
						}
						return true;
				};
			};


		}
	}
}
#endif