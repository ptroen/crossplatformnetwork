#ifndef MENU_BAR_VIEW_H
#define MENU_BAR_VIEW_H
#include "../interface/ViewVMWidget.h"
#include "../../../MVC/View/GUI/Link.h"
#include "../../../MVC/Model/Menus/ForumMenuBarModel.h"
#include "../../../MVC/Model/Forums/Forum.h"
#include <map>
namespace MVC
{
	namespace View
	{
		namespace Menus
		{
			class MenuBarView : public virtual Interface::IViewVMWidget
			{
				std::vector<GUI::Link> _links;
				MVC::Model::Menus::ForumMenuBarModel* forum;
				const size_t addAUserLinkIndex = 0;
				const size_t userLoginLinkIndex = addAUserLinkIndex + 1;
				const size_t addAForumPageLinkIndex = userLoginLinkIndex + 1;
				const size_t displayAllForumsLinkIndex = addAForumPageLinkIndex + 1;
				const size_t addAForumPostIndexLink = displayAllForumsLinkIndex + 1;
				const size_t menuLimit = addAForumPostIndexLink + 1;
				void BuildLinks(void)
				{
					/*
					 * 			const std::string forumAddAForumPage = "/forum/addaforumPage";
								const std::string forumAddAPostPage = "/forum/addapost";
								const std::string forumDisplayPost = "/forum/displaypost";
								const std::string forumDisplayAllForums = "/forum/viewForums";
								const std::string forumDisplayAForum = "/forum/viewForum";
					 *
					 *
					 */

					
					_links.clear();
					_links.resize(menuLimit);
					GUI::Link& addAUserLink = _links[addAUserLinkIndex];
					addAUserLink.url = Model::User::addUserUrl;
					addAUserLink.description = "addauser";
					addAUserLink.inactive = false;
					GUI::Link& loginPageLink = _links[userLoginLinkIndex];
					loginPageLink.inactive = false;
					loginPageLink.url = Model::User::loginPage;
					loginPageLink.description = "login";
					GUI::Link& addAForum = _links[addAForumPageLinkIndex];
					addAForum.inactive = false;
					addAForum.url = MVC::Model::Forum::forumAddAForumPage;
					addAForum.description = "Add A Forum";
					addAForum.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_userID, forum->userid));
					size_t forumid = atoi(forum->fk_forumid.c_str());
					size_t userid = atoi(forum->userid.c_str());
					if(userid == 0)
						addAForum.inactive = true;
					GUI::Link& displayAllForums = _links[displayAllForumsLinkIndex];
					displayAllForums.inactive = false;
					displayAllForums.url = MVC::Model::Forum::forumDisplayAllForums;
					displayAllForums.description = "Display Forums";
					displayAllForums.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_userID, forum->userid));
					if (userid == 0)
						displayAllForums.inactive = true;
					GUI::Link& addAForumPost = _links[addAForumPostIndexLink];
					
					if (forumid > 0) // if forum id exists
					{
						addAForumPost.inactive = false;
						addAForumPost.url = Model::Forum::forumAddAPostPage;
						addAForumPost.description = "Add A Forum Post";
						addAForumPost.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_userID, forum->userid));
						addAForumPost.getMap.insert(std::pair<std::string, std::string>(MVC::Model::Forum::FK_forumID,forum->fk_forumid));
					}
					else
						addAForumPost.inactive = true;
					//1. Add A ForumPage
						// if(forumid) => embedded in a forum page Add A Forum Post
					// 2.DisplayAllForums
					//		3.(DisplayAllForum it's really display a forum) -> Links embedded in Display a Forum
					//		 4. Displays a Post.( this could be rendered as a link within the rendered page)
				};
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (View::Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						BuildLinks();
						for(auto link : _links)
						{
							link.RenderConsoleDisplay(os);
						}
						return true;
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					if (View::Interface::IViewVMWidget::RenderHTMLDisplay(os))
					{
						BuildLinks();
						for (auto link : _links)
						{
							link.RenderHTMLDisplay(os);
						}
						return true;
					}
					return false;
				};

				void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					_model = model;
					forum = dynamic_cast<MVC::Model::Menus::ForumMenuBarModel*>(_model);
					View::Interface::IViewVMWidget::ProcessRequest(model, content, outViewAsBinaryString);
				};

				MenuBarView(void) {
					//_model = new MVC::Model::Menus::ForumMenuBarModel(); // controller manipulates the model
				};
			};
			
		}
	}
}

#endif