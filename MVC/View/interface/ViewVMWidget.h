#ifndef VIEW_VM_HTTP_H
#define VIEW_VM_HTTP_H
#include <map>
#include "../../../OSI/Session/HTTP/ContentRecordType.h"
#include "../../../OSI/Session/interface/IRequestBuilder.h"
#include "../GUI/IGUIWidget.h"
#include "../../Model/interface/IModel.h"
#include <sstream>
#include <iostream>
namespace MVC
{
	namespace View
	{
		namespace Interface
		{
			class IViewVMWidget : public virtual IGUIWidget
			{
			protected:
				Model::Interface::IModel* _model;
			public:
				IViewVMWidget(void):_model(nullptr) {};

				virtual   bool RenderJSONDisplay(std::ostringstream& os) { return true; };

				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (_model == nullptr) return false;
					if (_model->errorMessages.size() > 0)
					{
						os << "errors detected!" << std::endl;
						for (auto error : _model->errorMessages)
						{
							os << "error:" << error << std::endl;
						}
						return false;
					}
					
					return true;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					os << "<!DOCTYPE html><html>";
					if (_model == nullptr) return false;
					if (_model->errorMessages.size() > 0)
					{
						os << "<H1>errors detected!</H1>" << std::endl;
						for (auto error : _model->errorMessages)
						{
							os << "<P>error:" << error << "</P><br/>" << std::endl;
						}
						return false;
					}
					
					return true;
				};
				// process any kind of request
				// could be a gui request but we reuse the html principles

				/*
				 BETTER_ENUM(HTTP_CONTENT_TYPE, int32_t, MIME, text_html,text_xml, APPLICATION_JAVASCRIPT, text_plain,AUDIO_MPEG,image_gif,image_jpeg,image_png,image_tiff, VIDEO, JSON, CONSOLE, NOTCONTENTTYPE)
				 */
				virtual void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					_model = &(*model);
					switch (content)
					{
					case OSI::Session::HTTP_CONTENT_TYPE::CONSOLE:
						RenderConsoleDisplay(outViewAsBinaryString);
						break;
					case OSI::Session::HTTP_CONTENT_TYPE::text_html:
						RenderHTMLDisplay(outViewAsBinaryString);
						break;
					case OSI::Session::HTTP_CONTENT_TYPE::JSON:
						RenderJSONDisplay(outViewAsBinaryString);
						break;
					};

					// in case we need to close the tag
					switch (content)
					{
					case OSI::Session::HTTP_CONTENT_TYPE::text_html: outViewAsBinaryString << "</HTML>"; break;
					}
				}; // end process request
				}; // end IViewVMWidget
			} // end Interface
	} // end View
} // end MVC
#endif