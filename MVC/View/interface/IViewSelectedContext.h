#ifndef I_VIEW_SELECTED_CONTEXT
#define I_VIEW_SELECTED_CONTEXT
// defines which medium we rendering on
enum IViewSelectedContext
{
	HTML, // this is merely a stream of passing html
	GRAPHICSCANVAS
}

class IViewSelectedContext
{
	static IViewSelectedContext currentContext;
	
};

#endif