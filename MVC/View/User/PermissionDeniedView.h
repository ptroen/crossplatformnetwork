#ifndef PERMISSION_DENIED_VIEW
#define PERMISSION_DENIED_VIEW
#include "../interface/ViewVMWidget.h"
namespace MVC
{
	namespace View
	{
		namespace User
		{
			class PermissionDeniedView : public virtual Interface::IViewVMWidget
			{
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					os << "Permission Denied!" << std::endl;
					return true;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					os << "<H1> Permission Denied </H1>" << std::endl;
					return true;
				};
			};
		}
	}
}

#endif