#ifndef ADD_A_USER_GROUP_VIEW
#define ADD_A_USER_GROUP_VIEW
#include "../interface/ViewVMWidget.h"

namespace MVC
{
	namespace View
	{		
		namespace User
		{
			class AddAUserGroupView : public virtual Interface::IViewVMWidget
			{
			public:
				virtual bool RenderConsoleDisplay(std::ostringstream& os)
				{
					if (Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						os << "User Group Added" << std::endl;
						return true;
					}
					return false;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os)
				{
					if (Interface::IViewVMWidget::RenderHTMLDisplay(os))
					{
						os << "<H1> User Group Added </H1>" << std::endl;
						return true;
					}
					return false;
				};
			};
		}
	}
}

#endif