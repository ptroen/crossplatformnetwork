#ifndef LOGIN_PAGE_VIEW_H
#define LOGIN_PAGE_VIEW_H
#include "../interface/ViewVMWidget.h"
#include "../../Model/Forums/PostModel.h"
namespace MVC
{
	namespace View
	{
		namespace User
		{
			class LoginPageView : public virtual Interface::IViewVMWidget
			{
			public:
				MVC::Model::User::Interface::IUser* iusr;
				MVC::View::GUI::Form loginfrm;
				void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					loginfrm._visibleFormsAndLabels.clear();

					loginfrm.submitLink = Model::User::loginPage;
					MVC::View::GUI::FormLabelAndfieldName usernamelabel;
					usernamelabel._key = MVC::Model::User::KeyUsername;
					usernamelabel._labelName = "Username";
					loginfrm._visibleFormsAndLabels.push_back(usernamelabel);

					MVC::View::GUI::FormLabelAndfieldName pwlabel;
					pwlabel._key = MVC::Model::User::KeyPassword;
					pwlabel._labelName = "Password";
					loginfrm._visibleFormsAndLabels.push_back(pwlabel);
					Interface::IViewVMWidget::ProcessRequest(model, content, outViewAsBinaryString);
				};

				virtual bool RenderConsoleDisplay(std::ostringstream& os) {
					if(Interface::IViewVMWidget::RenderConsoleDisplay(os))
						os << " logged in successfully" << std::endl;
					return true;
				};

				virtual bool RenderHTMLDisplay(std::ostringstream& os) {
					loginfrm.RenderHTMLDisplay(os);
					if (Interface::IViewVMWidget::RenderHTMLDisplay(os))
					{
						MVC::Model::User::User * user = dynamic_cast<MVC::Model::User::User*>(_model);
						if (user->id > 0 && user->loggedOn==1)
						{
							std::string redirecturl = Model::User::loginPage + "?" + MVC::Model::Forum::FK_userID + "=" + std::to_string(user->id) + "&" + MVC::Model::User::KeyUsername + "=" + user->name;
							os << "<H1> logged in successfully</H1>" << std::endl;
							os << "<p>You will be redirected in 3 seconds</p>" << std::endl;
							os << "<script>" << std::endl;
							os << "var timer = setTimeout(function() {" << std::endl;
							os << "window.location = '" << redirecturl << "'" << std::endl;
							os << "}, 3000);" << std::endl;
							os << "</script>" << std::endl;
							user->loggedOn = 2; // new state that logon was visible
						}
						return true;
					}
					return false;
					};
				LoginPageView(void) :iusr(nullptr), loginfrm() {};
				};
		}
	}
}
#endif