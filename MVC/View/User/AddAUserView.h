#ifndef ADD_A_USER_VIEW_H
#define ADD_A_USER_VIEW_H
#include "../interface/ViewVMWidget.h"
#include <iostream>
#include "../GUI/Form.h"
namespace MVC
{
	namespace View
	{
		namespace User
		{
			class AddAUserView : public virtual Interface::IViewVMWidget
			{
            protected:
                MVC::Model::User::Interface::IUser* iusr;
				MVC::View::GUI::Form addusrfrm;
                virtual bool RenderConsoleDisplay(std::ostringstream& os){
					addusrfrm.RenderConsoleDisplay(os);
					if (Interface::IViewVMWidget::RenderConsoleDisplay(os))
					{
						os << "added user" << std::endl;
						os << "username: " << iusr->name << std::endl;
						os << "age " << iusr->age << std::endl;
						os << "bio :" << std::endl;
						for (auto b : iusr->bio)
						{
							os << b << std::endl;
						}
						return true;
					}
					return false;
                };

               virtual bool RenderHTMLDisplay(std::ostringstream& os){
				   addusrfrm.RenderHTMLDisplay(os);
				   if (Interface::IViewVMWidget::RenderHTMLDisplay(os))
				   {
					   os << "<H1>added user</H1><br/>" << std::endl;
					   os << "<P>username: " << iusr->name << "</P><br/>" << std::endl;
					   os << "<P>age " << iusr->age << "</P></br>" << std::endl;
					   os << "<P>bio :"; 
					   for (auto b : iusr->bio)
					   {
						   os << b << std::endl;
					   }
					   os << " </P><br/>" << std::endl;
					   return true;
				   }
				   return false;
                };
			public:
				AddAUserView(void) {
				};
                virtual void ProcessRequest(Model::Interface::IModel* model, OSI::Session::HTTP_CONTENT_TYPE& content, std::ostringstream& outViewAsBinaryString) {
					addusrfrm._visibleFormsAndLabels.clear();

					addusrfrm.submitLink = Model::User::addUserUrl;
					MVC::View::GUI::FormLabelAndfieldName usernamelabel;
					usernamelabel._key = MVC::Model::User::KeyUsername;
					usernamelabel._labelName = "Username";
					addusrfrm._visibleFormsAndLabels.push_back(usernamelabel);

					MVC::View::GUI::FormLabelAndfieldName agelabel;
					agelabel._key = MVC::Model::User::KeyAge;
					agelabel._labelName = "Age";
					addusrfrm._visibleFormsAndLabels.push_back(agelabel);

					MVC::View::GUI::FormLabelAndfieldName pwlabel;
					pwlabel._key = MVC::Model::User::KeyPassword;
					pwlabel._labelName = "Password";
					addusrfrm._visibleFormsAndLabels.push_back(pwlabel);

					MVC::View::GUI::FormLabelAndfieldName biolabel;

					biolabel._key = MVC::Model::User::KeyBio;
					biolabel._labelName = "Biography";
					addusrfrm._visibleFormsAndLabels.push_back(biolabel);
					if (_model != nullptr)
					{
						iusr = dynamic_cast<MVC::Model::User::Interface::IUser*>(_model);
					}
					IViewVMWidget::ProcessRequest(model, content, outViewAsBinaryString);
					
                };
			};
		}
	}
}
#endif