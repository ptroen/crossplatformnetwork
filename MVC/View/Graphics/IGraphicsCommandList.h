#ifndef IGRAPHICSCOMMANDLIST_H
#define IGRAPHICSCOMMANDLIST_H

#include "../../../algorithm/circularbuffer.h"
/*
class ITextureCopyDimensionsToSurface;
class ISentenceCodeToSurface;
class ITileViewMapToImageSurface;
class IShaderInitialization;
class IMouseCursor;
class IUILayer;
*/

using namespace std;

enum GraphicsCommandType
{
	SentenceCodeDrawRequest,
	ImageBufferCopyRequest,
	Draw
};

union GraphicsCommandOptionBuffer
{
	//ISentenceCodeToSurface _sentenceCodeToSurface;
};

struct GraphicsCommandOption
{
	GraphicsCommandType _command;
	GraphicsCommandOptionBuffer _buffer;
};

class ImageBufferRequest
{
   public:
	size_t textureIndex;
	// ITextureCopyDimensionsToSurface dimensions;
};


class IGraphicsCommandList
{
    private: const int _commandOptionBufferSize = 1024;
    protected:
	CircularBuffer<GraphicsCommandOption> _commandOption;

	public:
	IGraphicsCommandList():_commandOption(_commandOptionBufferSize){  };
	
	void Clear(void) { _commandOption.Clear(); }

	void AddCommand(GraphicsCommandOption& command)
	{
		_commandOption.Assign(command);
	};
	/*
	void CopyTextureToImageBuffer(size_t textureIndex, ITextureCopyDimensionsToSurface& dimensions);
	void CopyFontStringToSurface(ISentenceCodeToSurface& sentenceCode);
	void CopyTileViewMapToImageBuffer(ITileViewMapToImageSurface& tileMap);
	void CopyMouseCursorToImageBuffer(IMouseCursor& mouseCursor);
	void InitializeShaderToImageBuffer(IShaderInitialization& shader);
	void RetireShaderToImageBuffer(IShaderInitialization& shader);
	void CopyUserInterfaceLayerToImageBuffer(IUILayer& uiLayerList );
	*/
};

#endif