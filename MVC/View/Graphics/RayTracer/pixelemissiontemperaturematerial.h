#ifndef PIXEL_EMMISION_TEMPERATURE_MATERIAL_H
#define PIXEL_EMMISION_TEMPERATURE_MATERIAL_H
#include "../Primitives/Wave.h"
#include "PixelEmmissionTemperature.h"
#include <cstdint>
#include "../Primitives/SpectrumPair.h"
struct PixelEmissionTemperatureMaterial : PixelEmmisionTemperature
{
	uint16_t lumens; // how bright the material is transmitting radiation(spectra could be transmitting shadows)
	//uint8_t transparency_refraction_Percentage; // what's the percentage it goes through the material 100%(0% reflection) - 0% Opaque(100% reflective) is 0%
	mvc::view::rendering::SpectrumPair spectrumInformation;
	uint8_t reflection;         /// surface transparency and reflectivity
	mvc::view::rendering::WaveRGBBlending Reflection() { return spectrumInformation.emisson; }



	mvc::view::rendering::WaveRGBBlending Emittance() { return spectrumInformation.emisson;
	};
	bool Zero()
	{
		return lumens == 0 || EmmissionType == 0 || _temperature.GetTemperature() == 0;
	};

	bool IsLight()
	{
		return lumens > 0;
	}
};

#endif