#ifndef CAMERA_H
#define CAMERA_H
#include <Eigen/Core>
#include "Ray.h"
#include "../Texture.h"
namespace Graphics
{
	namespace RayTracer
	{
		class Camera
		{
		public:
			Eigen::Vector3d centre;

			mvc::view::ThreeDimensional::Ray CameraFacingRay;
			Texture cameraFacingTextureInARGB;
		};
	}

}
#endif