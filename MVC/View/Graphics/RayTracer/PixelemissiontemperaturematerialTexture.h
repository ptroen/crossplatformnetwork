﻿#ifndef SURFACE_THREAD_H
#define SURFACE_THREAD_H

#include "../Texture.h"
#include "../RayTracer/Ray.h"
class SurfaceVoxel
{
public:
	mvc::view::rendering::WaveRGBBlending EmittanceColor;
	mvc::view::rendering::WaveRGBBlending ReflectionColor; // what color we reflect
	mvc::view::rendering::WaveRGBBlending Defraction;
	uint32_t temperature;
	uint16_t displacementAmount; // how far away it is from the cube
	Eigen::Vector3i normalVector;
};

class Surface3d : public Texture
{
	const size_t Surface3dBitsPerPixel = sizeof(SurfaceVoxel) * 8;
public:
	Surface3d(Texture& temperatureTexture, // controls the specular
		Texture& materialLookupTexture,
		Texture& DisplacementMap,
		Texture& normalTexture,
		Eigen::Vector3i centre): Texture(materialLookupTexture._width,materialLookupTexture._width, Surface3dBitsPerPixel){};

	Eigen::Matrix3Xi surfaceTuTvTz; // Tu TV standard texture vector tz is thickness. If the linear combination of a vector is 1  
	
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection
	bool doesRayIntersectTheSurface(mvc::view::ThreeDimensional::Ray& ray, SurfaceVoxel& out_pixel) {
		//ray.direction
		
		return false;
		};

};


#endif
