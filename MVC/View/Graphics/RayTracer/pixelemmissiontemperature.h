#ifndef PIXEL_EMMISON_TEMPERATURE_H
#define PIXEL_EMMISON_TEMPERATURE_H
#include <stdint.h>
#include "../Primitives/Temperature.h"
struct PixelEmmisionTemperature
{
	uint16_t EmmissionType;
	Temperature _temperature; // how much it's shifted
};


#endif