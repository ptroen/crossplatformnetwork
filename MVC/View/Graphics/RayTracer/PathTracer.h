#ifndef RAY_TRACER_H
#define RAY_TRACER_H
#include "RayTracerSettings.h"
#include <algorithm>
#include "../ITexture.h"
#include "../Texture.h"
#include "../../../../algorithm/circularbuffer.h"
#include "../Primitives/PhotonType.h"
#include "../Primitives/ModelCube.h"
#include "Scene.h"
#include <Eigen/Core>
#include "Ray.h"
#include "Camera.h"
#include "PathTracerThread.h"
#include "Scene.h"
#include "Ray.h"
namespace MVC
{
	namespace View
	{
		namespace Graphics
		{
			namespace ThreeDimensional
			{
				class PathTracer
				{

				public:
					void render(Graphics::RayTracer::Camera& camera, Scene& scene, Texture& cameraTexture)
					{
						size_t numberOfPasses = 1;
						size_t depthCount = 1;
						mvc::view::ThreeDimensional::Ray ray = camera.CameraFacingRay;
						for (size_t y = 0; y < cameraTexture._height; y++)
						{
							for (size_t x = 0; x < cameraTexture._width; x++)
							{
								for (size_t p = 0; p < numberOfPasses; p++)
								{
									// shoot a ray based off the camera

									rendering::WaveRGBBlending TracePath(ray, depthCount);
								}
							}
						}

					};
				};
			}
			};
		}
	}
}

#endif	