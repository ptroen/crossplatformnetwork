#ifndef SCENE_H
#define SCENE_H

#include "../Primitives/ModelCube.h"
#include <vector>

/*
 *  A Scene. A Group of models to render
 */
class Scene
{
public:
	std::vector<ModelCube> scene;
};

#endif