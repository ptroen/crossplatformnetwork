#include "Renderer.h"
#include "../../Tools/Diagnostics/Logger.h"
void Renderer::StartProcessing(Renderer* protocol)
{
	// if frame not null process
	RenderInfo local;
	// update stats
	protocol->lock();
	memcpy(&local, &protocol->_renderInfo, sizeof(Renderer));
	protocol->unlock();
	logger log(protocol->_renderInfo._pid);

	while (local.frame_number < local.max_framenumber)
	{
		std::stringstream ss;
		// get status
		ss << "frame number" << protocol->_renderInfo._pid
			<< " maxthreads"
			<< protocol->_renderInfo._maxthreads;
		log << ss.str();
		std::stringstream ss2;
		ss2 << protocol->_renderInfo._pid << "_" << local.frame_number << ".tga";

		// double buffering logic could memcpy to parent thread but no time right
		// probably about 4 hrs work todo it
		string s = ss2.str();
		protocol->_texture1.Clear();
		if (local.frame_number % 2 == 0)
		{
	
		//	protocol->_texture1.GenerateImage(128 + local.frame_number, 128 + local.frame_number, local.frame_number);
			
		}
		else
		{
		//	protocol->_texture2.GenerateImage(128 + local.frame_number, 128 + local.frame_number, local.frame_number);
		}
		protocol->_texture1.Save(s);
		local.frame_number++;
		// update stats
		protocol->lock();
		memcpy(&protocol->_renderInfo, &local, sizeof(Renderer));
		protocol->unlock();
	}
	std::stringstream ss;
	ss << "frame number" << protocol->_renderInfo._pid
		<< " maxthreads"
		<< protocol->_renderInfo._maxthreads;
	log << ss.str();
}

void MasterRenderer::StartProcessing(MasterRenderer* protocol)
{
	bool done = false;
	logger log(protocol->_renderinfo._maxthreads);
	RenderInfo local;

	do
	{
		done = true;
		// cycle thru the locks and see if we are done
		for (size_t i = 0; protocol->_renderinfo._maxthreads; i++)
		{
			protocol->SetCallersRenderInfo(local);
			// get status
			std::stringstream ss;
			// get status
			RenderInfo& _renderInfo = local;
			ss << "frame number" << _renderInfo._pid
				<< " maxthreads"
				<< _renderInfo._maxthreads;
			log << ss.str();
			if (done)
			{
				if (_renderInfo.frame_number < _renderInfo._maxthreads)
				{
					done = false;
				}
			}
		}
	} while (!done);
}