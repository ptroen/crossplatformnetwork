#ifndef RAY_H
#define RAY_H

#include <Eigen/Core>
namespace mvc
{
	namespace view
	{
		namespace ThreeDimensional
		{

			class Ray
			{
			    public:
					Eigen::RowVector3i origin;
					Eigen::RowVector3i direction;
				/*
				Eigen::RowVector3d NormalWhereObjectWasHit()
				{
					
					return Eigen::RowVector3d();
				};

				Eigen::RowVector3d PointWhereObjectWasHit()
				{
					return Eigen::RowVector3d();
				};*/

				const Ray& operator=(Ray& rhs) { origin = rhs.origin; direction = rhs.direction; };

			};
		}
	}
}

#endif