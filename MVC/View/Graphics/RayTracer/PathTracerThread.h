#ifndef  PATH_TRACER_THREAD_H
#define PATH_TRACER_THREAD_H
#include "Ray.h"
#include "../../algorithm/icriticalthread.h"
#include "../Primitives/ModelTileView.h"
class PathTracerThread;

const double M_PI = 3.141592653589793;

namespace mvc
{
	namespace view
	{
		namespace ThreeDimensional
		{
			class PathTracerThread : ICriticalThread
			{
				Eigen::Vector3i RandomUnitVectorInHemisphereOf(Eigen::Vector3i point);
				inline double DotProduct(Eigen::Vector3i direction, Eigen::RowVector3i whereObjectWasHit) { return direction.dot(whereObjectWasHit); };

				bool hitSomething(Ray& r, SurfaceVoxel** out_pixel)
				{
					// TileMap here
					ModelTileView* model;
					return false;
				};

			public:
				size_t _maxDepth;
				PathTracerThread(size_t maxDepth) :_maxDepth(maxDepth) {};

				rendering::WaveRGBBlending TracePath(Ray ray, size_t depthCount) {
					static rendering::WaveRGBBlending blackEmptySurface;
					if (depthCount >= _maxDepth) {
						return blackEmptySurface;  // Bounced enough times so return default object which is black
					}

					SurfaceVoxel* voxel;
					if (hitSomething(ray, &voxel) == false) {
						return blackEmptySurface;   // Nothing was hit.
					}
					// Pick a random direction from here and keep going.
					Ray newRay;
					newRay.origin = voxel->PointWhereObjectWasHit(ray);
					Eigen::Vector3i normalWhereObjectWasHit = voxel->NormalWhereObjectWasHit(ray);
					// simply matter of randomizing the orthonogonal vector
					newRay.direction = RandomUnitVectorInHemisphereOf(normalWhereObjectWasHit);
					// Probability of the newRay
					const double probalityOfNewRay = 1 / (2 * M_PI);

					// Compute the BRDF for this ray (assuming Lambertian reflection)
					double cos_theta = DotProduct(newRay.direction, normalWhereObjectWasHit);
					rendering::WaveRGBBlending BidirectionalReflectanceDistributionFunction = voxel->ReflectionColor(newRay.direction) / M_PI;

					// Recursively trace reflected light sources.
					rendering::WaveRGBBlending incoming = TracePath(newRay, depthCount + 1);

					// Apply the Rendering Equation here.
					// cos_theta / Probaility Of New Ray
					// note this is adjusting the angle to lower the emmitance. We can just take the emmitance value and bypass this calculation back to the render to save more clock cycles.
					// we do this by using the angle to adjust the emmitance strength
					// BRDF * incoming is a blending calculation
					auto angleOfNewRay = cos_theta / probalityOfNewRay;
					return voxel->EmittanceColor(newRay.direction) + (BidirectionalReflectanceDistributionFunction  * (incoming * angleOfNewRay));
				}
			};
		}
	}
#endif