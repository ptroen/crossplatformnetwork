#ifndef GENERATE_MAP_H
#define GENERATE_MAP_H
#include "../../Tools/OSInclude.h"
#include "../../algorithm/icriticalthread.h"
#include "../../algorithm/VM/VM.h"
#include "../GraphicsVM.h"
#include <vector>
#include "../Texture.h"

struct RenderInfo
{
	size_t frame_number;
	size_t max_framenumber;
	size_t _pid;
	size_t _maxthreads;
	short width;
	short height;
	unsigned char bpp;
};
// fan pattern
class Renderer;
class MasterRenderer;

class Renderer : public virtual ICriticalThread
{

	MasterRenderer* _parent;
	RenderInfo _renderInfo;
public:
	
	Texture      _texture1;
	Texture      _texture2;
	Renderer(RenderInfo& renderinfo,
		MasterRenderer* parent) :_parent(parent),
		_texture1(renderinfo.width, renderinfo.height / renderinfo._maxthreads, renderinfo.bpp),
		_texture2(renderinfo.width, renderinfo.height / renderinfo._maxthreads, renderinfo.bpp)

	{
		memcpy(&_renderInfo, &renderinfo, sizeof(RenderInfo));
		ICriticalThread();
	};

	static void StartProcessing(Renderer* protocol);

	virtual void InvokeThread()
	{
#if defined( WIN32 )
		_workerthread = new std::thread(StartProcessing, this);
#else
		pthread_create(&_workerthread, NULL, StartProcessing, (void*)this);
#endif
	};
};

// fan multiprocessing pattern to launch the child threads
// Master Renderer talks to the Graphics VM
class MasterRenderer : public virtual ICriticalThread
{
	RenderInfo _renderinfo;
	std::vector<Renderer*>  _child_renderer;
	static void StartProcessing(MasterRenderer*);
public:

	void SetCallersRenderInfo(RenderInfo& caller)
	{
		lock();
		memcpy(&caller, &_renderinfo, sizeof(RenderInfo));
		unlock();
	};

	MasterRenderer(RenderInfo& renderinfo) : ICriticalThread()
	{
		memcpy(&_renderinfo, &renderinfo, sizeof(RenderInfo));
		_child_renderer.resize(_renderinfo._maxthreads);
	};

	virtual void InvokeThread()
	{
		size_t i;
		for (i = 0; i < _renderinfo._maxthreads; i++)
		{
			_child_renderer[i] = new Renderer(_renderinfo, this);
			_renderinfo._pid++;
		}
		for (i = 0; i < _renderinfo._maxthreads; i++)
		{
			Renderer& render = *_child_renderer[i];
			render.InvokeThread();
		}

		// invoke the master thread
#if defined( WIN32 )
		_workerthread = new std::thread(StartProcessing, this);
#else
		pthread_create(&_workerthread, NULL, StartProcessing, (void*)this);
#endif
		for (i = 0; i < _renderinfo._maxthreads; i++)
		{
			_child_renderer[i]->join();
		}
	};
};

#endif