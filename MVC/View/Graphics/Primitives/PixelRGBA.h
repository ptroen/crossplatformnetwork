#ifndef PIXEL_RGBA_H
#define PIXEL_RGBA_H
#include <cstdint>
struct PixelRGBA
{
	uint8_t blue;
	uint8_t green;
	uint8_t red;
	uint8_t alpha;
	PixelRGBA() { blue = 0; green = 0; red = 0; alpha = 0; }
	PixelRGBA& operator+=(PixelRGBA i)
	{
		blue += i.blue;
		green += i.green;
		red += i.red;
		alpha += i.alpha;
		return *this;
	};

	static PixelRGBA EmptySurface()
	{
		PixelRGBA a;
		a.alpha = 0;
		return a;
	};
};
#endif