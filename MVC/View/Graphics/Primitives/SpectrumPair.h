#ifndef SPECTRUM_PAIR_H
#define SPECTRUM_PAIR_H
#include "../Primitives/Wave.h"
namespace mvc
{
	namespace view
	{
		namespace rendering
		{
			class SpectrumPair;
			class SpectrumPair
			{
			public:
				SpectrumPair():emisson(), absorption(), transparency() {};

				WaveRGBBlending absorption; //  absorbption as 6 categories
				WaveRGBBlending emisson; // emission as 6 categories
				WaveRGBBlending transparency; // what goes thru as the primary ray  transparcency could be a percentage of not absorbed
			};
		}
	}
}
#endif