#ifndef SPECTRUM_PROFILE_H
#define SPECTRUM_PROFILE_H

#include "../Primitives/ElementType.h"
#include "../../../../ThirdParty/enum.h"
#include <array>
#include "../Primitives/Wave.h"
#include "SpectrumPair.h"
namespace mvc
{
	namespace view
	{
		namespace rendering
		{

			// this contains all the different types of spectrums
			class SpectrumProfile
			{
			public:
				SpectrumPair _spectrum;
			};

			class SpectrumProfiles
			{
			public:
				std::array<SpectrumProfile, numberOfTotalSpectrum >_SpectrumProfiles;
				SpectrumProfile& operator[](Row1ElementType __Row1ElementType) { return _SpectrumProfiles[__Row1ElementType]; };
				SpectrumProfile& operator=(Row2ElementType __Row2ElementType) { return _SpectrumProfiles[__Row2ElementType]; };
				SpectrumProfile& operator=(Row3ElementType __Row3ElementType) { return _SpectrumProfiles[__Row3ElementType]; };
				SpectrumProfile& operator=(Row4ElementType __Row4ElementType) { return _SpectrumProfiles[__Row4ElementType]; };
				SpectrumProfile& operator=(Row5ElementType __Row5ElementType) { return _SpectrumProfiles[__Row5ElementType]; };
				SpectrumProfile& operator=(Row6ElementType __Row6ElementType) { return _SpectrumProfiles[__Row6ElementType]; };
				SpectrumProfile& operator=(Row6LanthanoidsElementType __Row6LanthanoidsElementType) { return _SpectrumProfiles[__Row6LanthanoidsElementType]; };
				SpectrumProfile& operator=(Row7ElementType __Row7ElementType) { return _SpectrumProfiles[__Row7ElementType]; };
				SpectrumProfile& operator=(Row7AuctinoidsElementType __Row7AuctinoidsElementType) { return _SpectrumProfiles[__Row7AuctinoidsElementType]; };
				SpectrumProfile& operator=(OrganicGreensElementType __OrganicGreensElementType) { return _SpectrumProfiles[__OrganicGreensElementType]; };
				SpectrumProfile& operator=(PrimarySpectrumColorsElementType _PrimarySpectrumColorsElementType) { return _SpectrumProfiles[_PrimarySpectrumColorsElementType]; };
			};

			// this can also be casted as a texture
			class PhotonContainer
			{
				std::array<SpectrumProfile, numberOfTotalSpectrum > _spectrumProfiles;

			public:
				PhotonContainer(void);
				SpectrumProfile & getProfile(uint16_t i);
				SpectrumProfile& getProfile(ElementType type) { return getProfile((uint16_t)type._ElementGroups.value); };
				void CreateSpectrumTexture(ElementType minType, ElementType maxType);
				// general t est that the Spectrum type renders correctly the whole gamut
				void PaletteTest(void);
			private:
				void LoadFushia(void);
				void LoadBlue(void);
				void LoadTeal(void);
				void LoadGreen(void);
				void LoadYellow(void);
				void LoadRed(void);
				void LoadHydrogen(void);
				void LoadWhiteEmission(void);
				void LoadBlackEmission(void);
				void LoadHelliumI(void);
				void LoadHeliumII(void);
				void LoadLithiumI(void);
				void LoadLithiumII(void);
				void LoadBeryliumI(void);
				void LoadBeryliumII(void);
				void LoadBoronI(void);
				void LoadBoronII(void);
				void LoadCarbonI(void);
				void LoadCarbonII(void);
				void LoadNitrogenI(void);
				void LoadNitrogenII(void);
				void LoadOxygenI(void);
				void LoadOxygenII(void);
				void LoadFlourineI(void);
				void LoadFlourineII(void);
				void LoadNeonI(void);
				void LoadNeonII(void);
				void LoadSodium(void);
				void LoadMagnesum(void);
				void LoadAluminum(void);
				void LoadSilicon(void);
				void LoadPhosphorus(void);
				void LoadSulfur(void);
				void LoadChlorine(void);
				void LoadArgon(void);

				// Row4 Periodic Table
				void LoadPottasium(void);
				void LoadCalcium(void);
				void LoadScandium(void);
				void LoadTitanium(void);
				void LoadVanadium(void);
				void LoadChromium(void);
				void LoadManganese(void);
				void LoadIron(void);
				void LoadCobalt(void);
				void LoadNickel(void);
				void LoadCopper(void);
				void LoadZinc(void);
				void LoadGallium(void);
				void LoadGermanium(void);
				void LoadArsenic(void);
				void LoadSelenium(void);
				void LoadBromine(void);
				void LoadKrypton(void);


				// Row5
				void LoadRubidium(void);
				void LoadStronium(void);
				void LoadYttrium(void);
				void LoadZironium(void);
				void LoadNiobium(void);
				void LoadMolybdenum(void);
				void LoadTechnetium(void);
				void LoadRuthenium(void);
				void LoadRhodium(void);
				void LoadPalladium(void);
				void LoadSilver(void);
				void LoadCadmium(void);
				void LoadIndium(void);
				void LoadTin(void);
				void LoadAntimoney(void);
				void LoadTellurium(void);
				void LoadIodine(void);
				void LoadXenon(void);

				// Row6
				void LoadCaeesium(void);
				void LoadBarium(void);

				// Row6LanthanoidsElementType
				void Lanthanum(void);
				void Cerium(void);
				void Praseodymium(void);
				void Neodymium(void);
				void Promethium(void);
				void Samarium(void);
				void  Europium(void);
				void  Gadolinium(void);
				void  Terbium(void);
				void  Dysprosium(void);
				void  Holmium(void);
				void  Erbium(void);
				void  Ytterbium(void);
				void  Lutetium(void);

				// Row6AfterLanthanoidsElementType
				void Hafnium(void);
				void  Tantalum(void);
				void  Tungsten(void);
				void  Rhenium(void);
				void  Osmium(void);
				void  Iridium(void);
				void  Platinum(void);
				void  Gold(void);
				void  Mercury(void);
				void  Thallium(void);
				void  Lead(void);
				void  Bismuth(void);
				void  Polonium(void);
				void  Astantine(void);
				void  Radon(void);

				// Row7ElementType
				void	Francium(void);
				void  Radium(void);

				// Row7AuctinoidsElementType
				void Actinium(void);
				void  Thorium(void);
				void  Protactinium(void);
				void  Uranium(void);
				void  Plutonium(void);
				void  Americium(void);
				void  Curium(void);
				void  Berkelium(void);
				void  Californium(void);
				void  Einsteinium(void);
				void  Fermium(void);
				void  Mendelevium(void);
				void  Nobelium(void);
				void  Lawrencium(void);

				// Row7AfterAuctinoidsElementType
				void Rutherfordium(void);
				void  Dubnium(void);
				void  Seaborgium(void);
				void  Bohrium(void);
				void  Hassium(void);
				void  Meitnerium(void);
				void  Darmstadtium(void);
				void  Roentgenium(void);
				void  Copernicium(void);
				void  Nihonium(void);
				void  Flerovium(void);
				void  Moscovium(void);
				void  Livermorium(void);
				void  Tennessine(void);
				void  Oganesson(void);
			};
		}
	}
}
#endif