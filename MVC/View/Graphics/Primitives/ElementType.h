#ifndef ELEMENT_TYPE_H
#define ELEMENT_TYPE_H

#include "../../../../ThirdParty/enum.h"
#include <array>
#include <cstdint>

namespace mvc
{
	namespace view
	{
		namespace rendering
		{
			// playing around with visual spectrum. Define all elements using spectrum databases to see what it looks like
			BETTER_ENUM(PrimarySpectrumColorsElementType, uint16_t, blankLight, whiteLight, blackBlankLight, FushiaLight, RedLight, BlueLight, GreenLight, TealLight, YellowLight)

				BETTER_ENUM(Row1ElementType, uint16_t, Hydrogen = PrimarySpectrumColorsElementType::YellowLight + 1, HeliumI, HeliumII)

				BETTER_ENUM(Row2ElementType, uint16_t, LithiumI = Row1ElementType::HeliumII + 1, LithiumII, BeryliumI, BeryliumII, BoronI, BoronII, CarbonI, CarbonII, NitrogenI, NitrogenII, OxygenI, OxygenII, FlourineI, FlourineII, NeonI, NeonII)

				BETTER_ENUM(Row3ElementType, uint16_t, SodiumI = Row2ElementType::NeonII + 1, SodiumII, MagnesumI, MagnesumII, AluminumI, AluminumII, SiliconI, SiliconII, PhosphorusI, PhosphorusII, SulfurI, SulfurII, ChlorineI, ChlorineII, ArgonI, ArgonII)

				BETTER_ENUM(Row4ElementType, uint16_t, PotassiumI = Row3ElementType::ArgonII + 1, PotassiumII, CalciumI, CalciumII, ScandiumI, ScandiumII, TitaniumI, TitaniumII, VanadiumI, VanadiumII, ChromiumI, ChromiumII, ManganeseI, ManganeseII, IronI, IronII, CobaltI, CobaltII, NickelI, NickelII, CooperI, CooperII, ZincI, ZincII, GalliumI, GalliumII, GermamiumI, GermamiumII, ArsenicI, ArsenicII, SeleniumI, SeleniumII, BromineI, BromineII, KryptonI, KryptonII)

				BETTER_ENUM(Row5ElementType, uint16_t, RubidumI = Row4ElementType::KryptonII + 1, RubidumII, StroniumI, StroniumII, YttriumI, YttriumII, ZirconiumI, ZirconiumII, NiobiumI, NiobiumII, MolybdenumI, MolybdenumII, TechnetiumI, TechnetiumII, RutheniumI, RutheniumII, RhodiumI, RhodiumII, PalladiumI, PalladiumII, SilverI, SilverII, CadmiumI, CadmiumII, IndiumI, IndiumII, TinI, TinII, AntimonyI, AntimonyII, TelliumI, TelliumII, IodineI, IodineII, XenonI, XenonII)

				BETTER_ENUM(Row6ElementType, uint16_t, CaeesiumI = Row5ElementType::XenonII + 1, CaeesiumII, BariumI, BariumII)

				BETTER_ENUM(Row6LanthanoidsElementType, uint16_t, LanthanumI = Row6ElementType::BariumII + 1, LanthanumII, CeriumI, CeriumII, PraseodymiumI, PraseodymiumII, NeodymiumI, NeodymiumII, PromethiumI, PromethiumII, SamariumI, SamariumII, EuropiumI, EuropiumII, GadoliniumI, GadoliniumII, TerbiumI, TerbiumII, DysprosiumI, DysprosiumII, HolmiumI, HolmiumII, ErbiumI, EribumII, YtterbiumI, YtterbiumII, LutetiumI, LutetiumII)

				BETTER_ENUM(Row6AfterLanthanoidsElementType, uint16_t, HafniumI = Row6LanthanoidsElementType::LutetiumII + 1, HafniumII, TantalumI, TantalumII, TungstenI, TungstenII, RheniumI, RheniumII, OsmiumI, OsmiumII, IridiumI, IridiumII, PlatinumI, PlatinumII, GoldI, GoldII, MercuryI, MercuryII, ThalliumI, ThalliumII, LeadI, LeadII, BismuthI, BismuthII, Polonium, PoloniumI, AstantineI, RadonI)

				BETTER_ENUM(Row7ElementType, uint16_t, FranciumI = Row6AfterLanthanoidsElementType::RadonI + 1, RadiumI, RadiumII)

				BETTER_ENUM(Row7AuctinoidsElementType, uint16_t, ActiniumI = Row7ElementType::RadiumII + 1, ActiniumII, ThoriumI, ThoriumII, ProtactiniumI, ProtactiniumII, UraniumI, UraniumII, PlutoniumI, PlutoniumII, AmericiumI, AmericiumII, CuriumI, CuriumII, BerkeliumI, BerkeliumII, CaliforniumI, CaliforniumII, Einsteinium ,EinsteiniumI, EinsteiniumII, FermiumI, FermiumII, MendeleviumI, MendeleviumII, NobeliumI, NobeliumII, LawrenciumI, LawrenciumII)

				BETTER_ENUM(Row7AfterAuctinoidsElementType, uint16_t, RutherfordiumI = Row7AuctinoidsElementType::LawrenciumII + 1, RutherfordiumII, DubniumI, DubniumII, SeaborgiumI, SeaborgiumII, BohriumI, BohriumII, HassiumI, HassiumII, MeitneriumI, MeitneriumII, DarmstadtiumI, DarmstadtiumII, RoentgeniumI, RoentgeniumII, CoperniciumI, CoperniciumII, NihoniumI, NihoniumII, FleroviumI, FleroviumII, MoscoviumI, MoscoviumII, LivermoriumI, LivermoriumII, TennessineI, TennessineII, OganessonI, OganessonII)

				BETTER_ENUM(OrganicGreensElementType, uint16_t, Leaf = Row7AfterAuctinoidsElementType::OganessonII + 1, TreeBark)


				// https://en.wikipedia.org/wiki/Periodic_table#/media/File:Simple_Periodic_Table_Chart-en.svg
				BETTER_ENUM(PeriodicTableType, uint16_t, Row1 = 0, Row2, Row3, Row4, Row5, Row6, Row6Lanthanoids, Row6AfterLanthanoids, Row7ElementType, Row7Auctinoids, Row7AfterAuctinoids, OrganicGreens, SpectrumColors)

				//, , , 
				// , , , 
				// increasing better enum size
			// BETTER_ENUM(ElementType, uint16_t,  , , )
				static const size_t numberOfTotalSpectrum = 4096; // 2^6 * 2^6

			union ElementGroups
			{
				uint16_t value;
				Row1ElementType _Row1ElementType;
				Row2ElementType _Row2ElementType;
				Row3ElementType _Row3ElementType;
				Row4ElementType _Row4ElementType;
				Row5ElementType _Row5ElementType;
				Row6ElementType _Row6ElementType;
				Row6LanthanoidsElementType _Row6LanthanoidsElementType;
				Row6AfterLanthanoidsElementType _Row6AfterLanthanoidsElementType;
				Row7ElementType _Row7ElementType;
				Row7AuctinoidsElementType _Row7AuctinoidsElementType;
				Row7AfterAuctinoidsElementType _Row7AfterAuctinoidsElementType;
				OrganicGreensElementType _OrganicGreensElementType;
				PrimarySpectrumColorsElementType _PrimarySpectrumColorsElementType;
				ElementGroups() :value(0) {};
			};

			// Phase state table
			// 4096 2^6 * 2^6
			class ElementType
			{
			public:

				ElementGroups _ElementGroups;
				PeriodicTableType _PeriodicTableType;
				ElementType() :_ElementGroups(), _PeriodicTableType(PeriodicTableType::SpectrumColors) {};
				ElementType& operator= (const uint16_t& rhs)
				{
					_from_index(rhs);
					return *this;
				}
				size_t _to_index(void);
				void _from_index(size_t i);

				const ElementType& operator++ (void)     // prefix ++
				{
					_from_index(_to_index() + 1);
					return *this;
				};
				inline bool operator<=(const ElementType& rhs) { return this->_ElementGroups.value <= rhs._ElementGroups.value; }

				std::string _to_string(void);
			};
		} // end rendering
	} // end view
} // end mvc

#endif