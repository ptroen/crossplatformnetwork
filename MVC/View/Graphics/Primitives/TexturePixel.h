#ifndef TexturePIXEL_H
#define TexturePIXEL_H
#include <stdint.h>
struct PixelRGB
{
	int8_t blue;
	int8_t green;
	int8_t red;
	int8_t alpha;
};

union TexturePixel
{
	PixelRGB rgba;
	uint32_t emissionTemperature;

};

#endif