#ifndef IMAGE_BUFFER_H
#define IMAGE_BUFFER_H

#include "../Texture.h"

class ImageBuffer : public Texture
{
	public:
	ImageBuffer(short width, short height, unsigned char bpp) :Texture(width, height, bpp){};
};

#endif