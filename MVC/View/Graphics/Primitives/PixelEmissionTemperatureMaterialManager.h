#ifndef PIXEL_EMISSION_TEMPERATURE_MATERIAL_MANAGER_H
#define PIXEL_EMISSION_TEMPERATURE_MATERIAL_MANAGER_H
#include <cstdint>
#include "../Texture.h"
#include <string>
#include "SpectrumProfile.h"
#include "ElementType.h"

// this is actually just a texture
// a 64 bit texture
// 32bits is intensity of each channel [10 nm from 370nm[uv] (350)(violet) to 770 nm[infared] with 8 byte blending weight per 10 nm channel yielding 40 bytes of information
// with 64k rows
// with multiple columns to represent the respective states
// good reference to compare results https://physics.info/planck/
class PhotonEmissionTemperatureMaterialManager : Texture
{
public:
	PhotonEmissionTemperatureMaterialManager() :Texture(Pixel(sizeof(mvc::view::rendering::SpectrumProfile)), Pixel(mvc::view::rendering::numberOfTotalSpectrum), 32)
	{
		std::string filename("PhotonEmissionTemperatureManager");
		std::string filename_with_extension = getFileNameExtension(filename);
		if (!file_exists(filename_with_extension))
		{
			Save(filename);
		}
		Load(filename);
	}
};

#endif