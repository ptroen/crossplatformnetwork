#ifndef MATERIAL_SPECTRUM_POOL_H
#define MATERIAL_SPECTRUM_POOL_H
#include "PhotonType.h"
#include "TexturePixel32bit.h"
#include "Texture.h"
class MaterialSpectrumPool : Texture
{
public:
	private const size_t maxSpectrum = 65536;
};

#endif