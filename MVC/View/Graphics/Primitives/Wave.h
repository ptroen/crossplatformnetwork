#ifndef WAVE_H
#define WAVE_H

#include <math.h>       /* log2 */
#include <limits>
#include <array>
#include <vector>
#include "WaveGroups.h"
#include "RGBBlending.h"

namespace mvc
{
	namespace view
	{
		namespace rendering
		{
			long double operator "" _angstrom(long double unprocessed);
			long double operator "" _nm(long double unprocessed);
			class WaveRGBBlending;

			class WaveRGBBlending
			{
				// internal representation of the spectrum
				WaveGroups _waveGroups;

				static bool runCorrection;
				static TexturePixel32bit whiteCorrection;

			public:
				bool usesWhiteCorrection;
				WaveRGBBlending& operator /=(size_t d)
				{
					_waveGroups /= d;
					return *this;
				};


				WaveRGBBlending& operator/(size_t d)
				{
					return *this / d;
				};

				WaveRGBBlending& operator *=(size_t d)
				{
					_waveGroups *= d;
					return *this;
				};
				WaveRGBBlending& operator *(size_t d)
				{
					return *this * d;
				};

				WaveRGBBlending& operator *(double d)
				{
					if(d<1.0)
					{
						double inverse = 1 / d;
						size_t i = static_cast<size_t>(d);
						_waveGroups /= i;
					}
					else
					{
						size_t i = static_cast<size_t> (d);
						_waveGroups *= i;
					}
					
					return *this;
				};

				WaveRGBBlending& operator *(WaveRGBBlending&  rhs)
				{
					_waveGroups  * rhs._waveGroups;
					return *this;
				};

				WaveRGBBlending& operator+(WaveRGBBlending& rhs)
				{
					_waveGroups + rhs._waveGroups;
					return *this;
				};


				WaveRGBBlending();

				void SetWaves(std::vector<long double> wavelengths);
				void SetWavesAngstrom(std::vector<long double> wavelengthsthatAreInAngstroms);
				void SetWave(double wavelength);

				// Gets the blending of color
				TexturePixel32bit GetSpectraColor(void);

				// interface
				operator TexturePixel32bit() { return GetSpectraColor(); };
			protected:
				RGBBlending  getColorFromWaveLength(double Wavelength);
				RGBColorBuckets GetSpectumColorChanelValueFromWavelength(double Wavelength);

			private:
				TexturePixel32bit makeColorCorrection(void);
			};
		}
	}
}
#endif