#ifndef WAVE_GROUPS_H
#define WAVE_GROUPS_H
#include "TexturePixel32bit.h"
#include <bitset>
#include <cstdint>
#include <array>

namespace mvc
{
	namespace view
	{
		namespace rendering
		{

			class bitset64
			{
				// https://softwareengineering.stackexchange.com/questions/284160/is-there-any-advantage-to-c-style-bit-manipulation-over-stdbitset
				uint64_t mArray;
			public:
				
				void set(size_t n, bool setter)
				{
					mArray |= (setter ? 1LL : 0LL)  << n;
				}

				bitset64& operator/=(uint64_t n) { mArray /= n; return *this; };

				bitset64& operator*=(uint64_t n) { mArray *= n; return *this; };
				bitset64& operator |= (bitset64& rhs)
				{
					mArray |= rhs.mArray;
					return *this;
				};

				bitset64& operator &= (bitset64& rhs)
				{
					mArray &= rhs.mArray;
					return *this;
				};


				bool test(size_t n)
				{
					return mArray & (1LL << n);
				};


				bool GetBit(size_t n) { return mArray << n; };
					uint64_t GetBits() { return mArray; };
			};

			// calculates the blending slope of the color
			struct RGBColorBuckets
			{
				double Blue;
				double Green;
				double Red;
				RGBColorBuckets() :Blue(0.0), Green(0.0), Red(0.0) {};
			};

			struct ColorCount
			{
				RGBColorBuckets color;
				RGBColorBuckets count;

				ColorCount() :count(), color() {};
			};

			class SpectrumWaveLengths
			{
			public:
#define memoryWavelengthSegmentsLimit 64



				// with bitset intensity becomes obsolete as the edge of each segment is full color. Hence blending of each channel becomes the prefered way
				//std::bitset <memoryWavelengthSegmentsLimit > waves;
				bitset64 waves;

				SpectrumWaveLengths& operator |= (SpectrumWaveLengths& rhs)
				{
					waves |= rhs.waves;
					return *this;
				};

				SpectrumWaveLengths& operator &= (SpectrumWaveLengths& rhs)
				{
					waves &= rhs.waves;
					return *this;
				};


				SpectrumWaveLengths& operator/=(size_t d)
				{
					waves /= d;
					return *this;
				};
				SpectrumWaveLengths& operator*=(size_t d)
				{
					waves *= d;
					return *this;
				};

			};

			class WaveGroups;

			class WaveGroups
			{
			public:
				struct ColorChannelDeltas
				{
					double startingWavelength;
					RGBColorBuckets deltaToNextWaveLength;
				};
#define numberOfWaveSegments 6

				// set the bitset based on the wave length
				void setColorFromWaveLength(double wavelength);

				WaveGroups& operator/=(size_t d)
				{
					for(auto element : _waveGroups)
					{
						element /= d;
					}
					return *this;
				};

				WaveGroups& operator *=(size_t d)
				{
					for (auto element : _waveGroups)
					{
						element *= d;
					}
					return *this;
				};

				WaveGroups& operator+(WaveGroups& rhs)
				{
					for (size_t i =0; i< _waveGroups.size(); i++)
					{
						_waveGroups[i] |= rhs._waveGroups[i];
					}
					return *this;
				};

				WaveGroups& operator *(WaveGroups&  rhs)
				{
					for (size_t i = 0; i < _waveGroups.size(); i++)
					{
						_waveGroups[i] &= rhs._waveGroups[i];
					}
						return *this;
				}

				std::array < SpectrumWaveLengths, numberOfWaveSegments> _waveGroups;
#undef numberOfWaveSegments
				// loop thru each and compute the average color for the wavelength;
				void GetSpectraColor(ColorCount& colorCount);
				TexturePixel32bit GetSpectraColor(void);
			private:

				void GetSpectraFushisaColor(ColorCount& colorCount);
				void GetSpectraColorBlueColor(ColorCount& colorCount);
				void GetSpectraTealColor(ColorCount& colorCount);
				void GetSpectraGreenColor(ColorCount& colorCount);
				void GetSpectraYellowColor(ColorCount& colorCount);
				void GetSpectraRedColor(ColorCount& colorCount);
			};
		}
	}
}
#endif