#ifndef PIXEL_H
#define PIXEL_H
#include <cstdint>

class Pixel
{
public:
	uint32_t _pixel;
	Pixel(const uint32_t pix) { _pixel = pix; };
};

Pixel operator"" _pixel(unsigned long long px);
#endif