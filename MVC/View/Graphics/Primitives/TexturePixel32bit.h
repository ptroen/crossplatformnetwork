#ifndef TexturePIXEL_H
#define TexturePIXEL_H
#include "PixelRGBA.h"

union TexturePixel32bit
{
	TexturePixel32bit() {};
	PixelRGBA rgba;

	TexturePixel32bit& operator+=(TexturePixel32bit& rhs) { rgba += rhs.rgba; return *this; };

};

#endif