﻿#ifndef MODEL_H
#define MODEL_H
#include <Eigen/Core>


#include "../RayTracer/PixelemissiontemperaturematerialTexture.h"
/*
 *Represesnts a model to render
 *
 */

namespace mvc {
	namespace view {
		namespace ThreeDimensional {
			class Ray;
		}
	}
}

class ModelCube
{
	const static size_t numberOfFacesInCube = 6;
	std::vector<Surface3d> surfaceTextures; //  0 top(z-) , 5 bottom(z+( , 1 left face(y-) , 4 right(y+) face ,2 back(x-), 3 front(x+)
	Eigen::Vector3i centre;
	float radius;

public:

	ModelCube(void) :surfaceTextures(numberOfFacesInCube) {};
	Eigen::Vector3i PointWhereObjectWasHit(mvc::view::ThreeDimensional::Ray& ray) {};
	Eigen::Vector3i NormalWhereObjectWasHit(mvc::view::ThreeDimensional::Ray& ray);

	// reference of obj file format http://paulbourke.net/dataformats/obj/minobj.html
	void LoadFromObjFile()
	{
		
	};

	Eigen::Vector3i  _positionInWorldSpace; /// position of the sphere
	
	bool hitSomethingOnTheModel(mvc::view::ThreeDimensional::Ray& ray, SurfaceVoxel& out_pixel)
	{
		
		for (Surface3d surface3d : surfaceTextures)
		{
			if (surface3d.doesRayIntersectTheSurface( ray, out_pixel))
				return true;
	  }

		return false;
	};

	


};

#endif