#ifndef TEMPERATURE_H
#define TEMPERATURE_H
#include <stdint.h>
class Temperature
{
public:
	uint32_t _temperatureInCentiKelvins;
	Temperature(uint32_t t = 0) :_temperatureInCentiKelvins(t) {};
	uint32_t GetTemperature() { return _temperatureInCentiKelvins * 100; };
};

Temperature operator"" _Kelvin(unsigned long long temperature);
Temperature operator"" _Celsius(unsigned long long temperature);
#endif