#ifndef RGB_BLENDING_H
#define RGB_BLENDING_H

struct RGBBlending
{
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	uint16_t intensity;
	uint32_t blendingWeight;

	// need to apply painters algorithm to blend properly
	RGBBlending& operator +=(RGBBlending& rhs) {
		red += rhs.red;
		green += rhs.green;
		blue += rhs.blue;
		intensity += rhs.intensity;
		blendingWeight += rhs.blendingWeight;
		return *this;
	};

	RGBBlending() :red(0), green(0), blue(0), intensity(0), blendingWeight(0) {}
};

#endif