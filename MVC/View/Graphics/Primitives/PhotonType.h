#ifndef PHOTON_TYPE_H
#define PHOTON_TYPE_H

#define PHOTOTYPE_SIZE 8
#include <cstdint>
#include "../RayTracer/pixelemissiontemperaturematerial.h"
#include "TexturePixel32bit.h"
class PhotonType
{
	PixelEmissionTemperatureMaterial _photonType[PHOTOTYPE_SIZE];
	float fresnal_photon_weight[PHOTOTYPE_SIZE];
public:

	PhotonType()
	{
		memset(this, 0, sizeof(PhotonType));
	};


	void ToTexturePixel32bit(TexturePixel32bit& out_texture_pixel32_bit)
	{


	};

	void SetPixelEmissionColor(PixelEmissionTemperatureMaterial& pixel_emission_temperature_material)
	{
		for (int i = 0; i < PHOTOTYPE_SIZE; i++)
		{
			if (_photonType[i].Zero())
			{
				_photonType[i] = pixel_emission_temperature_material;
				return;
			}
		}
	};

	// decreases the overall transmission strength because the surface is usually not perpendicular to the viewer hence we use 
	// the angle of the normal
	void SetAllPhotonBlendingWeightByTheAngleOfTheNormalToTheSurface(float normal)
	{
		for (int i = 0; i < PHOTOTYPE_SIZE; i++)
		{
			fresnal_photon_weight[i] *= normal;
		}
	};

	void SetCurrentPhotonFresnalWeight(float weight)
	{
		for (int i = 0; i < PHOTOTYPE_SIZE; i++)
		{
			uint16_t currentElement = static_cast<uint16_t>(_photonType[i].EmmissionType);
			if (currentElement == 0)
			{
				fresnal_photon_weight[i] = weight;
				return;
			}
		}
	};

	const PhotonType& operator+=(const PhotonType& rhs) {

		//	this->m_iNumber += rhs.m_iNumber;
		return *this;
	}
};

#endif