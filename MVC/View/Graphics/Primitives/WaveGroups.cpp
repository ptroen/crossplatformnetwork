#include "WaveGroups.h"
using namespace mvc::view::rendering;
TexturePixel32bit WaveGroups::GetSpectraColor(void)
{
	// each color channel has independent intensity
	TexturePixel32bit rgb;
	ColorCount colorCount;
	GetSpectraColor(colorCount);

	// turn it into one channel
	colorCount.color.Red /= (colorCount.count.Red <= 0.0 ? 1.0 : colorCount.count.Red);
	colorCount.color.Red *= 255;
	colorCount.color.Green /= (colorCount.count.Green <= 0.0 ? 1.0 : colorCount.count.Green);
	colorCount.color.Green *= 255;
	colorCount.color.Blue /= (colorCount.count.Blue <= 0.0 ? 1.0 : colorCount.count.Blue);
	colorCount.color.Blue *= 255;

	TexturePixel32bit rgba;
	rgba.rgba.blue = colorCount.color.Blue;
	rgba.rgba.green = colorCount.color.Green;
	rgba.rgba.red = colorCount.color.Red;
	return rgba;
}

void WaveGroups::GetSpectraColor(ColorCount& colorCount)
{
	GetSpectraFushisaColor(colorCount);
	GetSpectraColorBlueColor(colorCount);
	GetSpectraTealColor(colorCount);
	GetSpectraGreenColor(colorCount);
	GetSpectraYellowColor(colorCount);
	GetSpectraRedColor(colorCount);
}

// set the bitset based on the wave length
void WaveGroups::setColorFromWaveLength(double wavelength)
{
	// 350 && Wavelength <= 439
	if (wavelength < 440.0)
	{
		const double denom = 440.0;
		int lookup = static_cast<int>(((wavelength - 350.00000000000000) / denom) * 64.000000000000000);
		if (lookup != 64)
			_waveGroups[0].waves.set(lookup, true);
		else
			_waveGroups[0].waves.set(63, true);
	}
	// (Wavelength >= 440 && Wavelength < 490)
	else if (wavelength < 490.0)
	{
		const double denom = 490.0;
		int lookup = static_cast<int>(((wavelength - 440.00000000000000) / denom) * 64.0);
		if (lookup != 64)
			_waveGroups[1].waves.set(lookup, true);
		else
			_waveGroups[1].waves.set(63, true);
	}
	// Wavelength >= 490.0 && Wavelength < 510
	else if (wavelength < 510.0)
	{
		const double denom = 510.0;
		int lookup = static_cast<int>(((wavelength - 490.000000000000000) / denom) * 64.0);
		if (lookup != 64)
			_waveGroups[2].waves.set(lookup, true);
		else
			_waveGroups[2].waves.set(63, true);
	}
	// Wavelength >= 510 && Wavelength < 580
	else if (wavelength < 580.0)
	{
		const double denom = 580.0;
		int lookup = static_cast<int>(((wavelength - 510.00000000000000) / denom) * 64.0);
		if (lookup != 64)
			_waveGroups[3].waves.set(lookup, true);
		else
			_waveGroups[3].waves.set(63, true);
	}
	// // Wavelength >= 580 && Wavelength < 645
	else if (wavelength < 645.0)
	{
		const double denom = 645.0;
		int lookup = static_cast<int>(((wavelength - 580.00000000000000) / denom) * 64.0);
		if (lookup != 64)
			_waveGroups[4].waves.set(lookup, true);
		else
			_waveGroups[4].waves.set(63, true);
	}
	//  -(Wavelength - 780) / (780 - 645);
	else if (wavelength < 780)
	{
		const double denom = 780.0;
		int lookup = static_cast<int>(((wavelength - 645.0) / denom) * 64.0);
		if (lookup != 64)
			_waveGroups[5].waves.set(lookup, true);
		else
			_waveGroups[5].waves.set(63, true);
	}
}

void WaveGroups::GetSpectraFushisaColor(ColorCount& colorCount)
{
	// 350 && Wavelength <= 439

	// testing 350 nm
	colorCount.count.Red += _waveGroups[0].waves.test(0) ? (1) : 0; // rgb at 350 nm
	colorCount.color.Red += _waveGroups[0].waves.test(0) ? (1) : 0;
	colorCount.count.Blue += _waveGroups[0].waves.test(0) ? (1) : 0;
	colorCount.color.Blue += _waveGroups[0].waves.test(0) ? (1) : 0;
	double Wavelength = 350;
	const double denom = 440.0 - 350.0;
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		colorCount.color.Red += _waveGroups[0].waves.test(i) ? -((Wavelength - 440.0) / denom) : 0.0;
		colorCount.count.Red += _waveGroups[0].waves.test(i) ? (1.0) : 0.0;
		colorCount.count.Blue += _waveGroups[0].waves.test(i) ? (1.0) : 0.0;
		colorCount.color.Blue += _waveGroups[0].waves.test(i) ? (1.0) : 0.0;
		Wavelength += slope;
	}
}

// Wavelength >= 440 && Wavelength <= 489
void WaveGroups::GetSpectraColorBlueColor(ColorCount& colorCount)
{
	/*
	 * if (Wavelength >= 440 && Wavelength < 490) { // 0, 0,1 at edge
	bucket.Red = 0.0;
	bucket.Green = (Wavelength - 440.0) / (490.0 - 440.0);
	bucket.Blue = 1.0;
	 */
	colorCount.count.Green += _waveGroups[1].waves.test(0) ? (0) : 0; //  at 440 nm
	colorCount.color.Green += _waveGroups[1].waves.test(0) ? (0) : 0;
	colorCount.count.Blue += _waveGroups[1].waves.test(0) ? (1) : 0;
	colorCount.color.Blue += _waveGroups[1].waves.test(0) ? (1) : 0;
	double Wavelength = 440;
	const double denom = 490.0 - 440.0;
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		colorCount.color.Green += _waveGroups[1].waves.test(i) ? (Wavelength - 440.0) / denom : (0.0);
		colorCount.count.Green += _waveGroups[1].waves.test(i) ? (1.0) : 0.0;
		colorCount.count.Blue += _waveGroups[1].waves.test(i) ? (1.0) : 0.0;
		colorCount.color.Blue += _waveGroups[1].waves.test(i) ? (1.0) : 0.0;
		Wavelength += slope;
	}
}

// Wavelength >= 490.0 && Wavelength <= 509
void WaveGroups::GetSpectraTealColor(ColorCount& colorCount)
{

	/*
	 *else if (Wavelength >= 490.0 && Wavelength < 510) { // 0, 1,1 at edge
	bucket.Red = 0.0;
	bucket.Green = 1.0;
	bucket.Blue = -(Wavelength - 510.0) / (510.0 - 490.0);
	 */
	colorCount.color.Blue += _waveGroups[2].waves.test(0) ? (1) : 0; //  at 490 nm
	colorCount.count.Blue += _waveGroups[2].waves.test(0) ? (1) : 0;
	colorCount.count.Green += _waveGroups[2].waves.test(0) ? (1) : 0;
	colorCount.color.Green += _waveGroups[2].waves.test(0) ? (1) : 0;
	double Wavelength = 490;
	const double denom = 510.0 - 490.0;
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		bool test = _waveGroups[2].waves.test(i);
		auto eval = -((Wavelength - 510.0) / denom);
		colorCount.color.Blue += test ? eval : 0.0;
		colorCount.count.Blue += test ? (1.0) : 0.0;
		colorCount.count.Green += test ? (1.0) : 0.0;
		colorCount.color.Green += test ? (1.0) : 0.0;
		Wavelength += slope;
	}
}

// Wavelength >= 510 && Wavelength <= 579
void WaveGroups::GetSpectraGreenColor(ColorCount& colorCount)
{
	/*
	 * 	else if (Wavelength >= 510 && Wavelength < 580) { // 0,1,0 at edge
	bucket.Red = (Wavelength - 510.0) / (580.0 - 510.0);
	bucket.Green = 1.0;
	bucket.Blue = 0.0;
	 */
	 //colorCount.count.Red += _waveGroups[3].waves.test(0) ? (0) : 0; //  at 510 nm
	 //colorCount.color.Red += _waveGroups[3].waves.test(0) ? (0) : 0;
	colorCount.count.Green += _waveGroups[3].waves.test(0) ? (1) : 0;
	colorCount.color.Green += _waveGroups[3].waves.test(0) ? (1) : 0;
	double Wavelength = 510.0;
	const double denom = (580.0 - 510.0);
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		colorCount.color.Red += _waveGroups[3].waves.test(i) ? ((Wavelength - 510.0) / denom) : 0.0;
		colorCount.count.Red += _waveGroups[3].waves.test(i) ? (1.0) : 0.0;
		colorCount.count.Green += _waveGroups[3].waves.test(i) ? (1.0) : 0.0;
		colorCount.color.Green += _waveGroups[3].waves.test(i) ? (1.0) : 0.0;
		Wavelength += slope;
	}
}

// Wavelength >= 580 && Wavelength <= 644
void WaveGroups::GetSpectraYellowColor(ColorCount& colorCount)
{
	/*
	 *	else if (Wavelength >= 580 && Wavelength < 645) { // 1,1,0 at edge
	bucket.Red = 1.0;
	bucket.Green = (645 - Wavelength) / (645.0 - 580.0);
	bucket.Blue = 0.0;
	 */
	colorCount.count.Green += _waveGroups[4].waves.test(0) ? (1) : 0; //  at 580 nm
	colorCount.color.Green += _waveGroups[4].waves.test(0) ? (1) : 0;
	colorCount.count.Red += _waveGroups[4].waves.test(0) ? (1) : 0;
	colorCount.color.Red += _waveGroups[4].waves.test(0) ? (1) : 0;
	double Wavelength = 580.0;
	const double denom = (645.0 - 580.0);
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		colorCount.color.Green += _waveGroups[4].waves.test(i) ? ((645 - Wavelength) / denom) : 0.0;
		colorCount.count.Green += _waveGroups[4].waves.test(i) ? (1.0) : 0.0;
		colorCount.count.Red += _waveGroups[4].waves.test(i) ? (1.0) : 0.0;
		colorCount.color.Red += _waveGroups[4].waves.test(i) ? (1.0) : 0.0;
		Wavelength += slope;
	}
}

// Wavelength >= 645 && Wavelength <= 780
void WaveGroups::GetSpectraRedColor(ColorCount& colorCount)
{
	/*
		else if (Wavelength >= 645 && Wavelength <= 780) { // 1,0 at edge
			bucket.Red = -(Wavelength - 780) / (780 - 645);
		}
	*/
	colorCount.count.Red += _waveGroups[5].waves.test(0) ? (1) : 0; //  at 645 nm
	colorCount.color.Red += _waveGroups[5].waves.test(0) ? (1) : 0;
	double Wavelength = 645.0;
	const double denom = (780 - 645);
	const double slope = denom / 64;

	for (size_t i = 1; i < memoryWavelengthSegmentsLimit; i++)
	{
		colorCount.color.Red += _waveGroups[5].waves.test(i) ? (-((Wavelength - 780) / denom)) : 0.0;
		colorCount.count.Red += _waveGroups[5].waves.test(i) ? (1.0) : 0.0;
		Wavelength += slope;
	}
};
