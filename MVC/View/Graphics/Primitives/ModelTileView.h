#ifndef MODEL_TILE_VIEW_H
#define MODEL_TILE_VIEW_H
#include "ModelCube.h"
// a view of the Model Tile to the viewer

class ModelTileView
{
	public:
	
		std::array<std::array<ModelCube,16>,16>+ _modelTiles;
};

#endif