#include "ElementType.h"
using namespace mvc::view::rendering;

size_t ElementType::_to_index()
{
	return _ElementGroups.value;
}
void ElementType::_from_index(size_t i)
{
	_ElementGroups.value = i;
	if (_ElementGroups.value <= static_cast<size_t>(PrimarySpectrumColorsElementType::YellowLight)) // r 1
		this->_PeriodicTableType = PeriodicTableType::SpectrumColors;
	else if (_ElementGroups.value <= static_cast<size_t>(Row1ElementType::HeliumII)) // r 1
		this->_PeriodicTableType = PeriodicTableType::Row1;
	else if (_ElementGroups.value <= static_cast<size_t>(Row2ElementType::NeonII)) // r 1
		this->_PeriodicTableType = PeriodicTableType::Row2;
	else if (_ElementGroups.value <= static_cast<size_t>(Row3ElementType::ArgonII)) // r2
		this->_PeriodicTableType = PeriodicTableType::Row3;
	else if (_ElementGroups.value <= static_cast<size_t>(Row4ElementType::KryptonII))
		this->_PeriodicTableType = PeriodicTableType::Row4;
	else if (_ElementGroups.value <= static_cast<size_t>(Row5ElementType::XenonII))
		this->_PeriodicTableType = PeriodicTableType::Row5;
	else if (_ElementGroups.value <= static_cast<size_t>(Row6ElementType::BariumII))
		this->_PeriodicTableType = PeriodicTableType::Row6;
	else if (_ElementGroups.value <= static_cast<size_t>(Row6LanthanoidsElementType::LutetiumII))
		this->_PeriodicTableType = PeriodicTableType::Row6Lanthanoids;
	else if (_ElementGroups.value <= static_cast<size_t>(Row6AfterLanthanoidsElementType::RadonI))
		this->_PeriodicTableType = PeriodicTableType::Row6AfterLanthanoids;
	else if (_ElementGroups.value <= static_cast<size_t>(Row7ElementType::RadiumII))
		this->_PeriodicTableType = PeriodicTableType::Row7ElementType;
	else if (_ElementGroups.value <= static_cast<size_t>(Row7AuctinoidsElementType::LawrenciumII))
		this->_PeriodicTableType = PeriodicTableType::Row7Auctinoids;
	else if (_ElementGroups.value <= static_cast<size_t>(Row7AfterAuctinoidsElementType::OganessonII))
		this->_PeriodicTableType = PeriodicTableType::Row7AfterAuctinoids;
	else if (_ElementGroups.value <= static_cast<size_t>(OrganicGreensElementType::TreeBark))
		this->_PeriodicTableType = PeriodicTableType::OrganicGreens;
};

std::string ElementType::_to_string()
{
	switch (this->_PeriodicTableType)
	{
	case PeriodicTableType::Row1:
		return _ElementGroups._Row1ElementType._to_string();
	case PeriodicTableType::Row2:
		return _ElementGroups._Row2ElementType._to_string();
	case PeriodicTableType::Row3:
		return _ElementGroups._Row3ElementType._to_string();
	case PeriodicTableType::Row4:
		return _ElementGroups._Row4ElementType._to_string();
	case	PeriodicTableType::Row5:
		return _ElementGroups._Row5ElementType._to_string();
	case PeriodicTableType::Row6:
		return _ElementGroups._Row6ElementType._to_string();
	case PeriodicTableType::Row6Lanthanoids:
		return _ElementGroups._Row6LanthanoidsElementType._to_string();
	case PeriodicTableType::Row6AfterLanthanoids:
		return _ElementGroups._Row6AfterLanthanoidsElementType._to_string();
	case PeriodicTableType::Row7ElementType:
		return _ElementGroups._Row7ElementType._to_string();
	case PeriodicTableType::Row7Auctinoids:
		return _ElementGroups._Row7AuctinoidsElementType._to_string();
	case PeriodicTableType::Row7AfterAuctinoids:
		return _ElementGroups._Row7AfterAuctinoidsElementType._to_string();
	case PeriodicTableType::OrganicGreens:
		return _ElementGroups._OrganicGreensElementType._to_string();
	case PeriodicTableType::SpectrumColors:
		return _ElementGroups._PrimarySpectrumColorsElementType._to_string();
	default: return _to_string();
	}
};