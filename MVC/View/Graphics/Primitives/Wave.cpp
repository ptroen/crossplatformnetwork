#include "Wave.h"
#include <vector>
using namespace std;
using namespace mvc::view::rendering;
TexturePixel32bit WaveRGBBlending::whiteCorrection;
bool WaveRGBBlending::runCorrection = false;

long double mvc::view::rendering::operator "" _angstrom(long double unprocessed)
{
	return unprocessed /= 10;
}
long double mvc::view::rendering::operator "" _nm(long double unprocessed)
{
	return unprocessed;
}

RGBColorBuckets WaveRGBBlending::GetSpectumColorChanelValueFromWavelength(double Wavelength)
{
	RGBColorBuckets bucket;
	if (Wavelength >= 350 && Wavelength < 440) { // 1,0,1 at edge
		bucket.Red = -(Wavelength - 440.0) / (440.0 - 350.0);
		bucket.Green = 0.0;
		bucket.Blue = 1.0;
	}
	else if (Wavelength >= 440 && Wavelength < 490) { // 0, 0,1 at edge
		bucket.Red = 0.0;
		bucket.Green = (Wavelength - 440.0) / (490.0 - 440.0);
		bucket.Blue = 1.0;
	}
	else if (Wavelength >= 490.0 && Wavelength < 510) { // 0, 1,1 at edge
		bucket.Red = 0.0;
		bucket.Green = 1.0;
		bucket.Blue = -(Wavelength - 510.0) / (510.0 - 490.0);
	}
	else if (Wavelength >= 510 && Wavelength < 580) { // 0,1,0 at edge
		bucket.Red = (Wavelength - 510.0) / (580.0 - 510.0);
		bucket.Green = 1.0;
		bucket.Blue = 0.0;
	}
	else if (Wavelength >= 580 && Wavelength < 645) { // 1,1,0 at edge
		bucket.Red = 1.0;
		bucket.Green = (645 - Wavelength) / (645.0 - 580.0);
		bucket.Blue = 0.0;
	}
	else if (Wavelength >= 645 && Wavelength <= 780) { // 1,0 at edge
		bucket.Red = -(Wavelength - 780) / (780 - 645);
		bucket.Green = 0.0;
		bucket.Blue = 0.0;
	}
	else
	{
		bucket.Red = 0.0;
		bucket.Green = 0.0;
		bucket.Blue = 0.0;
	}
	return bucket;
};

void WaveRGBBlending::SetWavesAngstrom(std::vector<long double> wavelengthsthatAreInAngstroms)
{
	for (long double wavelength : wavelengthsthatAreInAngstroms)
	{
		SetWave(static_cast<double>(wavelength / 10));
	}
}


void WaveRGBBlending::SetWaves(vector<long double> wavelengths)
{
	// in nm
	for (long double wavelength : wavelengths)
	{
		SetWave(static_cast<double>(wavelength));
	}
}

void WaveRGBBlending::SetWave(double wavelength)
{
	if (wavelength < 350.0) return;
	if (wavelength >= 780.0) return;
	_waveGroups.setColorFromWaveLength(wavelength);
}

// builds the internal white correction to adjust
TexturePixel32bit WaveRGBBlending::makeColorCorrection(void)
{
	RGBBlending total;
	double limit = 1;
	// determine
	// l
	for (double i = 0; i < limit; i += 1.0)
	{
		double Wavelength;
		// 350 && Wavelength <= 439
		Wavelength = 350 + (89 / limit * i);
		auto a = getColorFromWaveLength(Wavelength);
		total += a;

		// Wavelength >= 440 && Wavelength <= 489
		Wavelength = 440 + (49 / limit * i);
		auto b = getColorFromWaveLength(Wavelength);
		total += b;

		// Wavelength >= 490.0 && Wavelength <= 509
		Wavelength = 490 + (19 / limit * i);
		auto c = getColorFromWaveLength(Wavelength);
		total += c;
		// Wavelength >= 510 && Wavelength <= 579
		Wavelength = 510 + (69 / limit * i);
		auto d = getColorFromWaveLength(Wavelength);
		total += d;
		// Wavelength >= 580 && Wavelength <= 644
		Wavelength = 580 + (64 / limit * i);
		auto e = getColorFromWaveLength(Wavelength);
		total += e;
		// Wavelength >= 645 && Wavelength <= 780
		Wavelength = 645 + (135 / limit * i);
		auto f = getColorFromWaveLength(Wavelength);
		total += f;
	}

	whiteCorrection.rgba.green = (numeric_limits<uint8_t>::max)() - total.green;
	whiteCorrection.rgba.blue = (numeric_limits<uint8_t>::max)() - total.blue;
	whiteCorrection.rgba.red = (numeric_limits<uint8_t>::max)() - total.red;
	return whiteCorrection;
}

// used to obtain the color from the wave length. This is a utility function
RGBBlending  WaveRGBBlending::getColorFromWaveLength(double Wavelength)
{
	RGBColorBuckets bucket = GetSpectumColorChanelValueFromWavelength(Wavelength);
	const uint32_t  colorChannelResolution = 255;
	RGBBlending rgba;
	rgba.red =(bucket.Red * colorChannelResolution);
	rgba.green = (bucket.Green * colorChannelResolution);
	rgba.blue = (bucket.Blue * colorChannelResolution);
	return rgba;
}

WaveRGBBlending::WaveRGBBlending()
{
	if (!runCorrection)
	{
		runCorrection = true;
		whiteCorrection = makeColorCorrection();
	}
}

TexturePixel32bit WaveRGBBlending::GetSpectraColor()
{
	return _waveGroups.GetSpectraColor();
}
