#ifndef Texture_H
#define Texture_H
#include "ITexture.h"

#include <iostream>
#include <stdio.h>
#include "Primitives/Pixel.h"
class Texture;

class Texture : public ITexture
{
public:

	Texture(Pixel width, Pixel height, uint32_t bpp) :ITexture(width._pixel, height._pixel, bpp) {}


	virtual void Clear(void)
	{
		memset(_buffer, 0, _bpp*_width* _height);

	};

	void virtual Load(std::string& filename)
	{
		FILE *tga;               // Pointer to a FILE
		targa_header header;     // Variable of targa_header type
		errno_t err;
		std::string resultant = getFileNameExtension(filename);
		err = fopen_s(&tga, resultant.c_str(), "rb"); /* GenerateMap the header information  */

		if (err != 0) return;

		fread(&header, 1, sizeof(targa_header), tga);
		fread(&_buffer[0], sizeof(TexturePixel32bit), _width*_height, tga);
		fclose(tga);
	};

	void virtual Save(std::string& filename)
	{
		FILE *tga;               // Pointer to a FILE
		targa_header header;     // Variable of targa_header type
		errno_t err;
		/* First, set all the fields in the header to appropriate values */
		header.id_len = 0;          /* no ID field */
		header.map_type = 0;        /* no colormap */
		header.img_type = 2;        /* trust me */
		header.map_first = 0;       /* not used */
		header.map_len = 0;         /* not used */
		header.map_entry_size = 0;  /* not used */
		header.x = 0;               /* image starts at (0,0) */
		header.y = 0;
		header.width = _width;
		header.height = _height;
		header.bpp = 32;
		header.misc = 0x20;         /* scan from upper left corner */
		std::string resultant = getFileNameExtension(filename);
		err = fopen_s(&tga, resultant.c_str(), "wb"); /* GenerateMap the header information  */
		if (err != 0) return;

		writeheader(header, tga);

		// writing the whole image
		for (uint32_t y = 0; y < _height; y++)
		{
			for (uint32_t x = 0; x < _width; x++)
			{
				fputc(_buffer[(y*_width) + x].rgba.blue, tga);
				fputc(_buffer[(y*_width) + x].rgba.green, tga);
				fputc(_buffer[(y*_width) + x].rgba.red, tga);
				fputc(_buffer[(y*_width) + x].rgba.alpha, tga);
			}
		}

		/* close the file */
		fclose(tga);
	};

	/*
	Same Function to generate map	This is only a demo function
	*/
	void GenerateMap()
	{
		static int democolor = 128;
		++democolor;
		if (democolor > 255)
			democolor = 1;
		for (uint32_t y = 0; y < _height; y++)
			for (uint32_t x = 0; x < _width; x++)
			{
				_buffer[(y*_width) + x].rgba.blue = x % democolor;
				_buffer[(y*_width) + x].rgba.green = 0;
				_buffer[(y*_width) + x].rgba.red = y % democolor;
				_buffer[(y*_width) + x].rgba.alpha = 255;

			}
	};
	/* This function writes the header information to the file.  The function
	has some unusual manipulations because integers in a targa file are two
	bytes long and characters are only one byte long.  Because Targa files
	need to be read by machines of different architectures, arithmetic must
	be done to write the low order bytes first using MOD (%), followed by
	the high order bytes using DIV (/) for any integers.  */

	void writeheader(targa_header h, FILE *tga)
	{
		fputc(h.id_len, tga);          // GenerateMap chars for ID, map, and image type
		fputc(h.map_type, tga);
		fputc(h.img_type, tga);
		fputc(h.map_first % 256, tga); // GenerateMap integer, low order byte first
		fputc(h.map_first / 256, tga); // GenerateMap second byte of integer, high order
		fputc(h.map_len % 256, tga);   // Another integer 
		fputc(h.map_len / 256, tga);
		fputc(h.map_entry_size, tga);  // GenerateMap a char - only one byte
		fputc(h.x % 256, tga);         // More integers
		fputc(h.x / 256, tga);
		fputc(h.y % 256, tga);
		fputc(h.y / 256, tga);
		fputc(h.width % 256, tga);     // Even more integers
		fputc(h.width / 256, tga);
		fputc(h.height % 256, tga);
		fputc(h.height / 256, tga);
		fputc(h.bpp, tga);             // GenerateMap two chars
		fputc(h.misc, tga);
	};

protected:
	std::string getFileNameExtension(std::string& filename)
	{
		std::string extension = filename + ".tga\0";
		return  extension;
	};


};


#endif