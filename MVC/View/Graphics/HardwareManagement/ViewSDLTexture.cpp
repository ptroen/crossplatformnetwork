#include "ViewSDLTexture.h"
#include "../../../Controller/BootLoader/SDLDevicePointers.h"
#include <SDL.h>
#include <stdio.h>
#include <string>

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_image.lib")

SDL_Texture* ViewSDLTexture::_SDLTexture = NULL;

ViewSDLTexture::ViewSDLTexture(int width, int height)
{
	//Initialize
	_SDLTexture = NULL;
	_width = width;
	_height = height;
	mPixels = nullptr;
	_pitch = 0;
}

ViewSDLTexture::~ViewSDLTexture(void)
{
	//Free texture if it exists
	if (_SDLTexture != NULL)
	{
		SDL_DestroyTexture(_SDLTexture);
		_SDLTexture = NULL;
		_width = 0;
		_height = 0;
		mPixels = NULL;
		_pitch = 0;
	}
}
		
void ViewSDLTexture::createBlank(void)
{
	//Create uninitialized texture
	_SDLTexture = SDL_CreateTexture(SDL_Device_Pointers::_SDLRenderer, 
																SDL_PIXELFORMAT_ARGB8888,
															   SDL_TEXTUREACCESS_STREAMING, 
															   _width, 
															   _height);
	if(_SDLTexture == NULL )
	{
		throw new std::exception( SDL_GetError() );
	}
}

void ViewSDLTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, _width, _height };

	//Set clip rendering dimensions
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(SDL_Device_Pointers::_SDLRenderer, _SDLTexture, clip, &renderQuad, angle, center, flip);
}

int ViewSDLTexture::getWidth(void)
{
	return _width;
}

int ViewSDLTexture::getHeight(void)
{
	return _height;
}

bool ViewSDLTexture::lockTexture(void)
{
	bool success = true;

	//Texture is already locked
	if( mPixels != NULL )
	{
		printf( "Texture is already locked!\n" );
		success = false;
	}
	//Lock texture
	else
	{
		if( SDL_LockTexture( _SDLTexture, NULL, &mPixels, &_pitch ) != 0 )
		{
			printf( "Unable to lock texture! %s\n", SDL_GetError() );
			success = false;
		}
	}

	return success;
}

bool ViewSDLTexture::unlockTexture(void)
{
	bool success = true;

	//Texture is not locked
	if( mPixels == NULL )
	{
		printf( "Texture is not locked!\n" );
		success = false;
	}
	//Unlock texture
	else
	{
		SDL_UnlockTexture( _SDLTexture );
		mPixels = NULL;
		_pitch = 0;
	}

	return success;
}

void* ViewSDLTexture::getPixels(void)
{
	return mPixels;
}

void ViewSDLTexture::copyPixels( void* pixels )
{
	//Texture is locked
	if( mPixels != NULL )
	{
		//Copy to locked pixels
		memcpy( mPixels, pixels, _pitch * _height );
	}
}

int ViewSDLTexture::getPitch(void)
{
	return _pitch;
}

Uint32 ViewSDLTexture::getPixel32( unsigned int x, unsigned int y )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32*)mPixels;

    //Get the pixel requested
    return pixels[ ( y * ( _pitch / 4 ) ) + x ];
}
