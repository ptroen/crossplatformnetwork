#ifndef VIEWSDLTEXTURE_H
#define VIEWSDLTEXTURE_H
#include <SDL.h>
#undef main // https://stackoverflow.com/questions/6847360/error-lnk2019-unresolved-external-symbol-main-referenced-in-function-tmainc
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_image.lib")

//Texture wrapper class
class ViewSDLTexture
{
	public:
		//Initializes variables
		ViewSDLTexture(int width, int height);

		//Deallocates memory
		virtual ~ViewSDLTexture(void);

		//Creates blank texture
		void createBlank(void);
		
		//Renders texture at given point
		void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

		//Gets image dimensions
		int getWidth(void);
		int getHeight(void);

		//Pixel manipulators
		bool lockTexture(void);
		bool unlockTexture(void);
		void* getPixels(void);
		void copyPixels( void* pixels );
		int getPitch(void);
		Uint32 getPixel32( unsigned int x, unsigned int y );
		static SDL_Texture* _SDLTexture;
	    private:
		//The actual hardware texture
		
		
		void* mPixels;
		int _pitch;

		//Image dimensions
		int _width;
		int _height;
};

#endif