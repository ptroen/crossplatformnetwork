#ifndef ITexture_H
#define ITexture_H
#include "TargaHeader.h"
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <fstream>
#include <cstdlib>
#include "Primitives/TexturePixel32bit.h"
class ITexture;

class ITexture
{
public:
	TexturePixel32bit * _buffer;
	uint32_t _width;
	uint32_t _height;
	uint32_t _bpp;
	inline bool file_exists(std::string& filename) {
		filename += ".tga";
		std::ifstream f(filename.c_str());
		return f.good();
	};
	ITexture(uint32_t width, uint32_t height, uint32_t bpp)
	{
		size_t sizeInBytes = width * height*(bpp / 8);
		_buffer = static_cast<TexturePixel32bit*>(_aligned_malloc(sizeInBytes, 32));
		_width = width;
		_height = height;
		_bpp = bpp;
	}

	virtual ~ITexture()
	{
		_aligned_free(_buffer);
	};

	void SetPixel(uint32_t x, uint32_t y, TexturePixel32bit& color)
	{
		_buffer[y*_width + x] = color;
	};

	/*
	Same Function to generate map	This is only a demo function
	*/
	virtual void GenerateMap() = 0;
	virtual void Load(std::string& filename) = 0;
	virtual void Save(std::string& filename) = 0;
	virtual void Clear(void) = 0;
};


#endif