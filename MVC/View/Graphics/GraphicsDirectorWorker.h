#ifndef GRAPHICS_DIRECTOR_WORKER_H
#define GRAPHICS_DIRECTOR_WORKER_H
// don't include this file directly
#include "IGraphicsDirector.h"
#include "../../../algorithm/icriticalthread.h"

namespace MVC
{
	namespace View
	{
		namespace Graphics
		{
			struct TextureRenderingBounds
			{
				size_t beginX;
				size_t endX;
			};
			class GraphicsDirector;
			class GraphicsDirectorWorker : Algorithm::ICriticalThread
			{
			public:
				GraphicsDirectorWorker(IGraphicsDirector& directorWhichWeReportTo){};
				virtual void RenderSceneDispatcher(IGraphicsCommandList& commandList, ITexture& out_texture, TextureRenderingBounds& bounds){}
			};
		}
	}
}
#endif