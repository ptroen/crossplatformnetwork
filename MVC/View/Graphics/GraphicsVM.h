#ifndef GRAPHICS_VM_H
#define GRAPHICS_VM_H
#include "./Primitives/ImageBuffer.h"
#include "../algorithm/SingletonMacro.h"
#include "IGraphicsView.h"
#include <cstdint>
#include "../algorithm/VM/VM.h"

enum class Graphics_VM_Direction : uint8_t
{
	NoDirection,
	Initialize,
	GenerateView,
	TakeScreenShot,
	Release,
	SelectSplashScreen
};


class Graphics_VM : public VM
{
	MAKEINSTANCE(Graphics_VM)

	protected:
	IGraphicsView* activeScreen;

	public:
	Graphics_VM() 
	{
			// Create Image Buffer
			activeScreen = NULL;
	};

		virtual uint8_t GetMinVMInstruction(){ return (uint8_t)Graphics_VM_Direction::NoDirection; };
		virtual uint8_t GetMaxVMInstruction(){ return (uint8_t)Graphics_VM_Direction::TakeScreenShot; };

		virtual VM& vm(uint8_t input)
		{

			switch ((Graphics_VM_Direction)input)
			{
				case Graphics_VM_Direction::TakeScreenShot: 
					if (activeScreen)
					   activeScreen->TakeScreenShot("buffer\0");
					return *this;
				case Graphics_VM_Direction::Initialize:
					Get_Instance(); 
					return *this;
				case Graphics_VM_Direction::GenerateView:
					activeScreen->GenerateView();
				case Graphics_VM_Direction::Release:
					Release();
					return *this;
				default: 
					return *this;
			}
		};
};
#endif