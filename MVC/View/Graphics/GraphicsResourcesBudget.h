#ifndef GRAPHICS_RESOURCES_BUDGET_H
#define GRAPHICS_RESOURCES_BUDGET_H
#include "../../../algorithm/interface/IThreadingBudget.h"
namespace MVC
{
	namespace View
	{
		namespace Graphics
		{
			class GraphicsResourcesBudget : public virtual IThreadingBudget
			{
				 size_t GraphicWorkerAmount = 24;
				 size_t MainThread = 1;
				 size_t KeyGraphicsWoker = 0;
			public:
				GraphicsResourcesBudget(void):IThreadingBudget()
				{
					_threadBudget[KeyGraphicsWoker] = GetNumberOfSystemThreads() - MainThread;
				};
			};
		}
	}
}
#endif