#ifndef CURRENT_GRAPHICS_LIST_H
#define CURRENT_GRAPHICS_LIST_H
#include "GraphicsCommandList.h"
class CurrentCommandGraphicsList : GraphicsCommandList
{
	static CurrentCommandGraphicsList* instance;
	CurrentCommandGraphicsList() {};
	public:
		static	CurrentCommandGraphicsList & Get_Instance(void) { 
			            if (instance == nullptr) 
							instance = new CurrentCommandGraphicsList();
						return *instance;
		};
};

#endif