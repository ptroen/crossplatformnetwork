#ifndef IGRAPHICS_DIRECTOR_H
#define IGRAPHICS_DIRECTOR_H
#include "IGraphicsCommandList.h"
#include "ITexture.h"
class IGraphicsDirector
{
public:
	virtual void JobsToYield() {};
	virtual void RenderSceneDispatcher(IGraphicsCommandList& commandList, ITexture& out_texture) {};
};

#endif