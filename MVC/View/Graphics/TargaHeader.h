#ifndef TARGA_HEADER_H
#define TARGA_HEADER_H

/*
0  -  No image data included.
1  -  Uncompressed, color-mapped images.
2  -  Uncompressed, RGB images.
3  -  Uncompressed, black and white images.
*/
enum class DataTypeCode : unsigned char {
	NoImageDataIncluded = 0x00,
	UncompressedColorMappedImages = 0x01,
	UncompressedRGBImages = 0x02,
	UncompressedBlackAndWhiteImages = 0x03
};

enum class ColorMapType : unsigned char {
	NoMapType = 0x00
};

typedef struct {
	char id_len;                 // ID Field (Number of bytes - max 255)
	char map_type;               // Colormap Field (0 or 1)
	char img_type;               // Image Type (7 options - color vs. compression)
	int  map_first;              // Color Map stuff - first entry index
	int  map_len;                // Color Map stuff - total entries in file
	char map_entry_size;         // Color Map stuff - number of bits per entry
	int  x;                      // X-coordinate of origin 
	int  y;                      // Y-coordinate of origin
	int  width;                  // Width in Pixels
	int  height;                 // Height in Pixels
	char bpp;                    // Number of bits per TexturePixel32bit
	char misc;                   // Other stuff - scan origin and alpha bits
} targa_header;

#endif