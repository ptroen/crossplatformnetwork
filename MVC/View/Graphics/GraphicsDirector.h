#ifndef GRAPHICS_DIRECTOR_H
#define GRAPHICS_DIRECTOR_H
#include "IGraphicsDirector.h"
#include "GraphicsDirectorWorker.h"
#include "../../../algorithm/icriticalthread.h"
#include "GraphicsResourcesBudget.h"
#include <vector>
namespace MVC
{
	namespace View
	{
		namespace Graphics
		{
			class GraphicsDirector : public virtual IGraphicsDirector, public Algorithm::ICriticalThread
			{
				void SetWorkerBudget() { }; // obtain the graphics budget	
			public:
				GraphicsDirector() { SetWorkerBudget();};
				
				// this renders the scene. override it to render something different
				virtual void RenderSceneDispatcher(IGraphicsCommandList& commandList, ITexture& out_texture){
					this->lock();
					// default behavior draw some sample data
					out_texture.GenerateMap();
					this->unlock();
				};

			private:
				
				void AssignJobs(void) {};
				// the director directs the graphics workers???
				std::vector<std::tuple<GraphicsDirectorWorker, TextureRenderingBounds>> _graphicWorkers;
			}; // end class
	}
}
}
#endif