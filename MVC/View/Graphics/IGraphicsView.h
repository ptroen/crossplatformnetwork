#ifndef IGRAPHICS_VIEW_H
#define IGRAPHICS_VIEW_H

class IGraphicsView
{
	private:
	// put this in a graphics view
	short width;
	short height;
	unsigned char bpp;
	ImageBuffer backbuffer;

	public:
	IGraphicsView() :bpp(32), width(1024), height(768),
		backbuffer(width, height, bpp)
	{
		
	};

	void TakeScreenShot(std::string filename)
	{
		backbuffer.Save(filename);
	};

	virtual void GenerateView();
};

#endif