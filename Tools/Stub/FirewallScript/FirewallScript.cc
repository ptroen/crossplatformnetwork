#include "../../../OSManagement/Firewall/FirewallHeader.h"
using namespace OSManagement::Firewall;

int main(int argc,char* argv[]){
	FirewallConfiguration configuration;
	FirewallFlusher flusher(configuration);
	flusher.Run();
	FirewallListRules listRules;
	listRules.Run();
	
	PreroutingChain preroutingChain(configuration);
	preroutingChain.Run();
	listRules.Run();
	ForwardChain forwardChain(configuration);
	forwardChain.Run();
	listRules.Run();
	InputChain inputChain(configuration);
	inputChain.Run();
	listRules.Run();
	OutputChain outputChain(configuration);
	outputChain.Run();
	listRules.Run();
	listRules.Run();
	return 0;
}