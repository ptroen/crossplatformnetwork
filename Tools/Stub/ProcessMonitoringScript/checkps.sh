
function GetWindowsProcess()
{
tasklist /fo csv
return $OSTYPE;
}

function GetLinuxProcess()
{
# ptroen,ptroen,ptroen, gnom
 ps -eo pid,euser,ruser,suser,fuser,f,comm,label
}

echo "$OSTYPE"

if [[ "$OSTYPE" == "msys" ]]; then
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
		GetWindowsProcess
elif [[ "$OSTYPE" == "win32" ]]; then
        # I'm not sure this can happen.
		GetWindowsProcess
else
        # Unknown.
		GetLinuxProcess
fi
