/*
Loop through all active processes and verify they are in the accepted list.
If the active process is not in the list or the size is unusable send a socket alert to a local non existing IP which the IDS will pick up.
*/

#include <string>
#include <vector>

#include <iostream>
#include "../../../OSI/Transport/UDP/UDPClientTransport.h"
#include "../../../OSI/Transport/interface/IClientTransportInitializationParameters.h"
#include "../../../OSI/Application/Stub/TCPServer/SampleProtocol.h"
#ifdef _WIN32
#include <psapi.h>
#else
#include <stdio.h> // compile with g++
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <cstring>
#include <vector>
#include <iterator>
#include <stdlib.h>
#endif



struct Processes
{
	std::string processName;
	int processID;
};
#ifdef _WIN32
Processes PrintProcessNameAndID(DWORD processID)
{
	TCHAR szProcessName[MAX_PATH] = TEXT("\0");
	Processes proc;
	// Get a handle to the process.

	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
		PROCESS_VM_READ,
		FALSE, processID);

	// Get the process name.

	if (NULL != hProcess)
	{
		HMODULE hMod;
		DWORD cbNeeded;

		if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
			&cbNeeded))
		{
			GetModuleBaseName(hProcess, hMod, szProcessName,
				sizeof(szProcessName) / sizeof(TCHAR));
		}
	}

	// Print the process name and identifier.
	std::wstring str(szProcessName);
	std::string ss(str.begin(), str.end());
	proc.processName = ss;
	proc.processID = processID;

	// Release the handle to the process.

	CloseHandle(hProcess);
	return proc;
}

BOOL TerminateMyProcess(DWORD dwProcessId, UINT uExitCode)
{
	DWORD dwDesiredAccess = PROCESS_TERMINATE;
	BOOL  bInheritHandle = FALSE;
	HANDLE hProcess = OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
	if (hProcess == NULL)
		return FALSE;

	BOOL result = TerminateProcess(hProcess, uExitCode);

	CloseHandle(hProcess);

	return result;
}


std::vector<Processes>  winmain(void)
{
	// Get the list of process identifiers.
	std::vector<Processes> proc;

	DWORD aProcesses[1024], cbNeeded, cProcesses;
	unsigned int i;

	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
	{
		return proc;
	}


	// Calculate how many process identifiers were returned.

	cProcesses = cbNeeded / sizeof(DWORD);

	// Print the name and process identifier for each process.

	for (i = 0; i < cProcesses; i++)
	{
		if (aProcesses[i] != 0)
		{
			Processes pro = PrintProcessNameAndID(aProcesses[i]);
			proc.push_back(pro);
		}
	}
	return proc;
}
#else
class ConsoleExecutor
{
public:
	virtual lineCallback(std::string& s, size_t n) {};
	Execute(std::string& command, std::vector<std::string>& output)
	{
		FILE *pf;
		std::string data;
		data.resize(1048576);
		// Execute a process listing
		// Setup our pipe for reading and execute our command.
		pf = popen(const_cast<char*>(command.c_str()), "r");
		while (fgets(const_cast<char*>(data.c_str()), 1048576, pf))
		{
			size_t lineLength = strlen(data.c_str());
			lineCallback(data, lineLength);
		}
		if (pclose(pf) != 0)
			fprintf(stderr, " Error: Failed to close command stream \n");
	};
};
std::vector<Processes> get_ps_commands(void)
{
	std::vector<Processes> vect;
	FILE *pf;
	char command[120];
	memset(command, 0, 120);
	std::string data;
	data.resize(1048576);
	// Execute a process listing
	sprintf(command, "ps -eo pid,comm    --no-headers"); // shows pid and label of process

	// Setup our pipe for reading and execute our command.
	pf = popen(command, "r");
	//sleep(3);
	// Error handling

	// Get the data from the process execution
	while (fgets(const_cast<char*>(data.c_str()), 1048576, pf))
	{
		size_t lineLength = strlen(data.c_str());
		std::string line;
		line.resize(lineLength);
		memcpy(&line[0], &data[0], lineLength);
		std::stringstream ss(line);
		size_t count = 0;
		Processes p;
		for (auto w = std::istream_iterator<std::string>(ss); w != std::istream_iterator<std::string>(); w++, count++)
		{
			const std::string& word = *w;

			switch (count)
			{
			case 0:
				p.processID = atoi(word.c_str());
				break;
			case 1:
				p.processName = word;
				break;
			}
		}
		vect.push_back(p);
	}
	if (pclose(pf) != 0)
		fprintf(stderr, " Error: Failed to close command stream \n");
	return vect;
}
#endif
void ReportOnProcessNotOnApprovedList(std::vector<std::string>& approvedList, std::vector<Processes>& processes)
{
	for(auto it: processes)
	{
		bool approved = false;
		std::string notOnApproved;
		for(auto it2 : approvedList)
		{
			if(it2 == it.processName)
			{
				approved = true; break;
			}
			notOnApproved = it.processName;

		}
		// if a process not on approved list fire on port 911
		if (!approved)
		{
			std::string notOnApproved;
			std::cout << "Process is not on the approved list "<< it.processName << notOnApproved << std::endl;
			OSI::Transport::Interface::IClientTransportInitializationParameters init_parameters;
			init_parameters._incomingPortApplicationLayerAddress = 911;
			init_parameters._outgoingPortApplicationLayerAddress = 911;
			ofstream report;
			report.open(".\911.txt");
			report << "Process is not on the approved list "<< it.processName << std::endl;
			report.seekp(0, std::ios::end);
			init_parameters.ipAddress = "192.168.1.1"; // just send to router but we actually listening on the IDS silently
            report.close();
            system("nc -u 192.168.1.1 911 <.\911.txt");

		}
	}
}

// kill -9 1234

int main()
{
//https://docs.microsoft.com/en-us/windows/win32/psapi/enumerating-all-processes

#ifdef _WIN32
	std::vector<std::string> approvedList{ "explorer.exe\0","","\0","svchost.exe\0" };
	auto proc = winmain();
#else
	std::vector<std::string> approvedList{ "ps","bash" };
	auto proc = get_ps_commands();
#endif
	for (auto p : proc)
	{
		std::cout << p.processID << " " << p.processName << std::endl;
	}

	// now terminate
	ReportOnProcessNotOnApprovedList(approvedList, proc);

	return 0;
}
