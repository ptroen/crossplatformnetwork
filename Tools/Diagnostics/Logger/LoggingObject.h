#ifndef LOGGING_OBJECT_H
#define LOGGING_OBJECT_H
#include "../../../algorithm/interface/IType.h"
#include "LoggerInitializer.h"
namespace Tools
{
	namespace Diagnostics
	{
		namespace Logger
		{
			class LoggingObject : public virtual Algorithm::Interface::IType
			{
			public:
				LoggingObject(void) {};
				LoggingObject(LoggerInitializer& log) {};
				virtual std::string TypeName(void) { return "LoggingObject"; };
				virtual size_t size(void) { return sizeof(LoggingObject); };

				// says the maximum size of the type
				virtual size_t max_size(void) { return sizeof(LoggingObject); };

				virtual void ToString(char* data, size_t& dataSize) { /* not implemented*/ };

				// FromString not necessary as we only casting things to string to log
				// Although FromString could be useful if you want to support reconstructing the scenario of the crash but  this is BEYOND scope
			};
		}
	}
}
#endif