#ifndef LOGGER_INITIALIZER_H
#define LOGGER_INITIALIZER_H
#include "../../../algorithm/interface/IType.h"
#include "../../../algorithm/interface/IJSONSerialize.h"
namespace Tools
{
	namespace Diagnostics
	{
		namespace Logger
		{
			class LoggerInitializer : public virtual Algorithm::Interface::IType
			{
				public:
                std::string filename;
				size_t LoggerBufferSize;
				size_t LoggerStringSize;
				bool Win32OutputDebugStringLogging;
				bool fileLogging;
				std::string filenameToLogTo;
				bool stdCoutLogging;
				bool stdErrorLogging;
				bool formatTime;
				LoggerInitializer(void):filename("logconfig.json"),LoggerBufferSize(300000),LoggerStringSize(1024),Win32OutputDebugStringLogging(false),fileLogging(false),filenameToLogTo("log.log"),stdCoutLogging(true),stdErrorLogging(false),formatTime(true)
				{
					if (exist(filename))
					{
						FromFile();
					}
					else
						ToFile(); // write it back so next time you can feed in the json
				};
				virtual void FromFile(void)
				{
					std::string testJSON(filename);
					auto toProperty=ToPropertyTree();
					Algorithm::Interface::IJSONSerialize test(testJSON, toProperty);
					test.ReadFile();
					this->FromPropertyTree(test.GetPropertyTree());
				};

				virtual void ToFile(void) {
					std::string testJSON(filename);
					auto toProperty=ToPropertyTree();
					Algorithm::Interface::IJSONSerialize test(testJSON, toProperty);
					test.WriteFile();
				};

				virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					Algorithm::Interface::IPropertyTree pt;
					PADD(LoggerBufferSize)
					PADD(LoggerStringSize)
					PADD(Win32OutputDebugStringLogging)
					PADD(fileLogging)
					PADDS(filenameToLogTo)
					PADD(stdCoutLogging)
					PADD(stdErrorLogging)
					PADD(formatTime)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					PGET(LoggerBufferSize,size_t)
					PGET(LoggerStringSize,size_t)
					PGET(Win32OutputDebugStringLogging,bool)
					PGET(fileLogging,bool)
					PGET(filenameToLogTo,std::string)
					PGET(stdCoutLogging,bool)
					PGET(stdErrorLogging,bool)
					PGET(formatTime,bool)

				};
			};
		}
	}
}
#endif
