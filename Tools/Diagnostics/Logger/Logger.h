#ifndef _logger_H
#define _logger_H

#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <sstream>
#include <cstdio>
#include <ctime>
#include <time.h>
//#define BOOST_ASIO_HAS_STD_CHRONO
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include "../../../algorithm/circularbuffer.h"

#include <chrono>
#include "../../../algorithm/icriticalthread.h"
//#include <unordered_map>
#include "LoggingObject.h"
#include "../../../algorithm/interface/IJSONSerialize.h"
#include <list>
#define MAKESTR(value) #value
#define VAR_LINE(value) msg = MAKESTR(value);\
msg += "=" + value;

#define LOGIT(str) {\
auto str_str=str;\
str_str+="\n";\
Tools::Diagnostics::Logger::Logger::Get_Instance().Log(str_str);\
}

#define LOGIT1(str) {\
auto str_str=str;\
str_str+="\n";\
Tools::Diagnostics::Logger::Logger::Get_Instance().Log(str_str);\
}
using namespace std;
namespace Tools
{
	namespace Diagnostics
	{
		namespace Logger
		{
            class Logger;

            class LogMessage
            {
                public:
                std::string msg;
                size_t used;
                public:
                LogMessage(LoggerInitializer& initializer):msg(){
                    //msg.resize(initializer.LoggerStringSize);
                    msg.resize(1024*1024*10);
                    memset(const_cast<char*>(msg.c_str()),0,1024*1024*10);
                    used = 0;
                }

                void clear(){ used = 0;}

                inline size_t setRemainingUsed(size_t request){ size_t amountleft = ( request + used > msg.size() ? msg.size() - 1 - used : used + request);
                    used += amountleft;
                    return used;
                }
            };

			class Logger : public virtual Algorithm::ICriticalThread
			{
                LogMessage _log;
				ofstream file;
				size_t i;
				static Logger* _instance;
				LoggerInitializer& _initializer;
				Logger(LoggerInitializer& initializer) :i(0),
                                                        file(),
                                                        _initializer(initializer),
                                                        _log(initializer){

					if (_initializer.fileLogging)
					{
						std::stringstream ss;
						ss << _initializer.filenameToLogTo;
						file = ofstream(ss.str(), std::ofstream::app);
					}
                    #ifndef ANDROID
					start = boost::chrono::thread_clock::now();
                    #endif
				};
			public:
                boost::chrono::thread_clock::time_point start;
				static Logger& Get_Instance(void)
				{
					if(_instance==nullptr)
					{
						LoggerInitializer init;

						_instance = new Logger(init);
						_instance->_workerthread = new std::thread(&LoggingLoop);

					}
					return *_instance;
				};
				void Log(std::string& log)
				{
                    lock();
                    size_t used = _log.used;
                    size_t remsize = _log.setRemainingUsed(log.size());
                    if(used + remsize < _log.msg.size())
                    {
                        strncpy(&_log.msg[used],log.c_str(),remsize);
                    }
                    unlock();
				};

			private:
				static void LoggingLoop(void)
				{
                    std::string msg;
                    msg.resize(1024*1024*10);
                    memset(&msg[0],0,1024*1024*10);
					while(1)
					{
                        _instance->lock();
						size_t msgused = _instance->_log.used;
						if(msgused > 0)
                        memcpy(&msg[0], _instance->_log.msg.c_str(), _instance->_log.used < msg.size() ? _instance->_log.used : msg.size() - 1 );
                        _instance->_log.clear();
                        _instance->unlock();
                        if(msgused > 0)
                        {
                        if(msgused < msg.size())
                        msg[msgused] = 0;


                        // if std::console logging
                        if (_instance->_initializer.stdCoutLogging)
                        {
							std::cout << msg << std::endl;
                        }
                        else if (_instance->_initializer.fileLogging)
                        {
                            // if file logging
							_instance->file << msg << std::endl;
                            _instance->file.flush();
                        }
                        #ifdef WIN32
                        if (_instance->_initializer.Win32OutputDebugStringLogging)
                        {
                            // if win32 logging
                            OutputDebugStringA(msg.c_str());
                        }
                        #endif
                        }
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
					}
				};
			};
		}
	}
}

#endif
