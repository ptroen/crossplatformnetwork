#ifndef MANUALLY_INSPECT_H
#define MANUALLY_INSPECT_H

class ManuallyInspect
{
public:
	static void inspect(void)
	{
#ifdef  _WIN32
		system("explorer .");
#elif __APPLE__
		system("open .");
#else // assuming linux
		system("xdg-open .");
#endif
	};
};

#endif