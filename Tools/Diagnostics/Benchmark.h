#ifndef BENCHMARK_H
#define BENCHMARK_H
#include "../OSInclude.h"
#include <chrono>
// http://stackoverflow.com/questions/1487695/c-cross-platform-high-resolution-timer


class Class_Benchmark
{
public:
	virtual void BenchMarkme(void){};

};

class IBenchmarkBase
{

protected:
	std::chrono::time_point<std::chrono::high_resolution_clock> startPoint;
	std::chrono::nanoseconds timeDifference;
public:
	  void StartTimer(void)
		{
		  startPoint = std::chrono::high_resolution_clock::now();
		};
	  void ResetTimer(void) { StartTimer(); }
		void  ManuallyStopTimer(void)
		{
			std::chrono::time_point<std::chrono::high_resolution_clock> t1 = std::chrono::high_resolution_clock::now();
			timeDifference = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - startPoint);
		};

		size_t TimeInNanoseconds() { return static_cast<size_t>(timeDifference.count()); }
		size_t TimeInMilliSeconds() { return TimeInMicroseconds() / 1000; };
		size_t TimeInMicroseconds() { return TimeInNanoseconds() / 1000;}
		size_t TimeInSeconds() { return TimeInMilliSeconds() / 1000; };
};

class Benchmark : public virtual IBenchmarkBase
{
public:
		static size_t BenchmarkEndInMilliSeconds(const std::chrono::time_point<std::chrono::high_resolution_clock>& startPoint = std::chrono::high_resolution_clock::now())
		{
			std::chrono::time_point<std::chrono::high_resolution_clock> t1 = std::chrono::high_resolution_clock::now();
			std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - startPoint);
			return static_cast<size_t>(ms.count());
		};

		virtual std::chrono::nanoseconds Run(Class_Benchmark& b){
			StartTimer();
			b.BenchMarkme();
			ManuallyStopTimer();
			return timeDifference;
		};

};
#endif
