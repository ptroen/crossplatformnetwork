#ifndef OS_SOCKET_INCLUDE_H
#define OS_SOCKET_INCLUDE_H
#ifdef _WIN32
#define _WINSOCKAPI_ 
#include <WinSock2.h>
#include <Windows.h>
#endif
#ifndef _WIN32
#include <pthread.h> // is this faster then std::thread???
#endif
#endif