#!/bin/bash
# $1 Program
echo "stop watch begin in UTC since 1970"
date +%s >measure.txt
for i in {0..1000000}
do     
     (nc $1 80 <measure.txt)
done
echo "stop watch begin since 1970"
date +%s >>measure.txt
