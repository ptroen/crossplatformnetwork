#!/bin/bash
# $1 multicast IP
echo "stop watch begin in UTC since 1970"
date +%s
for i in {0..100000}
do     
     (sockperf ping-pong -i $1 -p 1234)
done
echo "stop watch begin since 1970"
date +%s
