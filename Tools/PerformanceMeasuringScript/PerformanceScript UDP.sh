#!/bin/bash
# $1 Program
echo "stop watch begin in UTC since 1970"
date +%s >measure.txt
for i in {0..100000}
do     
     (nc -u 127.0.0.1 80 <measure.txt)
done
echo "stop watch begin since 1970"
date +%s >>measure.txt
