#include "../../OSManagement/Firewall/FirewallHeader.h"
using namespace OSManagement::Firewall;

int main(int argc,char* argv[])
{
    std::cout << "Firewall Script" << std::endl;
    Tools::Diagnostics::Logger::Logger::Get_Instance();

    std::string msg("FirewallScript.cc");

    LOGIT1(msg);
    msg = "Firewall Configuration";

    FirewallConfiguration configuration;
    msg = "ParseServerArgs";
    LOGIT1(msg)
    configuration.ParseServerArgs(&(*argv), argc);

    FirewallFlusher flusher(configuration);
    flusher.Run();

	ForwardChain forwardChain(configuration);
	forwardChain.Run();

	PreroutingChain routing(configuration);
	routing.Run();

	InputChain inputChain(configuration);
	inputChain.Run();
	OutputChain outputChain(configuration);
	outputChain.Run();

	FirewallListRules listRules;
	listRules.Run();
	configuration.Print();
	return 0;
}
