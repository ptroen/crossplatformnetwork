#ifndef INPUT_EVENT_MANAGER_H
#define INPUT_EVENT_MANAGER_H

#include <SDL.h>
#include "RTSGameEvents.h"
#include <map>
class InputEventManager
{
	public:
	 void ProcessEvents(SDL_Event event)
	{
		if (event.type == SDL_KEYDOWN)
		{
			ProcessKeyboard(event.key);
			return;
		}
		if (event.type==SDL_MOUSEBUTTONDOWN)
		{
			ProcessMouse(event.button);
		}
		if (event.type==SDL_MOUSEMOTION)
		{
			ProcessMouseMotion(event.motion);
		}
		if (event.type==SDL_MOUSEWHEEL)
		{
			ProcessMouseWheel(event.wheel);
		}

	};
	private:
	void ProcessKeyboard(SDL_KeyboardEvent& event)
	{
		
	};


	void ProcessMouseMotion(SDL_MouseMotionEvent& event)
	{

	};

	void ProcessMouseWheel(SDL_MouseWheelEvent& event)
	{

	};

	void ProcessMouse(SDL_MouseButtonEvent& event)
	{
		
	};

};

#endif