#ifndef I_TASK_DIRECTOR_DISPATCHER_H
#define I_TASK_DIRECTOR_DISPATCHER_H
#include "ITaskDirector.h"
#include <vector>
class ITaskDirectorDispatcher
{
public:
	std::vector<ITaskDirector*> task_directors;
	/*
	 *  OrderDirectorsToDispatchTheirRespectiveJobs - Calls JobsToYield for each Task Director and
	 *  JobsToJoin
	 */
	virtual void OrderDirectorsToDispatchTheirRespectiveJobs()
	{
		for (auto i : task_directors)
		{
			i->JobsToYield();
		};

		for (auto i : task_directors)
		{
			i->JobsToJoin();
		};

	};
};


#endif
