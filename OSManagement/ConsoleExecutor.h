#ifndef CONSOLE_EXECUTOR_H
#define CONSOLE_EXECUTOR_H
#include <string>
#include <iostream>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <cstdint>
#include <iterator>
#include <stdio.h>
#include <thread>
#include <chrono>
#ifndef _WIN32
#include <unistd.h> // Linux
#include <stdlib.h>
#endif
namespace OSManagement
{
	class ConsoleExecutor
	{
		const size_t arbitraryDelayInMilliseconds = 1000;
	public:
		virtual void lineCallback(std::string& s, size_t n) { std::cout << s << std::endl; };

		void Execute(std::string& command, std::vector<std::string>& output)
		{
			std::string data;
			data.resize(1048576);
			const std::string consoleFile = "fileData.txt";
			std::cout << command << std::endl;
#ifdef _WIN32
			// use system + io redrection
			std::string overallCommand;
			overallCommand += command;
			overallCommand += " >.\\";
			overallCommand += consoleFile;
			system(overallCommand.c_str());
			std::this_thread::sleep_for(std::chrono::milliseconds(arbitraryDelayInMilliseconds));
			FILE* pf;
			pf= fopen(consoleFile.c_str(), "r");
			// then read from the ioredirection
#else
			FILE *pf;
			
			// Execute a process listing
			// Setup our pipe for reading and execute our command.
			std::this_thread::sleep_for(std::chrono::milliseconds(arbitraryDelayInMilliseconds));
			pf = popen(const_cast<char*>(command.c_str()), "r");
#endif
			while (fgets(const_cast<char*>(data.c_str()), 1048576, pf))
			{
				size_t lineLength = strlen(data.c_str());
				lineCallback(data, lineLength);
			}
#ifdef _WIN32
			fclose(pf);
#else
			if (pclose(pf) != 0)
				fprintf(stderr, " Error: Failed to close command stream \n");
#endif
		};
		void Execute(std::vector<std::string>& commands, std::vector<std::string>& output)
		{
			for (auto command : commands)
			{
				Execute(command, output);
			}
		};
	};
};
#endif
