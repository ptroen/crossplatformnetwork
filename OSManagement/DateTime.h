#ifndef DATE_TIME_H
#define DATE_TIME_H
#include <boost/date_time.hpp>
#include <cstdint>
#include <iostream>
namespace OSManagement
{
    // https://thispointer.com/get-current-date-time-in-c-example-using-boost-date-time-library/

    class IDateObject
    {
    public:
        uint16_t year; // if year > 10000 then its universal
        uint8_t months;
        uint8_t days;
        uint8_t hours;
        uint8_t minutes;
        uint16_t milliseconds; // 60 seconds * 1000 seconds in a minute
        
    };

union DateObject
{
public:
    uint64_t timestamp;
    IDateObject dateobj;
    
    boost::gregorian::date GetDate(void){
        uint16_t year = dateobj.year;
        if(year>=10000)
            year-=10000;
        boost::gregorian::date date(year,dateobj.months, dateobj.days);
        //date(greg_year, greg_month, greg_day)
        return date;
    };
    
    boost::posix_time::time_duration GetDuration(void){
        // time_duration(hours,minutes,seconds,fractional_seconds)
        boost::posix_time::time_duration time_duration(dateobj.hours,
                                                       dateobj.minutes,
                                                       dateobj.milliseconds / 1000,
                                                       dateobj.milliseconds % 1000);
        return time_duration;
    };
    
    void SetDuration(boost::posix_time::time_duration time){
        dateobj.hours = time.hours();
        dateobj.minutes =time.hours();
        dateobj.milliseconds = ( time.seconds() * (time.total_milliseconds()% 1000));
    }
    
    void SetDate(boost::gregorian::date date){
        dateobj.year = date.year();
        dateobj.months = date.month();
        dateobj.days = date.day();
    }
    
    void SetUTC(bool universal){
        if(universal)
        {
            dateobj.year +=1000;
        }
    }
    
    bool IsUTC(void){
        if(dateobj.year >= 10000) // year 9999 problem
        {
            return true;
        }
        return false;
    }
};

class DateTime
{
        boost::posix_time::ptime _ptime;
        DateObject _obj;
    protected:
    boost::gregorian::date GetDate(void){ return _ptime.date();};
        boost::posix_time::time_duration GetDuration(void){ return _ptime.time_of_day();};
        void SetDateObject(void){
            _obj.SetDuration(GetDuration());
            _obj.SetDate(GetDate());
            _obj.SetUTC(_utcTime);
        }
    public:
        bool _utcTime;
        DateTime(std::string dateString, bool UTCTime):_utcTime(UTCTime){
            _ptime = boost::posix_time::time_from_string(dateString);
            SetDateObject();
        }
        DateTime(bool UTCTime) :_utcTime(UTCTime) {
            Update();
        };
        DateTime(uint64_t timeAs64bitType){
            _obj.timestamp =timeAs64bitType;
            _utcTime = _obj.IsUTC();
            _ptime = boost::posix_time::ptime(_obj.GetDate(), _obj.GetDuration());
        };

        void Update(void) {
            if(!_utcTime)
                _ptime = boost::posix_time::second_clock::local_time();
            else
                _ptime = boost::posix_time::second_clock::universal_time();
            SetDateObject();
        };

        void outputStringDateStamp(std::ostream& o) {
           o << _ptime;
        };

        void ConvertLocalToUTC(void) {
                _utcTime = true;
        };

        void ConvertUTCToLocal(void) {
                _utcTime = false;
        };
        uint64_t To64bitType() {
            return _obj.timestamp; };
    };
}

/*
#include <iostream>
int main(void)
{
    OSManagement::DateTime utc(true);
    OSManagement::DateTime loc(false);

    std::cout << "UTC Date ";
    utc.outputStringDateStamp(std::cout);
    std::cout << std::endl;
    std::cout << "Local Date ";
    loc.outputStringDateStamp(std::cout);

    return 0;

}
Local Date users-MacBook-Air-4:untitled folder user$ g++ *.cc
users-MacBook-Air-4:untitled folder user$ ./a.out
UTC Date 2020-Apr-02 07:42:17
Local Date 2020-Apr-02 00:42:17users-MacBook-Air-4:untitled folder user$
*/


#endif
