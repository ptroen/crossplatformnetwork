#ifndef I_TASK_DIRECTOR_H
#define I_TASK_DIRECTOR_H
#include "ICriticalThread.h"

class ITaskDirector
{
public:
	/*
	 *  JobsToYield: A interface To yield threads and join them. This is needed so we can have a clear calling chain for
	 *  joinable threads
	 */
	virtual void JobsToYield();

	virtual void JobsToJoin();
};


#endif
