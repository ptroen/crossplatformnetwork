#ifndef PIPES_VM_H
#define PIPES_VM_H
#ifdef WIN32
#include <windows.h>
#endif 
#include <cstdint>
#include <string>
#include <iostream>

enum class PipeReader_Direction : uint8_t
{
	NoDirection,
	ReadRecord,
};

class PipeBase;


// http://stackoverflow.com/questions/2640642/c-implementing-named-pipes-using-the-win32-api

class PipeBase
{
public:
	PipeBase(std::string& pipename)
	{
		_PipeName = pipename;
	};


	virtual ~PipeBase()
	{
		
	}
	protected:

	LPCWSTR GetPipeName()
	{
		return s2ws( _PipeName.c_str());
	}
	std::string	_PipeName;

	private:
	std::wstring s2ws(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	};

	#ifdef WIN32
	protected:
		HANDLE pipe;
	#endif


};

class PipeReader : public virtual PipeBase
{
	public:
	PipeReader(std::string& pipeName): PipeBase(pipeName){};

	void Read(){
		while (true)
		{
			HANDLE pipe = CreateNamedPipe(GetPipeName(), PIPE_ACCESS_INBOUND | PIPE_ACCESS_OUTBOUND, PIPE_WAIT, 1, 1024, 1024, 120 * 1000, NULL);

			if (pipe == INVALID_HANDLE_VALUE)
			{
				std::cout << "Error: " << GetLastError();
			}

			char data[1024];
			DWORD numRead;

			ConnectNamedPipe(pipe, NULL);

			ReadFile(pipe, data, 1024, &numRead, NULL);

			if (numRead > 0)
				std::cout << data << std::endl;

			CloseHandle(pipe);
		}
	};

};

enum class Graphics_VM_Direction : uint8_t
{
	NoDirection,
	Initialize,
	TakeScreenShot,
	Release,
};

class PipeWriter : public virtual PipeBase
{
	public:
		PipeWriter(std::string& pipeName) : PipeBase(pipeName){};

		void Write(std::string& message)
		{
			 pipe = CreateFile(GetPipeName(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

			if (pipe == INVALID_HANDLE_VALUE)
			}
		
			DWORD numWritten;
			WriteFileA(pipe, message.c_str(), message.length(), &numWritten, NULL);

		};


};

#endif