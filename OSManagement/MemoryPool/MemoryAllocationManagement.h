#ifndef MEMORY_ALLOCATION_MANAGEMENT_H
#define MEMORY_ALLOCATION_MANAGEMENT_H
#include "CircularBufferMemoryPool.h"
#include "MemoryAllocationNode.h"
namespace OSManagement
{
	namespace MemoryPool
	{
	class MemoryAllocationManagement
	{
	public:
		MemoryAllocationManagement() :rootToAllocated(nullptr), rootToFreeAddresses(nullptr) {};
		MemoryAllocationNode* rootToAllocated;
		MemoryAllocationNode* rootToFreeAddresses;
	};

	MemoryAllocationManagement root;
	void* MemoryAllocate(size_t n)
	{
		if (root.rootToAllocated == nullptr)
		{
			root.rootToAllocated = (MemoryAllocationNode*)malloc(sizeof(MemoryAllocationNode));
			root.rootToAllocated->Clear();
			root.rootToFreeAddresses = (MemoryAllocationNode*)malloc(sizeof(MemoryAllocationNode));
			root.rootToFreeAddresses->Clear();
		}
		MemoryAllocationNode* freshNode = nullptr;

		// search within free addresses to see if we have free memory
		if ((freshNode = root.rootToFreeAddresses->SearchBySize(n)) == nullptr)
		{
			// alloc new node
			MemoryAllocationNode* freshNode = (MemoryAllocationNode*)malloc(sizeof(MemoryAllocationNode));
			freshNode->Clear();
			freshNode->AllocateSpace(n);
			root.rootToAllocated->InsertByMemorySize(*freshNode);
			root.rootToAllocated->InsertByAddress(*freshNode);
			return freshNode->dataInHeap;
		}
		// else we found a entry so move it out of free addresses
		freshNode->SwapParentRoot(*(root.rootToAllocated));
		return freshNode->dataInHeap;
	}
	void MemoryReturn(void* ptr)
	{
		if (root.rootToAllocated == nullptr) return;
		MemoryAllocationNode* nodeToFree = root.rootToAllocated->SearchByAddress(ptr);
		if(nodeToFree != nullptr)
		{
			root.rootToAllocated->SwapParentRoot(*(root.rootToFreeAddresses));
		}
		else if (ptr == 0 || ptr == nullptr) return; 
		//else
		//{
			//std::cout << boost::stacktrace::stacktrace() << std::endl;
			//throw new std::exception("invalid address passed to free check calling object for usage");
		//}
	}
	} // end namespace
} // end namespace

#endif