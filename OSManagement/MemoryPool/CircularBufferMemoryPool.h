#ifndef CIRCULAR_BUFFER_MEMORY_POOL_H
#define CIRCULAR_BUFFER_MEMORY_POOL_H
#include "MemoryEntry.h"
#include "stdlib.h"
namespace OSManagement
{
	namespace MemoryPool
	{
		class CircularBufferMemoryPool
		{
			MemoryEntry* pool;
			void* out_of_bounds;
			size_t bytesInPool;
			MemoryEntry* NextFreeCell;
			const size_t alignedSize = 32;
			const size_t threadPoolSize = 134217728; // 128 megs
		public:
			CircularBufferMemoryPool(void) {
				// gonna use aligned_malloc which would break multi platform potentially but wouldn't be HARD to just wrap a macro around it and just use mod to  get it working for
				// open source stacks. Sorry not enough time right now gonna get this working.. so align malloc it is...
				// citation: http://msdn.microsoft.com/en-us/library/8z34s9c6.aspx
#ifdef WIN32
				void* poolPtr = _aligned_malloc(
					threadPoolSize,
					alignedSize);
#elif
				void* poolPtr = aligned_alloc(alignedSize, threadPoolSize);
#endif
				bytesInPool = threadPoolSize;
				pool = (MemoryEntry*)poolPtr;
				NextFreeCell = pool;
				out_of_bounds = (void*)((size_t)poolPtr + threadPoolSize);
			};

			virtual ~CircularBufferMemoryPool() {
#ifdef WIN32
				_aligned_free(pool);
#elif
				free(pool);
#endif
				pool = nullptr;
			};

			// assuming non zero value
			 void* falloc(size_t BytesRequested, CircularBufferMemoryPool& pool_to_use) {
				size_t numberOf32ByteWords = BytesRequested / 32 + (BytesRequested % 32);
				if ((pool_to_use.NextFreeCell + numberOf32ByteWords) >= pool_to_use.out_of_bounds) {
					pool_to_use.NextFreeCell = pool_to_use.pool; // assign the initial entry of the pool
				}
				MemoryEntry* entry_to_return = pool_to_use.NextFreeCell;

				entry_to_return->memoryAllocated = BytesRequested;

				pool_to_use.NextFreeCell += numberOf32ByteWords;
				return entry_to_return;
			};

			void ffree(void* ptr) {
				
			};
		};
	}
}
#endif