#ifndef MEMORY_ENTRY_H
#define MEMORY_ENTRY_H
namespace OSManagement
{
	namespace MemoryPool
	{
		class MemoryEntry
		{
			public:
			size_t memoryAllocated;
			MemoryEntry* nextEntry;
			void* dataRepresentingAvailableMemoryInHeap;
		};
	}
}
#endif