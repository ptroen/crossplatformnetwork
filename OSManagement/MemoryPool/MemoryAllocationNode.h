#ifndef MEMORY_ALLOCATION_NODE_H
#define MEMORY_ALLOCATION_NODE_H
namespace OSManagement
{
	namespace MemoryPool
	{
class MemoryAllocationNode
	{
	public:
		void* dataInHeap;
		size_t sizeAllocated;
		bool used;
	private:
		MemoryAllocationNode* right;
		MemoryAllocationNode*left;
		MemoryAllocationNode* parent;
		MemoryAllocationNode* rightAddressNode;
		MemoryAllocationNode*leftAddressNode;
	public:
		void AllocateSpace(size_t n)
		{
			dataInHeap = malloc(n);
			sizeAllocated = n;
		};

		MemoryAllocationNode() { Clear(); };
		virtual ~MemoryAllocationNode() {
			if(dataInHeap!= nullptr)
		    {
			    free(dataInHeap);
		    }
		};
		void Clear(void) {
			dataInHeap = nullptr;
			sizeAllocated = 0;
			left = nullptr;
			right = nullptr;
			parent = nullptr;
			rightAddressNode = nullptr;
			leftAddressNode = nullptr;
			used = false;
		};

		MemoryAllocationNode& operator=(MemoryAllocationNode& rhs)
		{
			dataInHeap = rhs.dataInHeap;
			sizeAllocated = rhs.sizeAllocated;
			left = rhs.left;
			right = rhs.right;
			return *this;
		};

		// struct MemoryAllocationNode duplicateRoot; use <=
		void ReplaceInNode(MemoryAllocationNode& nodeToReplace, MemoryAllocationNode& newEntry) { (left == &nodeToReplace) ? left = &newEntry : right = &newEntry; }
	public:
		// callers responsibility to allocate the address
		void InsertByMemorySize(MemoryAllocationNode& freshNode)
		{
			if (dataInHeap == nullptr)
			{
				// parent relative to memory size only
				MemoryAllocationNode* _parent = (parent == nullptr ? nullptr : &(*parent));
				*this = freshNode;
				// only modify if parent is non null
				parent = (_parent != nullptr ? _parent : parent);
			}
			else if (this->sizeAllocated <=freshNode.sizeAllocated)
			{
				if (left == nullptr)
					left = &freshNode;
				else
					left->InsertByMemorySize(freshNode);
			}
			else
			{
				if (right == nullptr)
					right = &freshNode;
				else
					right->InsertByMemorySize(freshNode);
			}
		}

		// assumes insert was called first. This is used to index the addresses
		void InsertByAddress(MemoryAllocationNode& freshNode)
		{
			if(freshNode.dataInHeap==nullptr)
			{
				MemoryAllocationNode* _parent = parent;
				*this = freshNode;
				if(parent != NULL || parent != nullptr)
				parent = _parent; // restore parent
			}
			if (freshNode.dataInHeap < dataInHeap)
			{
				if (leftAddressNode == nullptr)
					leftAddressNode = &freshNode;
				else
					leftAddressNode->InsertByAddress(freshNode);
			}
			else
			{
				if (rightAddressNode == nullptr)
					rightAddressNode = &freshNode;
				else
					rightAddressNode->InsertByAddress(freshNode);
			}
		}
	void SwapParentRoot(MemoryAllocationNode& alternateRoot)
	{
		// rebalance the tree
		ReplaceInNode(*this,*left);
		if(this->right != nullptr)
		{
			this->right->parent = nullptr; // we have a new parent
			alternateRoot.InsertByMemorySize(*right);
		}
	}
	MemoryAllocationNode* SearchForMemoryAllocationNode(void* ptr)
	{
		// search by addresses here
		if (dataInHeap == ptr)
		{
			return this;
		}
		MemoryAllocationNode* ptrToFind;
		if (left)
		{
			ptrToFind = left->SearchForMemoryAllocationNode(ptr);
			if (ptrToFind->dataInHeap == ptr)
				return ptrToFind;
		}
		ptrToFind = right->SearchForMemoryAllocationNode(ptr);
		if (ptrToFind->dataInHeap == ptr)
			return ptrToFind;
		return nullptr;
	} // end SearchForMemoryAllocationNode
	MemoryAllocationNode* SearchBySize(size_t desiredSizeAllocated)
	{
		if (dataInHeap == nullptr)
		{
			return nullptr;
		}
		if (desiredSizeAllocated == sizeAllocated)
		{
			return this;
		}
		if (desiredSizeAllocated <= sizeAllocated)
		{
			if (left)
			{
				return left->SearchBySize(desiredSizeAllocated);
			}
			else
			{
				return nullptr;
			}
		}
		if (right)
		{
			return right->SearchBySize(desiredSizeAllocated);
		}
		return nullptr;
	}

	MemoryAllocationNode* SearchByAddress(void* address)
	{
		if (dataInHeap == nullptr)
		{
			return nullptr;
		}
		else if (dataInHeap == address)
		{
			return this;
		}
		if (dataInHeap < address)
		{
			if (leftAddressNode)
			{
				return leftAddressNode->SearchByAddress(address);
			}
			else
			{
				return nullptr;
			}
		}
		if (rightAddressNode)
		{
			return rightAddressNode->SearchByAddress(address);
		}
		return nullptr;
	}

	}; // end class
	}
}
#endif