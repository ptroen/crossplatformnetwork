#ifndef FileIOFacade_H
#define FileIOFacade_H
#include <cstdio>
#include <stdio.h>
#include <string.h>
#include <sstream>
#ifndef MAXPATHLEN
#define MAXPATHLEN 2048
#endif
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#endif
#include <cstddef>
#include <exception>
#include <fstream>
#include <filesystem>
#include <boost/filesystem.hpp>

namespace OSManagement
{
   namespace FileSystem
   {
	   struct FileNotOpenedException : std::runtime_error
	   {
		   FileNotOpenedException(const std::string& file): std::runtime_error("Can't open file. The file name is" + file){};
	   };

	   struct ReadFileNotAllocatedException : std::runtime_error
	   {
		   ReadFileNotAllocatedException(const std::string& file, std::string& bytesAllocated, std::string& bytesExpected) : std::runtime_error("Not enough space allocated for the " + file + " bytes allocated:" + bytesAllocated + "bytes expected: " +bytesExpected) {};
	   };

		// this is C++ wrapper for fstream incase the compiler doesn't have full fstream implementation
		// but would recommend fstream over this..
		class FileIOFacade;
		class FileIOFacade
		{
			FILE* file;
			boost::filesystem::path _relativePath;
			public:
			FileIOFacade(boost::filesystem::path& relativePath, bool read = true, bool write = false, bool append = false):_relativePath(relativePath),file(nullptr)
			{
				file = nullptr;
				char* flags = new char[3];
				memset(flags, 0, 3);
				if ((append && read) || (append && write))
				{
					flags = "a+\0";
				}
				else if (append)
				{
					flags = "a\0";
				}
				else if (read && write)
				{
					flags = "w+\0";
				}
				else if (write)
				{
					flags = "w\0";
				}
				else
				{
					flags = "r\0";
				}
				std::string abspath = GetAbsolutePath().string();
#ifdef _WIN32
				if (fopen_s(&file, abspath.c_str(), flags) != 0)
#else  
				if (!(file = fopen(abspath.c_str(), flags)))
				#endif
				{
					throw new FileNotOpenedException(GetAbsolutePath().string());
				}
			};
			
			virtual ~FileIOFacade(){ close(); };

			boost::filesystem::path GetAbsolutePath(void)
			{
				boost::filesystem::path absPath = boost::filesystem::current_path();
#ifdef _WIN32
				std::string slash("\\");
#else  
				std::string slash("/");
#endif
				 absPath += slash;
				 absPath += _relativePath;
				return absPath;
			};

			void DeleteFile(void)
			{
				close();
				boost::filesystem::remove(_relativePath);
			};
			

			unsigned char GetCharacter(void)
			{
				return fgetc(file);
			};
		public:
			static void ReadFileSystem(bool recursive,const boost::filesystem::path currentPath, void* classPassedIn,void(*foo)(void* classPassedIn, const boost::filesystem::path& pathFound))
			{
				// works cited: https://beta.boost.org/doc/libs/1_45_0/libs/filesystem/v3/doc/tutorial.html
				if (boost::filesystem::exists(currentPath))    // does p actually exist?
				{
					if (boost::filesystem::is_directory(currentPath))      // is p a directory?
					{
						for (boost::filesystem::directory_entry dir : boost::filesystem::directory_iterator(currentPath))
						{
							if (recursive && boost::filesystem::is_directory(dir))
							{
								ReadFileSystem(true,dir.path(), classPassedIn, &(*foo));
							}
							else if (boost::filesystem::is_regular_file(dir))
							{
								(*foo)(classPassedIn, dir.path());
							}
						}
					}
				}
			}

			void AddCharacter(unsigned char c)
			{
				fputc(c,file);
			};

			size_t write(void* v, size_t size)
			{
				size_t i= fwrite(v, size,1, file);
				fflush(file);
				return i;
			};

			size_t read(void* v,size_t size)
			{
				return fread(v, 1, size, file);
			};
			bool eof(){ return (bool) feof(file); };
		
			void seek(long seek, long origin){ fseek(file,seek,origin);};
					
			void close()
			{
				if (file != nullptr)
				{
					fclose(file);
					file = nullptr;
				}
			};

			boost::filesystem::path CurrentPath() { return boost::filesystem::current_path(); };

			size_t SizeOfFile(void)
			{
				return boost::filesystem::file_size(_relativePath);
			};

			static bool exists(boost::filesystem::path& relativePath)
			{
				return boost::filesystem::exists(relativePath);
			};

		void WriteContents(void* object, size_t bytesAllocated)
		{
			unsigned char* obj = reinterpret_cast<unsigned char*>(object);
			std::string filePath = GetAbsolutePath().string();
			size_t bytesWritten = 0;
			while (bytesWritten < bytesAllocated)
			{
				bytesWritten += write(&obj[bytesWritten], bytesAllocated - bytesWritten);
			}
		}

		// Reads the Entire File and places contents into n
		void ReadContents(size_t numberOfBytesAllocated, void* objectToWriteTo)
		{
			std::string filePath = GetAbsolutePath().string();
			unsigned char* obj = static_cast<unsigned char*>(objectToWriteTo);
			size_t FileSize =SizeOfFile();
			size_t bytesRead = 0;
			if(numberOfBytesAllocated < FileSize)
			{
				std::stringstream ssBytesAllocated;
				ssBytesAllocated << numberOfBytesAllocated;
				std::stringstream ssFileSize;
				ssFileSize << FileSize;
				std::string ssBytes = ssBytesAllocated.str(); // linux compilation issue fix to add this variable
std::string ssFileSizeStr = ssFileSize.str(); // linux compilation issue fix to add this variable
				throw new ReadFileNotAllocatedException(_relativePath.string(), ssBytes, ssFileSizeStr);
			}
			while (bytesRead <= FileSize)
			{
				size_t lastBytesRead = bytesRead;
				bytesRead += read(&obj[bytesRead], FileSize - bytesRead);
				if (lastBytesRead == bytesRead)
					return;
			}
		} // end Read_Contents()
		}; // end class
		}
}
#endif
