#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <map>
#include <string>
#include <iostream>
#include <string>
#include <set>
#include <sstream>
#include <exception>
#include <fstream>
#include <boost/config.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <vector>
#include <variant>
namespace OSManagement
{
	namespace FileSystem
	{
		// see http://techgate.fr/boost-property-tree/
        
		enum class FileParsingMode { BINARY, JSON, INI,SQLLITE};
		
		enum class PrimitiveType { stringType, wstringType, INI,SQLLITE};
		union FilePrimitiveTypes
		{
			std::string& str;
			std::vector<int> vec;
			~FilePrimitiveTypes() {} // needs to know which member is active, only possible in union-like class 
		};          // the whole union occupies max(sizeof(string), sizeof(vector<int>))
 
		union MemoryEntry
		{
			
		};
		
		// https://stackoverflow.com/questions/6175502/how-to-parse-ini-file-with-boost
		class FileSerializer
		{
		public:
			std::string filename;
			std::map<std::string, std::map<std::string, std::string>> schema;

			FileSerializer(std::string& filename_to_read) :filename(filename_to_read) {};

			// Read the Schema(the thing that describes the file) into sectionMap based off a filename
			void ReadConfig(std::map<std::string, FileParsingMode& file) {
				boost::property_tree::ptree pt;

				// read ini;
				/*
				boost::property_tree::ini_parser::read_ini(filename.c_str(), pt);
				for (auto it = sectionMap.begin(); it != sectionMap.end();)
				{
					for (std::map<std::string, std::vector<std::string>>::iterator schemasection = schema.begin(); schemasection != schema.end();) // schema section
						for (std::vector<std::string>::iterator schemakey = schemasection->second.begin(); schemakey != schemasection->second.end();) // schema key
						{
							auto value = pt.get<std::string>(schemasection->first + *schemakey);
							sectionMap[schemasection->first][*schemakey] = value;
						}
				}*/
			}
			/* WriteConfig() Write the sectionMap to the filename
			 *
			 */
			void WriteConfig() {
				std::ifstream s(filename.c_str());
				if (!s)
				{
					std::set<std::string> options;
					for (auto it = sectionMap.begin(); it != sectionMap.end();)
					{
						std::string sectionName;
						std::string value;
						options.insert(sectionName + "." + value);
					}
					for (boost::program_options::detail::config_file_iterator i(s, options), e; i != e; ++i)
						std::cout << i->value[0] << std::endl;
					return;
				}
			}
		};
	}
}
#endif