#ifndef I_RealTime_EVENTS_H
#define I_RealTime_EVENTS_H
#include "IGraphicsCommandList.h"
enum class IRealtimeEventType
{
	GraphicsCommand,
	Sound,
	UnitMove
};

class IRealtimeEvents
{
	IRealtimeEventType eventTypes;
	// put classes/structs in here
	union EventPayload
	{
		IGraphicsCommandList
	};
};

#endif