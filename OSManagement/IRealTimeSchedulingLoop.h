#ifndef I_REALTIME_SCHEDULING_LOOP_H
#define I_REALTIME_SCHEDULING_LOOP_H
#include "IRealtimeEvents.h"
#include "CircularBuffer.h"
using namespace std;

class IDispatcher
{
	
	void Dispatch(IRealtimeEvents& event) {};
};

class IRealtimeSchedulingLoop
{
	public:

		void RunRealTimeSchedulingLoop()
		{
			
		};
	CircularBuffer<IDispatcher> currentDispatchers;
	CircularBuffer<IRealtimeEvents> currentRealtimeEvents;
};

#endif
