#ifndef FIREWALL_PROTOCOL_H
#define FIREWALL_PROTOCOL_H

namespace OSManagement
{
	namespace Firewall
	{
		enum Protocol
		{
			TCP = 0,
			UDP = 1
		};
	}
};

#endif