#ifndef FIREWALL_CHAIN_TYPE_H
#define FIREWALL_CHAIN_TYPE_H
#include <string>
namespace OSManagement
{
	namespace Firewall
	{
	        enum FirewallChainTypeOptions { INPUT, OUTPUT,FORWARD,ALL};

	        class FirewallChainType
	        {
	           public:
	           FirewallChainTypeOptions options;

	           std::string ToString(void){
	           	switch(options)
	           	{
	           	   case FirewallChainTypeOptions::INPUT: return "INPUT";
	           	   case FirewallChainTypeOptions::OUTPUT: return "OUTPUT";
	           	   case FirewallChainTypeOptions::FORWARD: return "FORWARD";
	           	   default: return "";
	           	};
	           };

	           std::string AddOnChain(void){
	               std::string chain = ToString();
                    if(chain.size() > 0)
                    {
                       chain = "-A " + chain; // adds the rule on the chain
                    }
                    return chain;
	           };

	           std::string RemoveOnChain(void){
	               std::string chain = ToString();
                        if(chain.size() > 0)
                    {
                       chain = "-D " + chain; // deletes the rule on the chain
                    }
                    return chain;
	           };
	        }; // end FirewallChainType
   }
}

#endif
