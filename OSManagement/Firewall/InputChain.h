#ifndef INPUT_CHAIN_H
#define INPUT_CHAIN_H
#include "Firewall.h"
namespace OSManagement
{
	namespace Firewall
	{

		class InputChain : protected Firewall
		{
		public:
			InputChain(FirewallConfiguration& configuration) :Firewall(configuration){};

			void Run(void)
			{
				std::vector<std::string> output;
				std::vector<std::string> commands;
				FirewallChainType inputType;
                inputType.options = INPUT;
#ifndef _WIN32
				if (_configuration.flushExistingRules)
				{
					// 1st set the policy to drop packets if a rule is not hit.

                    commands.push_back(setChainPolicy(inputType,false));
                    std::string command;
                    command = "iptables -A INPUT -i lo -j ACCEPT";
                    command += " ";
                    command += comment("Allow Loopback");
                    command += " ";
                    command += save();
					commands.push_back(command);

					command = "iptables -A INPUT -m conntrack --ctstate INVALID -j DROP";
                    command += " ";
                    command += comment("Drop Invalid Packets");
                    command += " ";
                    command += save();
					commands.push_back(command);
                   // commands.push_back("iptables -A INPUT -p  tcp --tcp-flags SYN,RST,ACK,FIN -j ACCEPT"); //Force SYN packets check

                    command = "iptables -A INPUT -f -j DROP";
                    command += " ";
                    command += comment("DROP fragmented packets");
                    command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p tcp --tcp-flags SYN,ACK,FIN,RST FIN -j DROP";
                    command += " ";
                    command += comment("DROP if FIN flag is set");
                    command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP";
                    command += " ";
                    command += comment("DROP xmas attack packets");
                    command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP";
                    command += " ";
                    command += comment("DROP All Null packets");
                    command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p icmp -m limit --limit 2/second --limit-burst 2 -j ACCEPT";
                    command += " ";
                    command += comment("Block Smurf Attack");
                    command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p icmp -j DROP";
                    command += " ";
                   // command += comment("Block All ICMP Traffic");
                   // command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p tcp -m state --state NEW -m limit --limit 2/second --limit-burst 2 -j ACCEPT";
                    command += " ";
                   // command += comment("SYN FLOOD Protection Part 1");
                  //  command += " ";
                    command += save();
                    commands.push_back(command);

                    command = "iptables -A INPUT -p tcp -m state --state NEW -j DROP";
                    command += " ";
                  //  command += comment("SYN FLOOD Protection Part 2");
                  // command += " ";
                    command += save();
                    commands.push_back(command);

				}

				// 2. Set policy to block ports
				// external
				for (auto it : _configuration.externalFirewallConfiguration.tcp.incomingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.externalFirewallConfiguration.interfaceName,inputType, false,true));
				}

				for (auto it : _configuration.externalFirewallConfiguration.udp.incomingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.externalFirewallConfiguration.interfaceName,inputType, false, true));
				}

				// internal
				for (auto it : _configuration.internalfirewallConfiguration.tcp.incomingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.internalfirewallConfiguration.interfaceName,inputType, false,true));
				}

				for (auto it : _configuration.internalfirewallConfiguration.udp.incomingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.internalfirewallConfiguration.interfaceName,inputType, false,true));
				}

				if (_configuration.flushExistingRules)
				{
					// if we allow any incoming then we allow it. But this may not desirable if we want to be super secure
					if(_configuration.allowAnyLocalNetworkTraffic)
					{
						commands.push_back("iptables -A INPUT -i \"" + _configuration.internalfirewallConfiguration.interfaceName + "\" -j ACCEPT "+comment("allowAnyLocalNetworkTraffic") + " " + save());
					}

					if(_configuration.allowDHCP)
					{
commands.push_back("iptables -A INPUT -i \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp --dport 67:68 --sport 67:68 -m state --state NEW,ESTABLISHED -m comment --comment \"allow dhcp\" -j ACCEPT | iptables-save");
					}

					if(_configuration.allowDNS)
					{
						// allow both tcp and udp dns
						commands.push_back("iptables -A INPUT -i \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp --sport 53 -m state --state NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allow dns\" | iptables-save");
						commands.push_back("iptables -A INPUT -i \""+ _configuration.externalFirewallConfiguration.interfaceName + "\" -p tcp --sport 53 -m state --state NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allow dns\" | iptables-save");
					}

					if(_configuration.allowHTTP)
					{
						commands.push_back("iptables -A INPUT -p tcp -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allow http\" | iptables-save");
					}

					if(_configuration.allowSSH)
					{
						commands.push_back("iptables -A INPUT -i \"" + _configuration.internalfirewallConfiguration.interfaceName + "\" -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allow ssh\" | iptables-save");
					}
				}
                // set policy to allow
				// external
				for(auto it : _configuration.externalFirewallConfiguration.tcp.incomingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.externalFirewallConfiguration.interfaceName,inputType,true,true));
				}

                for(auto it : _configuration.externalFirewallConfiguration.udp.incomingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.externalFirewallConfiguration.interfaceName,inputType, true,true));
				}

				// internal
				for (auto it : _configuration.internalfirewallConfiguration.tcp.incomingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.internalfirewallConfiguration.interfaceName,inputType, true,true));
				}

				for (auto it : _configuration.internalfirewallConfiguration.udp.incomingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.internalfirewallConfiguration.interfaceName,inputType, true,true));
				}
#endif
				Execute(commands, output);
			};
		};

	}
};

#endif
