#ifndef FORWARD_CHAIN_H
#define FORWARD_CHAIN_H
#include "../ConsoleExecutor.h"
namespace OSManagement
{
	namespace Firewall
	{
		class ForwardChain : protected ConsoleExecutor
		{
			FirewallConfiguration& _configuration;
		public:
			ForwardChain(FirewallConfiguration& configuration) :_configuration(configuration)
			{

			};
			void Run(void)
			{
				std::vector<std::string> output;

				std::string command;
				if (_configuration._internalFirewall)
				{
#ifdef _WIN32
					// not supported
#else
					command = "iptables -P FORWARD DROP"; // if internal firewall you don't need to forward any ports so drop em
#endif
				}
				Execute(command, output);
			};
		};
	}
}
#endif
