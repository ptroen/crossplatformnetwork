#ifndef CONNECTION_STATE_H
#define CONNECTION_STATE_H

namespace OSManagement
{
	namespace Firewall
	{

        enum ConnectionStateTypes { NEW, ESTABLISHED, RELATED, INVALID};

        class ConnectionState
        {
           public:
           ConnectionState():stateTypes(ConnectionStateTypes::NEW){};
           ConnectionStateTypes stateTypes;
           std::string ToString(void){
              std::string s;
#ifndef _WIN32
              switch(stateTypes)
              {
                case NEW: return "NEW";
                case ESTABLISHED: return "ESTABLISHED";
                case RELATED: return "RELATED";
                default:
                return "INVALID";

              }
#endif
             return s;
           };
        };
	}
}

#endif
