#ifndef FIREWALL_INTERFACE_CONFIGURATION_H
#define FIREWALL_INTERFACE_CONFIGURATION_H
#include <vector>
#include <string>
#include <utility>
#include <cstdint>
#include "Protocol.h"
#include "../../algorithm/interface/IFileSerializer.h"
namespace OSManagement
{
	namespace Firewall
	{
	    class ProtocolGroup;
		class ProtocolGroup : public virtual Algorithm::Interface::IFileSerializer
		{
		   public:
		   std::vector<uint16_t> incomingPortsToAllow;
		   std::vector<uint16_t> outgoingPortsToAllow;
		   std::vector<uint16_t> incomingPortsToBlock;
		   std::vector<uint16_t> outgoingPortsToBlock;
		   ProtocolGroup():incomingPortsToAllow(),
		   outgoingPortsToAllow(),
		   incomingPortsToBlock(),
		   outgoingPortsToBlock(){};

		   private:
		   std::string PortsToString(std::vector<uint16_t>& ports){
		   	std::string s;
		   	std::stringstream os;

		   	for (auto a : ports)
		   	{

		   		os << a;
		   		os << ",";
		   	}
		   	s = os.str();
		   	if(s.size()== 0)
		   	   return s;
		   	size_t lastCharacter = s.size() - 1 ;
			// comma
			s.erase(s.begin() + lastCharacter);
		   	return s;
		   }
		   std::vector<uint16_t> ExtractVectorOfPorts(std::string& commaDelimitedPorts){
		   	std::vector<uint16_t> ports;
		   	std::string s;
		   	for (size_t i = 0; i < commaDelimitedPorts.size(); i++)
            {
                if(commaDelimitedPorts[i] == ',')
                {
                    uint16_t ii;
                    std::stringstream ss(s);
                    ss >> ii;
                    ports.push_back(ii);
                    s.clear();
                }
                else
                {
                   s += commaDelimitedPorts[i];
                }
            }

            if(s.size() > 0)
            {
               uint16_t ii;
               std::stringstream ss(s);
               ss >> ii;
               ports.push_back(ii);
            }
		   	return ports;
		   }
        public:

		   virtual std::string TypeName(void) { return "ProtocolGroup"; };

		   virtual Algorithm::Interface::IPropertyTree  ToPropertyTree() {
                    std::string msg = "ProtocolGroup::ToPropertyTree()";
			   		LOGIT1(msg)
					Algorithm::Interface::IPropertyTree pt;

					std::string _incomingPortsToAllow;
					msg = "_incomingPortsToAllow";
					LOGIT1(msg)
					_incomingPortsToAllow = PortsToString(incomingPortsToAllow);
					PADDS(_incomingPortsToAllow);

					std::string _outgoingPortsToAllow;
					msg = _outgoingPortsToAllow;
					LOGIT1(msg)
					_outgoingPortsToAllow = PortsToString(outgoingPortsToAllow);
					PADDS(_outgoingPortsToAllow)

					std::string _incomingPortsToBlock;
					msg = "_incomingPortsToBlock";
					LOGIT1(msg)
					_incomingPortsToBlock = PortsToString(incomingPortsToBlock);
					PADDS(_incomingPortsToBlock)

					std::string _outgoingPortsToBlock;
					msg = "_outgoingPortsToBlock";
					LOGIT1(msg)
					_outgoingPortsToBlock = PortsToString(outgoingPortsToBlock);
					PADDS(_outgoingPortsToBlock)

					msg = "ProtocolGroup:: End ToPropertyTree()";
			   		LOGIT1(msg)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
                    std::string msg = "ProtocolGroup::FromPropertyTree()";
			   		LOGIT1(msg)
					std::string _incomingPortsToAllow;
					msg = "_incomingPortsToAllow";
			   		LOGIT1(msg)
					PGET(_incomingPortsToAllow, std::string)
					incomingPortsToAllow = ExtractVectorOfPorts(_incomingPortsToAllow);

					std::string _outgoingPortsToAllow;
					msg = "_outgoingPortsToAllow";
			   		LOGIT1(msg)

					PGET(_outgoingPortsToAllow, std::string)
					outgoingPortsToAllow = ExtractVectorOfPorts(_outgoingPortsToAllow);

					std::string _incomingPortsToBlock;
					msg = "_incomingPortsToBlock";
			   		LOGIT1(msg)
					PGET(_incomingPortsToBlock, std::string)
					incomingPortsToBlock = ExtractVectorOfPorts(_incomingPortsToBlock);

					msg = "_outgoingPortsToBlock";
			   		LOGIT1(msg)
					std::string _outgoingPortsToBlock;
					PGET(_outgoingPortsToBlock, std::string)
					outgoingPortsToBlock = ExtractVectorOfPorts(_outgoingPortsToBlock);

					msg = "ProtocolGroup::FromPropertyTree()";
			   		LOGIT1(msg)
				};
		};

		class FirewallInterfaceConfiguration : public virtual Algorithm::Interface::IFileSerializer
		{
			public:
			std::string interfaceName;
			ProtocolGroup tcp;
			ProtocolGroup udp;

			virtual Algorithm::Interface::IPropertyTree  ToPropertyTree()
			{
			   std::string msg = "FirewallInterfaceConfiguration::ToPropertyTree()";
			   LOGIT1(msg)
			   Algorithm::Interface::IPropertyTree pt;
			   msg = "interfaceName";
			   LOGIT1(msg)
			   PADDS(interfaceName)
			   msg = "tcp add member";
			   LOGIT1(msg)
			   PADDMEMBER(tcp)
			   msg = "udp add member";
			   LOGIT1(msg)
			   PADDMEMBER(udp)
			   msg = "end FirewallInterfaceConfiguration::ToPropertyTree()";
			   LOGIT1(msg)
			   return pt;
			}

			virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
			   std::string msg = "FirewallInterfaceConfiguration::FromPropertyTree()";
			   LOGIT1(msg)
			   PGET(interfaceName, std::string)
			   PGETMEMBER(tcp)
			   PGETMEMBER(udp)
			}
		};
	}
};

#endif
