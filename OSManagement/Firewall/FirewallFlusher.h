#ifndef FIREWALL_FLUSHER_H
#define FIREWALL_FLUSHER_H
#include "../ConsoleExecutor.h"
#include "FirewallConfiguration.h"
#include "Firewall.h"
namespace OSManagement
{
	namespace Firewall
	{
		class FirewallFlusher : protected Firewall
		{
		public:
			FirewallFlusher(FirewallConfiguration& config) :Firewall(config) {};
			void Run(void)
			{
			    std::cout << "FirewallFlusher::Run()" << std::endl;
				std::vector<std::string> output;
				if (_configuration.flushExistingRules)
				{
					std::string msg = "Flush Existing Rules";
					std::cout << msg << std::endl;
#ifdef _WIN32
					// citation: https://support.microsoft.com/en-ca/help/947709/how-to-use-the-netsh-advfirewall-firewall-context-instead-of-the-netsh
					std::vector<std::string>  commands = { "netsh advfirewall reset", // reset the firewall
						"netsh advfirewall set currentprofile state on",
					   "netsh advfirewall set domainprofile state on",
						"netsh advfirewall set domainprofile firewallpolicy blockinbound,blockoutbound",
						"netsh advfirewall set rule all NEW enable=no" };
#else
					// citation: https://linux.die.net/man/8/iptables
					std::vector<std::string> commands = { "iptables -F | iptables-save", // flush the iptables
																				 "iptables -t nat -F | iptables-save", // specify nat flush
"iptables -t mangle -F | iptables-save", // specify the packet alteration table to be flushed
"iptables -X | iptables-save", // delete chain
"iptables -t nat -X | iptables-save",
"iptables -t mangle -X | iptables-save",
"iptables -P INPUT ACCEPT | iptables-save", // accept from the input
"iptables -P FORWARD ACCEPT | iptables-save", // accept from the forward
"iptables -P OUTPUT ACCEPT | iptables-save" }; // accept from the output
#endif
					Execute(commands, output);
				}

			};
		};
	}
};
#endif
