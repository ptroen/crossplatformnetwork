#ifndef PREROUTING_CHAIN_H
#define PREROUTING_CHAIN_H
#include "../ConsoleExecutor.h"
#include "FirewallConfiguration.h"
#include "Protocol.h"
#include "../../OSI/Transport/IPType.h"
namespace OSManagement
{
	namespace Firewall
	{
		class PreroutingChain : protected ConsoleExecutor
		{
			FirewallConfiguration& _configuration;
			// ban ip's go here
		protected:
			std::string blockIP(std::string& ipaddress,std::string chain,bool unblock)
			{
                std::string ip;
                std::string switchC="-A";

#ifndef _WIN32
if(unblock)
{
   switchC = "-D";
}
#endif
				if (OSI::Transport::ISIPV4(ipaddress))
				{
				   std::cout << "BLOCKING IPv4 address" << ipaddress << std::endl;
#ifdef _WIN32
					ip = "netsh advfirewall firewall add rule name = \"IP Block_" + ipaddress + "\" dir = in interface = any action = block remoteip = " + ipaddress + "/ 32";
				}
				else
				{

				}
#else
				// ips need to be prepended so they are inserted at top of the tables
					ip = " iptables ";
					ip+= switchC;
                    ip+=" " + chain + "  -s " + ipaddress + " -j DROP -m comment --comment \"BLOCK IP "+ipaddress+"\" | iptables-save";
				}
				else
				{
				    std::cout << "BLOCKING IPv6 address" << std::endl;
					ip = " iptables6 "+switchC+" " + chain + "  -s " + ipaddress + " -j DROP --comment \"BLOCK IP "+ipaddress+"\" | iptables-save";
                }
#endif
				return ip;
			}

		public:
			PreroutingChain(FirewallConfiguration& configuration) :_configuration(configuration)
			{

			};
			void Run(void)
			{
				std::vector<std::string> output;

				std::vector<std::string> commands;
				std::string forwardchain("FORWARD");
				std::string inputchain("INPUT");
				std::string outputchain("OUTPUT");
				std::string msg;
                for (auto ip : _configuration.ipAddressesToBlock)
                {
                        if(ip.size()>0)
                        {
                        msg = "ip to block " +  ip;
                        LOGIT1(msg)
						commands.push_back(blockIP(ip, inputchain,false));
                        }
                }
                for (auto ip : _configuration.ipAddressesToUnBlock)
                {
                    if(ip.size() >0)
                    {
                       msg = "ip to unblock" +  ip;
                       LOGIT1(msg)
                       commands.push_back(blockIP(ip, inputchain,true));
                    }
                }
				Execute(commands, output);
			};
		};
	}
};

#endif
