#ifndef OUTPUT_CHAIN_H
#define OUTPUT_CHAIN_H
#include "../ConsoleExecutor.h"
#include "../../OSI/Transport/IPType.h"
#include "Firewall.h"
namespace OSManagement
{
	namespace Firewall
	{
		class OutputChain : protected Firewall
		{

		public:
			OutputChain(FirewallConfiguration& configuration) :Firewall(configuration)
			{

			};
			void Run(void)
			{
				std::vector<std::string> output;

				std::vector<std::string> commands;
#ifdef _WIN32

#else
                FirewallChainType outputType;
                outputType.options = OUTPUT;
                commands.push_back(setChainPolicy(outputType,true));
#endif
				if(_configuration.flushExistingRules)
				{
                     std::string command;
                    command = "iptables -A OUTPUT -o lo -j ACCEPT";
                    command += " ";
                    command += comment("Allow Loopback");
                    command += " ";
                    command += save();
					commands.push_back(command);


				 	if(_configuration.blockPortZero)
				 	{
#ifdef _WIN32

#else
					 	commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p tcp -j DROP --sport 0 -m comment --comment \"blockPortZero \" | iptables-save");
					 	commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp -j DROP --sport 0 -m comment --comment \"blockPortZero \" | iptables-save");
					 	commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p tcp -j DROP --dport 0 -m comment --comment \"blockPortZero \" | iptables-save");
					 	commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp -j DROP --dport 0 -m comment --comment \"blockPortZero \" | iptables-save");
#endif
				 	}
				 }

				//internal
				for (auto it : _configuration.internalfirewallConfiguration.tcp.outgoingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.internalfirewallConfiguration.interfaceName,outputType,true,false));
				}

				for (auto it : _configuration.internalfirewallConfiguration.udp.outgoingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.internalfirewallConfiguration.interfaceName,outputType,true,false));
				}

				// external
				for (auto it : _configuration.externalFirewallConfiguration.tcp.outgoingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.externalFirewallConfiguration.interfaceName,outputType,true,false));
				}

				for (auto it : _configuration.externalFirewallConfiguration.udp.outgoingPortsToBlock)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.externalFirewallConfiguration.interfaceName,outputType,true,false));
				}

				if(_configuration.flushExistingRules)
				{
				 	if(_configuration.allowAnyLocalNetworkTraffic)
					{
#ifdef _WIN32

#else
						commands.push_back("iptables -A OUTPUT -o \"" + _configuration.internalfirewallConfiguration.interfaceName + "\" -j ACCEPT -m comment --comment \"allowAnyLocalNetworkTraffic \" | iptables-save");
#endif
					}

				 	if(_configuration.allowDHCP)
					{
#ifdef _WIN32

#else
						commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp --dport 67:68 --sport 67:68 -m state --state NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allowDHCP \" | iptables-save");
#endif
					}

					if(_configuration.allowDNS)
					{
#ifdef _WIN32

#else
						commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p udp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allowDNS \" | iptables-save");
						commands.push_back("iptables -A OUTPUT -o \"" + _configuration.externalFirewallConfiguration.interfaceName + "\" -p tcp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allowDNS \" | iptables-save");
#endif
					}

					if (_configuration.allowSSH)
					{
#ifdef _WIN32

#else
						commands.push_back("iptables -A OUTPUT -o \"" + _configuration.internalfirewallConfiguration.interfaceName + "\" -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT -m comment --comment \"allowSSH \" | iptables-save");
#endif
					}
					if (_configuration.allowHTTP)
					{
#ifdef _WIN32

#else
						commands.push_back("iptables -A OUTPUT -p tcp --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment \"allowHTTP\" | iptables-save");

#endif

						// add way to add out going port
					}
				}

				// internal
				for(auto it :_configuration.internalfirewallConfiguration.tcp.outgoingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.internalfirewallConfiguration.interfaceName,outputType,true,false));
				}

				for(auto it :_configuration.internalfirewallConfiguration.udp.outgoingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.internalfirewallConfiguration.interfaceName,outputType,true,false));
				}

				//external
				for (auto it : _configuration.externalFirewallConfiguration.tcp.outgoingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::TCP, _configuration.externalFirewallConfiguration.interfaceName,outputType,true,false));
				}

				for (auto it : _configuration.externalFirewallConfiguration.udp.outgoingPortsToAllow)
				{
					commands.push_back(allowOrDropPort(it, Protocol::UDP, _configuration.externalFirewallConfiguration.interfaceName,outputType,true,false));
				}
				Execute(commands, output);
			};
		};
	}
};

#endif
