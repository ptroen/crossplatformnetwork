#ifndef FIREWALL_H
#define FIREWALL_H
#include "../ConsoleExecutor.h"
#include "../../OSI/Transport/IPType.h"
#include "FirewallChainType.h"
#include "ConnectionState.h"
namespace OSManagement
{
	namespace Firewall
	{
        // Base class for Firewall methods
		class Firewall : protected ConsoleExecutor
		{
			protected:
            FirewallConfiguration& _configuration;
			public:
			Firewall(FirewallConfiguration& configuration):_configuration(configuration){};
            protected:
            std::string program(void){
#ifndef _WIN32
               return "iptables";
#endif
            };

            std::string programipv6(void){
#ifndef _WIN32
               return "ip6tables";
#endif
               };

            std::string comment(std::string comment){
#ifndef _WIN32
               return "-m comment --comment \"" + comment + "\"";
#endif
            };

            std::string save(void){
#ifndef _WIN32
                return "| iptables-save";
#endif
            };

            std::string makeProtocol(Protocol protocol){
#ifndef _WIN32
               std::string s;
               s = " -p";
				if (protocol == Protocol::TCP)
					s += " tcp";
				else
					s += " udp";
					return s;
#endif
            };

            std::string dport(uint16_t portNumber){
#ifndef _WIN32
               std::ostringstream ss;
               ss << "--dport " << portNumber;
               return ss.str();
#endif
            };
            std::string sport(uint16_t portNumber){
#ifndef _WIN32
               std::ostringstream ss;
               ss << "--sport " << portNumber;
               return ss.str();
#endif
            };

            std::string makeInterface(std::string interfaceName){
#ifndef _WIN32
              std::string interfaceID = interfaceName;
				if(interfaceID.size() > 0)
				{
				   interfaceID = "-o \"" + interfaceName + "\"";
				}
				return interfaceID;
#endif
            };

             std::string makeInterfacei(std::string interfaceName){
#ifndef _WIN32
              std::string interfaceID = interfaceName;
				if(interfaceID.size() > 0)
				{
				   interfaceID = "-i \"" + interfaceName + "\"";
				}
				return interfaceID;
#endif
            };

            std::string accept(){
#ifndef _WIN32
            return "-j ACCEPT";
#endif
}
            std::string drop(){
#ifndef _WIN32
return "-j DROP";
#endif
}

            std::string makeState(std::vector<ConnectionState>& states){
#ifndef _WIN32
                std::string s;
                if(states.size() <= 0) return "";
                s = "-m conntrack --ctstate ";
                for(size_t i = 0; i < states.size() ; i++)
                {
                 std::string state = states[i].ToString();
                 s += state;
                 if(i < states.size() - 1)
                    s+= ",";
                }
#endif
            }

            std::string allowOrDropPort(uint16_t portNumber,Protocol protocol, std::string& interfaceName, FirewallChainType options, bool allow,bool incoming){
               std::ostringstream ss;
#ifdef _WIN32

#else
				ss << program()
				   << " "
				   << options.AddOnChain()
				   << " ";
				   if(options.options == FirewallChainTypeOptions::INPUT)
				   {
				      ss <<  makeInterfacei(interfaceName);
				   }
				   else
				   {
                    ss <<  makeInterface(interfaceName);
				   }
				   ss << " "
                   << makeProtocol(protocol)
                   << " ";
                   if(incoming)
                   {
                      ss << dport(portNumber); // this is the initial port in
                   }
                   else
                   {
                      ss << sport(portNumber); // sport is used by the sender to notify that something arrived in the port
                   }
                   ss << " ";
				std::string thecomment;
				if(allow)
				{
				   if(protocol==Protocol::TCP && incoming)
				   {
				    ss << "-m conntrack --ctstate NEW,ESTABLISHED";
				    ss << " ";
				   }

				   thecomment = "Allow Port";
				   thecomment = comment(thecomment);
				   ss << accept() << " " << thecomment;
                }
                else
                {
                   thecomment = "Drop Port";
                   thecomment = comment(thecomment);
                   ss << drop() << " " << thecomment;
                }

                ss << " " << save();
                std::string portstr="port added or blocked";
                LOGIT(portstr)
                LOGIT(ss.str())

#endif
					return ss.str();

            };

            std::string setChainPolicy(FirewallChainType chainType,bool accept){
                std::string s;
#ifndef _WIN32
               s = program();
               s += " -P ";
               s += chainType.ToString(); // ToString example "INPUT"
               s += " ";
               if(accept)
               {
                 s+= "ACCEPT";
               }
               else
               {
                    s+="DROP";
               }
               s += " ";
            //s += comment("Chain Policy")
            s += " ";
            s+= save();
#endif
                return s;
            };


            std::string blockorUnblockIP(std::string& ip,FirewallChainType options, bool block){
                std::string ss;
                if (OSI::Transport::ISIPV4(ip))
				{
#ifdef _WIN32

#else
					ss = program();

#endif
				}
				else
				{
#ifdef _WIN32

#else
					ss = programipv6();
#endif
                }
                ss += " ";
                if(block)
                {
                    ss += options.AddOnChain();
                }
                else
                {
                    ss += options.RemoveOnChain();
                }

                ss += " -d " + ip + " " + drop() + " ";

                if(block)
                {
                    std::string thecomment = "Block IP";
                    thecomment = comment(thecomment);
                    ss += thecomment;
                }
                else
                {
                    // no need your removing the rule
                    //ss += comment("Block IP");
                }
                ss += " " + save();
				return ss;

            }

		}; // end Firewall
	}
}

#endif
