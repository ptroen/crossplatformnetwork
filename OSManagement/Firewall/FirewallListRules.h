#ifndef FIREWALL_LIST_RULES_H
#define FIREWALL_LIST_RULES_H
#include "../ConsoleExecutor.h"
namespace OSManagement
{
	namespace Firewall
	{
		class FirewallListRules : protected ConsoleExecutor
		{
		public:
			void Run(void)
			{
			    std::cout << "FirewallListRules::Run()" << std::endl;
				std::vector<std::string> output;
#ifdef _WIN32
				std::string command("netsh advfirewall firewall show rule name=all"); // untested
#else
				std::string command("iptables -L");
				Execute(command, output);
				command ="iptables -S INPUT";
				Execute(command, output);
				command = "iptables -S OUTPUT";
				Execute(command, output);
				command = "iptables -S FORWARD";
				Execute(command, output);
#endif

			};
		};
	}
};

#endif
