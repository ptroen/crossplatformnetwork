#ifndef FIREWALL_CONFIGURATION_H
#define FIREWALL_CONFIGURATION_H
#include "FirewallInterfaceConfiguration.h"
#include <vector>
#include <iostream>
#include <sstream>
#include "../../algorithm/interface/IFileSerializer.h"
#define PPrint(var) {ostringstream ss; ss << __str__(var) << " "<< var << std::endl; \
LOGIT1(ss.str()) }
namespace OSManagement
{
	namespace Firewall
	{
		class FirewallConfiguration  : public virtual Algorithm::Interface::IFileSerializer
		{
		public:
			FirewallInterfaceConfiguration internalfirewallConfiguration;
			FirewallInterfaceConfiguration externalFirewallConfiguration;
			bool _internalFirewall;
			bool flushExistingRules;
			bool allowAnyLocalNetworkTraffic;
			bool allowDHCP; // input chain
			bool allowSSH; // input chain
			bool allowDNS; // input chain
			bool allowHTTP;
			bool blockPortZero;
			std::vector<std::string> ipAddressesToBlock;
			std::vector<std::string> ipAddressesToUnBlock;

			FirewallConfiguration(void): internalfirewallConfiguration(),
			externalFirewallConfiguration(),
			_internalFirewall(false),
			flushExistingRules(false),
			allowAnyLocalNetworkTraffic(false),
			allowDHCP(false),
			allowSSH(false),
			allowDNS(false),
			allowHTTP(false),
			blockPortZero(true),
			ipAddressesToBlock(),
			ipAddressesToUnBlock(){};

			void GetInterfacesSet(void) {};

			void Print(void){
               PPrint(_internalFirewall)
                PPrint( flushExistingRules)
                PPrint( allowAnyLocalNetworkTraffic)
                PPrint(allowDHCP)
                PPrint(allowSSH)
                PPrint(allowDNS)
                PPrint(allowHTTP)
                PPrint(blockPortZero)
                 std::string msg = "addresses to block ";
                 PPrint(msg)
                for(std::string s : ipAddressesToBlock)
                {
                   msg = s;
                   PPrint(msg)
                }

                msg = "addresses to unblock ";
                PPrint(msg)
                for(std::string s : ipAddressesToUnBlock)
                {
                   msg = s;
                   PPrint(msg)
                }
                msg = "Print done";
                PPrint(msg)
			};


			virtual Algorithm::Interface::IPropertyTree  ToPropertyTree(void) {
					auto msg = TypeName() + "::ToPropertyTree()\n";
					LOGIT1(msg)
					Algorithm::Interface::IPropertyTree pt = FirewallConfigurationToPropertyTree();
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
				        auto msg = TypeName() + "::FromPropertyTree()\n";
					LOGIT1(msg)
					FirewallConfigurationFromPropertyTree(pt);
				};

				virtual std::string TypeName(void) { return "FirewallConfiguration"; };

				private:
				std::string MakeCommaDelimitedString(std::vector<string>& arrayOfStrings){
					std::string tempString;
					if(arrayOfStrings.size() == 0)
					return "";
					for(auto s : ipAddressesToBlock)
					{
					   tempString += s;
					   tempString += ",";
					}
					size_t lastCharacter = tempString.size() - 1 ;
					// comma
					tempString.erase(tempString.begin() + lastCharacter);
					return tempString;
				}
				void ExtractCommaDelimitedString(std::string& arrayOfCommaStrings, std::vector<string>& arrayOfStrings){
                    if(arrayOfCommaStrings.size() == 0) return;
                    arrayOfStrings.clear();
                    std::string nextString;
                    for (size_t i = 0; i < arrayOfCommaStrings.size();i++)
                    {
                        if(arrayOfCommaStrings[i] == ',')
                        {
                            arrayOfStrings.push_back(nextString);
                            std::cout << nextString << std::endl;
                            nextString.clear();
                        }
                        else
                            nextString += arrayOfCommaStrings[i];
                    }
                    if(nextString.size() > 0 )
                    {
                        arrayOfStrings.push_back(nextString);
                    }
				};
				protected:
				virtual Algorithm::Interface::IPropertyTree  FirewallConfigurationToPropertyTree(void) {
				        std::string msg = TypeName() + "::FirewallConfigurationToPropertyTree() \n";
					LOGIT1(msg)
					Algorithm::Interface::IPropertyTree pt;
					msg = "PADDMEMBER(internalfirewallConfiguration)";
					LOGIT1(msg)
					PADDMEMBER(internalfirewallConfiguration)
					msg = "PADDMEMBER(externalFirewallConfiguration)";
					LOGIT1(msg)
					PADDMEMBER(externalFirewallConfiguration)
					msg = "_internalFirewall";
					LOGIT1(msg)
					PADD(_internalFirewall)
                    msg = "flushExistingRules";
					LOGIT1(msg)
					PADD(flushExistingRules)
					msg = "allowAnyLocalNetworkTraffic";
					LOGIT1(msg)
					PADD(allowAnyLocalNetworkTraffic)
					msg = "allowDHCP";
					LOGIT1(msg)
					PADD(allowDHCP)
					PADD(allowSSH)
					PADD(allowDNS)
					PADD(allowHTTP)
					PADD(blockPortZero)
					std::string _IPAddressToBlock = MakeCommaDelimitedString(ipAddressesToBlock);
					PADDS(_IPAddressToBlock)
					std::string _IPAddressToUnBlock  = MakeCommaDelimitedString(ipAddressesToUnBlock);
					PADDS(_IPAddressToUnBlock)
					msg = TypeName() + "::FirewallConfigurationToPropertyTree() end \n";
					LOGIT(msg)
					return pt;
				};

				// method which extracts the values from property tree
				virtual void FirewallConfigurationFromPropertyTree(Algorithm::Interface::IPropertyTree& pt) {
					std::string msg = TypeName() + "::FirewallConfigurationFromPropertyTree() \n";
					LOGIT(msg)
					PGET(_internalFirewall, bool)
					PGET(flushExistingRules,bool)
					PGET(allowAnyLocalNetworkTraffic,bool)
					PGET(allowDHCP,bool)
					PGET(allowSSH,bool)
					PGET(allowDNS,bool)
					PGET(allowHTTP,bool)
					PGET(blockPortZero,bool)
					std::string _IPAddressToBlock;
					PGET(_IPAddressToBlock, std::string)
					ExtractCommaDelimitedString(_IPAddressToBlock, ipAddressesToBlock);
					std::string _IPAddressToUnBlock;
					PGET(_IPAddressToUnBlock, std::string)
					ExtractCommaDelimitedString(_IPAddressToUnBlock, ipAddressesToUnBlock);

					PGETMEMBER(internalfirewallConfiguration)
					PGETMEMBER(externalFirewallConfiguration)
				}; // end FirewallConfigurationFromPropertyTree
		};
	}
}
#endif
