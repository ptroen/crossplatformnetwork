-- premake5.lua

function useBoost()
    files { os.getenv("BOOST")}
	includedirs { "{%BOOST}/"}
	libdirs { "{%BOOST}/lib/" }
end	

workspace "OverrideGlobalNewAndDelete"
   configurations { "Debug", "Release" }
   architecture "x86_64"
project "OverrideGlobalNewAndDelete"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"
   useBoost()
   files {"./*.c*"  }
   removefiles { "*.lua", "*.sln","*.v*","*.sh","*.log","*.obj","*.*db","*.suo","*.pch" }
   libdirs {} 
   excludes { }
   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"
	  
	  
	  



	