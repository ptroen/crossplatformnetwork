#include "../../OverrideGlobalNewAndDelete.h"

int main(int argc, char* argv[])
{
	for (size_t iii = 0; iii < 104857600; iii++)
	{
		int* i = new int();

		int* ii = new int[2];

		delete i;

		delete ii;
	}
	return 0;
}