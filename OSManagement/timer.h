#ifndef TIMER_H
#define TIMER_H
#include "../Tools/Diagnostics/Benchmark.h"

namespace OSManagement
{

	enum TimerResolution
	{
		Nanoseconds,
		Milliseconds,		
		seconds
	};

	class Timer : public virtual IBenchmarkBase
	{
		int64_t timeInNanoSeconds;
	public:
		// units is used by the expired macro but not needed if you manually stop the timer
		Timer(int64_t units, TimerResolution resolution)
		{
			UpdateTimerResolution(units, resolution);
			
		};
		bool Expired(void){ auto end = std::chrono::high_resolution_clock::now();
				if (std::chrono::duration_cast<std::chrono::milliseconds>(end - startPoint).count() >= timeInNanoSeconds)
					return true;
				return false;
		};
		
		void UpdateTimerResolution(int64_t units, TimerResolution resolution) {
			if (resolution >= TimerResolution::Milliseconds)
			{
				units *= 1000;
			}
			if (resolution >= TimerResolution::seconds)
			{
				units *= 1000;
			}
			timeInNanoSeconds = units;
		};
	};
}
#endif