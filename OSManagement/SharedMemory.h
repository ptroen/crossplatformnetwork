#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H
// citation: https://stackoverflow.com/questions/12413034/shared-map-with-boostinterprocess
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <functional>
#include <utility>
#include <string>
#include <string>
#include <cstdlib> //std::system
#include <vector>
/*
namespace OSManagement
{
	//Define an STL compatible allocator of ints that allocates from the managed_shared_memory.
	//This allocator will allow placing containers in the segment
	typedef boost::interprocess::allocator<int, boost::interprocess::managed_shared_memory::segment_manager>  sharedMemoryAllocator;

	enum SharedMemoryTypeProcess
	{
		ProducerProcess,
		ConsumerProcess
	};

	// https://www.boost.org/doc/libs/1_63_0/doc/html/interprocess/quick_guide.html#interprocess.quick_guide.qg_memory_pool
	template <typename valuetype> // keyType is size_t
	class SharedMemory
	{

		boost::interprocess::vector<valuetype, sharedMemoryAllocator>* memory;
	public:
		SharedMemory(SharedMemoryTypeProcess processType) : memory(nullptr) {};
		void CreateSharedMemory() {}

		// refactor this into the template
		void samplecode()
		{
			if (argc == 1) { //Parent process
							 //Remove shared memory on construction and destruction
				struct shm_remove
				{
					shm_remove() { shared_memory_object::remove("MySharedMemory"); }
					~shm_remove() { shared_memory_object::remove("MySharedMemory"); }
				} remover;

				//Create a new segment with given name and size
				managed_shared_memory segment(create_only, "MySharedMemory", 65536);

				//Initialize shared memory STL-compatible allocator
				const ShmemAllocator alloc_inst(segment.get_segment_manager());

				//Construct a vector named "MyVector" in shared memory with argument alloc_inst
				MyVector *myvector = segment.construct<MyVector>("MyVector")(alloc_inst);

				for (int i = 0; i < 100; ++i)  //Insert data in the vector
					myvector->push_back(i);

				//Launch child process
				std::string s(argv[0]); s += " child ";
				if (0 != std::system(s.c_str()))
					return 1;

				//Check child has destroyed the vector
				if (segment.find<MyVector>("MyVector").first)
					return 1;
			}
			else { //Child process
				   //Open the managed segment
				managed_shared_memory segment(open_only, "MySharedMemory");

				//Find the vector using the c-string name
				MyVector *myvector = segment.find<MyVector>("MyVector").first;

				//Use vector in reverse order
				std::sort(myvector->rbegin(), myvector->rend());

				//When done, destroy the vector from the segment
				segment.destroy<MyVector>("MyVector");
			}
		};
		bool find(size_t) { return false; }
		valuetype& operator[](size_t key) { return valuetype }
		SharedMemory<valuetype>& operator = (std::pair < size_t, valuetype>& other) { return *this; }
	}; // end class
} // end OSManagement
*/
#endif
