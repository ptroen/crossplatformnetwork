#ifndef OVERRIDE_GLOBAL_NEW_AND_DELETE_H
#define OVERRIDE_GLOBAL_NEW_AND_DELETE_H
#include <cstdio>
#include <stdlib.h>
#include <exception>
#define BOOST_STACKTRACE_USE_ADDR2LINE
#include <boost/stacktrace.hpp>
#include <iostream>

#include "./MemoryPool/MemoryAllocationManagement.h"
// citation c binary tree: https://www.thegeekstuff.com/2013/02/c-binary-tree/ [used as reference for memory allocator]

void * operator new(size_t n)
{
	return OSManagement::MemoryPool::MemoryAllocate(n);
}
void operator delete(void * p)
{
	OSManagement::MemoryPool::MemoryReturn(p);
}

void *operator new[](size_t s)
{
	return OSManagement::MemoryPool::MemoryAllocate(s);
}
void operator delete[](void *p) throw()
{
	OSManagement::MemoryPool::MemoryReturn(p);
}

#endif