#include <string>
#include <iostream>
#include "../../OSI/Session/interface/IRequestBuilder.h"
#include "../../MVC/Controller/Forums/ForumsDispatcher.h"
#include "../../../OSI/Transport/All/AllServer.h"
#include "../../../OSI/Transport/interface/IAllInitiializationParameters.h"
#include "../../../MVC/MVCRequest.h"
#include "../../../Tools/Diagnostics/Logger/Logger.h"
// controller code

int main(int argc,char* argv[])
{
	
	OSI::Transport::Interface::IAllInitiializationParameters init_parameters;
	auto defaultHTTPSPort = 443;
	try
	{
		init_parameters.ParseServerArgs(&(*argv), argc, defaultHTTPSPort, defaultHTTPSPort);
		std::string serverSelected("server selected:");
		serverSelected += init_parameters.protocol._to_string();
		serverSelected += "\n";
		LOGIT(serverSelected)
		std::string portSelected("port selected:");
		portSelected += std::to_string(init_parameters._incomingPortApplicationLayerAddress);
		LOGIT(portSelected)
		OSI::Transport::All::AllServerTransport<MVC::MVCRequest<OSI::Transport::Interface::IAllInitiializationParameters>,
																		OSI::Transport::HTTP::HTTPResponse<OSI::Transport::Interface::IAllInitiializationParameters>,
																		MVC::Controller::Forum::ForumsDispatcher<OSI::Transport::Interface::IAllInitiializationParameters>,
																		OSI::Transport::Interface::IAllInitiializationParameters> allTransport(init_parameters);
		allTransport.RunServer();
	}
	catch (std::exception& e)
	{
		std::string msg;
		msg = "Program Exception: ";
		msg += e.what();
		auto ss = std::string(msg); 
		LOGIT(ss)
	}
	

	// input and read forum via console
	// input and read forum via html
			// code to mimic the resquest being filled
	/*
			OSI::Session::Interface::IRequestBuilder resp;
			std::cin >> resp;
			std::cout << resp << std::endl;
			std::string databasefilenamewithdbextension = "forum.db";

			
			OSI::Transport::Interface::IAllInitiializationParameters initializer;
			MVC::Controller::Forum::ForumsDispatcher< OSI::Transport::Interface::IAllInitiializationParameters> dispatcher(initializer);
			OSI::Transport::HTTP::HTTPRequest<OSI::Transport::Interface::IAllInitiializationParameters> request(initializer);
			OSI::Transport::HTTP::HTTPResponse<OSI::Transport::Interface::IAllInitiializationParameters> response(initializer);
			std::string ipaddress = "127.0.0.1";
			// regardless of console mode run the dispatcher
			if (dispatcher.IncomingHook(request, response, ipaddress))
			{
				std::string s;
				size_t n = response.size();
				s.resize(n);
				response.ToString(const_cast<char*>(s.c_str()), n);
				std::cout << s << std::endl;
			}
 */
	return 0;
}
